diff --git a/Model/Entity/EntityData.php b/Model/Entity/EntityData.php
index 37aa53b05c0bb81f8c0b013a60777a24ebbdaf21..9594daebdb43e94e387982e8f5386aaec3743ac7 100644
--- a/Model/Entity/EntityData.php
+++ b/Model/Entity/EntityData.php
@@ -15,6 +15,8 @@ use Magento\Framework\Indexer\StateInterface;
  */
 class EntityData extends \Magento\Framework\Model\AbstractExtensibleModel implements EntityDataInterface
 {
+    const ROW_ID = 'row_id';
+
     protected $_eventPrefix = 'amasty_orderattr_entitydata';

     protected $_cacheTag = false;
diff --git a/Model/Entity/Handler/Save.php b/Model/Entity/Handler/Save.php
index 48cc1b77535f1bae0a666560097b7bb2eaf8bf43..d4f272f6c55cf4b5e1c050249b3092106a1ecb19 100644
--- a/Model/Entity/Handler/Save.php
+++ b/Model/Entity/Handler/Save.php
@@ -34,6 +34,10 @@ class Save
     public function execute(EntityDataInterface $entityData)
     {
         try {
+            if (!$entityData->getEntityId()) {
+                $entityData->setEntityId($this->entityResource->reserveEntityId());
+            }
+
             $this->entityResource->save($entityData);
         } catch (\Exception $e) {
             $this->logger->critical('Unable to save Amasty Order Attributes', ['exception' => $e->getMessage()]);
diff --git a/Model/ResourceModel/Entity/Entity.php b/Model/ResourceModel/Entity/Entity.php
index b2702974c3e438348b2a5698bc5c1292f127772a..df403a393a60aec81de1dce774880a37f05d48fb 100644
--- a/Model/ResourceModel/Entity/Entity.php
+++ b/Model/ResourceModel/Entity/Entity.php
@@ -17,6 +17,8 @@ class Entity extends \Magento\Eav\Model\Entity\AbstractEntity

     const GRID_INDEXER_ID = 'amasty_order_attribute_grid';

+    const INITIAL_AUTOINCREMENT_VALUE = 1;
+
     protected $linkIdField = 'entity_id';
     protected $_entityIdField = 'entity_id';

@@ -177,49 +179,47 @@ class Entity extends \Magento\Eav\Model\Entity\AbstractEntity
      * Generate new entity_id to object
      * entity_id can be not unique in checkout attributes
      *
+     * This method resolves the race condition problem when customers try to save two or more
+     * order attributes at the same time and records receive same entity IDs.
+     * Entity ID receives the next auto increment value and automatically increases auto increment value
+     * simultaneously to make sure that the next order attribute to be saved won't receive the same entity ID.
+     * The logic of picking the next entity ID has been moved to the save handler
+     * (@see \Amasty\Orderattr\Model\Entity\Handler\Save) because DDL statements are not allowed in transactions.
+     *
      * @return int
      */
-    public function fetchNewEntityId()
+    public function reserveEntityId()
     {
-        $select = $this->getConnection()->select()
-            ->from($this->getEntityTable(), [$this->getEntityIdField()])
-            ->order($this->getEntityIdField() . ' DESC')->limitPage(1, 1);
-
-        $lastEntity = (int)$this->getConnection()->fetchOne($select);
-
-        return $lastEntity + 1;
+        $nextEntityId = $this->getNextAutoIncrement();
+        $this->updateAutoIncrement($nextEntityId + 1);
+        return $nextEntityId;
     }

     /**
-     * Prepare static value for save
-     *
-     * @param string $key
-     * @param mixed $value
-     * @return mixed
+     * @param int $autoIncrement
      */
-    protected function _prepareStaticValue($key, $value)
+    protected function updateAutoIncrement(int $autoIncrement): void
     {
-        $value = parent::_prepareStaticValue($key, $value);
-        if (!$value && $key == $this->getEntityIdField()) {
-            $value = $this->fetchNewEntityId();
-        }
-
-        return $value;
+        $this->getConnection()->query(
+            sprintf(
+                'ALTER TABLE %s AUTO_INCREMENT = %s',
+                $this->getConnection()->quoteTableAs($this->getEntityTable()),
+                $autoIncrement
+            )
+        );
     }

     /**
-     * Before delete Entity process
-     *
-     * @param \Magento\Framework\DataObject $object
-     * @return $this
+     * @return int
      */
-    protected function _beforeSave(\Magento\Framework\DataObject $object)
+    protected function getNextAutoIncrement(): int
     {
-        parent::_beforeSave($object);
-        if (!$object->getEntityId()) {
-            $object->setEntityId($this->fetchNewEntityId());
+        $tableStatus = $this->getConnection()->showTableStatus($this->getEntityTable());
+        if ($tableStatus && !empty($tableStatus['Auto_increment'])) {
+            return $tableStatus['Auto_increment'];
         }
-        return $this;
+
+        return self::INITIAL_AUTOINCREMENT_VALUE;
     }

     protected function _collectSaveData($object)
diff --git a/Setup/Operation/ReplacePrimaryColumn.php b/Setup/Operation/ReplacePrimaryColumn.php
new file mode 100644
index 0000000000000000000000000000000000000000..8ab30bdc21fff5e31feffc9b24c18d6cecaed3b6
--- /dev/null
+++ b/Setup/Operation/ReplacePrimaryColumn.php
@@ -0,0 +1,39 @@
+<?php
+declare(strict_types=1);
+
+namespace Amasty\Orderattr\Setup\Operation;
+
+use Amasty\Orderattr\Api\Data\CheckoutEntityInterface;
+use Amasty\Orderattr\Model\Entity\EntityData;
+use Magento\Framework\DB\Adapter\AdapterInterface;
+use Magento\Framework\DB\Ddl\Table;
+use Magento\Framework\Setup\SchemaSetupInterface;
+
+class ReplacePrimaryColumn
+{
+    /**
+     * @param SchemaSetupInterface $setup
+     */
+    public function execute(SchemaSetupInterface $setup): void
+    {
+        $tableName = $setup->getTable(CreateEntityTable::TABLE_NAME);
+
+        $setup->getConnection()->dropIndex(
+            $tableName,
+            $setup->getConnection()->getIndexName(
+                $tableName,
+                [CheckoutEntityInterface::ENTITY_ID],
+                AdapterInterface::INDEX_TYPE_PRIMARY
+            )
+        );
+
+        $setup->getConnection()->addColumn($tableName, EntityData::ROW_ID, [
+            'type'      => Table::TYPE_INTEGER,
+            'identity'  => true,
+            'primary'   => true,
+            'unsigned'  => true,
+            'nullable'  => false,
+            'comment'   => 'Row ID'
+        ]);
+    }
+}
diff --git a/Setup/UpgradeSchema.php b/Setup/UpgradeSchema.php
index 878ea98bf24fca5bbdd2194c565d34c7491f986a..b70b252316cbda4da12a6b1907c46872312a75c8 100644
--- a/Setup/UpgradeSchema.php
+++ b/Setup/UpgradeSchema.php
@@ -28,16 +28,23 @@ class UpgradeSchema implements UpgradeSchemaInterface
      */
     private $addEntityTableIndex;

+    /**
+     * @var Operation\ReplacePrimaryColumn
+     */
+    private $replacePrimaryColumn;
+
     public function __construct(
         Operation\UpgradeTo300 $upgradeTo300,
         Operation\UpgradeTo310 $upgradeTo310,
         Operation\ChangeDuplicateEntries $changeDuplicateEntries,
-        Operation\AddEntityTableIndex $addEntityTableIndex
+        Operation\AddEntityTableIndex $addEntityTableIndex,
+        Operation\ReplacePrimaryColumn $replacePrimaryColumn
     ) {
         $this->upgradeTo300 = $upgradeTo300;
         $this->upgradeTo310 = $upgradeTo310;
         $this->changeDuplicateEntries = $changeDuplicateEntries;
         $this->addEntityTableIndex = $addEntityTableIndex;
+        $this->replacePrimaryColumn = $replacePrimaryColumn;
     }

     /**
@@ -64,6 +71,10 @@ class UpgradeSchema implements UpgradeSchemaInterface
             $this->addEntityTableIndex->execute($setup);
         }

+        if (!$context->getVersion() || version_compare($context->getVersion(), '3.7.6', '<')) {
+            $this->replacePrimaryColumn->execute($setup);
+        }
+
         $setup->endSetup();
     }
 }
diff --git a/composer.json b/composer.json
index ff7f82b57f908109f71676d19c9f4710e5dbe788..eab34e42ee1c9a08120ad7d57ac0bec747595ed7 100644
--- a/composer.json
+++ b/composer.json
@@ -10,7 +10,7 @@
         "amasty/module-orderattr-mftf-3": "Install module-orderattr-mftf-3 module to be able to run Order Attribute MFTF tests for Magento 2.4.0+ version."
     },
     "type": "magento2-module",
-    "version": "3.7.5",
+    "version": "3.7.6",
     "license": [
         "Commercial"
     ],
diff --git a/etc/module.xml b/etc/module.xml
index c7492c7a363b5ef0979ccbdfa6cc22a16708adda..7c3fd5344417c559dd5f1eb51730b6147828511a 100644
--- a/etc/module.xml
+++ b/etc/module.xml
@@ -2,7 +2,7 @@

 <config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
-    <module name="Amasty_Orderattr" setup_version="3.7.3">
+    <module name="Amasty_Orderattr" setup_version="3.7.6">
         <sequence>
             <module name="Magento_Sales"/>
             <module name="Magento_Checkout"/>
