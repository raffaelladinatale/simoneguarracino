<?php

namespace Magenest\MigrationTool\Helper;

use Magento\Framework\App\Config;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\DeploymentConfig;

class ConfigFile extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_CONFIG_FILE = 'migration_tool/config/file_path';

    protected $_fieldsMap = [
        'username' => 'user',
        'dbname' => 'name'
    ];

    protected $_validConfig = [
        'host', 'port', 'name', 'user', 'password', 'ssl_ca', 'ssl_key', 'ssl_cert'
    ];

    protected $configWriter;
    protected $config;
    protected $extendedFilesHelper;
    protected $deploymentConfig;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        ExtendedFiles $extendedFilesHelper,
        WriterInterface $configWriter,
        Config $config,
        DeploymentConfig $deploymentConfig
    ) {
        parent::__construct($context);
        $this->extendedFilesHelper = $extendedFilesHelper;
        $this->configWriter        = $configWriter;
        $this->config              = $config;
        $this->deploymentConfig    = $deploymentConfig;
    }

    public function saveConfigXml($config, $edition, $version)
    {
        if (is_array($config)) {
            $filePath = $this->extendedFilesHelper->getModulePath('etc/' . $edition . '/' . $version . '/config.xml');
            $xml      = simplexml_load_file($filePath);
            $xml      = $this->_saveMagento1DatabaseConfig($xml, $config);
            $xml      = $this->_saveMagento2DatabaseConfig($xml);
            $xml->asXml($filePath);
            $this->_saveConfigFilePath($filePath);
        }
    }

    protected function _saveMagento1DatabaseConfig($xml, $config)
    {
        if (isset($config['magento1'])) {
            $magento1Config = $this->processDatabaseConfig($config['magento1']);
            if (isset($magento1Config['crypt_key'])) {
                $xml->options->crypt_key = $magento1Config['crypt_key'];
                unset($magento1Config['crypt_key']);
            }
            foreach ($magento1Config as $key => $value) {
                $key = $this->getDatabaseConfigMap($key);
                if (isset($xml->source->database->attributes()[$key])) {
                    $xml->source->database->attributes()->{$key} = $value;
                } else {
                    $xml->source->database->attributes()->addAttribute($key, $value);
                }
            }
        }
        return $xml;
    }

    public function getDatabaseConfigMap($key)
    {
        return isset($this->_fieldsMap[$key]) ? $this->_fieldsMap[$key] : $key;
    }

    public function isValidDatabaseConfig($key)
    {
        return in_array($key,$this->_validConfig);
    }

    protected function _saveMagento2DatabaseConfig($xml)
    {
        $magento2Config = $this->deploymentConfig->get('db/connection/default');
        $magento2Config = $this->processDatabaseConfig($magento2Config);
        foreach ($magento2Config as $key => $value) {
            $key = $this->getDatabaseConfigMap($key);
            if (!$this->isValidDatabaseConfig($key)) {
                continue;
            }
            if (isset($xml->destination->database->attributes()[$key])) {
                $xml->destination->database->attributes()->{$key} = $value;
            } else {
                $xml->destination->database->attributes()->addAttribute($key, $value);
            }
        }
        return $xml;
    }

    public function processDatabaseConfig($databaseConfig)
    {
        $hostPort = explode(':', $databaseConfig['host']);
        if (is_array($hostPort) && count($hostPort) === 2) {
            $databaseConfig['host'] = $hostPort[0];
            if ($hostPort[1] != '3306') {
                $databaseConfig['port'] = $hostPort[1];
            }
        }
        return $databaseConfig;
    }

    protected function _saveConfigFilePath($configFilePath)
    {
        $this->configWriter->save(self::XML_PATH_CONFIG_FILE, $configFilePath);
        $this->config->clean();
    }
}
