<?php
/**
 * Data
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem\DriverPool;
use Magento\Framework\Module\FullModuleList;
use Magento\Framework\Module\ModuleListInterface;

class Data extends AbstractHelper
{
    const MIGRATION_LOG_FILE_NAME = 'migration.log';
    const MAGENTO_MIGRATION_TOOL_MODULE_NAME = 'Magento_DataMigrationTool';
    const MAGENTO_MIGRATION_TOOL_COMPOSER_PACKAGE_NAME = 'magento/data-migration-tool';
    const MAGENEST_MIGRATION_TOOL_MODULE_NAME = 'Magenest_MigrationTool';
    const COMPOSER_JSON_FILE_NAME = 'composer.json';

    protected $toolData;

    protected $_productMetadata;

    protected $componentRegistrar;

    protected $dirReadFactory;

    protected $fileReadFactory;

    protected $directoryList;

    protected $driverFile;

    protected $moduleList;

    protected $fullModuleList;

    public function __construct(
        Context $context,
        ProductMetadataInterface $productMetadata,
        ComponentRegistrar $componentRegistrar,
        \Magento\Framework\Filesystem\Directory\ReadFactory $dirReadFactory,
        \Magento\Framework\Filesystem\File\ReadFactory $fileReadFactory,
        DirectoryList $directoryList,
        File $driverFile,
        ModuleListInterface $moduleList,
        FullModuleList $fullModuleList
    ) {
        parent::__construct($context);
        $this->componentRegistrar = $componentRegistrar;
        $this->_productMetadata   = $productMetadata;
        $this->dirReadFactory     = $dirReadFactory;
        $this->fileReadFactory    = $fileReadFactory;
        $this->directoryList      = $directoryList;
        $this->driverFile         = $driverFile;
        $this->moduleList         = $moduleList;
        $this->fullModuleList     = $fullModuleList;
    }

    public function getMigrationEditionOptions($edition)
    {
        if ($edition) {
            $toolPath = $this->componentRegistrar->getPath(ComponentRegistrar::MODULE, Data::MAGENTO_MIGRATION_TOOL_MODULE_NAME);
            $path     = $toolPath . '/etc/' . $edition;
            return $this->getAllDirectory($path);
        }
        return [];
    }

    /**
     * @param string $path
     * @return array
     */
    public function getAllDirectory($path)
    {
        $dir         = $this->dirReadFactory->create($path);
        $directories = [];
        if (!$dir->isExist()) {
            return [];
        }
        foreach ($dir->read() as $item) {
            if ($dir->isDirectory($item)) {
                $directories[] = $item;
            }
        }

        return $directories;
    }

    public function getMagentoVersion()
    {
        return $this->_productMetadata->getVersion();
    }

    public function getMagentoEdition()
    {
        return $this->_productMetadata->getEdition();
    }

    public function getDataMigrationToolComposerData()
    {
        if ($this->toolData === null) {
            $path      = $this->componentRegistrar->getPath(ComponentRegistrar::MODULE, self::MAGENTO_MIGRATION_TOOL_MODULE_NAME);
            $directory = $this->dirReadFactory->create($path);
            $data      = [];
            if ($directory->isFile(self::COMPOSER_JSON_FILE_NAME)) {
                try {
                    $data = json_decode($directory->readFile(self::COMPOSER_JSON_FILE_NAME), true);
                } catch (FileSystemException $e) {
                    $data = [];
                }
            }
            $this->toolData = new DataObject($data);
        }
        return $this->toolData;
    }

    /**
     * @param string $extendedPath
     * @param string $module
     * @return mixed|string|null
     */
    public function getModulePath($extendedPath = '', $module = Data::MAGENEST_MIGRATION_TOOL_MODULE_NAME)
    {
        $modulePath = $this->componentRegistrar->getPath(\Magento\Framework\Component\ComponentRegistrar::MODULE, $module);
        if ($extendedPath) {
            $modulePath .= ('/' . $extendedPath);
        }
        return $modulePath;
    }

    public function isMagentoMigrationToolEnabled()
    {
        return $this->_moduleManager->isEnabled(self::MAGENTO_MIGRATION_TOOL_MODULE_NAME);
    }

    public function isMagentoMigrationToolInstalled()
    {
        return $this->fullModuleList->getOne(self::MAGENTO_MIGRATION_TOOL_MODULE_NAME) !== null;
    }

    public function getLogFile($fileName = self::MIGRATION_LOG_FILE_NAME)
    {
        return $this->fileReadFactory->create($this->getLogDirectory() . '/' . $fileName, DriverPool::FILE);
    }

    public function getLogFiles($type = null)
    {
        $types = [
            'general' => self::MIGRATION_LOG_FILE_NAME,
            'debug'   => 'log/migration/debug.log'
        ];
        if ($type !== null && isset($types[$type])) {
            return $types[$type];
        }
        return $types;
    }

    public function getLogDirectory()
    {
        return $this->directoryList->getPath(DirectoryList::VAR_DIR);
    }

    public function cleanLog($fileName)
    {
        $logDir   = $this->getLogDirectory() . '/';
        $filePath = $logDir . $fileName;
        if (!$this->driverFile->isExists($filePath) || !$this->driverFile->isWritable($filePath)) {
            $dirPaths = explode('/', $filePath);
            unset($dirPaths[count($dirPaths) - 1]);
            $this->driverFile->createDirectory(implode('/', $dirPaths));
            $this->driverFile->touch($filePath);
        }
        return $this->driverFile->filePutContents($filePath, PHP_EOL);
    }
}
