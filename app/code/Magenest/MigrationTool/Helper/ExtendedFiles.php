<?php

namespace Magenest\MigrationTool\Helper;

use Magenest\MigrationTool\Model\Config\Source\Edition;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\Filesystem\Directory\ReadFactory;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Class MapFiles
 * @package Magenest\MigrationTool\Helper
 */
class ExtendedFiles extends AbstractHelper
{
    const MIGRATION_TOOL_DIRECTORY_NAME = 'data_migration';
    /**
     * @var File
     */
    protected $_file;

    protected $dirReadFactory;

    /**
     * @var ComponentRegistrar
     */
    protected $componentRegistrar;

    /**
     * @var Edition
     */
    protected $editionOptions;

    protected $helper;

    protected $directoryList;


    public function __construct(
        Context $context,
        Data $data,
        File $file,
        ComponentRegistrar $componentRegistrar,
        Edition $edition,
        ReadFactory $dirReadFactory,
        DirectoryList $directoryList
    ) {
        parent::__construct($context);
        $this->helper = $data;
        $this->_file = $file;
        $this->componentRegistrar = $componentRegistrar;
        $this->editionOptions = $edition;
        $this->dirReadFactory = $dirReadFactory;
        $this->directoryList = $directoryList;
    }

    /**
     * @param $edition
     * @param $version
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function copyExtendedFiles($edition, $version)
    {
        $this->createDirectories(...func_get_args());
        $this->copyConfigFile(...func_get_args());
//        $this->copyMapFile(...func_get_args());
//        $this->copyGlobalMapFiles($edition, '');
        return $this->getFilesAndDirs($edition);
    }

    /**
     * @param $edition
     * @param $version
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function createDirectories($edition, $version)
    {
        if (!$this->isExist($this->getModulePath('etc/' . $edition))) {
            $this->_file->createDirectory($this->getModulePath('etc/' . $edition));
        }
        if (!$this->isExist($this->getModulePath('etc/' . $edition . '/' . $version))) {
            $this->_file->createDirectory($this->getModulePath('etc/' . $edition . '/' . $version));
        }
    }

    /**
     * @param $edition
     * @param $version
     * @return $this
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function copyConfigFile($edition, $version)
    {
        $file = 'config.xml';
        $this->copyFile($edition, $version, $file);
//        $filePath = $this->getModulePath('etc/' . $edition . '/' . $version . '/' . $file);
//        $content = $this->_file->fileGetContents($filePath);
//        $content = preg_replace('/\.xml\.dist<\//m', '.xml</', $content);
//        $this->_file->filePutContents($filePath, $content);
        return $this;
    }

    /**
     * @param $edition
     * @param $version
     * @return $this
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function copyMapFile($edition, $version)
    {
        $this->copyFile($edition, $version, 'map.xml');
        return $this;
    }

    /**
     * @param $edition
     * @param $version
     * @return $this
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function copyGlobalMapFiles($edition, $version)
    {
        foreach ($this->editionOptions->toEditionMapFiles($edition) as $file) {
            $this->copyFile($edition, $version, $file);
        }
        return $this;
    }

    /**
     * @param $edition
     * @param $version
     * @param $fileName
     * @return $this
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function copyFile($edition, $version, $fileName)
    {
        $path = 'etc';
        if ($edition) {
            $path .= ('/' . $edition);
        }
        if ($version) {
            $path .= ('/' . $version);
        }
        $path .= '/';
        if (!$this->isExist($this->getModulePath($path . $fileName))) {
            $this->_file->copy(
                $this->getModulePath($path, Data::MAGENTO_MIGRATION_TOOL_MODULE_NAME, false) . $fileName . '.dist',
                $this->getModulePath($path) . $fileName
            );
        }
        return $this;
    }

    public function getFilesAndDirs($edition)
    {
        try {
            $data = [];
            $dir = $this->dirReadFactory->create($this->getModulePath('etc/' . $edition));
            $entities = $dir->read();
            foreach ($entities as $entity) {
                if ($dir->isDirectory($entity)) {
                    $children = [];
                    foreach ($dir->read($entity) as $item) {
                        $children[$item] = $item;
                    }
                    $data[$entity] = $children;
                } elseif ($dir->isFile($entity)) {
                    $data[$entity] = $entity;
                }
            }
        } catch (\Exception $e) {
            $data = [];
        }
        return $data;
    }

    /**
     * @param $path
     * @return bool
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function isExist($path)
    {
        return $this->_file->isExists($path);
    }

    /**
     * @param string $extendedPath
     * @param string $module
     * @param bool $isTemp
     * @return mixed|string|null
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getModulePath($extendedPath = '', $module = '', $isTemp = true)
    {
        if (!$module) {
            $module = Data::MAGENEST_MIGRATION_TOOL_MODULE_NAME;
        }
        if ($isTemp) {
            $path = $this->directoryList->getPath(DirectoryList::VAR_DIR);
            $path .= ('/' . self::MIGRATION_TOOL_DIRECTORY_NAME);
            if (!$this->isExist($path)) {
                $this->_file->createDirectory($path);
            }
            $path .= ('/' . $extendedPath);
            return $path;
        } else {
            return $this->helper->getModulePath($extendedPath, $module);
        }
    }
}
