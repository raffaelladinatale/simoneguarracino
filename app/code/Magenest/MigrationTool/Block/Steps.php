<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_migrate extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_migrate
 * @author LamPH
 */

namespace Magenest\MigrationTool\Block;

use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Model\Config\Source\Edition;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

class Steps extends \Magento\Framework\View\Element\Template
{
    protected $editionOptions;

    protected $toolHelper;

    public function __construct(
        Template\Context $context,
        Edition $editionOptions,
        Data $toolHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->editionOptions = $editionOptions;
        $this->toolHelper = $toolHelper;
    }

    /**
     * @return array
     */
    public function getEditionOptions()
    {
        $options = $this->editionOptions->toArray();
        $currentEdition = $this->toolHelper->getMagentoEdition();
        $editionMap = $this->editionOptions->getEditionMapValue();
        $mappedEdition = $editionMap[$currentEdition];
        $options = array_filter($options, function ($index) use ($mappedEdition) {
            return in_array($index, $mappedEdition);
        }, ARRAY_FILTER_USE_KEY);
        array_unshift($options, __('Please select'));
        return $options;
    }

    public function getLogFileContent()
    {
        return nl2br($this->_escaper->escapeHtml($this->toolHelper->getLogFileContent()->readAll()));
    }
}
