<?php
/**
 * MigrateMedia
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Widget\Button;
use Magento\Config\Block\System\Config\Form\Field;

class MigrateMedia extends Field
{
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->getLayout()->createBlock(Button::class)->setData([
            'id' => 'migrate_media',
            'label' => __('Contact Us'),
            'onclick' => 'location.href="mailto:sales@magenest.com"'
        ])->toHtml();
    }
}
