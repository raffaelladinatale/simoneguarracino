<?php
/**
 * Steps
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Block\Adminhtml\Migration;


use Magento\Backend\Block\Widget\Container;

class Steps extends Container
{

    protected function _prepareLayout()
    {
        $this->addButton('open_steps_wizard', [
            'id' => 'open_steps_wizard',
            'label' => __('Setup'),
            'class' => 'primary',
            'onclick' => 'openMigrationSetupDialog()'
        ]);
        return parent::_prepareLayout();
    }

    public function getMigrationStepsWizard($initData)
    {
        /** @var \Magento\Ui\Block\Component\StepsWizard $wizardBlock */
        $wizardBlock = $this->getChildBlock('migration-steps-wizards');
        if ($wizardBlock) {
            $wizardBlock->setInitData($initData);
            return $wizardBlock->toHtml();
        }
        return '';
    }
}
