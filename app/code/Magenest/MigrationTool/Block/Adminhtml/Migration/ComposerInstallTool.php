<?php
/**
 * ComposerInstallTool
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Block\Adminhtml\Migration;

use Magenest\MigrationTool\Helper\Data;
use Magento\Backend\Block\Template;

class ComposerInstallTool extends Template
{
    protected $_template = 'Magenest_MigrationTool::migration/composer-install-tool.phtml';

    protected $helper;

    public function __construct(
        Template\Context $context,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    public function getMagentoVersion()
    {
        return $this->helper->getMagentoVersion();
    }

    public function getRootDirectoryPath()
    {
        return parent::getRootDirectory()->getAbsolutePath();
    }
}
