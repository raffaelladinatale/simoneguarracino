<?php
/**
 * LogViewer
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Block\Adminhtml\Migration;


use Magenest\MigrationTool\Helper\Data;
use Magento\Backend\Block\Template;
use Magento\Framework\Exception\FileSystemException;

class LogViewer extends Template
{
    protected $helper;

    public function __construct(
        Template\Context $context,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    public function getLogFileContent($fileName)
    {
        try {
            return nl2br($this->_escaper->escapeHtml($this->helper->getLogFile($fileName)->readAll()));
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getReloadUrl()
    {
        return $this->getUrl('*/ajax/reloadLog');
    }

    public function getDownloadUrl()
    {
        return $this->getUrl('*/ajax/downloadLog');
    }

    public function getCleanUrl()
    {
        return $this->getUrl('*/ajax/cleanLog');
    }

    public function getLogFiles()
    {
        return $this->helper->getLogFiles();
    }

    public function getReloadMode()
    {
        // TODO: 1 = auto, 0 = manually
        return 0;
    }
}
