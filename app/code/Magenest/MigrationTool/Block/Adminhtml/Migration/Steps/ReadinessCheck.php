<?php
/**
 * First
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Block\Adminhtml\Migration\Steps;

use Magenest\MigrationTool\Model\Session;
use Magento\Framework\View\Element\Template\Context;
use Magento\Ui\Block\Component\StepsWizard\StepAbstract;

class ReadinessCheck extends StepAbstract
{
    protected $migrationConfig;

    public function __construct(
        Context $context,
        Session $migrationConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->migrationConfig = $migrationConfig;
    }

    /**
     * The first is wizard registration name, the rest is steps registration name
     *
     * @return string
     */
    public function getAllComponentsNames()
    {
        /** @var array $steps */
        $steps = $this->getParentBlock()->getStepComponents();
        array_unshift($steps, $this->getParentComponentName());
        return '\'' . implode('\', \'', $steps) . '\'';
    }

    public function isReady()
    {
        return (bool)$this->migrationConfig->getConfig('is_ready');
    }

    public function getSaveUrl()
    {
        return $this->getUrl('migration/ajax/checkReadiness');
    }

    /**
     * @inheritDoc
     */
    public function getCaption()
    {
        return __('Readiness Check');
    }
}
