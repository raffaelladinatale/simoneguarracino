<?php
/**
 * EditionVersion
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Block\Adminhtml\Migration\Steps;

use Magento\Ui\Block\Component\StepsWizard\StepAbstract;

class Magento1Version extends StepAbstract
{
    public function getSaveUrl()
    {
        return $this->getUrl('*/ajax/selectEditionVersion');
    }

    /**
     * @inheritDoc
     */
    public function getCaption()
    {
        return __('Magento 1 Version');
    }
}
