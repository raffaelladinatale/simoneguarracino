<?php
/**
 * Second
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Block\Adminhtml\Migration\Steps;

use Magento\Ui\Block\Component\StepsWizard\StepAbstract;

class DatabaseConnection extends StepAbstract
{
    public function getCheckUrl()
    {
        return $this->getUrl('*/ajax/checkDatabaseConnection');
    }

    /**
     * @inheritDoc
     */
    public function getCaption()
    {
        return __('Magento %1 Database Check', str_replace('magento', '', $this->getData('config/version')));
    }
}
