<?php
/**
 * Magento1
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Ui\DataProvider\DatabaseConnection;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;

class Magento1 extends DataProvider
{

    public function getData()
    {
        return [];
    }
}
