<?php
/**
 * EditionVersion
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Ui\DataProvider;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;

class EditionVersion extends DataProvider
{
    public function getData()
    {
        return [];
    }
}
