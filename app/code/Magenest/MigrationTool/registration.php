<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_migrate extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_migrate
 * @author LamPH
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Magenest_MigrationTool',
    __DIR__
);
