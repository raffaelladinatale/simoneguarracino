// jscs:disable jsDoc
define([
    'uiComponent',
    'underscore',
    'jquery',
    'mage/template',
    'mage/storage',
    'Magento_Ui/js/lib/spinner',
    'Magenest_MigrationTool/js/lib/step-wizard/modify-wizard',
    'mage/translate'
], function (Component, _, $, mageTemplate, storage, Spinner, customizeWizard) {
    'use strict';

    return Component.extend({
        defaults: {
            isReady: false,
            clientConfig: {
                urls: {
                    save: '${ $.save_url }'
                }
            },
            readinessCheckItemListElem: $('.readiness-check-item-list'),
            notificationMessage: {
                text: null,
                error: null
            },
            readinessCheckItemTemplate: '' +
                '            <div class="readiness-check-item">' +
                '                <div class="readiness-check-item-content">' +
                '                    <h3 class="readiness-check-item-title"><%- data.title %></h3>' +
                '                    <div class="messages">' +
                '                        <div class="message message-<% if (data.error === true) { %>error<% } else { %>success<% } %>">' +
                '                            <div data-ui-id="messages-message"><%- data.message %></div>' +
                '                        </div>' +
                '                    </div>' +
                '                    <% if (data.hasOwnProperty("info") && data.info !== "") { %><div class="info">' +
                '                        <div class="message message">' +
                '                            <div data-ui-id="messages-message"><%- data.info %></div>' +
                '                        </div>' +
                '                    </div><% } %>' +
                '                </div>' +
                '            </div>'
        },

        initialize: function () {
            this._super();
            return this;
        },

        initObservable: function () {
            this._super()
                .observe('isReady');
            return this;
        },

        startReadinessCheck: function () {
            Spinner.get(this.name).show();
            storage.get(this.clientConfig.urls.save).success(function (response) {
                this.isReady(!response.error);
                this.readinessCheckItemListElem.html('');
                _.each(response.data || [response.data] || [], function (item) {
                    this.readinessCheckItemListElem.append(_.unescape(mageTemplate(this.readinessCheckItemTemplate, {data: item})));
                }.bind(this));
                if (this.isReady() === true) {
                    this.wizard.setNotificationMessage($.mage.__('Completed! You can now move on to the next step.'));
                } else {
                    this.wizard.setNotificationMessage($.mage.__('Error! Please fix following error message to move the next step.'), true);
                }

            }.bind(this)).always(function () {
                Spinner.get(this.name).hide();
            }.bind(this));
        },

        render: function (wizard) {
            this.wizard = customizeWizard(wizard);
            if (this.is_ready === true) {
                this.isReady(this.is_ready);
                this.wizard.setNotificationMessage($.mage.__('Completed! You can now move on to the next step.'));
            }
        },

        force: function (wizard) {
            if (!this.isReady()) {
                this.wizard.cleanNotificationMessage();
                this.startReadinessCheck();
                return false;
            } else {
                this.wizard.cleanErrorNotificationMessage();
                return true;
            }
        },
        back: function () {
        }
    });
});
