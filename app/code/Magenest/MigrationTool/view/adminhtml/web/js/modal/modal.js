define([
    'jquery'
], function ($) {
    "use strict";

    $.widget('mage.modal', $.mage.modal, {
        closeModal: function () {
            this._super();
            window.location.reload();
        }
    });

    return $.mage.modal;
});