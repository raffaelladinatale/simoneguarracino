define([
    'uiComponent',
    'jquery',
    'underscore',
    'mage/storage',
    'Magento_Ui/js/lib/spinner',
    'mage/translate'
], function (Component, $, _, storage, Spinner) {
    "use strict";

    return Component.extend({
        defaults: {
            isConnected: false,
            clientConfig: {
                urls: {
                    save: '${ $.check_url }'
                }
            },
            modules: {
                settingsForm: '${ $.formName }'
            },
            notificationMessage: {
                text: null,
                error: null
            }
        },
        initialize: function () {
            this._super();
            Spinner.get(this.name).hide();
            return this;
        },
        initObservable: function () {
            this._super().observe('isConnected');
            return this;
        },
        render: function (wizard) {
            this.wizard = wizard;
        },
        force: function () {
            if (!this.isConnected()) {
                var form = this.settingsForm();
                if (!form || !form.source) {
                    throw new Error($.mage.__('Form is undefined, please try again.'))
                }
                form.validate();
                if (form.source.get('params.invalid')) {
                    throw new Error($.mage.__('Step is invalid.'));
                }
                this.modifyPayload(form);
                Spinner.get(this.name).show();
                $.ajax({
                    url: this.clientConfig.urls.save,
                    type: 'POST',
                    data: form.source.get('data'),
                }).done(function (response) {
                    if (response && response.is_connected === true) {
                        this.isConnected(response.is_connected);
                        this.disableFormElements(form);
                        this.wizard.setNotificationMessage($.mage.__('Completed! You can now move on to the next step.'));
                    } else {
                        this.wizard.setNotificationMessage(response.message, true);
                    }
                    return true;
                }.bind(this)).always(function () {
                    Spinner.get(this.name).hide();
                    return false;
                }.bind(this));
                return false;
            } else {
                this.wizard.cleanNotificationMessage();
                return true;
            }
        },
        back: function () {
        },
        modifyPayload: function (form) {
            form.source.set('data.version', this.version);
        },
        disableFormElements: function (form) {
            _.each(form.elems(), function (container) {
                _.each(container.elems(), function (element) {
                    element.disabled(true);
                });
            });
        },
    });
});
