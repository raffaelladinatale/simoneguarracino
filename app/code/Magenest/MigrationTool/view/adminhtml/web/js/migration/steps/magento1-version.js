define([
    'uiComponent',
    'jquery',
    'underscore',
    'mage/storage',
    'Magento_Ui/js/lib/spinner',
    'mage/translate'
], function (Component, $, _, storage, Spinner) {
    "use strict";

    return Component.extend({
        defaults: {
            clientConfig: {
                urls: {
                    save: '${ $.save_url }'
                }
            },
            modules: {
                settingsForm: '${ $.formName }'
            },
            notificationMessage: {
                text: null,
                error: null
            }
        },
        nextLabelText: $.mage.__('Finish'),
        initialize: function () {
            this._super();
            Spinner.get(this.name).hide();
            return this;
        },
        render: function (wizard) {
            this.wizard = wizard;
        },
        force: function () {
            var form = this.settingsForm();
            if (!form || !form.source) {
                throw new Error($.mage.__('Form is undefined, please try again.'))
            }
            form.validate();
            if (form.source.get('params.invalid')) {
                throw new Error($.mage.__('Step is invalid.'));
            }
            Spinner.get(this.name).show();
            $.ajax({
                url: this.clientConfig.urls.save,
                type: 'POST',
                data: form.source.get('data'),
            }).done(function (response) {
                if (response && response.error === false) {
                    this.wizard.setNotificationMessage($.mage.__('Completed! You have finished the setup wizard, you can close it now.'));
                } else {
                    this.wizard.setNotificationMessage(response.message, true);
                }
                return true;
            }.bind(this)).always(function () {
                Spinner.get(this.name).hide();
                return false;
            }.bind(this));
            return false;
        },
        back: function () {
        }
    });
});
