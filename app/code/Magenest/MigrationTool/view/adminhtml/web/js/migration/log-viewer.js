define([
    'jquery',
    'domReady!'
], function ($) {
    "use strict";

    function doReload(config) {
        $(config.log_spinner).show();
        $.get(config.reload_url, {type: config.log_type}, function (response) {
            $(config.log_content).html(response.content);
            $(config.log_spinner).hide();
        });
    }

    function doDownload(config) {
        window.open(config.download_url + 'type/' + config.log_type, '_blank');
    }

    function doClean(config) {
        $(config.log_spinner).show();
        $.get(config.clean_url, {type: config.log_type}).done(function () {
            doReload(config);
        });
    }

    function autoReload(config) {
        return setInterval(function () {
            if ($(config.reload_mode).val() == 1) {
                doReload(config);
            }
        }, 5000);
    }

    function listen(config) {

        autoReload(config);

        $(config.reload_mode).on('change', function () {
            if ($(this).val() == 1) {
                $(config.reload_button).attr('disabled', true);
                $(config.reload_mode_button).html($.mage.__('Disable Auto Reload'));
            } else {
                $(config.reload_button).attr('disabled', false);
                $(config.reload_mode_button).html($.mage.__('Auto Reload (5s)'));
            }
        }).trigger('change');

        $(config.reload_mode_button).on('click', function () {
            if ($(config.reload_mode).val() == 1) {
                $(config.reload_mode).val(0).trigger('change');
            } else {
                $(config.reload_mode).val(1).trigger('change');
            }
        });

        $(config.reload_button).on('click', function () {
            doReload(config);
        });

        $(config.download_button).on('click', function () {
            doDownload(config);
        });

        $(config.clean_button).on('click', function () {
            doClean(config);
        });
    }

    return function (config) {
        listen(config);
        $(config.log_spinner).hide();

    }
});
