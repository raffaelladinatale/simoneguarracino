define([
    'underscore'
], function (_) {
    "use strict";

    return function (wizard) {
        var mixin = {
            /**
             * @param {Number} newIndex
             * @return {Boolean}
             * @private
             */
            _next: function (newIndex) {
                newIndex = _.isNumber(newIndex) ? newIndex : this.index + 1;

                try {
                    if (!this.getStep().force(this)) {
                        return false;
                    }

                    if (newIndex >= this.steps.length) {
                        return false;
                    }
                } catch (e) {
                    this.setNotificationMessage(e.message, true);

                    return false;
                }
                this.cleanErrorNotificationMessage();
                this.index = newIndex;
                this.cleanNotificationMessage();
                this.render();
            }
        };
        return _.extend(wizard, mixin);
    }
});
