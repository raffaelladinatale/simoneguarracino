define([
    'jquery'
], function ($) {
    "use strict";

    var wizardDialogElement = '[data-role="migration-steps-wizard-dialog"]',
        mixin = {
            initialize: function () {
                this._super();
                window.openMigrationSetupDialog = function () {
                    $(wizardDialogElement).trigger('openModal');
                };
                return this;
            },
            close: function () {
                $(wizardDialogElement).trigger('closeModal');
            }
        };

    return function (target) {
        return target.extend(mixin);
    }
});
