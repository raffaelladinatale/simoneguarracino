# Magenest M1 Migration Tool 

Thank you for buying our product.
This extension address to [Magenest](https://store.magenest.com/).


## Requirement
1. Magento Open Source or Commerce 2.2.x, 2.3.x
2. PHP 7.0 or newer
3. Apache2/Nginx webserver
4. MySQL 5.6 or newer
4. Composer

## User guide

Please find the user guide [here](https://confluence.izysync.com/display/DOC/Magenest+Migration+Tool+User+Guide)

### Setup
- move the module folder to [site root]/app/code/Magenest/

From Magento root folder, run the following commands:
- php bin/magento setup:upgrade
- php bin/magento setup:di:compile
- php bin/magento setup:static-content:deploy
