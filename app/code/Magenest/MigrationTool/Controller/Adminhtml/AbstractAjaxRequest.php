<?php
/**
 * AbstractAjaxRequest
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml;

use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Model\Session;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;

abstract class AbstractAjaxRequest extends Action
{
    protected $toolHelper;

    protected $migrationConfig;

    public function __construct(
        Action\Context $context,
        Data $helper,
        Session $migrationConfig
    ) {
        parent::__construct($context);
        $this->toolHelper = $helper;
        $this->migrationConfig = $migrationConfig;
    }

    /**
     * @param null|array $data
     * @return Json
     */
    public function createJsonResultPage($data = null)
    {
        $jsonResult = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if (is_array($data)) {
            $jsonResult->setData($data);
        }
        return $jsonResult;
    }
}
