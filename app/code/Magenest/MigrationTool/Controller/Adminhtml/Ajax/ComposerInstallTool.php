<?php
/**
 * ComposerInstallTool
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml\Ajax;

use Magenest\MigrationTool\Controller\Adminhtml\AbstractAjaxRequest;
use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Model\ComposerAuthFile;
use Magenest\MigrationTool\Model\Session;
use Magento\Backend\App\Action;
use Magento\Framework\Composer\MagentoComposerApplicationFactory;

class ComposerInstallTool extends AbstractAjaxRequest
{
    const USE_COMMAND_LINE_INSTEAD_MESSAGE = 'Please provide your Magento authentication keys using the interactive console to authenticate first or you can try the command line to install manually instead.';

    protected $magentoComposerApplicationFactory;

    protected $composerAuthFile;

    public function __construct(
        Action\Context $context,
        Data $helper,
        Session $migrationConfig,
        MagentoComposerApplicationFactory $magentoComposerApplicationFactory,
        ComposerAuthFile $composerAuthFile
    ) {
        parent::__construct($context, $helper, $migrationConfig);
        $this->magentoComposerApplicationFactory = $magentoComposerApplicationFactory;
        $this->composerAuthFile                  = $composerAuthFile;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        try {
            $memoryLimit      = ini_get('memory_limit');
            $maxExecutionTime = ini_get('max_execution_time');
            ini_set('memory_limit', '2G');
            ini_set('max_execution_time', 1800);
            try {
                $isExists = $this->composerAuthFile->isExists();
                if ($isExists) {
                    $this->composerAuthFile->createBackup();
                }
                $this->createAuthFile();
                $magentoComposerApp = $this->magentoComposerApplicationFactory->create();
                $response           = $magentoComposerApp->runComposerCommand([
                    'command'  => 'require',
                    'packages' => [Data::MAGENTO_MIGRATION_TOOL_COMPOSER_PACKAGE_NAME . ':' . $this->toolHelper->getMagentoVersion()]
                ]);
                if ($isExists) {
                    $this->composerAuthFile->rollback();
                    $this->composerAuthFile->deleteBackup();
                } else {
                    $this->composerAuthFile->deleteAuthFile();
                }
                ini_set('memory_limit', $memoryLimit);
                ini_set('max_execution_time', $maxExecutionTime);
            } catch (\RuntimeException $exception) {
                $response = $exception->getMessage() . PHP_EOL . __(self::USE_COMMAND_LINE_INSTEAD_MESSAGE);
            }
        } catch (\Exception $e) {
            $response = $e->getMessage() . PHP_EOL . __(self::USE_COMMAND_LINE_INSTEAD_MESSAGE);
        }
        return $this->createJsonResultPage(['content' => nl2br($response)]);
    }

    protected function createAuthFile()
    {
        $username = $this->getRequest()->getParam('username');
        $password = $this->getRequest()->getParam('password');
        $this->composerAuthFile->createFile($username, $password);
    }
}
