<?php
/**
 * SelectEditionVersion
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml\Ajax;

use Magenest\MigrationTool\Controller\Adminhtml\AbstractAjaxRequest;
use Magenest\MigrationTool\Helper\ConfigFile;
use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Helper\ExtendedFiles;
use Magenest\MigrationTool\Model\Session;
use Magento\Backend\App\Action\Context;

class SelectEditionVersion extends AbstractAjaxRequest
{
    protected $extendedFilesHelper;

    protected $configFileHelper;

    public function __construct(
        Context $context,
        Data $helper,
        Session $migrationConfig,
        ExtendedFiles $extendedFilesHelper,
        ConfigFile $configFileHelper
    ) {
        parent::__construct($context, $helper, $migrationConfig);
        $this->extendedFilesHelper = $extendedFilesHelper;
        $this->configFileHelper = $configFileHelper;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $edition = $this->getRequest()->getParam('edition');
        $version = $this->getRequest()->getParam('version');
        $error = true;
        $message = '';
        if ($edition && $version) {
            foreach ([
                         'edition' => $edition,
                         'version' => $version
                     ] as $key => $value) {
                $this->migrationConfig->setConfig($key, $value);
            }
            try {
                $this->finish($edition, $version);
            } catch (\Exception $e) {
                $message = $e->getMessage();
            }
            $error = false;
        } else {
            $message = __('Edition and version are the required fields.');
        }
        return $this->createJsonResultPage(['error' => $error, 'message' => $message]);
    }

    /**
     * @param $edition
     * @param $version
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function finish($edition, $version)
    {
        $this->extendedFilesHelper->copyExtendedFiles($edition, $version);
        $this->configFileHelper->saveConfigXml($this->migrationConfig->getConfig(), $edition, $version);
    }
}
