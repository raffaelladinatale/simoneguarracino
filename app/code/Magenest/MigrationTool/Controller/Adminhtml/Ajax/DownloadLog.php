<?php
/**
 * DownloadLog
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml\Ajax;

use Magenest\MigrationTool\Controller\Adminhtml\AbstractAjaxRequest;
use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Model\Session;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;

class DownloadLog extends AbstractAjaxRequest
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    private $driverFile;

    public function __construct(
        Action\Context $context,
        Data $helper,
        Session $migrationConfig,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Filesystem\Driver\File $driverFile
    ) {
        $this->fileFactory = $fileFactory;
        $this->filesystem  = $filesystem;
        $this->driverFile  = $driverFile;
        parent::__construct($context, $helper, $migrationConfig);
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if (empty($fileName = $this->toolHelper->getLogFiles($this->getRequest()->getParam('type')))) {
            $fileName = $this->toolHelper->getLogFiles('general');
        }
        try {
            $path      = $fileName;
            $directory = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
            $filePath  = $directory->getAbsolutePath($path);
            if (!$directory->isExist($filePath) || $directory->isFile($filePath)) {
                $dirPaths = explode('/', $filePath);
                unset($dirPaths[count($dirPaths) - 1]);
                $this->driverFile->createDirectory(implode('/', $dirPaths));
                $this->driverFile->touch($filePath);
            }
            return $this->fileFactory->create(
                $path,
                $directory->readFile($path),
                DirectoryList::VAR_DIR
            );
        } catch (\Exception $exception) {
            $this->_forward('noroute');
        }
    }
}
