<?php
/**
 * ReadinessCheck
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml\Ajax;

use Magenest\MigrationTool\Block\Adminhtml\Migration\ComposerInstallTool;
use Magenest\MigrationTool\Controller\Adminhtml\AbstractAjaxRequest;
use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Model\Session;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Crontab\CrontabManagerInterface;

class CheckReadiness extends AbstractAjaxRequest
{
    protected $crontabManager;

    public function __construct(
        Context $context,
        Data $helper,
        Session $migrationConfig,
        CrontabManagerInterface $crontabManager
    ) {
        parent::__construct($context, $helper, $migrationConfig);
        $this->crontabManager = $crontabManager;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        return $this->createJsonResultPage($this->getResult());
    }

    /**
     * Get result as array from class with method format
     * checkReadiness + step key
     *
     * @return array
     */
    protected function getResult()
    {
        $methods = get_class_methods($this);
        $result  = [];
        foreach ($methods as $method) {
            if (substr($method, 0, 14) === 'checkReadiness') {
                $result = array_merge($result, $this->{$method}());
            }
        }
        $error = false;
        foreach ($result as $item) {
            if (isset($item['error']) && $item['error']) {
                $error = $item['error'];
                break;
            }
        }
        $this->migrationConfig->setConfig('is_ready', !$error);
        return [
            'error' => $error,
            'data'  => $result
        ];
    }

    protected function checkReadinessCronJobs()
    {
        $tasks = $this->crontabManager->getTasks();

        if (empty($tasks)) {
            $error   = false;
            $message = __('The cron jobs is not installed');
        } else {
            $error = false;
            foreach ($tasks as $task) {
                if (substr($task, 0, 1) !== '#') {
                    $error = true;
                    break;
                }
            }
            if ($error) {
                $message = __('Please disable the Magento cron jobs.');
            } else {
                $message = __('The cron jobs has been disabled');
            }
        }
        return [
            'cron_check' => [
                'error'   => $error,
                'title'   => __('Cron Jobs Check'),
                'message' => $message
            ]
        ];
    }

    protected function checkReadinessMagentoTool()
    {
        $title = __('Magento Data Migration Tool Check');
        if (!$this->toolHelper->isMagentoMigrationToolInstalled()) {
            return [
                'data_migration_tool_check' => [
                    'error'   => true,
                    'title'   => $title,
                    'message' => __('The tool is not installed.'),
                    'info'    => ($this->_view->getLayout()->createBlock(ComposerInstallTool::class)->setViaComposer(true)->toHtml())
                ]
            ];
        }

        if (!$this->toolHelper->isMagentoMigrationToolEnabled()) {
            return [
                'data_migration_tool_check' => [
                    'error'   => true,
                    'title'   => $title,
                    'message' => __('The tool is not enabled.'),
                    'info'    => $this->_view->getLayout()->createBlock(ComposerInstallTool::class)->toHtml()
                ]
            ];
        }

        $magentoVersion  = $this->toolHelper->getMagentoVersion();
        $dataToolVersion = $this->toolHelper->getDataMigrationToolComposerData()->getVersion();
        if ($magentoVersion !== $dataToolVersion) {
            return [
                'data_migration_tool_check' => [
                    'error'   => true,
                    'title'   => $title,
                    'message' => __(
                        'The %1\'s version (%2) does not match the Magento codebase\'s version (%3).',
                        $this->toolHelper->getDataMigrationToolComposerData()->getDescription(),
                        $dataToolVersion,
                        $magentoVersion
                    ),
                    'info'    => ($this->_view->getLayout()->createBlock(ComposerInstallTool::class)->setViaComposer(true)->toHtml())
                ]
            ];
        }
        return [
            'data_migration_tool_check' => [
                'error'   => false,
                'title'   => $title,
                'message' => __(
                    'The %1\'s version (%2) matched the Magento codebase\'s version (%3).',
                    $this->toolHelper->getDataMigrationToolComposerData()->getDescription(),
                    $dataToolVersion,
                    $magentoVersion
                )
            ]
        ];
    }
}
