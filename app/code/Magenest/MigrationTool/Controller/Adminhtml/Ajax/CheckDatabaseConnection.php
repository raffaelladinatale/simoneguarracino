<?php
/**
 * CheckDatabaseConnection
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml\Ajax;

use Magenest\MigrationTool\Controller\Adminhtml\AbstractAjaxRequest;
use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Model\Session;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResourceConnection\ConnectionFactory;

/**
 * Class CheckDatabaseConnection
 *
 * @package Magenest\MigrationTool\Controller\Adminhtml\Ajax
 */
class CheckDatabaseConnection extends AbstractAjaxRequest
{
    /**
     * @var array
     */
    protected $_validConfig = [
        'host', 'username', 'password', 'dbname', 'ssl_ca', 'ssl_key', 'ssl_cert', 'driver_options', 'crypt_key'
    ];
    /**
     * @var ConnectionFactory
     */
    protected $connectionFactory;

    /**
     * CheckDatabaseConnection constructor.
     *
     * @param Action\Context    $context
     * @param Data              $helper
     * @param Session           $migrationConfig
     * @param ConnectionFactory $connectionFactory
     */
    public function __construct(
        Action\Context $context,
        Data $helper,
        Session $migrationConfig,
        ConnectionFactory $connectionFactory
    ) {
        parent::__construct($context, $helper, $migrationConfig);
        $this->connectionFactory = $connectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $params  = $this->getRequest()->getParams();
        $params  = $this->parseRequestParams($params);
        $config  = $this->getValidConfig($params);
        $message = '';
        try {
            $connection  = $this->connectionFactory->create($config);
            $isConnected = (bool)$connection->getConnection();
            $this->migrationConfig->setConfig($this->getRequest()->getParam('version'), $config);
        } catch (\Exception $e) {
            $isConnected = false;
            $message     = $e->getMessage();
        }
        return $this->createJsonResultPage(['is_connected' => $isConnected, 'message' => $message]);
    }

    public function parseRequestParams($params)
    {
        if (isset($params['host'], $params['port'])) {
            $params['host'] = $params['host'] . ':' . $params['port'];
        }
        return $params;
    }

    /**
     * @param $config
     * @return array
     */
    public function getValidConfig($config)
    {
        return array_filter(
            $config,
            function ($value, $key) {
                if (is_array($value)) {
                    return $this->getValidConfig($value);
                }
                return in_array($key, $this->_validConfig) && $value;
            },
            ARRAY_FILTER_USE_BOTH
        );
    }
}
