<?php
/**
 * ReloadLog
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml\Ajax;

use Magenest\MigrationTool\Controller\Adminhtml\AbstractAjaxRequest;

class ReloadLog extends AbstractAjaxRequest
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        try {
            $content = nl2br($this->toolHelper->getLogFile(
                $this->toolHelper->getLogFiles(
                    $this->getRequest()->getParam('type')
                )
            )->readAll());
        } catch (\Exception $e) {
            $content = '';
        }
        return $this->createJsonResultPage([
            'content' => $content
        ]);
    }
}
