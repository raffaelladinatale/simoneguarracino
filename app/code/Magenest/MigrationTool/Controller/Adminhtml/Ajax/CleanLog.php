<?php
/**
 * CleanLog
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Controller\Adminhtml\Ajax;

use Magenest\MigrationTool\Controller\Adminhtml\AbstractAjaxRequest;

class CleanLog extends AbstractAjaxRequest
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        return $this->createJsonResultPage([
            'wrote' => $this->toolHelper->cleanLog(
                $this->toolHelper->getLogFiles(
                    $this->getRequest()->getParam('type')
                )
            )
        ]);
    }
}
