<?php

namespace Magenest\MigrationTool\Controller\Adminhtml\Setup;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_MigrationTool::migration');
        $resultPage->addBreadcrumb(__('Setup'), __('Setup'));
        $resultPage->getConfig()->getTitle()->prepend(__('Setup'));
        return $resultPage;
    }
}
