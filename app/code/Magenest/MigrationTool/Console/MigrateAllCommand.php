<?php
/**
 * MigrateCommand
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Console;

use Magenest\MigrationTool\Helper\Data;
use Magenest\MigrationTool\Model\Handler\Migrate;
use Magenest\MigrationTool\Model\MigrateMode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateAllCommand extends Command
{
    protected $migrateHandler;

    protected $migrateMode;

    protected $helper;

    public function __construct(
        MigrateMode $migrateMode,
        Migrate $migrateHandler,
        Data $data,
        string $name = null
    ) {
        $this->migrateHandler = $migrateHandler;
        $this->migrateMode    = $migrateMode;
        $this->helper         = $data;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('migrate:all')
            ->setDescription('Migrate system configuration, data');
        $this->setDefinition([
            new InputOption(
                'reset',
                'r',
                InputOption::VALUE_NONE,
                'Reset the current position of migration to start from the beginning'
            ),
            new InputOption(
                'auto',
                'a',
                InputOption::VALUE_NONE,
                'Automatically resolve issues on the way of migration'
            ),
        ]);
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->helper->isMagentoMigrationToolEnabled()) {
            $output->writeln('<error>The Magento Data Migration Tool is not installed.</error>');
        } else if (!$this->migrateHandler->validate()) {
            $output->writeln('<error>The config file has not been set up.</error>');
        } else {
            $this->migrateMode->execute();
        }
    }
}
