<?php
/**
 * Migration
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model\Handler;

use Magenest\MigrationTool\Helper\ConfigFile;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Filesystem\Driver\File;

class Migrate
{
    protected $scopeConfig;

    protected $fileDriver;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        File $fileDriver
    ) {
        $this->fileDriver = $fileDriver;
        $this->scopeConfig = $scopeConfig;
    }

    public function validate()
    {
        $filePath = $this->scopeConfig->getValue(ConfigFile::XML_PATH_CONFIG_FILE);
        if (!$filePath || !$this->fileDriver->isFile($filePath) || !$this->fileDriver->isReadable($filePath)) {
            return false;
        }
        return true;
    }
}
