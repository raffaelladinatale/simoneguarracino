<?php
/**
 * Session
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model;

class Session extends \Magento\Framework\Session\SessionManager
{
    public function setConfig($key, $value)
    {
        $config = $this->getMigrationConfig();
        $config[$key] = $value;
        $this->setMigrationConfig($config);
        return $this;
    }

    public function addConfig(array $config)
    {
        if (is_array($config)) {
            $this->setMigrationConfig($this->getMigrationConfig() + $config);
        }
        return $this;
    }

    public function getConfig($key = null)
    {
        $config = $this->getMigrationConfig();
        if ($key !== null) {
            return isset($config[$key]) ? $config[$key] : null;
        }
        return $config;
    }

    public function getMigrationConfig()
    {
        if (!is_array($this->getData('migration_config'))) {
            $this->setMigrationConfig([]);
        }
        return $this->getData('migration_config');
    }

    public function setMigrationConfig($config)
    {
        return $this->setData('migration_config', $config);
    }
}
