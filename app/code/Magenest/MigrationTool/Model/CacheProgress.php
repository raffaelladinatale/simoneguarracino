<?php
/**
 * CacheProgress
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model;

use Magento\Framework\App\Cache\Manager;
use Psr\Log\LoggerInterface;

class CacheProgress
{
    protected $cacheManager;

    protected $logger;

    public function __construct(
        Manager $cacheManager,
        LoggerInterface $logger
    ) {
        $this->cacheManager = $cacheManager;
        $this->logger       = $logger;
    }

    public function execute()
    {
        foreach ($this->cacheManager->getAvailableTypes() as $type) {
            try {
                $this->cacheManager->clean([$type]);
            } catch (\Exception $e) {
                $this->logger->error(__('Clean cache type %1 error %2', $type, $e->getMessage()));
            }
        }
    }
}
