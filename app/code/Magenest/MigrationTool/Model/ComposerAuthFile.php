<?php
/**
 * ComposerAuthFile
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Directory\ReadFactory;
use Magento\Framework\Filesystem\Driver\File;

class ComposerAuthFile
{
    const AUTH_JSON_FILE = 'auth.json';
    const BACKUP_AUTH_JSON_FILE = 'auth.json.bak';
    protected $isExists;

    protected $rootDirectory;

    protected $driverFile;

    public function __construct(
        DirectoryList $directoryList,
        ReadFactory $readFactory,
        File $driverFile
    ) {
        $rootDirectory       = $directoryList->getRoot();
        $this->driverFile    = $driverFile;
        $this->rootDirectory = $readFactory->create($rootDirectory);
        $this->driverFile    = $driverFile;
        $this->isExists      = $driverFile->isExists($this->rootDirectory->getAbsolutePath(self::AUTH_JSON_FILE));
    }

    public function isExists()
    {
        return $this->isExists;
    }

    public function createFile($username, $password)
    {
        $data     = sprintf('{
    "http-basic": {
        "repo.magento.com": {
            "username": "%s",
            "password": "%s"
        }
    }
}',
            $username,
            $password
        );
        $filePath = $this->rootDirectory->getAbsolutePath(self::AUTH_JSON_FILE);
        $this->driverFile->touch($filePath);
        $this->driverFile->filePutContents($filePath, $data);
        return $this;
    }

    public function createBackup()
    {
        $this->driverFile->copy(
            $this->rootDirectory->getAbsolutePath(self::AUTH_JSON_FILE),
            $this->rootDirectory->getAbsolutePath(self::BACKUP_AUTH_JSON_FILE)
        );
        return $this;
    }

    public function rollback()
    {
        $this->driverFile->copy(
            $this->rootDirectory->getAbsolutePath(self::BACKUP_AUTH_JSON_FILE),
            $this->rootDirectory->getAbsolutePath(self::AUTH_JSON_FILE)
        );
        return $this;
    }

    public function deleteBackup()
    {
        $this->driverFile->deleteFile($this->rootDirectory->getAbsolutePath(self::BACKUP_AUTH_JSON_FILE));
        return $this;
    }

    public function deleteAuthFile()
    {
        $this->driverFile->deleteFile($this->rootDirectory->getAbsolutePath(self::AUTH_JSON_FILE));
        return $this;
    }
}
