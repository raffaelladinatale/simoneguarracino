<?php
/**
 * IndexerProcess
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model;


use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Indexer\ConfigInterface;
use Magento\Framework\Indexer\IndexerInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Indexer\StateInterface;
use Psr\Log\LoggerInterface;

class IndexerProcess
{
    protected $sharedIndexesComplete = [];

    protected $collectionFactory;

    protected $logger;

    protected $config;

    protected $indexRegistry;

    public function __construct(
        \Magento\Indexer\Model\Indexer\CollectionFactory $collectionFactory,
        LoggerInterface $logger,
        ConfigInterface $config,
        IndexerRegistry $indexerRegistry
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->logger            = $logger;
        $this->config            = $config;
        $this->indexRegistry     = $indexerRegistry;
    }

    public function execute()
    {
        $indexers = $this->getAllIndexers();
        $this->reset($indexers);
        $this->reindex($indexers);
        return $this;
    }

    /**
     * @return IndexerInterface[]
     */
    public function getAllIndexers()
    {
        return $this->collectionFactory->create()->getItems();
    }

    public function reset($indexers)
    {
        foreach ($indexers as $indexer) {
            try {
                $indexer->getState()
                    ->setStatus(\Magento\Framework\Indexer\StateInterface::STATUS_INVALID)
                    ->save();
            } catch (\Exception $e) {
                $this->logger->error(__('Reset indexer %1 error: %2', $indexer->getTitle(), $e->getMessage()));;
            }
        }
        return $this;
    }

    public function reindex($indexers)
    {
        foreach ($indexers as $indexer) {
            try {
                $this->validateIndexerStatus($indexer);
                $startTime     = microtime(true);
                $indexerConfig = $this->config->getIndexer($indexer->getId());
                $sharedIndex   = $indexerConfig['shared_index'];

                // Skip indexers having shared index that was already complete
                if (!in_array($sharedIndex, $this->sharedIndexesComplete)) {
                    $indexer->reindexAll();
                    if ($sharedIndex) {
                        $this->validateSharedIndex($sharedIndex);
                    }
                }
                $resultTime = microtime(true) - $startTime;
                $this->logger->info(
                    $indexer->getTitle() . ' index has been rebuilt successfully in ' . gmdate('H:i:s', $resultTime)
                );
            } catch (\Exception $e) {
                $this->logger->error(__('Reindex indexer %1 error: %2', $indexer->getTitle(), $e->getMessage()));;
            }
        }
        return $this;
    }

    /**
     * Validate indexers by shared index ID
     *
     * @param string $sharedIndex
     * @return $this
     * @throws \Exception
     */
    private function validateSharedIndex($sharedIndex)
    {
        if (empty($sharedIndex)) {
            throw new \InvalidArgumentException(
                'The sharedIndex is an invalid shared index identifier. Verify the identifier and try again.'
            );
        }
        $indexerIds = $this->getIndexerIdsBySharedIndex($sharedIndex);
        if (empty($indexerIds)) {
            return $this;
        }
        foreach ($indexerIds as $indexerId) {
            $indexer = $this->indexRegistry->get($indexerId);
            /** @var \Magento\Indexer\Model\Indexer\State $state */
            $state = $indexer->getState();
            $state->setStatus(StateInterface::STATUS_VALID);
            $state->save();
        }
        $this->sharedIndexesComplete[] = $sharedIndex;
        return $this;
    }

    /**
     * Get indexer ids that have common shared index
     *
     * @param string $sharedIndex
     * @return array
     */
    private function getIndexerIdsBySharedIndex($sharedIndex)
    {
        $indexers = $this->config->getIndexers();
        $result   = [];
        foreach ($indexers as $indexerConfig) {
            if ($indexerConfig['shared_index'] == $sharedIndex) {
                $result[] = $indexerConfig['indexer_id'];
            }
        }
        return $result;
    }

    /**
     * Validate that indexer is not locked
     *
     * @param IndexerInterface $indexer
     * @return void
     * @throws LocalizedException
     */
    private function validateIndexerStatus(IndexerInterface $indexer)
    {
        if ($indexer->getStatus() == StateInterface::STATUS_WORKING) {
            throw new LocalizedException(
                __(
                    '%1 index is locked by another reindex process. Skipping.',
                    $indexer->getTitle()
                )
            );
        }
    }
}
