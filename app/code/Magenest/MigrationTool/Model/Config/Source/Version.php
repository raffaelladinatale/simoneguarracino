<?php
/**
 * Version
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model\Config\Source;

use Magenest\MigrationTool\Helper\Data;
use Magento\Framework\Data\OptionSourceInterface;

class Version implements OptionSourceInterface
{
    protected $edition;

    protected $helper;


    public function __construct(
        Edition $edition,
        Data $data
    ) {
        $this->helper = $data;
        $this->edition = $edition;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        $editions = $this->edition->toArray();
        $versions = [];
        foreach ($editions as $edition => $label) {
            foreach ($this->helper->getMigrationEditionOptions($edition) as $item) {
                $versions[] = [
                    'value' => $item,
                    'label' => $item,
                    'title' => $item,
                    'edition' => $edition
                ];
            }
        }
        array_unshift($versions, ['value' => '', 'label' => __('Please Select')]);
        return $versions;
    }
}
