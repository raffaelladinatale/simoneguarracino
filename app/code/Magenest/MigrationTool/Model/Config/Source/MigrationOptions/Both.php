<?php
/**
 * MigrationOptions
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model\Config\Source\MigrationOptions;

use Magento\Framework\Data\OptionSourceInterface;

class Both implements OptionSourceInterface
{
    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 0,
                'label' => __('None')
            ],
            [
                'value' => 1,
                'label' => __('Both auto resolve and reset')
            ],
            [
                'value' => 2,
                'label' => __('Only auto resolve')
            ],
            [
                'value' => 3,
                'label' => __('Only reset')
            ]
        ];
    }
}
