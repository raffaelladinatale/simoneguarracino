<?php
/**
 * OnlyAutoResolve
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model\Config\Source\MigrationOptions;

class OnlyAutoResolve extends Both
{
    const ONLY_AUTO_RESOLVE = [2];

    public function toOptionArray()
    {
        return array_filter(parent::toOptionArray(), function ($option) {
            return $option['value'] === 0 || in_array($option['value'], self::ONLY_AUTO_RESOLVE);
        }, ARRAY_FILTER_USE_BOTH);
    }
}
