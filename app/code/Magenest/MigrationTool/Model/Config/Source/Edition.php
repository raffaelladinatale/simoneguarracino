<?php
/**
 * Edition
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model\Config\Source;

use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Edition
 *
 * @package Magenest\MigrationTool\Model\Config\Source
 */
class Edition implements OptionSourceInterface
{
    const COMMERCE_TO_COMMERCE = 'commerce-to-commerce';
    const OPENSOURCE_TO_COMMERCE = 'opensource-to-commerce';
    const OPENSOURCE_TO_OPENSOURCE = 'opensource-to-opensource';

    protected $productMetadata;

    public function __construct(
        ProductMetadataInterface $productMetadata
    ) {
        $this->productMetadata = $productMetadata;
    }

    /**
     * @return array
     */
    public function getEditionMapValue()
    {
        return [
            'Enterprise' => [self::COMMERCE_TO_COMMERCE, self::OPENSOURCE_TO_COMMERCE],
            'Community'  => [self::OPENSOURCE_TO_OPENSOURCE]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::COMMERCE_TO_COMMERCE     => 'Commerce to Commerce',
            self::OPENSOURCE_TO_COMMERCE   => 'Opensource to Commerce',
            self::OPENSOURCE_TO_OPENSOURCE => 'Opensource to Opensource',
        ];
    }

    public function toEditionMapFiles($edition = null)
    {
        $options = [
            self::COMMERCE_TO_COMMERCE     => [
                'class-map.xml',
                'customer-attr-document-groups.xml',
                'customer-attr-map.xml',
                'customer-attribute-groups.xml',
                'customer-document-groups.xml',
                'deltalog.xml',
                'eav-attribute-groups.xml',
                'eav-document-groups.xml',
                'log-document-groups.xml',
                'map-customer.xml',
                'map-document-groups.xml',
                'map-eav.xml',
                'map-log.xml',
                'map-sales.xml',
                'map-stores.xml',
                'map-tier-price.xml',
                'order-grids-document-groups.xml',
                'settings.xml',
                'visual_merchandiser_attribute_groups.xml',
                'visual_merchandiser_document_groups.xml',
                'visual_merchandiser_map.xml'
            ],
            self::OPENSOURCE_TO_COMMERCE   => [
                'class-map.xml',
                'customer-attribute-groups.xml',
                'customer-document-groups.xml',
                'deltalog.xml',
                'eav-attribute-groups.xml',
                'eav-document-groups.xml',
                'log-document-groups.xml',
                'map-customer.xml',
                'map-document-groups.xml',
                'map-eav.xml',
                'map-log.xml',
                'map-stores.xml',
                'map-tier-price.xml',
                'order-grids-document-groups.xml',
                'settings.xml'
            ],
            self::OPENSOURCE_TO_OPENSOURCE => [
                'class-map.xml',
                'customer-attribute-groups.xml',
                'customer-document-groups.xml',
                'deltalog.xml',
                'eav-attribute-groups.xml',
                'eav-document-groups.xml',
                'log-document-groups.xml',
                'map-customer.xml',
                'map-document-groups.xml',
                'map-eav.xml',
                'map-log.xml',
                'map-stores.xml',
                'map-tier-price.xml',
                'order-grids-document-groups.xml',
                'settings.xml'
            ]
        ];

        if ($edition and isset($options[$edition])) {
            return $options[$edition];
        }
        return $options;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        $options    = [];
        $editionMap = $this->getEditionMapValue();
        $edition    = $this->productMetadata->getEdition();
        foreach ($this->toArray() as $index => $option) {
            if (in_array($index, $editionMap[$edition])) {
                $options[] = [
                    'value' => $index,
                    'label' => $option
                ];
            }
        }
        array_unshift($options, ['value' => '', 'label' => __('Please Select')]);
        return $options;
    }
}
