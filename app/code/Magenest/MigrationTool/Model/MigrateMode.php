<?php
/**
 * MigrateMode
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\MigrationTool\Model;

use Magenest\MigrationTool\Helper\ConfigFile;
use Magenest\MigrationTool\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Psr\Log\LoggerInterface;

class MigrateMode
{
    protected $scopeConfig;
    protected $logger;
    protected $indexerProgress;
    protected $cacheProgress;
    protected $helper;
    protected $objectManager;


    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger,
        IndexerProcess $indexerProcess,
        CacheProgress $cacheProgress,
        Data $helper,
        ObjectManagerInterface $objectManager
    ) {
        $this->scopeConfig     = $scopeConfig;
        $this->logger          = $logger;
        $this->indexerProgress = $indexerProcess;
        $this->cacheProgress   = $cacheProgress;
        $this->helper          = $helper;
        $this->objectManager   = $objectManager;
    }

    public function execute()
    {
        try {
            $this->migrationSettings();
            $this->migrationData();
            $this->migrationDelta();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->logger->debug($e->getTraceAsString());
            return false;
        }
        $this->indexerProgress->execute();
        $this->cacheProgress->execute();
        return true;
    }

    protected function initialize($reset = null, $autoResolve = null, $logLevel = 'info')
    {
        $config = $this->scopeConfig->getValue(ConfigFile::XML_PATH_CONFIG_FILE);
        $this->getMigrationClass('Migration\Config')->init($config);
        if ($reset !== null && $reset) {
            $this->getMigrationClass('Migration\App\Progress')->reset();
        }
        if ($autoResolve !== null && $autoResolve) {
            $this->getMigrationClass('Migration\Config')->setOption('auto_resolve', 1);
        }
        $this->getMigrationClass('Migration\Logger\Manager')->process($logLevel);
    }

    public function migrationSettings()
    {
        $this->initialize(...$this->getMigrationOption('settings'));
        $this->getMigrationClass('Migration\Mode\Settings')->run();
        return $this;
    }

    public function migrationData()
    {
        $this->initialize(...$this->getMigrationOption('data'));
        $this->getMigrationClass('Migration\Mode\Data')->run();
        return $this;
    }

    public function migrationDelta()
    {
        $this->initialize(...$this->getMigrationOption('delta'));
        $this->getMigrationClass('Migration\Mode\Delta')->run();
        return $this;
    }

    public function getMigrationOption($mode)
    {
        $options     = $this->scopeConfig->getValue('migration_tool/migration_' . $mode . '/options');
        $reset       = null;
        $autoResolve = null;
        switch ($options) {
            case 1:
                $reset       = true;
                $autoResolve = true;
                break;
            case 2:
                $autoResolve = true;
                break;
            case 3:
                $reset = true;
                break;
        }
        return [$reset, $autoResolve];
    }

    /**
     * @param string $class
     * @return object
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getMigrationClass($class)
    {
        if (!class_exists($class)) {
            throw new LocalizedException(__('Class %1 does not exists.'));
        }
        return $this->objectManager->get($class);
    }
}
