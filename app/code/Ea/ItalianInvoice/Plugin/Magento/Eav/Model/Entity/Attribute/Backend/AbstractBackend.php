<?php

namespace Ea\ItalianInvoice\Plugin\Magento\Eav\Model\Entity\Attribute\Backend;

use Magento\Framework\Exception\LocalizedException;

class AbstractBackend
{

    private $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this -> logger = $logger;
    }

    /**
     * @param Magento\Eav\Model\Entity\Attribute\Backend\Increment\Interceptor $subject
     *
     * @return array
     */
    public function aroundValidate($subject, $address, $customer)
    {
        $attribute = $subject -> getAttribute();
        $attrCode = $attribute -> getAttributeCode();
        //$value = $customer->getData($attrCode);

        if ($attrCode == 'company' || $attrCode == 'taxvat') {
            return true;
        }
    }
}
