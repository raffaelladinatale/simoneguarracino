<?php

namespace Ea\ItalianInvoice\Plugin\Magento\Quote\Model;

class BillingAddressManagement
{

    /////SAVE DATA ON quote_address quando creo indirizzo da checkout form

    /**
     * @param \Magento\Quote\Model\BillingAddressManagement $subject
     * @param $cartId
     * \Magento\Quote\Api\Data\AddressInterface $address
     * @return array
     */
    public function beforeAssign(
        \Magento\Quote\Model\BillingAddressManagement $subject,
                                                      $cartId,
        \Magento\Quote\Api\Data\AddressInterface      $address
    )
    {
        $extAttributes = $address -> getExtensionAttributes();

        if (!empty($extAttributes)) {
            try {
                //$address->setFiscalCode($extAttributes->getFiscalCode());
                $address -> setData('fiscal_code', $extAttributes -> getFiscalCode());
                // $address->setFiscalSdi($extAttributes->getFiscalSdi());
                $address -> setData('fiscal_sdi', $extAttributes -> getFiscalSdi());
                //$address->setFiscalCompany($extAttributes->getFiscalCompany());
                $address -> setData('fiscal_company', $extAttributes -> getFiscalCompany());
            } catch (\Exception $e) {
            }
        }

        return [$cartId, $address];
    }
}
