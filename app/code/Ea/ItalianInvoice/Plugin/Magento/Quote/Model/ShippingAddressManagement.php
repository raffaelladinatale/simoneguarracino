<?php


namespace Ea\ItalianInvoice\Plugin\Magento\Quote\Model;

class ShippingAddressManagement
{

    /**
     * @param \Magento\Quote\Model\ShippingAddressManagement $subject
     * @param $cartId
     * \Magento\Quote\Api\Data\AddressInterface $address
     * @return array
     */
    public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
                                                       $cartId,
        \Magento\Quote\Api\Data\AddressInterface       $address
    )
    {
        $extAttributes = $address -> getExtensionAttributes();
        if (!empty($extAttributes)) {
            try {
                //$address->setFiscalCode($extAttributes->getFiscalCode());
                $address -> setData('fiscal_code', $extAttributes -> getFiscalCode());
                // $address->setFiscalSdi($extAttributes->getFiscalSdi());
                $address -> setData('fiscal_sdi', $extAttributes -> getFiscalSdi());
                //$address->setFiscalCompany($extAttributes->getFiscalCompany());
                $address -> setData('fiscal_company', $extAttributes -> getFiscalCompany());

            } catch (\Exception $e) {
            }
        }
        return [$cartId, $address];
    }
}
