<?php

namespace Ea\ItalianInvoice\Plugin\Magento\Customer\Widget;

class Taxvat
{
    public function beforeToHtml(\Magento\Customer\Block\Widget\Taxvat\Interceptor $block)
    {
        $block -> setTemplate('Ea_ItalianInvoice::widget/taxvat.phtml');
    }
}
