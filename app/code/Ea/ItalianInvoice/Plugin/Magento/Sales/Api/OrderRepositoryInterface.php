<?php

namespace Ea\ItalianInvoice\Plugin\Magento\Sales\Api;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderSearchResultInterface;

class OrderRepositoryInterface
{

    private $extensionFactory;

    public function __construct(
        OrderExtensionFactory $extensionFactory
    )
    {
        $this -> extensionFactory = $extensionFactory;
    }

    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        OrderInterface                              $order)
    {
        if ($order -> getBillingAddress()) {
            $billingAddress = $order -> getBillingAddress();
            $extensionAttributes = $order -> getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this -> extensionFactory -> create();
            $extensionAttributes -> setFiscalCode($billingAddress -> getFiscalCode());
            $extensionAttributes -> setFiscalSdi($billingAddress -> getFiscalSdi());
            $extensionAttributes -> setFiscalCompany($billingAddress -> getFiscalCompany());
            $order -> setExtensionAttributes($extensionAttributes);
        }

        return $order;
    }

    public function afterGetList(
        \Magento\Sales\Api\OrderRepositoryInterface         $subject,
        \Magento\Sales\Model\ResourceModel\Order\Collection $resultOrder
    )
    {
        /** @var  $order */
        foreach ($resultOrder -> getItems() as $order) {
            $this -> afterGet($subject, $order);
        }
        return $resultOrder;
    }
}
