<?php

namespace Ea\ItalianInvoice\Block\Address\Edit\Field;

use Magento\Framework\View\Element\Template;

/**
 * Class Note
 *
 * @package Ea\ItalianInvoice\Block\Address\Edit\Field
 */
class Note extends Template
{
    /**
     * @var string
     */
    protected $_template = 'address/edit/field/note.phtml';

    /**
     * @var \Magento\Customer\Api\Data\AddressInterface
     */
    protected $_address;

    protected $helper;

    /**
     * @return string
     */
    public function getFiscalCodeValue()
    {
        /** @var \Magento\Customer\Model\Data\Address $address */
        $address = $this -> getAddress();
        $fiscalCodeValue = $address -> getCustomAttribute('fiscal_code');

        if (!$fiscalCodeValue instanceof \Magento\Framework\Api\AttributeInterface) {
            return '';
        }

        return $fiscalCodeValue -> getValue();
    }

    /**
     * @return string
     */
    public function getCodiceSdiValue()
    {
        /** @var \Magento\Customer\Model\Data\Address $address */
        $address = $this -> getAddress();
        $codiceSdiValue = $address -> getCustomAttribute('fiscal_sdi');
        if (!$codiceSdiValue instanceof \Magento\Framework\Api\AttributeInterface) {
            return '';
        }

        return $codiceSdiValue -> getValue();
    }

    /**
     * @return mixed|string
     */
    public function getFiscalCompanyValue()
    {
        /** @var \Magento\Customer\Model\Data\Address $address */
        $address = $this -> getAddress();
        $FiscalCompany = $address -> getCustomAttribute('fiscal_company');

        if (!$FiscalCompany instanceof \Magento\Framework\Api\AttributeInterface) {
            return '';
        }

        return $FiscalCompany -> getValue();
    }

    /**
     * Return the associated address.
     *
     * @return \Magento\Customer\Api\Data\AddressInterface
     */
    public function getAddress()
    {
        return $this -> _address;
    }

    /**
     * Set the associated address.
     *
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     */
    public function setAddress($address)
    {
        $this -> _address = $address;
    }

    /**
     * Return the helper class.
     *
     */
    public function getHelper()
    {
        return $this -> helper;
    }

    /**
     * Set the helper class.
     *
     */
    public function setHelper($helper)
    {
        $this -> helper = $helper;
    }
}
