var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-billing-address': {
                'Ea_ItalianInvoice/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Ea_ItalianInvoice/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/action/create-shipping-address': {
                'Ea_ItalianInvoice/js/action/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/action/place-order': {
                'Ea_ItalianInvoice/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/create-billing-address': {
                'Ea_ItalianInvoice/js/action/set-billing-address-mixin': true
            }
        }
    },
    map: {
        '*': {
            "Magento_Checkout/template/shipping-address/address-renderer/default.html": "Ea_ItalianInvoice/template/shipping-address/address-renderer/default.html",
            "Magento_Checkout/template/shipping-information/address-renderer/default.html": "Ea_ItalianInvoice/template/shipping-information/address-renderer/default.html",
            "Magento_Checkout/template/billing-address/details.html": "Ea_ItalianInvoice/template/billing-address/details.html",
            editAddressValidation: "Ea_ItalianInvoice/js/validation/customer-address-validation",
            fiscalCompany: "Ea_ItalianInvoice/js/fiscal-company"
        }
    }
};
