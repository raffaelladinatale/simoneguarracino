define([
    'Magento_Ui/js/form/element/select',
    'mage/translate',
    'jquery',
    'ko',
    'mageUtils',
    'rjsResolver',
    'Magento_Ui/js/lib/validation/validator'
], function (AbstractField, $t, $, ko, utils, resolver, validator) {
    'use strict';
    validator.addRule(
        'validate-fiscal_code',
        function (value, params, additionalParams) {
            if (jQuery('div[name="' + params + '"]:visible').length) {
                return !utils.isEmpty(value);
            }
            return true;
        },
        $.mage.__("Fiscal Code is not valid.")
    );
    validator.addRule(
        'validate-fiscal_sdi',
        function (value, params, additionalParams) {
            if (jQuery('div[name="' + params + '"]:visible').length) {
                return !utils.isEmpty(value);
            }
            return true;
        },
        $.mage.__("This is a required field.")
    );
   /* validator.addRule(
        'validate-company',
        function (value, params, additionalParams) {
            if (jQuery('div[name="' + params + '"]:visible').length) {
                return !utils.isEmpty(value);
            }
            return true;
        },
        $.mage.__("This is a required field.")
    );*/
    validator.addRule(
        'validate-vat_id',
        function (value, params, additionalParams) {
            if (jQuery('div[name="' + params + '"]:visible').length) {
                return !utils.isEmpty(value);
            }
            return true;
        },
        $.mage.__("This is a required field.")
    );
    validator.addRule(
        'validate-is_a_company',
        function (value, params, additionalParams) {
            if (jQuery('div[name="' + params + '"]:visible').length) {
                return !utils.isEmpty(value);
            }
            return true;
        },
        $.mage.__("This is a required field.")
    );


    return AbstractField.extend({
        defaults: {
            modules: {
                country: '${ $.parentName }.country_id',
                fiscalCompany: '${ $.parentName }.fiscal_company',
                fiscalCode: '${ $.parentName }.fiscal_code',
                fiscalSdi: '${ $.parentName }.fiscal_sdi',
                vatId: '${ $.parentName }.vat_id',
                company: '${ $.parentName }.company',
            },
            imports: {
                updateFiscalForm: '${ $.parentName }.fiscal_company:value',
                updateCountryId: '${ $.parentName }.country_id:value',
            }
        },
        initialize: function () {
            var self = this;
            //initialize parent Component
            this._super();
            resolver(this.hideLoader, this);
            return this;
        },
        hideLoader: function (value) {
            var fiscalCompany = this.fiscalCompany();
            //IF FISCAL COMPANY DEFAULT VALUE == 1
            if (typeof fiscalCompany !== 'undefined' && fiscalCompany.fiscalCompanyDefautlValue == 1) {
                this.fiscalCompany().set('value', 2);
            }
            this.updateCountryId(value);
        },
        getFiscalCode: function () {
            var self = this;
            var fiscalCompany = this.fiscalCompany();
            var fiscalCode = this.fiscalCode();
            fiscalCode.show().required(true).reset();
        },
        updateCountryId: function (value) {
            var self = this;
            var countryValue = this.country().value();
            var fiscalCompany = this.fiscalCompany();
            var fiscalCode = this.fiscalCode();
            var fiscalSdi = this.fiscalSdi();
            var vatId = this.vatId();
            var company = this.company();
            if (typeof this.fiscal_company !== 'undefined') {
                var fiscalCompanyValue = this.fiscal_company().value();
            }

            if (typeof fiscalCompany !== 'undefined') {
                // DEFAULT SHOP VERSION
                if (fiscalCompany.shopVersion == 'default') {
                    /*if (!countryValue) {
                        fiscalCode.hide();
                        fiscalSdi.hide();
                        fiscalCompany.hide();
                        if (fiscalCompanyValue === "1") {
                            company.show();
                            vatId.show();
                        } else if (fiscalCompanyValue === "2") {
                            company.hide();
                            vatId.hide();
                        }
                    }*/
                    if (countryValue != 'IT') {
                        fiscalSdi.hide();
                    }
                    if (countryValue) {
                        if (typeof fiscalCompany !== 'undefined') {
                            fiscalCompany.show();
                            fiscalCode.hide();
                            fiscalCompany.set('value', 2);
                            if (countryValue === 'IT') {
                                fiscalSdi.hide();
                            }
                            //IS BUSINESS
                            if (fiscalCompany.value() === "1") {
                                //CONFIG FISCAL CODE ALSO FOR BUSINESS
                                this.getFiscalCode();
                                fiscalCompany.show();
                                if (countryValue === 'IT') {
                                    company.show().required(true);
                                    vatId.show().required(true);
                                    fiscalSdi.show().required(true);
                                    fiscalCode.show().required(true);
                                }
                            }
                            //NOT BUSINESS
                            else if (fiscalCompany.value() === "2") {
                                /*if (fiscalCompany.fiscalCodeIsRequired === 1) {
                                    fiscalCode.show().required(true);
                                } else {
                                    fiscalCode.show().required(false);
                                }*/

                                if (countryValue === 'IT') {
                                    fiscalCode.show().required(true);
                                }
                                if (countryValue === 'IT') {
                                    fiscalSdi.hide();
                                }
                                company.hide();
                                vatId.hide();
                            }
                        }
                    }
                }
                // B2B SHOP VERSION
                else if (fiscalCompany.shopVersion == 'b2b') {
                   /* if (!countryValue) {
                        fiscalCompany.hide();
                        fiscalSdi.hide();
                        this.getFiscalCode();
                    }*/
                    if (countryValue) {
                        fiscalCompany.hide();
                        if (countryValue === 'IT') {
                            fiscalCode.hide();
                        }
                        // company.show();
                        // vatId.show();
                        if (countryValue === 'IT') {
                            fiscalSdi.show().required(true);
                        }
                    }
                }
                // B2C SHOP VERSION
                else if (fiscalCompany.shopVersion == 'b2C') {
                   /* if (!countryValue) {
                        fiscalCompany.hide();
                        fiscalCode.hide();
                        fiscalSdi.hide();
                        //   company.hide();
                        //  vatId.hide();
                    }*/
                    if (countryValue) {
                        fiscalCompany.show();
                        fiscalCompany.set('value', 2);
                        if (countryValue === 'IT' ) {
                            fiscalSdi.hide();
                        }
                        if (countryValue === 'IT') {
                            fiscalCode.show().required(true);
                        }
                        //  company.hide();
                        //  vatId.hide();
                    }
                }
            }
        },

        updateFiscalForm: function (value) {
            var self = this;
            if (typeof this.country() !== 'undefined') {
                var countryValue = this.country().value();
            }
            var fiscalCompany = this.fiscalCompany();
            var fiscalCompanyValue = value;
            var fiscalCode = this.fiscalCode();
            var fiscalSdi = this.fiscalSdi();
            var vatId = this.vatId();
            var company = this.company();
            if (typeof fiscalCompany !== 'undefined' && fiscalCompany.shopVersion == 'default') {
                if (fiscalCompanyValue === "1") {

                    if (countryValue === 'IT') {
                        fiscalSdi.show().required(true);
                    }
                    if (countryValue === 'IT') {
                        this.getFiscalCode();
                    }
                    company.show().required(true);
                    vatId.show().required(true);
                } else if (fiscalCompanyValue === "2") {
                    /*if (fiscalCompany.fiscalCodeIsRequired === 1) {
                        fiscalCode.show().required(true);
                    } else {
                        fiscalCode.show().required(false);
                    }*/
                    if (countryValue === 'IT') {
                        fiscalSdi.hide();
                        fiscalSdi.reset();
                    }
                    if (countryValue === 'IT') {
                        fiscalCode.show().required(true);
                    }
                    vatId.hide().reset();
                    company.hide().reset();
                }
            }
        }
    });

});
