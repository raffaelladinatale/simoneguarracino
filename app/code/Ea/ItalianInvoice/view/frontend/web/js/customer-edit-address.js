define([
    'Magento_Ui/js/form/element/select',
    'mage/translate',
    'jquery',
    'ko',
    'Magento_Ui/js/lib/validation/utils'
], function (AbstractField, $t, $, ko, utils) {
    'use strict';
    self = this;
    return AbstractField.extend({
        initialize: function () {
            //initialize parent Component
            self = this;
            var fiscalCompany = $('select#fiscal_company.select');
            var fiscalCode = $('#fiscal_code');
            var fiscalSdi = $('#fiscal_sdi');
            var fiscalCompanyLabel = $('.field.fiscal_company');
            var fiscalCodeLabel = $('.field.fiscal_code');
            var fiscalSdiLabel = $('.field.fiscal_sdi');
            var vatId = $('#vat_id');
            var vatIdLabel = $('.field.taxvat');
            var company = $('#company');
            var companyLabel = $('.field.company');
            var country = $('#country');
            var isCompanyRequired = false;
            var isVatIdRequired = false;
            var pattern = /^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$/;
            if (company.hasClass('required-entry')) {
                isCompanyRequired = false
            }
            if (vatId.hasClass('required-entry')) {
                isVatIdRequired = false
            }

            //Hide Is a company select
            var hideFiscalCompany = function () {
                fiscalCompany.hide();
                fiscalCompanyLabel.hide();
            };

            var showFiscalCompany = function () {
                fiscalCompany.show();
                fiscalCompanyLabel.show();
            };


            var hideFiscalCode = function () {
                if (fiscalCode.hasClass('fiscal-code-business')) {
                    fiscalCode.show();
                    fiscalCodeLabel.show();
                } else {
                    fiscalCode.hide();
                    fiscalCodeLabel.hide();
                }
            };

            var showFiscalCode = function () {
                fiscalCode.show();
                fiscalCodeLabel.show();
                /*if (fiscalCompany.attr('data-fiscal-code-required') === 'true') {
                    fiscalCode.addClass('required-entry');
                    fiscalCodeLabel.addClass('required');
                }*/
            };

            var hideSdiCode = function () {
                fiscalSdi.hide();
                fiscalSdiLabel.hide();
                fiscalSdi.removeClass('required-entry');
                fiscalSdiLabel.removeClass('required');
            };

            var showSdiCode = function () {
                fiscalSdi.show();
                fiscalSdiLabel.show();
                fiscalSdi.removeClass('required-entry');
                fiscalSdiLabel.removeClass('required');
            };

            var hideVatId = function () {
                vatId.hide();
                vatIdLabel.hide();
                if (isVatIdRequired = true) {
                    vatId.removeClass('required-entry');
                    vatIdLabel.removeClass('required');
                }

            };
            var showVatId = function () {
                vatId.show();
                vatIdLabel.show();
                if (isVatIdRequired = true) {
                    vatId.addClass('required-entry');
                    vatIdLabel.addClass('required');
                }
            };

            var hideCompany = function () {
                company.hide();
                companyLabel.hide();
                company.removeClass('required-entry');
                companyLabel.removeClass('required');

            };

            var showCompany = function () {
                company.show();
                companyLabel.show();
                company.removeClass('required-entry');
                companyLabel.removeClass('required');
            };

            var notItaly = function () {
                showFiscalCompany();
                if (fiscalCompany.val() === "1") {
                    hideFiscalCode();
                    hideSdiCode();
                    showCompany();
                    showVatId();
                } else if (fiscalCompany.val() === "2") {
                    hideFiscalCode();
                    hideSdiCode();
                    hideCompany();
                    hideVatId();
                }
            };
            var isItaly = function () {
                showFiscalCompany();
                if (fiscalCompany.val() === "1") {
                    showSdiCode();
                    showCompany();
                    showVatId();
                } else if (fiscalCompany.val() === "2") {
                    showFiscalCode();
                    hideSdiCode();
                    hideCompany();
                    hideVatId();
                }
            };

            if (country.val() !== 'IT') {
                notItaly();
            } else {
                isItaly();
            }

            fiscalCompany.live('change', function () {
                if (country.val() !== 'IT') {
                    notItaly();
                } else {
                    isItaly();
                }
            });

            country.live('change', function () {
                if ($(this).val() !== "IT") {
                    notItaly();
                } else {
                    isItaly();
                }
            });

            this._super();
        },
        onSubmit: function () {
            alert('Submitted form');
            // trigger form validation
            this.source.set('params.invalid', false);
            this.source.trigger('customerTypeForm.data.validate');

            // verify that form data is valid
            if (!this.source.get('params.invalid')) {
                // data is retrieved from data provider by value of the customScope property
                var formData = this.source.get('customerTypeForm');
                // do something with form data
                console.dir(formData);
            }
        }
    });
});
