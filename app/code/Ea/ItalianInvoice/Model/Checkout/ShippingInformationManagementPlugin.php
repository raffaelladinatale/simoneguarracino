<?php

namespace Ea\ItalianInvoice\Model\Checkout;

class ShippingInformationManagementPlugin
{

    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    )
    {
        $this -> quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement   $subject,
                                                                $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
        //SHIPPING
        $shipping_extAttributes = $addressInformation -> getShippingAddress() -> getExtensionAttributes();

        $fiscalCode = str_replace("fiscal_code", "", $shipping_extAttributes -> getFiscalCode());
        $fiscalSdi = str_replace("fiscal_sdi", "", $shipping_extAttributes -> getFiscalSdi());
        $fiscalCompany =  $shipping_extAttributes -> getFiscalCompany();
        $quote = $this -> quoteRepository -> getActive($cartId);

        $addressInformation -> getShippingAddress() -> setData('fiscal_code', $fiscalCode);
        $addressInformation -> getShippingAddress() -> setData('fiscal_sdi', $fiscalSdi);
        $addressInformation -> getShippingAddress() -> setData('fiscal_company', $fiscalCompany);

        if ($addressInformation -> getBillingAddress() -> getData('fiscal_company') != '') {
            $addressInformation -> getBillingAddress() -> setData('fiscal_code', $fiscalCode);
            $addressInformation -> getBillingAddress() -> setData('fiscal_sdi', $fiscalSdi);
            $addressInformation -> getBillingAddress() -> setData('fiscal_company', $fiscalCompany);
        }
    }
}
