<?php

namespace Ea\ItalianInvoice\Model\Config\Source;

class Fiscalcompany extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this -> _options === null) {
            $this -> _options = [
                ['value' => '1', 'label' => __('Yes')],
                ['value' => '2', 'label' => __('No')]
            ];
        }
        return $this -> _options;
    }

    /**
     * @param $value
     * @return false|mixed
     */
    public function getOptionValue($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }

    public function getOptionText($value)
    {
        $options = $this->getAllOptions();
        // Fixed for tax_class_id and custom_design
        if (count($options) > 0) {
            foreach ($options as $option) {
                if (isset($option['value']) && $option['value'] == $value) {
                    return $option['label'];
                }
            }
        }
        return false;
    }
}
