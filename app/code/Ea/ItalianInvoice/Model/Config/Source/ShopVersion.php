<?php
namespace Ea\ItalianInvoice\Model\Config\Source;

class ShopVersion implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'default', 'label' => __('Default version')],
            ['value' => 'b2b', 'label' => __('B2B version')],
            ['value' => 'b2c', 'label' => __('B2C version')]
        ];
    }
}

?>
