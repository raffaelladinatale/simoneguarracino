<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Ea\ItalianInvoice\Model\Quote;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;


class Address extends \Magento\Quote\Model\Quote\Address
{
    const RATES_FETCH = 1;

    const RATES_RECALCULATE = 2;

    const ADDRESS_TYPE_BILLING = 'billing';

    const ADDRESS_TYPE_SHIPPING = 'shipping';

    /**
     * Prefix of model events
     *
     * @var string
     */
    protected $_eventPrefix = 'sales_quote_address';

    /**
     * Name of event object
     *
     * @var string
     */
    protected $_eventObject = 'quote_address';

    /**
     * Quote object
     *
     * @var \Magento\Quote\Model\Quote
     */
    protected $_items;

    /**
     * Quote object
     *
     * @var \Magento\Quote\Model\Quote
     */
    protected $_quote;

    /**
     * Sales Quote address rates
     *
     * @var \Magento\Quote\Model\Quote\Address\Rate
     */
    protected $_rates;

    /**
     * Total models collector
     *
     * @var \Magento\Quote\Model\Quote\Address\Total\Collector
     */
    protected $_totalCollector;

    /**
     * Total data as array
     *
     * @var array
     */
    protected $_totals = [];

    /**
     * @var array
     */
    protected $_totalAmounts = [];

    /**
     * @var array
     */
    protected $_baseTotalAmounts = [];

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Quote\Model\Quote\Address\ItemFactory
     */
    protected $_addressItemFactory;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\Address\Item\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory
     */
    protected $_rateCollector;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory
     */
    protected $_rateCollectionFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\Total\CollectorFactory
     */
    protected $_totalCollectorFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\TotalFactory
     */
    protected $_addressTotalFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateFactory
     * @since 100.2.0
     */
    protected $_addressRateFactory;

    /**
     * @var \Magento\Customer\Api\Data\AddressInterfaceFactory
     */
    protected $addressDataFactory;

    /**
     * @var Address\Validator
     */
    protected $validator;

    /**
     * @var \Magento\Customer\Model\Address\Mapper
     */
    protected $addressMapper;

    /**
     * @var Address\RateRequestFactory
     */
    protected $_rateRequestFactory;

    /**
     * @var Address\CustomAttributeListInterface
     */
    protected $attributeList;

    /**
     * @var TotalsCollector
     */
    protected $totalsCollector;

    /**
     * @var \Magento\Quote\Model\Quote\TotalsReader
     */
    protected $totalsReader;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        \Magento\Framework\Model\Context                                        $context,
        \Magento\Framework\Registry                                             $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory                       $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory                            $customAttributeFactory,
        \Magento\Directory\Helper\Data                                          $directoryData,
        \Magento\Eav\Model\Config                                               $eavConfig,
        \Magento\Customer\Model\Address\Config                                  $addressConfig,
        \Magento\Directory\Model\RegionFactory                                  $regionFactory,
        \Magento\Directory\Model\CountryFactory                                 $countryFactory,
        AddressMetadataInterface                                                $metadataService,
        AddressInterfaceFactory                                                 $addressDataFactory,
        RegionInterfaceFactory                                                  $regionDataFactory,
        \Magento\Framework\Api\DataObjectHelper                                 $dataObjectHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface                      $scopeConfig,
        \Magento\Quote\Model\Quote\Address\ItemFactory                          $addressItemFactory,
        \Magento\Quote\Model\ResourceModel\Quote\Address\Item\CollectionFactory $itemCollectionFactory,
        \Magento\Quote\Model\Quote\Address\RateFactory                          $addressRateFactory,
        \Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory        $rateCollector,
        \Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory $rateCollectionFactory,
        \Magento\Quote\Model\Quote\Address\RateRequestFactory                   $rateRequestFactory,
        \Magento\Quote\Model\Quote\Address\Total\CollectorFactory               $totalCollectorFactory,
        \Magento\Quote\Model\Quote\Address\TotalFactory                         $addressTotalFactory,
        \Magento\Framework\DataObject\Copy                                      $objectCopyService,
        \Magento\Shipping\Model\CarrierFactoryInterface                         $carrierFactory,
        \Magento\Quote\Model\Quote\Address\Validator                            $validator,
        \Magento\Customer\Model\Address\Mapper                                  $addressMapper,
        \Magento\Quote\Model\Quote\Address\CustomAttributeListInterface         $attributeList,
        \Magento\Quote\Model\Quote\TotalsCollector                              $totalsCollector,
        \Magento\Quote\Model\Quote\TotalsReader                                 $totalsReader,
        \Magento\Framework\Model\ResourceModel\AbstractResource                 $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb                           $resourceCollection = null,
        array                                                                   $data = [],
        Json                                                                    $serializer = null,
        StoreManagerInterface                                                   $storeManager = null
    )
    {
        $this -> serializer = $serializer ?: ObjectManager ::getInstance() -> get(Json::class);
        $this -> storeManager = $storeManager ?: ObjectManager ::getInstance() -> get(StoreManagerInterface::class);
        parent ::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $directoryData,
            $eavConfig,
            $addressConfig,
            $regionFactory,
            $countryFactory,
            $metadataService,
            $addressDataFactory,
            $regionDataFactory,
            $dataObjectHelper,
            $scopeConfig,
            $addressItemFactory,
            $itemCollectionFactory,
            $addressRateFactory,
            $rateCollector,
            $rateCollectionFactory,
            $rateRequestFactory,
            $totalCollectorFactory,
            $addressTotalFactory,
            $objectCopyService,
            $carrierFactory,
            $validator,
            $addressMapper,
            $attributeList,
            $totalsCollector,
            $totalsReader,
            $resource,
            $resourceCollection,
            $data,
            $this -> serializer,
            $this -> storeManager
        );
    }

    /**
     * Validate address attribute values
     *
     *
     *
     * @return bool|array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function validate()
    {
        $errors = [];
        if (!\Zend_Validate ::is($this -> getFirstname(), 'NotEmpty')) {
            $errors[] = __('%fieldName is a required field.', ['fieldName' => 'firstname']);
        }

        if (!\Zend_Validate ::is($this -> getLastname(), 'NotEmpty')) {
            $errors[] = __('%fieldName is a required field.', ['fieldName' => 'lastname']);
        }

        if (!\Zend_Validate ::is($this -> getStreetLine(1), 'NotEmpty')) {
            $errors[] = __('%fieldName is a required field.', ['fieldName' => 'street']);
        }

        if (!\Zend_Validate ::is($this -> getCity(), 'NotEmpty')) {
            $errors[] = __('%fieldName is a required field.', ['fieldName' => 'city']);
        }

        if ($this -> isTelephoneRequired()) {
            if (!\Zend_Validate ::is($this -> getTelephone(), 'NotEmpty')) {
                $errors[] = __('%fieldName is a required field.', ['fieldName' => 'telephone']);
            }
        }

        if ($this -> isFaxRequired()) {
            if (!\Zend_Validate ::is($this -> getFax(), 'NotEmpty')) {
                $errors[] = __('%fieldName is a required field.', ['fieldName' => 'fax']);
            }
        }

/*        if ($this->isCompanyRequired()) {
         if (!\Zend_Validate::is($this->getCompany(), 'NotEmpty')) {
               $errors[] = __('%fieldName is a required field.', ['fieldName' => 'company']);
           }
        }*/

        $_havingOptionalZip = $this -> _directoryData -> getCountriesWithOptionalZip();
        if (!in_array(
                $this -> getCountryId(),
                $_havingOptionalZip
            ) && !\Zend_Validate ::is(
                $this -> getPostcode(),
                'NotEmpty'
            )
        ) {
            $errors[] = __('%fieldName is a required field.', ['fieldName' => 'postcode']);
        }

        if (!\Zend_Validate ::is($this -> getCountryId(), 'NotEmpty')) {
            $errors[] = __('%fieldName is a required field.', ['fieldName' => 'countryId']);
        }

        if ($this -> getCountryModel() -> getRegionCollection() -> getSize() && !\Zend_Validate ::is(
                $this -> getRegionId(),
                'NotEmpty'
            ) && $this -> _directoryData -> isRegionRequired(
                $this -> getCountryId()
            )
        ) {
            $errors[] = __('%fieldName is a required field.', ['fieldName' => 'regionId']);
        }

        if (empty($errors) || $this -> getShouldIgnoreValidation()) {
            return true;
        }
        return $errors;
    }

    /**
     * @return bool
     * @since 100.2.0
     */
    protected function isCompanyRequired()
    {
       // return ($this -> _eavConfig -> getAttribute('customer_address', 'company') -> getIsRequired());
        return false;
    }

    /**
     * @return bool
     * @since 100.2.0
     */
    protected function isTelephoneRequired()
    {
        return ($this -> _eavConfig -> getAttribute('customer_address', 'telephone') -> getIsRequired());
    }

    /**
     * @return bool
     * @since 100.2.0
     */
    protected function isFaxRequired()
    {
        return ($this -> _eavConfig -> getAttribute('customer_address', 'fax') -> getIsRequired());
    }

}
