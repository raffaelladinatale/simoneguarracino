<?php

namespace Ea\ItalianInvoice\Model\ResourceModel\Order\Customer;

class Collection extends \Magento\Sales\Model\ResourceModel\Order\Customer\Collection
{
    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent ::_initSelect();
        $this -> addAttributeToSelect(
            'fiscal_code'
        );
        $this -> addAttributeToSelect(
            'fiscal_sdi'
        );
        $this -> addAttributeToSelect(
            'fiscal_company'
        );
        return $this;
    }
}
