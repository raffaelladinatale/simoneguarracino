<?php

namespace Ea\ItalianInvoice\Model\Address;

use Magento\Framework\Api\AbstractSimpleObject;

// @todo: Create an interface for this class

/**
 * Class ExtensionAttributes
 *
 * @package Ea\ItalianInvoice\Model\Address
 */
class AdditionalAttributes extends AbstractSimpleObject implements \Magento\Customer\Api\Data\AddressExtensionInterface
{
    /**
     * @param string $note
     * @return void
     */
    public function setFiscalCode($note)
    {
        $this -> setData('fiscal_code', $note);
    }

    /**
     * @return mixed|null
     */
    public function getFiscalCode()
    {
        return $this -> _get('fiscal_code');
    }


    /**
     * @param string $note
     * @return void
     */
    public function setFiscalSdi($fiscalSdi)
    {
        $this -> setData('fiscal_sdi', $fiscalSdi);
    }

    /**
     * @return mixed|null
     */
    public function getFiscalSdi()
    {
        return $this -> _get('fiscal_sdi');
    }


    /**
     * @param $fiscalCompany
     * @return AdditionalAttributes|void
     */
    public function setFiscalCompany($fiscalCompany)
    {
        $this -> setData('fiscal_company', $fiscalCompany);
    }

    /**
     * @return mixed|null
     */
    public function getFiscalCompany()
    {
        return $this -> _get('fiscal_company');
    }
}
