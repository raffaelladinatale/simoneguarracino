<?php

namespace Ea\ItalianInvoice\Observer;

use \Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AddCustomAttrToQuote implements ObserverInterface
{

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var \Magento\Sales\Model\Order $order */
        $order = $observer -> getEvent() -> getData('order');
        /* @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer -> getEvent() -> getData('quote');

        if ($order -> getBillingAddress() -> getFiscalCode() != '') {
            $quote -> getBillingAddress() -> setFiscalCode(str_replace("fiscal_code", "", $order -> getBillingAddress() -> getFiscalCode()));
            $quote -> getShippingAddress() -> setFiscalCode(str_replace("fiscal_code", "", $order -> getBillingAddress() -> getFiscalCode()));
        }
        if ($order -> getBillingAddress() -> getFiscalSdi() != '') {
            $quote -> getBillingAddress() ->  setFiscalSdi(str_replace("fiscal_sdi", "", $order -> getBillingAddress() -> getFiscalSdi()));
            $quote -> getShippingAddress() -> setFiscalSdi(str_replace("fiscal_sdi", "", $order -> getBillingAddress() -> getFiscalSdi()));
        }
        if ($order -> getBillingAddress() -> getFiscalCompany() != '') {
            $quote -> getBillingAddress() -> setFiscalCompany(str_replace("fiscal_company", "", $order -> getBillingAddress() -> getFiscalCompany()));
            $quote -> getShippingAddress() -> setFiscalCompany(str_replace("fiscal_company", "", $order -> getBillingAddress() -> getFiscalCompany()));
        }

        $quote -> getBillingAddress() -> save();
        $quote -> getShippingAddress() -> save();

        return $this;
    }
}
