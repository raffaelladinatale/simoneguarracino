<?php

namespace Ea\ItalianInvoice\Observer;

use \Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;


use \Magento\Checkout\Model\Session as CheckoutSession;

class CustomerAttributeSave implements ObserverInterface
{

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $helper;

    protected $logger;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;

    protected $addressFactory;


    public function __construct(
        \Psr\Log\LoggerInterface                         $logger,
        \Ea\ItalianInvoice\Helper\Data                   $helper,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepositoryInterface,
        \Magento\Customer\Model\AddressFactory           $addressFactory
    )
    {
        $this -> logger = $logger;
        $this -> helper = $helper;
        $this -> addressRepository = $addressRepositoryInterface;
        $this -> addressFactory = $addressFactory;
    }


    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $order = $observer -> getOrder();
        $address = $order -> getShippingAddress();

        if ($address) {
            $customerAddressId = $address -> getCustomerAddressId();
        } else {
            $address = $order -> getBillingAddress();
            $customerAddressId = $address -> getCustomerAddressId();
        }

        $fiscalCode =  str_replace("fiscal_code", "",$address -> getFiscalCode());
        $fiscalSdi = str_replace("fiscal_sdi", "",$address -> getFiscalSdi());
        $fiscalCompany =  $address -> getFiscalCompany();

        if ($customerAddressId) {

            $addressObject = $this -> addressFactory -> create() -> load($customerAddressId);

            $addressObject -> setFiscalCode($fiscalCode);
            $addressObject -> setFiscalSdi($fiscalSdi);
            $addressObject -> setFiscalCompany($fiscalCompany);
            $addressObject -> save();

        }

        $billingAddress = $order -> getBillingAddress();

        if ($billingAddress) {
            $billAddCusId = $billingAddress -> getCustomerAddressId();
        } else {
            $billAddCusId = '';
        }

        if (isset($billAddCusId) && $billAddCusId != null) {

            if ($billAddCusId != $customerAddressId) {

                $billFicalCode = str_replace("fiscal_code", "",$billingAddress -> getFiscalCode());
                $billFiscalSdi = str_replace("fiscal_sdi", "",$billingAddress -> getFiscalSdi());
                $billFiscalCompany = $billingAddress -> getFiscalCompany();

                if ($billAddCusId) {

                    $BillAddressObject = $this -> addressFactory -> create() -> load($billAddCusId);

                    $BillAddressObject -> setFiscalCode($billFicalCode);
                    $BillAddressObject -> setFiscalSdi($billFiscalSdi);
                    $BillAddressObject -> setFiscalCompany($billFiscalCompany);
                    $BillAddressObject -> save();

                }
            }
        }

    }
}
