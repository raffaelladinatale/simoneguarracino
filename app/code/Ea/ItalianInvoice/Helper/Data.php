<?php

namespace Ea\ItalianInvoice\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    protected $fieldsetConfig;

    protected $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\DataObject\Copy\Config          $fieldsetConfig,
        \Psr\Log\LoggerInterface                           $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface         $storeManager
    )
    {
        $this -> fieldsetConfig = $fieldsetConfig;
        $this -> logger = $logger;
        $this -> scopeConfig = $scopeConfig;
        $this -> _storeManager = $storeManager;
    }

    public function getExtraCheckoutAddressFields($fieldset = 'extra_checkout_billing_address_fields', $root = 'global')
    {

        $fields = $this -> fieldsetConfig -> getFieldset($fieldset, $root);

        $extraCheckoutFields = [];

        foreach ($fields as $field => $fieldInfo) {
            $extraCheckoutFields[] = $field;
        }

        return $extraCheckoutFields;

    }

    public function transportFieldsFromExtensionAttributesToObject(
        $fromObject,
        $toObject,
        $fieldset = 'extra_checkout_billing_address_fields'
    )
    {

        foreach ($this -> getExtraCheckoutAddressFields($fieldset) as $extraField) {

            $set = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $extraField)));
            $get = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $extraField)));
            $value = $fromObject -> $get();
            try {
                $toObject -> $set(str_replace($extraField, '', $value));
            } catch (\Exception $e) {
                $this -> logger -> critical('TRANSPORT FIELD ERROR' . $e -> getMessage());
            }
        }

        return $toObject;
    }

    public function getConfig($config_path)
    {
        $storeCode = $this -> _storeManager -> getStore() -> getCode();
        return $this -> scopeConfig -> getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeCode
        );
    }
}
