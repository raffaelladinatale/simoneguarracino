# CHANGELOG.md

## 2.7.0

Features:

    - Expose fiscal_code, fiscal_sdi, fiscal_company to Magento SOAP and REST API also for Order List. 

## 2.6.1

Bug fix:

    - Fix Fiscal Company undefined for virtual and downloadable product

## 2.6.0

Features:

    - Add ability to show Fiscal Code (Not Required) also for Business Address. 

## 2.5.1

Bug fix:

    - Call to a member function getFiscalCode() on null in OrderRepositoryInterface.php:31 for Virtual products orders

## 2.5.0

Features:

    - Expose fiscal_code, fiscal_sdi, fiscal_company to Magento SOAP and REST API. 

Bug fix:

    - Add Fiscal Code, SDI Code and "Is Business Address" when edit, create, reorder from Beck End

## 2.4.4

Bug fix:

    - Order saving error: SQLSTATE[42000]
    - Add Select Field fir Is a Company on BE Order Save and Edit Form

## 2.4.3

Bug fix:

    - Save Shipping Address on Customer Address on Checkout

## 2.4.2

Bug fix:

    - Cannot read property - 'fiscalCompanyVersion' of undefined

## 2.4.1

Bug fix:

    - Cannot read property 'shopVersion' of undefined

## 2.4.0

Features:

    - B2B and B2C versions
    - Set Are you a Company? to No as default value

## 2.3.0

Features:

    - Enable Company Yes/No select for all countries

## 2.2.1

Bug fix:

    - Fix select on prefix table

## 2.2.0

Features:

    - Remove Taxvat from Cuatomer edit/create form

## 2.1.0

Features:

    - Set FiscalCode as not required from BO

## 2.0.6

Bug fix:

    - Fix add and remove required class to vat_id and company

## 2.0.5

Bug fix:

    - Save Billing data on FE and in Paypal Checkout Orders

## 2.0.4

Bug fix:

    - Save Billing data on BO

## 2.0.3

Features:

    - Change custom fields position in checkout address
    - Fix sidebar address review print fieds codes

## 2.0.2

Bug fix:

    - Fix admin create order error bug: call to a member function getBeckend() on bolean

## 2.0.1

Bug fix:

    - Fix minor bug

## 2.0.0

Features:

    - Fiscal Code Syntax Validation
    - Save fiscal_company data on database and get fiscal_company data in customer address edit form for better UX

Bug fix:

    - Removed custom attributes codes in billing address detail template

Major Release:

    - Change Module Name
    - Change fisc_code with fiscal_code
    - Change is_a_company with fiscal_company
    - Replace inline jquery code with KO components

## 1.1.5

Bug fix:

    - Fixed company field issue "If set as required from admin and Is Company on checkout is selected as No then too validations were applied to it and we can't place orders." with magento 2.2.x & magento 2.3.x.

## 1.1.2

Bug fix:

    - Fixed compatibility issues with magento 2.3.x.

## 1.1.2

Bug fix:

    - Fix Translation text on checkout page shipping/billing address.

## 1.1.1

Bug fix:

    - Fix admin config value (vat_id, company) in edit address

## 1.1.0

Features:

    - Add Privato/Azienda and SDI code in checkout shipping/billing address if Country is IT
    - Add Privato/Azienda and SDI code in frontend customer edit/create address

## 1.0.0

Features:

    - Add required input text field "Fiscal Code" in checkout shipping/billing address if Country is IT
    - Add input text field "Fiscal Code" in frontend customer edit/create address
    - Add "Fiscal Code" in backend Order Details, Invoice and Credit Memo
