## Copyrigth

© Edizioni Altravista. All Rights Reserved. Edizioni Altravista - P. iva 02195800186

## Licenza d’uso e Condizioni di Vendita

Consulta la Licenza d’uso e Condizioni di Vendita alla pagina
seguente https://www.studiograficoaltravista.com/condizioni-generali/

## Descrizione

Aggiungo il campo Codice Fiscale e il Campo SDI alla Checkout. BO - Mostra i dati nell'indirizzo di fatturazione e nell'
indirizzo di spedizione nella rubrica ordine e nell'indirizzo del cliente FE - @todo: aggiungere dati a indirizzo utente

## Info e Assistenza

Per informazioni, assistenza e download visita il sito www.studiograficoaltravista.com oppure scrivi a
support@studiograficoaltravista.com

### Prerequisites

Magento 2.* version

### DISINSTALLARE il modulo FiscalCode per sostiutirlo con ItalianInvoice

!!!ATTENZIONE: Se avevi scaricato la versione precedente del modulo con nome Ea/FiscalCode devi disintallare il modulo
FiscalCode prima di installare il modulo ItalianInvoice

Elimina la cartella app/code/Ea/FiscalCode

elimina riga seguente dal file app/etc/config.php  'Ea_FiscalCode' => 1

Aggiorna il nome degli attributi custom:

UPDATE `eav_attribute`
SET `attribute_code` = 'fiscal_code' WHERE `attribute_code` = 'fisc_code'

### Installing

!!! Attenzione: Esegui una copia di backup dei file e del database del sito prima di procedere all'installazione. Se
possibile testa prima il modulo in ambiente di sviluppo.

Copia la cartella Ea nel percorso app/code/ del tuo sito

Esegui i seguenti comandi:

```
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy

```

### Abilita il modulo

Accedi al backend dal menu Stores >> Configuration >> Fatturazione Elettronica per Italia – Privato/Azienda per
abilitare il modulo

### DISINSTALLARE il modulo ItalianInvoice

Elimina la cartella app/code/Ea/ItalianInvoice

elimina riga seguente dal file app/etc/config.php  'Ea_ItalianInvoice' => 1

elimina i seguenti attributi dal DB

DELETE FROM `eav_attribute` WHERE (`attribute_code` = 'fiscal_code'); DELETE FROM `eav_attribute`
WHERE (`attribute_code` = 'fiscal_code'); DELETE FROM `eav_attribute` WHERE (`attribute_code` = 'fiscal_company');

### DISINSTALLARE il modulo FiscalCode per sostiutirlo con ItalianInvoice

!!!ATTENZIONE: Se avevi scaricato la versione precedente del modulo con nome Ea/FiscalCode e vuoi disinstallarla

Elimina la cartella app/code/Ea/FiscalCode

elimina riga seguente dal file app/etc/config.php  'Ea_FiscalCode' => 1

elimina i seguenti attributi dal DB
!!!ATTENZIONE: i dati contenuti nei seguenti attributi andranno persi

DELETE FROM `eav_attribute` WHERE (`attribute_code` = 'fisc_code'); DELETE FROM `eav_attribute` WHERE (`attribute_code`
= 'fiscal_sdi');

