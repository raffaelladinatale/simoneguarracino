<?php


namespace Ea\ItalianInvoice\Setup;

use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    private $customerSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    )
    {
        $this -> customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface   $context
    )
    {
        $customerSetup = $this -> customerSetupFactory -> create(['setup' => $setup]);

        $customerSetup -> addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY, 'fiscal_code', [
            'label' => 'Fiscal Code',
            'input' => 'text',
            'type' => 'varchar',
            'source' => '',
            'required' => false,
            'position' => 333,
            'visible' => true,
            'system' => false,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'is_searchable_in_grid' => false,
            'backend' => ''
        ]);

        $attribute = $customerSetup -> getEavConfig() -> getAttribute('customer_address', 'fiscal_code')
            -> addData(['used_in_forms' => [
                'adminhtml_customer_address',
                'customer_address_edit',
                'customer_register_address'
            ]
            ]);
        $attribute -> save();

        $setup -> getConnection() -> addColumn(
            $setup -> getTable('quote_address'),
            'fiscal_code',
            [
                'type' => 'text',
                'length' => 255,
                'comment' => 'Codice Fiscale'
            ]
        );

        $setup -> getConnection() -> addColumn(
            $setup -> getTable('sales_order_address'),
            'fiscal_code',
            [
                'type' => 'text',
                'length' => 255,
                'comment' => 'Codice Fiscale'
            ]
        );

        $customerSetup -> addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY, 'fiscal_sdi', [
            'label' => 'SDI Code or PEC Email',
            'input' => 'text',
            'type' => 'varchar',
            'source' => '',
            'required' => false,
            'position' => 333,
            'visible' => true,
            'system' => false,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'is_searchable_in_grid' => false,
            'backend' => ''
        ]);

        $attribute = $customerSetup -> getEavConfig() -> getAttribute('customer_address', 'fiscal_sdi')
            -> addData(['used_in_forms' => [
                'adminhtml_customer_address',
                'customer_address_edit',
                'customer_register_address'
            ]
            ]);
        $attribute -> save();

        $setup -> getConnection() -> addColumn(
            $setup -> getTable('quote_address'),
            'fiscal_sdi',
            [
                'type' => 'text',
                'length' => 255,
                'comment' => 'Codice SDI/Pec'
            ]
        );

        $setup -> getConnection() -> addColumn(
            $setup -> getTable('sales_order_address'),
            'fiscal_sdi',
            [
                'type' => 'text',
                'length' => 255,
                'comment' => 'Codice SDI/Pec'
            ]
        );

        //Your install script
    }
}
