<?php


namespace Ea\ItalianInvoice\Setup;

use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $customerSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    )
    {
        $this -> customerSetupFactory = $customerSetupFactory;
    }


    /**
     * {@inheritdoc}
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface   $context
    )
    {
        if (version_compare($context -> getVersion(), "1.1.0", "<")) {
            $customerSetup = $this -> customerSetupFactory -> create(['setup' => $setup]);

            $customerSetup -> addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY, 'fiscal_company', [
                'label' => 'Is a Company',
                'input' => 'select',
                'type' => 'varchar',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
                'required' => false,
                'position' => 335,
                'visible' => true,
                'system' => 0,
                'user_defined' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'option' =>
                    array(
                        'values' =>
                            array(
                                0 => '',
                                1 => 'Yes',
                                2 => 'No',
                            ),
                    ),
            ]);

            $attribute = $customerSetup -> getEavConfig() -> getAttribute('customer_address', 'fiscal_company')
                -> addData(['used_in_forms' => [
                    'adminhtml_customer_address',
                    'customer_address_edit',
                    'customer_register_address'
                ]
                ]);
            $attribute -> save();

            $setup -> getConnection() -> addColumn(
                $setup -> getTable('quote_address'),
                'fiscal_company',
                [
                    'type' => 'text',
                    'length' => 255,
                    'comment' => 'Is a company'
                ]
            );

            $setup -> getConnection() -> addColumn(
                $setup -> getTable('sales_order_address'),
                'fiscal_company',
                [
                    'type' => 'text',
                    'length' => 255,
                    'comment' => 'Is a company'
                ]
            );
        }

        if (version_compare($context -> getVersion(), "2.4.5", "<")) {
            $customerSetup = $this -> customerSetupFactory -> create(['setup' => $setup]);
            $customerSetup -> updateAttribute(
                \Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY,
                'fiscal_company',
                'source_model',
                'Ea\ItalianInvoice\Model\Config\Source\Fiscalcompany'
            );
        }
    }
}
