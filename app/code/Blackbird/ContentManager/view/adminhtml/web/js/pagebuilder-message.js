/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('mage.pagebuilderMessage', {
        options: {
            message: '',
            type: 'success'
        },
        _create: function () {
            if($('#blackbird_contentmanager_pagebuilder').length !== null){
                let messageBlock = '<div class="message message-'+this.options.type+'">' +
                '<div>'+this.options.message+'</div>' +
                '</div>';
                $(messageBlock).insertAfter('#blackbird_contentmanager_pagebuilder .comment');

                if(this.options.type === 'error'){
                    $('#blackbird_contentmanager_pagebuilder_enabled').prop( "disabled", true );
                }
            }
        }
    });

    return $.mage.pagebuilderMessage;
});
