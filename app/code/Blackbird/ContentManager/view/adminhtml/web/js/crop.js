var marker_edited;
var position_edited;
var jcrop_api;

PointEditor = function (options) {
    this.options = options;
    this.jwindow = jQuery(window);
    this.jeditor = null;
    this.jeditor_content = null;
    this.jtextarea = null;
    this.jcontextualmenu = null;
    this.mouse_event_owner = null;
    this.dialog_v_padding = 10;
    this.dialog_h_padding = 15;
    this.opened = false;
    this.shipping_code = null;
    this.mapping_positions = null;
}

var w, h, margin, h_padding, v_padding, sidebar_width, header_height,
    width, height, dialog_w, dialog_h, dialog_left, dialog_top, margin_left, margin_top;

PointEditor.prototype = {
    /**
     * @private
     */
    _init: function () {
        var jeditor = this._dialog('point-editor',"<div style=\"width:100%;height:100%;position:relative;\" id=\"point-editor-content\"></div>");
        this.jeditor = jeditor;
        var jdialogbox = jeditor.find('.dialog-box');
        this.jeditor_content = jeditor.find('#point-editor-content');
        this.jeditor_content.css({width: jdialogbox.innerWidth()-this.dialog_h_padding*2, height: jdialogbox.innerHeight()-this.dialog_v_padding*2, border: '0'});
    },

    /**
     * @private
     */
    _dialog: function (id, content) {
        w = this.jwindow.width(); // Window width
        h = this.jwindow.height(); // Window height
        margin = 50;
        h_padding = this.dialog_h_padding; // Dialog horizontal padding
        v_padding = this.dialog_v_padding; // Dialog vertical padding
        sidebar_width = 88; // Admin left sidebar width
        header_height = 70; // Admin header height
        width = (w - sidebar_width) - (2 * margin);
        height = (h - header_height) - (2 * margin);
        dialog_w = Math.max(width+2*h_padding,350); // Dialog width
        dialog_h = Math.max(height+2*v_padding,250); // Dialog height
        dialog_left = ((w - dialog_w - sidebar_width) / 2) + sidebar_width; // Dialog left position
        dialog_top = ((h - dialog_h - header_height) / 2) + header_height; // Dialog top position
        margin_left = (dialog_w-width)/2-h_padding;
        margin_top = (dialog_h-height)/2-v_padding;

        var jdialog = jQuery("<div style=\"position:fixed;top:0;left:0;width:100%;height:100%;z-index:500;\" id=\""+id+"\">"
            +"<div class=\"dialog-bg\" style=\"position:fixed;top:0;left:0;z-index:100;width:100%;height:100%;background:#000;\"></div>"
            +"<div class=\"dialog-box\" style=\"position:fixed;background:#fff;-moz-box-shadow: #000 0 0 10px;top:"+dialog_top+"px;left:"+dialog_left+"px;width:"+dialog_w+"px;"
            +"height:"+dialog_h+"px;z-index:200;\"><div style=\"padding:"+v_padding+"px "+h_padding+"px;margin:"+margin_top+"px 0 0 "+margin_left+"px;\">"
            +content+'</div></div></div>');
        jdialog.find('.dialog-bg').click(function(event){
            jdialog.fadeOut(function(){jdialog.hide();});
        })
            .css({
                opacity: '0.7'
            })
        ;
        jQuery('body').append(jdialog);
        return jdialog;
    },

    /**
     * @public
     */
    save: function () {
        jQuery('#'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_ctdi').val(jQuery('#positionx1').val()+':'+jQuery('#positiony1').val()+':'+jQuery('#positionx2').val()+':'+jQuery('#positiony2').val()+':'+jQuery('#contenttype_crop_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'').width()+':'+jQuery('#contenttype_crop_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'').height());
        jQuery('.contenttype-overlay-crop', jQuery('input#'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )).parent()).fadeIn('slow');

        this.opened = false;
        this.jeditor.fadeOut();
    },

    /**
     * @public
     */
    saveMapping: function (mappingId) {
        var current_map_values = jQuery('#content_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_map').val();

        if(!current_map_values)
        {
            current_map_values = new Array();
        }
        else
        {
            current_map_values = JSON.parse(current_map_values);
        }

        var this_value = {
            x1: jQuery('#point-editor-content #positionx1').val(),
            y1: jQuery('#point-editor-content #positiony1').val(),
            x2: jQuery('#point-editor-content #positionx2').val(),
            y2: jQuery('#point-editor-content #positiony2').val(),
            width: jQuery('#point-editor-content #width').val(),
            height: jQuery('#point-editor-content #height').val(),
            url_type: jQuery('#point-editor-content #url_type').val(),
            url_value: jQuery('#point-editor-content #url_value').val(),
            url_title: jQuery('#point-editor-content #url_title').val()
        };

        if(mappingId >= 0)
        {
            current_map_values[mappingId] = this_value;
        }
        else
        {
            current_map_values[current_map_values.length] = this_value;
        }



        jQuery('#content_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_map').val(JSON.stringify(current_map_values));


        jQuery('.contenttype-overlay-crop', jQuery('input#'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )).parent()).fadeIn('slow');

        this.opened = false;
        this.jeditor.fadeOut();
        this.updateSummaryMapping(current_map_values);
    },

    /**
     * @public
     */
    deleteMapping: function (mappingId) {
        var current_map_values = jQuery('#content_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_map').val();

        if(!current_map_values)
        {
            current_map_values = new Array();
        }
        else
        {
            current_map_values = JSON.parse(current_map_values);
        }

        current_map_values.splice(mappingId-1, 1);

        jQuery('#content_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_map').val(JSON.stringify(current_map_values));

        this.updateSummaryMapping(current_map_values);
    },

    /**
     * @public
     */
    updateSummaryMapping: function (current_map_values) {
        jQuery('#'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_mapping_formatted').html('');

        for(var i = 0; i < current_map_values.length; i++)
        {
            var currentArea = current_map_values[i];
            pointeditor_mapping_current[this.options.identifier][i+1] = currentArea;

            jQuery('#'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_mapping_formatted').append('<div id="'+this.options.identifier+'_map_'+(i+1)+'" style="border: 1px #ccc solid; padding: 5px; background: #fff; margin-bottom: -1px;">'+
                'Area '+(i+1)+' : '+currentArea.url_type+' - '+currentArea.url_value+' - '+currentArea.url_title+' <button id="" title="Edit mapping area" type="button" class="scalable edit edit-crop" onclick="pointeditor[\''+this.options.identifier+'\'].openMapping(this, pointeditor_mapping_current[\''+this.options.identifier+'\']['+(i+1)+'], '+(i)+');" style=""><span><span><span>Edit</span></span></span></button>'+
                ' <button id="" title="Delete mapping area" type="button" class="scalable delete edit-crop" onclick="pointeditor[\''+this.options.identifier+'\'].deleteMapping(this, '+i+');" style=""><span><span><span>Delete</span></span></span></button><br>'+
                '</div>');
        }
    },

    /**
     * @public
     */
    open: function (object) {
        this.opened = true;

        if (this.jeditor==null) this._init();

        this.jeditor.fadeIn();
        this.jeditor_content.html('<div class=\"loading rule-param-wait\">'+this.options.loading_label+'</div>');

        jQuery('#point-editor .content-header').remove();
        this.jeditor_content.html(
            '<div class="content-header">'+
            '<p class="form-buttons" style="float: left;">'+
            '<label for="positionx1">x1 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positionx1" name="x1" value="" />'+
            '<label for="positionx2"> x2 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positionx2" name="x2" value="" />'+
            '<label for="positiony1"> y1 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positiony1" name="y1" value="" />'+
            '<label for="positiony2"> y2 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positiony2" name="y2" value="" />'+
            '<label for="width"> width </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="width" name="width" value="" />'+
            '<label for="height"> height </label>'+
            '<input style="width: 40px; margin-right: 50px;" class="input-text" type="text" id="height" name="height" value="" />'+
            '</p>'+
            '<p class="form-buttons" style="float: left;">'+
            '<button type="button" class="button back" onclick="pointeditor[\''+this.options.identifier+'\'].close();"><span><span></span>Cancel</span></button>'+
            '<button type="button" class="button" onclick="updateCropSelect();"><span><span></span>Update selection</span></button>'+
            '<button type="button" class="button save" onclick="pointeditor[\''+this.options.identifier+'\'].save();"><span><span></span>Save crop</span></button>'+
            '</p>'+
            '</div>'+
            '<div style="overflow: scroll; height: '+(jQuery('#point-editor-content').height()-35)+'px;">'+
            '<img onload="pointeditor[\''+this.options.identifier+'\'].jcrop();" src="'+this.options.url_image+'" id="contenttype_crop_'+this.options.identifier+'" />'+
            '</div>'+
            '');
    },

    /**
     * @public
     */
    openMapping: function (object, positions, mappingId) {
        this.opened = true;

        if (this.jeditor==null) this._init();

        this.jeditor.fadeIn();
        this.jeditor_content.html('<div class=\"loading rule-param-wait\">'+this.options.loading_label+'</div>');

        jQuery('#point-editor .content-header').remove();
        this.mapping_positions = positions;

        this.jeditor_content.html(
            '<div class="content-header">'+
            '<p class="form-buttons" style="float: left;">'+
            '<label for="positionx1">x1 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positionx1" name="x1" value="" />'+
            '<label for="positionx2"> x2 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positionx2" name="x2" value="" />'+
            '<label for="positiony1"> y1 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positiony1" name="y1" value="" />'+
            '<label for="positiony2"> y2 </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="positiony2" name="y2" value="" />'+
            '<label for="width"> width </label>'+
            '<input style="width: 40px; margin-right: 10px;" class="input-text" type="text" id="width" name="width" value="" />'+
            '<label for="height"> height </label>'+
            '<input style="width: 40px; margin-right: 50px;" class="input-text" type="text" id="height" name="height" value="" />'+
            '</p>'+
            '<p class="form-buttons" style="float: left;">'+
            '<button type="button" class="button back" onclick="pointeditor[\''+this.options.identifier+'\'].close();"><span><span></span>Cancel</span></button>'+
            '<button type="button" class="button save" onclick="pointeditor[\''+this.options.identifier+'\'].saveMapping('+mappingId+');"><span><span></span>Save mapping area</span></button>'+
            '</p>'+
            '</div>'+
            '<div class="content-header" style="padding: 10px;">'+
            '<label for="url_type">URL Type </label>'+
            '<select style="margin-right: 10px;" class="input-text" type="text" id="url_type" name="url_type"><option value="url_key">URL Key</option><option value="product">Product SKU</option><option value="absolute">Absolute</option></select>'+
            '<label for="url_value">URL Value </label>'+
            '<input style="width: 160px; margin-right: 50px;" class="input-text" type="text" id="url_value" name="url_value" value="" />'+
            '<label for="url_value">URL title </label>'+
            '<input style="width: 160px; margin-right: 50px;" class="input-text" type="text" id="url_title" name="url_title" value="" />'+
            '</div>'+
            '<div style="overflow: scroll; height: '+(jQuery('#point-editor-content').height()-70)+'px; "><div style="width: 3000px">'+
            '<img style="max-width: none;" onload="pointeditor[\''+this.options.identifier+'\'].jcropMapping(pointeditor[\''+this.options.identifier+'\'].mapping_positions);" src="'+this.options.url_image+'" id="contenttype_crop_'+this.options.identifier+'" />'+
            '</div></div>'+
            '');
    },

    jcrop: function() {
        jQuery('#contenttype_crop_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )).Jcrop(
            this.options.keep_aspect_ratio == 1 ? { aspectRatio: this.options.crop_w/this.options.crop_h, onSelect: showCoords }: { onSelect: showCoords }
            ,function(){
                jcrop_api = this;
                bindCropEvent();
            }
        );

        var positions = jQuery('#'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )+'_ctdi').val().split(':');
        jcrop_api.setSelect([positions[0], positions[1], positions[2], positions[3]]);
    },

    jcropMapping: function(positions) {
        jQuery('#contenttype_crop_'+this.options.identifier.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )).Jcrop(
            this.options.keep_aspect_ratio == 1 ? { aspectRatio: this.options.crop_w/this.options.crop_h, onSelect: showCoords }: { onSelect: showCoords }
            ,function(){
                jcrop_api = this;
                bindCropEvent();
            }
        );

        if(positions)
        {
            jcrop_api.setSelect([positions.x1, positions.y1, positions.x2, positions.y2]);
            jQuery('#url_type').val(positions.url_type);
            jQuery('#url_value').val(positions.url_value);
            jQuery('#url_title').val(positions.url_title);
        }
    },

    /**
     * @public
     */
    close: function () {
        this.opened = false;
        this.jeditor.fadeOut();
    }
}

function showCoords(c)
{
    jQuery('#positionx1').val(parseInt(c.x));
    jQuery('#positiony1').val(parseInt(c.y));
    jQuery('#positionx2').val(parseInt(c.x2));
    jQuery('#positiony2').val(parseInt(c.y2));
    jQuery('#width').val(parseInt(c.w));
    jQuery('#height').val(parseInt(c.h));
}

function bindCropEvent()
{
    jQuery('#positionx1, #positionx2, #positiony1, #positiony2').unbind('blur').blur(function() {
        updateCropSelect();
    });
    jQuery('#width, #height').unbind('blur').blur(function() {
        jQuery('#positionx2').val(parseInt(jQuery('#positionx1').val())+parseInt(jQuery('#width').val()));
        jQuery('#positiony2').val(parseInt(jQuery('#positiony1').val())+parseInt(jQuery('#height').val()));
    });
}

function updateCropSelect()
{
    jcrop_api.setSelect([parseInt(jQuery('#positionx1').val()), parseInt(jQuery('#positiony1').val()), parseInt(jQuery('#positionx2').val()), parseInt(jQuery('#positiony2').val())]);
}

