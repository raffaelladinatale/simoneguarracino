<?php

/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */
namespace Blackbird\ContentManager\Acl\Plugin\Integration;

/**
 * Class PageBuilder
 * @package Blackbird\ContentManager\Acl\Plugin\Integration
 */
class RestrictedResource
{
    public const RESTRICTED_ID = [];
    public const PAGE_BUILDER_IDS = ['Blackbird_ContentManager::config_page_builder', 'Blackbird_ContentManager::config_content_manager'];

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * RestrictedResource constructor.
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        $this->productMetadata = $productMetadata;
    }



    /**
     * Don't show restricted id in acl tree
     * @param \Magento\Integration\Helper\Data $helper
     * @param array $resources
     * @return array[]
     */
    public function beforeMapResources(\Magento\Integration\Helper\Data $helper, array $resources)
    {
        $restricted = $this->getRestrictedIds();
        foreach ($resources as $key => $resource) {
            if (in_array($resource['id'], $restricted, true)) {
                unset($resources[$key]);
            }
        }
        return [$resources];
    }

    /**
     * Do not allow with restricted id
     * @param $subject
     * @param $result
     * @param $roleId
     * @param $resourceId
     * @param null $privilege
     * @return false
     */
    public function afterIsAllowed($subject, $result, $roleId, $resourceId, $privilege = null){
        if($result){
            foreach ($this->getRestrictedIds() as $restrictedResourceId){
                if($resourceId === $restrictedResourceId){
                    return false;
                }
            }
        }
        return $result;
    }

    /**
     * Return array of Restricted ID
     * @return string[]
     */
    protected function getRestrictedIds()
    {
        $restrictedID = self::RESTRICTED_ID;
        if($this->productMetadata->getEdition()==='Community'){
            $restrictedID = array_merge($restrictedID, self::PAGE_BUILDER_IDS);
        }

        return $restrictedID;
    }
}