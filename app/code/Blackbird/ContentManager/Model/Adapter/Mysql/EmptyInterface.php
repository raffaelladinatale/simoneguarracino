<?php
/*
 *   Blackbird Redirection Module
 *
 *   NOTICE OF LICENSE
 *   If you did not receive a copy of the license and are unable to
 *   obtain it through the world-wide-web, please send an email
 *   to contact@bird.eu so we can send you a copy immediately.
 *
 *   @category  Blackbird
 *   @package   Blackbird_Redirection
 *   @copyright Copyright (c) 2020 Blackbird (https://black.bird.eu)
 *   @author    Blackbird Team
 */

namespace Blackbird\ContentManager\Model\Adapter\Mysql;

/**
 * Interface EmptyInterface
 * @deprecated Only used to replace deleted interface by this one in order to compile and keep compatibility for magento 2.3
 * @package Blackbird\ContentManager\Model\Adapter\Mysql
 */
interface EmptyInterface
{
}
