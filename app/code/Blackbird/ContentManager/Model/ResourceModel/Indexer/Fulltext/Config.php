<?php
/*
 * Blackbird Metadata Importer Module
 *  NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 * @category  Blackbird
 * @package   Blackbird_MetadataImporter
 * @copyright Copyright (c) 2020 Blackbird (https://black.bird.eu)
 * @author    Blackbird Team
 */

namespace Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext;

use Blackbird\ContentManager\Model\Indexer\IndexerHandlerFactory;
use Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext\Collection\SearchResultApplierElasticsearchFactory;
use Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext\Collection\SearchResultApplierMySQLFactory;
use Magento\AdvancedSearch\Model\Client\ClientInterface;
use Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection\SearchResultApplierInterfaceFactory;
use Magento\Elasticsearch\Model\Adapter\Elasticsearch;
use Magento\Elasticsearch\Model\Config as ElasticSearchConfig;
use Magento\Elasticsearch\Model\Indexer\IndexerHandlerFactory as IndexerHandlerElasticsearchFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Indexer\SaveHandler\IndexerInterfaceFactory;

class Config
{
    /**
     * @var ClientInterface
     */
    protected $searchClient;

    /**
     * @var SearchResultApplierInterfaceFactory
     */
    protected $searchResultApplierFactory;

    /**
     * @var IndexerInterfaceFactory
     */
    protected $indexerHandlerFactory;

    /**
     * @var null | Elasticsearch
     */
    protected $elasticSearchAdapter;
    /**
     * @var ElasticSearchConfig
     */
    protected $config;

    /**
     * Config constructor.
     *
     * @param SearchResultApplierElasticsearchFactory $searchResultApplierElasticsearchFactory
     * @param SearchResultApplierMySQLFactory $searchResultApplierMySQLFactory
     * @param IndexerHandlerFactory $indexerHandlerFactory
     * @param IndexerHandlerElasticsearchFactory $indexerHandlerElasticSearchFactory
     * @param $elasticsearchAdapter
     * @param ElasticSearchConfig $config
     */
    public function __construct(
        SearchResultApplierElasticsearchFactory $searchResultApplierElasticsearchFactory,
        SearchResultApplierMySQLFactory $searchResultApplierMySQLFactory,
        IndexerHandlerFactory $indexerHandlerFactory,
        IndexerHandlerElasticSearchFactory $indexerHandlerElasticSearchFactory,
        $elasticsearchAdapter,
        ElasticSearchConfig $config
    ) {
        $this->config = $config;
        if ($this->isElasticSearchClient()) {
            $this->searchResultApplierFactory = $searchResultApplierElasticsearchFactory;
            $this->indexerHandlerFactory = $indexerHandlerElasticSearchFactory;
            $this->elasticSearchAdapter = $elasticsearchAdapter;
        } else {
            $this->elasticSearchAdapter = null;
            $this->searchResultApplierFactory = $searchResultApplierMySQLFactory;
            $this->indexerHandlerFactory = $indexerHandlerFactory;
        }
    }

    /**
     * @return Elasticsearch|null
     */
    public function getElasticsearchAdapter()
    {
        if ($this->elasticSearchAdapter) {
            return ObjectManager::getInstance()->get($this->elasticSearchAdapter['instance']);
        }
        return $this->elasticSearchAdapter;
    }

    /**
     * @return bool
     */
    public function isElasticSearchClient():bool
    {
        if ($this->config->isElasticsearchEnabled()) {
            return true;
        }
        return false;
    }

    /**
     * @return ClientInterface
     */
    public function getSearchClient():ClientInterface
    {
        return $this->searchClient;
    }

    /**
     * @return SearchResultApplierInterfaceFactory
     */
    public function getSearchResultsApplier()
    {
        return $this->searchResultApplierFactory;
    }

    /**
     * @return IndexerInterfaceFactory
     */
    public function getIndexerHandlerFactory()
    {
        return $this->indexerHandlerFactory;
    }
}
