<?php
/*
 * Blackbird Metadata Importer Module
 *  NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 * @category  Blackbird
 * @package   Blackbird_MetadataImporter
 * @copyright Copyright (c) 2020 Blackbird (https://black.bird.eu)
 * @author    Blackbird Team
 */

namespace Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext\Collection;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Data\Collection;
use Magento\Framework\Search\Adapter\Mysql\TemporaryStorage;
use Magento\Framework\Search\Adapter\Mysql\TemporaryStorageFactory;

/**
 * Class SearchResultApplierMySQL
 * @deprecated for Deprecated for Magento > 2.4 because Mysql is not used anymore
 * @package Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext\Collection
 */
class SearchResultApplierMySQL implements \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection\SearchResultApplierInterface
{

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var SearchResultInterface
     */
    private $searchResult;

    /**
     * @var TemporaryStorageFactory
     */
    private $temporaryStorageFactory;

    /**
     * @var array
     */
    private $orders;

    /**
     * @param Collection $collection
     * @param SearchResultInterface $searchResult
     * @param TemporaryStorageFactory $temporaryStorageFactory
     * @param array $orders
     */
    public function __construct(
        Collection $collection,
        SearchResultInterface $searchResult,
        TemporaryStorageFactory $temporaryStorageFactory,
        array $orders
    ) {
        $this->collection = $collection;
        $this->searchResult = $searchResult;
        $this->temporaryStorageFactory = $temporaryStorageFactory;
        $this->orders = $orders;
    }

    /**
     * @throws \Zend_Db_Exception
     */
    public function apply()
    {
        $temporaryStorage = $this->temporaryStorageFactory->create();
        $table = $temporaryStorage->storeApiDocuments($this->searchResult->getItems());

        $this->collection->getSelect()->joinInner(
            ['search_result' => $table->getName()],
            'e.entity_id = search_result.' . TemporaryStorage::FIELD_ENTITY_ID,
            []
        );

        if ($this->orders && 'relevance' === $this->orders['field']) {
            $this->collection->getSelect()->order('search_result.' . TemporaryStorage::FIELD_SCORE . ' ' . $this->orders['dir']);
        }
    }
}
