<?php
/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */

namespace Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext;

use Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection\SearchResultApplierInterface;
use Magento\Framework\Api\Search\SearchResultFactory;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Search\Request\EmptyRequestDataException;
use Magento\Framework\Search\Request\NonExistingRequestNameException;

/**
 * Fulltext Collection
 *
 * Class Collection
 *
 * @package Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext
 */
class Collection extends \Blackbird\ContentManager\Model\ResourceModel\Content\Collection
{
    /**
     * @var string
     */
    protected $queryText;

    /**
     * @var string|null
     */
    protected $order = null;

    /**
     * @var string
     */
    protected $searchRequestName;

    /**
     * @var \Magento\Search\Api\SearchInterface
     */
    protected $search;

    /**
     * @var \Magento\Framework\Api\Search\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\Search\SearchResultInterface
     */
    protected $searchResult;

    /**
     * @var SearchResultFactory
     */
    protected $searchResultFactory;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;
    /**
     * @var Config
     */
    protected $fullTextConfig;
    /**
     * @var SortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * Collection constructor
     *
     * @param \Magento\Framework\Data\Collection\EntityFactory                $entityFactory
     * @param \Psr\Log\LoggerInterface                                        $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface    $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface                       $eventManager
     * @param \Magento\Eav\Model\Config                                       $eavConfig
     * @param \Magento\Framework\App\ResourceConnection                       $resource
     * @param \Magento\Eav\Model\EntityFactory                                $eavEntityFactory
     * @param \Magento\Eav\Model\ResourceModel\Helper                         $resourceHelper
     * @param \Magento\Framework\Validator\UniversalFactory                   $universalFactory
     * @param \Magento\Store\Model\StoreManagerInterface                      $storeManager
     * @param \Magento\Search\Api\SearchInterface                             $searchInterface
     * @param \Magento\Framework\Api\Search\SearchCriteriaBuilder             $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder                            $filterBuilder
     * @param SearchResultFactory                                             $searchResultFactory
     * @param \Magento\Framework\Search\Adapter\Mysql\TemporaryStorageFactory $temporaryStorageFactory
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null             $connection
     * @param Config                                                          $fullTextConfig
     * @param string                                                          $searchRequestName
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Eav\Model\EntityFactory $eavEntityFactory,
        \Magento\Eav\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Validator\UniversalFactory $universalFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Search\Api\SearchInterface $searchInterface,
        \Magento\Framework\Api\Search\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\SearchResultFactory $searchResultFactory,
        SortOrderBuilder $sortOrderBuilder,
        Config $fullTextConfig,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        $searchRequestName = 'quick_search_container_contentmanager'
    ) {
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->fullTextConfig = $fullTextConfig;
        $this->search = $searchInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->searchResultFactory = $searchResultFactory;
        $this->filterBuilder = $filterBuilder;
        $this->searchRequestName = $searchRequestName;
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $eavConfig,
            $resource,
            $eavEntityFactory,
            $resourceHelper,
            $universalFactory,
            $storeManager,
            $connection
        );
    }

    /**
     * Add search query filter
     *
     * @param string $query
     * @return $this
     */
    public function addSearchFilter($query)
    {
        $this->queryText = \trim($this->queryText . ' ' . $query);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function _renderFiltersBefore()
    {
        if ($this->isLoaded()) {
            return;
        }

        if ($this->searchRequestName !== 'quick_search_container'
            || strlen(trim($this->queryText))
        ) {
            $this->prepareSearchTermFilter();

            $filterBuilder = $this->getFilterBuilder()->create();
            $filterBuilder->setField('search_term');
            $filterBuilder->setValue($this->queryText);
            $this->getSearchCriteriaBuilder()->addFilter($filterBuilder);

            $searchCriteria = $this->getSearchCriteriaBuilder()->create();
            $searchCriteria->setRequestName($this->getSearchRequestName());
            $sortOrderRelevance = $this->sortOrderBuilder->setField('relevance')->setDescendingDirection()->create();
            $searchCriteria->setSortOrders([$sortOrderRelevance]);
            try {
                $this->searchResult = $this->getSearch()->search($searchCriteria);
                $this->_totalRecords = $this->searchResult->getTotalCount();
            } catch (EmptyRequestDataException $e) {
                /** @var \Magento\Framework\Api\Search\SearchResultInterface $searchResult */
                $this->searchResult = $this->searchResultFactory->create()->setItems([]);
            } catch (NonExistingRequestNameException $e) {
                $this->_logger->error($e->getMessage());
                throw new LocalizedException(__('Sorry, something went wrong. You can find out more in the error log.'));
            }
        } else {
            /** @var \Magento\Framework\Api\Search\SearchResultInterface $searchResult */
            $this->searchResult = $this->searchResultFactory->create()->setItems([]);
        }

        $this->getSearchResultApplier($this->searchResult)->apply();
        parent::_renderFiltersBefore();
    }

    /**
     * Get search result applier.
     *
     * @param SearchResultInterface $searchResult
     * @return SearchResultApplierInterface
     */
    private function getSearchResultApplier(SearchResultInterface $searchResult): SearchResultApplierInterface
    {
        return $this->fullTextConfig->getSearchResultsApplier()->create(
            [
                'collection' => $this,
                'searchResult' => $searchResult,
                /** This variable sets by serOrder method, but doesn't have a getter method. */
                'orders' => $this->_orders,
                'size' => $this->getPageSize(),
                'currentPage' => (int)$this->_curPage
            ]
        );
    }

    /**
     * Prepare search term filter for text query.
     *
     * @return void
     */
    private function prepareSearchTermFilter(): void
    {
        if ($this->queryText) {
            $this->filterBuilder->setField('search_term');
            $this->filterBuilder->setValue($this->queryText);
            $this->searchCriteriaBuilder->addFilter($this->filterBuilder->create());
        }
    }

    /**
     * Get filter builder
     *
     * @return \Magento\Framework\Api\FilterBuilder
     */
    protected function getFilterBuilder()
    {
        return $this->filterBuilder;
    }

    /**
     * Get search criteria builder
     *
     * @return \Magento\Framework\Api\Search\SearchCriteriaBuilder
     */
    protected function getSearchCriteriaBuilder()
    {
        return $this->searchCriteriaBuilder;
    }

    /**
     * Get search request name
     *
     * @return string
     */
    protected function getSearchRequestName()
    {
        return $this->searchRequestName;
    }

    /**
     * Get search model
     *
     * @return \Magento\Search\Api\SearchInterface
     */
    protected function getSearch()
    {
        return $this->search;
    }
}
