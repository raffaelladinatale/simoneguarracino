<?php
/*
 * Blackbird Metadata Importer Module
 *  NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 * @category  Blackbird
 * @package   Blackbird_MetadataImporter
 * @copyright Copyright (c) 2020 Blackbird (https://black.bird.eu)
 * @author    Blackbird Team
 */

namespace Blackbird\ContentManager\Model\Search\Adapter\Mapper;

use Magento\Elasticsearch\Model\Adapter\BatchDataMapperInterface;
use Magento\Elasticsearch\Model\Adapter\Document\Builder;
use Magento\Elasticsearch\Model\Adapter\FieldMapperInterface;

/**
 * Class ContentDataMapper
 * @package Blackbird\ContentManager\Model\Search\Adapter\Mapper
 */
class ContentDataMapper implements BatchDataMapperInterface
{

    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @var FieldMapperInterface
     */
    protected $fieldMapper;

    /**
     * Construction for DocumentDataMapper
     *
     * @param Builder              $builder
     * @param FieldMapperInterface $fieldMapper
     */
    public function __construct(
        Builder $builder,
        FieldMapperInterface $fieldMapper
    ) {
        $this->builder = $builder;
        $this->fieldMapper = $fieldMapper;
    }

    /**
     * Map index data for using in search engine metadata
     *
     * @param array $documentData
     * @param int $storeId
     * @param array $context
     * @return array
     */
    public function map(array $documentData, $storeId, array $context = [])
    {
        $documents = [];
        $documentData = $this->convertToContentData($documentData);

        foreach ($documentData as $entityId => $value) {
            $this->builder->addField('store_id', $storeId);
            foreach ($value as $attributeId => $dataIndex) {
                // Prepare processing attribute info
                if (strpos($attributeId, '_value') !== false) {
                    $this->builder->addField($attributeId, $dataIndex);
                    continue;
                }

                $this->builder->addField(
                    $this->fieldMapper->getFieldName(
                        $attributeId,
                        $context
                    ),
                    $dataIndex
                );
            }
            $documents[] = $this->builder->build();
        }
        return $documents;
    }

    /**
     * @param array $documents
     */
    public function convertToContentData(array $documents)
    {
        $insertDocuments = [];
        foreach ($documents as $entityId => $document) {
            foreach ($document as $attributeId => $fieldValue) {
                $insertDocuments[] = [
                    'entity_id' => $entityId,
                    'attribute_id' => $attributeId,
                    'data_index' => $fieldValue,
                ];
            }
        }
        return $insertDocuments;
    }
}
