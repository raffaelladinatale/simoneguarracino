<?php
/*
 * Blackbird Metadata Importer Module
 *  NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 * @category  Blackbird
 * @package   Blackbird_MetadataImporter
 * @copyright Copyright (c) 2020 Blackbird (https://black.bird.eu)
 * @author    Blackbird Team
 */

namespace Blackbird\ContentManager\Model\Search\Adapter\Mapper;

use Blackbird\ContentManager\Model\Indexer\Fulltext\Action\Full;
use Magento\Elasticsearch\Model\Adapter\FieldMapper\Product\FieldProvider\FieldIndex\ConverterInterface as IndexTypeConverterInterface;
use Magento\Elasticsearch\Model\Adapter\FieldMapper\Product\FieldProvider\FieldType\ConverterInterface as FieldTypeConverterInterface;

class ContentMapper implements \Magento\Elasticsearch\Model\Adapter\FieldMapperInterface
{
    /**
     * @var Full
     */
    protected $contentTypeIndexerFullAction;
    /**
     * @var FieldTypeConverterInterface
     */
    protected $fieldTypeConverter;
    /**
     * @var IndexTypeConverterInterface
     */
    protected $indexTypeConverter;

    /**
     * ContentMapper constructor.
     *
     * @param FieldTypeConverterInterface $fieldTypeConverter
     * @param IndexTypeConverterInterface $indexTypeConverter
     * @param Full                        $contentTypeIndexerFullAction
     */
    public function __construct(
        FieldTypeConverterInterface $fieldTypeConverter,
        IndexTypeConverterInterface $indexTypeConverter,
        Full $contentTypeIndexerFullAction
    ) {
        $this->fieldTypeConverter = $fieldTypeConverter;
        $this->indexTypeConverter = $indexTypeConverter;
        $this->contentTypeIndexerFullAction = $contentTypeIndexerFullAction;
    }

    /**
     * @deprecated Unused function
     */
    public function getFieldName($attributeCode, $context = [])
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getAllAttributesTypes($context = [])
    {
        $allAtributes['entity_id'] = [
            'type' => $this->fieldTypeConverter->convert(FieldTypeConverterInterface::INTERNAL_DATA_TYPE_INT)
        ];

        $allAtributes['attribute_id'] = [
            'type' => $this->fieldTypeConverter->convert(FieldTypeConverterInterface::INTERNAL_DATA_TYPE_INT)
        ];

        $allAtributes['data_index'] = [
            'type' => 'text'
                //$this->fieldTypeConverter->convert(FieldTypeConverterInterface::INTERNAL_DATA_TYPE_STRING)
        ];

        return $allAtributes;
    }
}
