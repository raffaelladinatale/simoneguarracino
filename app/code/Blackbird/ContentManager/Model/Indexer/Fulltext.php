<?php
/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */

namespace Blackbird\ContentManager\Model\Indexer;

use Blackbird\ContentManager\Model\Indexer\Fulltext\Action\FullFactory;
use Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext as FulltextResource;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Search\Request\Config as SearchRequestConfig;
use Magento\Framework\Search\Request\DimensionFactory;
use Magento\Indexer\Model\ProcessManager;
use Magento\Store\Model\StoreDimensionProvider;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Fulltext
 *
 * @package Blackbird\ContentManager\Model\Indexer
 */
class Fulltext implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{
    /**
     * Indexer ID in configuration
     */
    const INDEXER_ID = 'blackbird_contenttype_fulltext';

    /**
     * @var \Blackbird\ContentManager\Model\Indexer\Fulltext\Action\Full
     */
    protected $fullAction;

    /**
     * @var \Blackbird\ContentManager\Model\Indexer\IndexerHandlerFactory
     */
    protected $indexerHandlerFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var DimensionFactory
     */
    protected $dimensionFactory;

    /**
     * @var FulltextResource
     */
    protected $fulltextResource;

    /**
     * @var SearchRequestConfig
     */
    protected $searchRequestConfig;

    /**
     * @var array index structure
     */
    protected $data;
    /**
     * @var FulltextResource\Config
     */
    protected $fullTextConfig;

    /**
     * @var StoreDimensionProvider
     */
    protected $dimensionProvider;

    /**
     * @var mixed
     */
    protected $processManager;

    /**
     * @param \Blackbird\ContentManager\Model\Indexer\Fulltext\Action\FullFactory $fullActionFactory
     * @param \Blackbird\ContentManager\Model\Indexer\IndexerHandlerFactory       $indexerHandlerFactory
     * @param \Magento\Store\Model\StoreManagerInterface                          $storeManager
     * @param \Magento\Framework\Search\Request\DimensionFactory                  $dimensionFactory
     * @param \Blackbird\ContentManager\Model\ResourceModel\Indexer\Fulltext      $fulltextResource
     * @param \Magento\Framework\Search\Request\Config                            $searchRequestConfig
     * @param StoreDimensionProvider                                              $dimensionProvider
     * @param FulltextResource\Config                                             $config
     * @param array                                                               $data
     */
    public function __construct(
        FullFactory $fullActionFactory,
        IndexerHandlerFactory $indexerHandlerFactory,
        StoreManagerInterface $storeManager,
        DimensionFactory $dimensionFactory,
        FulltextResource $fulltextResource,
        SearchRequestConfig $searchRequestConfig,
        StoreDimensionProvider $dimensionProvider,
        FulltextResource\Config $config,
        array $data
    ) {
        $this->dimensionProvider = $dimensionProvider;
        $this->fullTextConfig = $config;
        //store indexer id in data
        $data['indexer_id'] = self::INDEXER_ID;
        $this->fullAction = $fullActionFactory->create(['data' => $data]);
        $this->indexerHandlerFactory = $indexerHandlerFactory;
        $this->storeManager = $storeManager;
        $this->dimensionFactory = $dimensionFactory;
        $this->fulltextResource = $fulltextResource;
        $this->searchRequestConfig = $searchRequestConfig;
        $this->data = $data;
        $this->processManager = ObjectManager::getInstance()->get(ProcessManager::class);
    }

    /**
     * Execute materialization on ids entities
     *
     * @param int[] $entityIds
     * @return void
     * @throws \InvalidArgumentException
     */
    public function execute($entityIds)
    {
        foreach ($this->dimensionProvider->getIterator() as $dimension) {
            $this->executeByDimensions($dimension, new \ArrayIterator($entityIds));
        }
    }

    /**
     * @inheritdoc
     *
     * @throws \InvalidArgumentException
     * @since 101.0.0
     */
    public function executeByDimensions(array $dimensions, \Traversable $entityIds = null)
    {
        if (count($dimensions) > 1 || !isset($dimensions[StoreDimensionProvider::DIMENSION_NAME])) {
            throw new \InvalidArgumentException('Indexer "' . self::INDEXER_ID . '" support only Store dimension');
        }
        //Get store id key
        $storeId = $dimensions[StoreDimensionProvider::DIMENSION_NAME]->getValue();
        $saveHandler = $this->createIndexerHandler();

        if (null === $entityIds) {
            $saveHandler->cleanIndex($dimensions);
            $documents = $this->fullAction->rebuildStoreIndex($storeId, $entityIds);
            $saveHandler->saveIndex($dimensions, $documents);

            $this->fulltextResource->resetSearchResultsByStore($storeId);
        } else {
            if ($saveHandler->isAvailable($dimensions)) {
                $saveHandler->deleteIndex($dimensions, new \ArrayIterator($entityIds));
                $saveHandler->saveIndex($dimensions, $this->fullAction->rebuildStoreIndex($storeId, $entityIds));
            }
        }
    }

    /**
     * Create indexer handler
     *
     * @return \Magento\Framework\Indexer\SaveHandler\IndexerInterface
     */
    protected function createIndexerHandler()
    {
        return $this->fullTextConfig->getIndexerHandlerFactory()->create(
            [
                'data' => $this->data,
                'adapter' => $this->fullTextConfig->getElasticSearchAdapter()
            ]
        );
    }

    /**
     * Execute full indexation
     *
     * @return void
     * @throws \InvalidArgumentException
     */
    public function executeFull()
    {
        $userFunctions = [];
        foreach ($this->dimensionProvider->getIterator() as $dimension) {
            $userFunctions[] = function () use ($dimension) {
                $this->executeByDimensions($dimension);
            };
        }
        $this->processManager->execute($userFunctions);
        $this->fulltextResource->resetSearchResults();
        $this->searchRequestConfig->reset();
    }

    /**
     * Execute partial indexation by ID list
     *
     * @param int[] $ids
     * @return void
     */
    public function executeList(array $ids)
    {
        $this->execute($ids);
    }

    /**
     * Execute partial indexation by ID
     *
     * @param int $id
     * @return void
     */
    public function executeRow($id)
    {
        $this->execute([$id]);
    }
}
