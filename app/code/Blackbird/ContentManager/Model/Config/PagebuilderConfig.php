<?php
/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */

declare(strict_types=1);

namespace Blackbird\ContentManager\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\Manager;
use Magento\Store\Model\ScopeInterface;

class PagebuilderConfig
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Framework\Module\Manager
     */
    private $moduleManager;

    /**
     * Pagebuilder constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Module\Manager $moduleManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Manager $moduleManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->moduleManager = $moduleManager;
    }

    /**
     * @return bool
     */
    public function isPagebuilderEnable():bool
    {
        return $this->isPagebuilderModuleEnable() && $this->isPagebuilderConfigEnable() && $this->isPagebuilderAcmEnable();
    }

    /**
     * @return bool
     */
    public function isPagebuilderModuleEnable():bool
    {
        return $this->moduleManager->isEnabled('Magento_PageBuilder');
    }

    /**
     * @return bool
     */
    public function isPagebuilderConfigEnable():bool
    {
        return (bool)$this->scopeConfig->getValue('cms/pagebuilder/enabled', ScopeInterface::SCOPE_STORE);
    }

    /**
     *
     */
    public function isPagebuilderAcmEnable():bool
    {
        return (bool)$this->scopeConfig->getValue('blackbird_contentmanager/pagebuilder/enabled', ScopeInterface::SCOPE_STORE);
    }
}
