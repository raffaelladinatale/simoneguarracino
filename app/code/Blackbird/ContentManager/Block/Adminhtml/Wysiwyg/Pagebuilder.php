<?php
/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */

declare(strict_types=1);

namespace Blackbird\ContentManager\Block\Adminhtml\Wysiwyg;

use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Data\Form\Element\Editor;
use Magento\Framework\Escaper;
use Magento\Framework\Module\Manager;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Data\Form\Element\Factory;

class Pagebuilder extends Editor
{
    /**
     * @var \Magento\PageBuilder\Model\Stage\Config
     */
    private $stageConfig;
    /**
     * @var array
     */
    private $data;

    /**
     * Pagebuilder constructor.
     * @param \Magento\Framework\Data\Form\Element\Factory $factoryElement
     * @param \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        ObjectManagerInterface $objectManager,
        Manager $moduleManager,
        $data = [],
        Json $serializer = null
    ) {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data, $serializer);
        if ($moduleManager->isEnabled('Magento_PageBuilder')){
            $this->stageConfig = $objectManager->create('Magento\PageBuilder\Model\Stage\Config');
        }
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getElementHtml():string
    {
        $html = '<div class="admin__field-control admin__control-wysiwig pagebuilder-field-content" id="'.$this->getName().'">';
        $html .= '<div class="admin__field" data-bind="scope:\'' . $this->getUniqComponentName() . '\'" data-index="index">';
        $html .= '<!-- ko foreach: elems() -->';
        $html .= '<textarea name="' . $this->data['name'] . '" title="" data-bind="value: value" style="display: none" data-ui-id="form-element-' . $this->_escaper->escapeHtml($this->getName()) . '"></textarea>';
        $html .= '<!-- ko template: elementTmpl --><!-- /ko -->';
        $html .= '<!-- /ko -->';
        $html .= '</div></div>';

        $html .= $this->getAfterElementHtml();
        return $html;
    }

    /**
     * @return mixed|string
     */
    public function getAfterElementHtml()
    {
        $config = $this->data;
        $config['component'] = 'Magento_PageBuilder/js/form/element/wysiwyg';

        // Override the templates to include Magento Pagebuilder KnockoutJS code
        $config['template'] = 'Magento_PageBuilder/form/element/wysiwyg';
        $config['elementTmpl'] = 'Magento_PageBuilder/form/element/wysiwyg';
        $config['content'] =  '';
        $config['value'] = $this->getValue();
        $config['wysiwygId'] = $this->getName();
        $config['formElement'] = 'wysiwyg';
        $config['wysiwyg'] = 'true';

        $wysiwygConfigData = $this->stageConfig->getConfig();
        $wysiwygConfigData['activeEditorPath'] = 'Magento_PageBuilder/pageBuilderAdapter';
        $config['wysiwygConfigData'] = isset($config['wysiwygConfigData']) ?
            array_replace_recursive($config['wysiwygConfigData'], $wysiwygConfigData) :
            $wysiwygConfigData;

        return '<script type="text/x-magento-init">
            {
                "*": {
                    "Magento_Ui/js/core/app": {
                        "components": {
                            "' . $this->getUniqComponentName() . '": {
                                "component": "uiComponent",
                                "children": {
                                    "content_editor": ' . \json_encode($config) . '
                                }
                            }
                        }
                    }
                }
            }
        </script>';
    }

    /**
     * @return string
     */
    protected function getUniqComponentName():string
    {
        return 'content_pagebuilder_' . $this->data['name'];
    }
}
