<?php
/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */

declare(strict_types=1);

namespace Blackbird\ContentManager\Block\Adminhtml\System\Config;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;

class Pagebuilder extends Template
{
    /**
     * @var \Blackbird\ContentManager\Model\Config\PagebuilderConfig
     */
    private $pagebuilderConfig;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * Pagebuilder constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Blackbird\ContentManager\Model\Config\PagebuilderConfig $pagebuilderConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Blackbird\ContentManager\Model\Config\PagebuilderConfig $pagebuilderConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->pagebuilderConfig = $pagebuilderConfig;
        $this->productMetadata = $productMetadata;
    }



    /**
     * @return array
     */
    public function getMessage(): array
    {
        $message = [
            'type' => 'success',
            'text' => __('You can enable PageBuilder on WYSIWYG if needed')
        ];

        if($this->productMetadata->getEdition()==='Community'){
            $message['text'] = __('PageBuilder compatibility is NOT AVAILABLE because you are using MAGENTO COMMUNITY EDITION which is not including PageBuilder.');
        }elseif (!$this->pagebuilderConfig->isPagebuilderConfigEnable() && !$this->pagebuilderConfig->isPagebuilderModuleEnable()) {
            $message['text'] = __('You can\'t enable PageBuilder on ACM\'s WYSIWYG. Magento\'s Pagebuilder module and config are disabled.');
        } elseif (!$this->pagebuilderConfig->isPagebuilderConfigEnable()) {
            $message['text'] =  __('You can\'t enable PageBuilder on ACM\'s WYSIWYG. Magento\'s Pagebuilder config is disabled.');
        } elseif (!$this->pagebuilderConfig->isPagebuilderModuleEnable()) {
            $message['text'] = __('You can\'t enable PageBuilder on ACM\'s WYSIWYG. Magento\'s Pagebuilder module is disabled.');
        }

        if(!$this->pagebuilderConfig->isPagebuilderConfigEnable() || !$this->pagebuilderConfig->isPagebuilderModuleEnable()){
            $message['type'] = 'error';
        }
        return $message;
    }
}
