<?php
/**
 * Blackbird ContentManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category        Blackbird
 * @package         Blackbird_ContentManager
 * @copyright       Copyright (c) 2018 Blackbird (https://black.bird.eu)
 * @author          Blackbird Team
 * @license         https://www.advancedcontentmanager.com/license/
 */

namespace Blackbird\ContentManager\Block\Adminhtml\Content;

use Blackbird\ContentManager\Model\ResourceModel\Content\Collection;
use Magento\Backend\Block\Store\Switcher;

/**
 * Content edit form block
 *
 * Class Edit
 *
 * @package Blackbird\ContentManager\Block\Adminhtml\Content
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Default block template
     *
     * @var string
     */
    protected $_template = 'Blackbird_ContentManager::content/edit.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var Collection
     */
    protected $_contentCollection;

    /**
     * Edit constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Collection $contentCollection
     * @param \Magento\Framework\UrlInterface $urlManager
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        Collection $contentCollection,
        array $data = []
    ) {
        $this->_contentCollection = $contentCollection;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Get Save Split Button html
     *
     * @return string
     */
    public function getSaveSplitButtonHtml()
    {
        return $this->getChildHtml('save-split-button');
    }

    /**
     * Get save and continue button Url
     *
     * @return string
     */
    public function getSaveAndContinueUrl()
    {
        return $this->getUrl(
            'contentmanager/*/save',
            ['_current' => true, 'back' => 'edit', 'tab' => '{{tab_id}}', 'active_tab' => null]
        );
    }

    /**
     * Get duplicate button Url
     *
     * @return string
     */
    public function getDuplicateUrl()
    {
        return $this->getUrl('contentmanager/*/duplicate', ['_current' => true]);
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        $contentTypeId = $this->isContentNew()
            ? $this->getRequest()->getParam('ct_id')
            : $this->getContent()
                ->getCtId();

        return $this->getUrl('contentmanager/*/', ['ct_id' => $contentTypeId]);
    }

    /**
     * Check if a content has more than one translation
     *
     * @return bool
     */
    public function hasMoreThanOneTranslation()
    {
        return count($this->getContent()->getStoreIds())>=2;
    }

    /**
     * Check whether new content is being created
     *
     * @return bool
     */
    public function isContentNew()
    {
        $content = $this->getContent();
        return (!$content || !$content->getId());
    }

    /**
     * Retrieve currently edited content object
     *
     * @return \Blackbird\ContentManager\Model\Content
     */
    public function getContent()
    {
        return $this->_coreRegistry->registry('current_content');
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_content';
        $this->_blockGroup = 'Blackbird_ContentManager';

        if (!$this->isContentNew()) {
            if ($this->hasMoreThanOneTranslation()) {
                $this->addButton('delete_translation', [
                    'label' => __('Delete Translation'),
                    'class' => 'delete',
                    'onclick' => 'deleteConfirm(\'Are you sure you want to delete this translation ?\', \'' . $this->getUrl(
                        'contentmanager/*/delete',
                        [
                                'id' => $this->getContentId(),
                                'store' => $this->getRequest()->getParam('store', 0),
                            ]
                    ) . '\')',
                ], 0, 30);
            }

            if ($this->translationExistsForSelectedStore()) {
                $this->addButton('preview', [
                    'class' => 'action-secondary',
                    'label' => __('Preview'),
                    'onclick' => 'window.open(\'' . $this->getPreviewUrl() . '\', \'_blank\')',
                ]);
            } elseif ($this->getSelectedStoreId()===0) {
                $storeId= $this->getFirstStoreIdWithTranslation();
                if ($storeId!==null) {
                    $this->addButton('preview', [
                        'class' => 'action-secondary',
                        'label' => __('Go to translation "Store View ' . $this->_storeManager->getStore($storeId)->getName() . '"'),
                        'onclick' => 'window.open(\'' . $this->getPreviewUrl() . '\', \'_self\')',
                    ]);
                }
            }
        }

        parent::_construct();

        $this->removeButton('save');
    }

    /**
     * Retrieve content ID
     *
     * @return int
     */
    public function getContentId()
    {
        return $this->getContent()->getId();
    }

    /**
     * Return true if translation exists for selected store.
     */
    public function translationExistsForSelectedStore()
    {
        return ($this->getContent()->existsForStore($this->getSelectedStoreId()) || $this->getContent()->existsForStore(0)); //0 = all stores
    }

    /**
     * Return selected store id from store switcher
     *
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSelectedStoreId()
    {
        /**
         * @var Switcher $storeSwitcher
         */
        $storeSwitcher = $this->getLayout()->getBlock('store_switcher');
        return $storeSwitcher->getStoreId();
    }

    /**
     * Return first Store Id With Translation
     *
     * @return int|null
     */
    private function getFirstStoreIdWithTranslation()
    {
        $content = $this->getContent();
        $storeId = $content->getStoreIds();
        if (count($storeId)>=1) {
            if ($content->getUrl() === null) { //if content translation does not exists for all store
                //get the store of the first translation that exists
                $storeId = array_shift($storeId);
            } else {
                //Get default store view.
                $store = $this->_storeManager->getDefaultStoreView();
                if (null !== $store) {
                    $storeId = $store->getId();
                } else {
                    $stores = $this->_storeManager->getStores();
                    $store = array_shift($stores);
                    $storeId = $store->getId();
                }
            }
        } else {
            $storeId = null;
        }
        return $storeId;
    }

    /**
     * Get URL for preview button
     *
     * @return string
     */
    public function getPreviewUrl()
    {
        $url = '';
        $storeId = $this->getSelectedStoreId();
        $content = $this->getContent();
        if ($storeId===0) { //if content is from all store
            $storeId = $this->getFirstStoreIdWithTranslation();
            if ($content->getUrl() === null) { //if content translation does not exists for all store
                /**
                 * @var Switcher $storeSwitcher
                 */
                $storeSwitcher = $this->getLayout()->getBlock('store_switcher');
                $url = $storeSwitcher->getSwitchUrl();
                $url .= "store/" . $storeId . "/";
                return $url;
            }
        }
        if (!$this->isContentNew()) {
            $url = $content->getLinkUrl($storeId, true);
            return $url;
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        $this->getToolbar()->addChild('save-split-button', 'Magento\Backend\Block\Widget\Button\SplitButton', [
            'id' => 'save-split-button',
            'label' => __('Save'),
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                ],
            ],
            'class_name' => 'Magento\Backend\Block\Widget\Button\SplitButton',
            'button_class' => 'widget-button-save',
            'options' => $this->_getSaveSplitButtonOptions(),
        ]);

        return parent::_prepareLayout();
    }

    /**
     * Get dropdown options for save split button
     *
     * @return array
     */
    protected function _getSaveSplitButtonOptions()
    {
        $options = [];

        $options[] = [
            'id' => 'new-button',
            'label' => __('Save & New'),
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndNew', 'target' => '#edit_form'],
                ],
            ],
        ];

        $options[] = [
            'id' => 'duplicate-button',
            'label' => __('Save & Duplicate'),
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndDuplicate', 'target' => '#edit_form'],
                ],
            ],
        ];

        $options[] = [
            'id' => 'close-button',
            'label' => __('Save & Close'),
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'saveAndClose', 'target' => '#edit_form']],
            ],
        ];

        return $options;
    }
}
