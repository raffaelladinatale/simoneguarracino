<?php
/**
 * Blackbird MenuManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @content            Blackbird
 * @package		Blackbird_MenuManager
 * @copyright           Copyright (c) 2016 Blackbird (http://black.bird.eu)
 * @author		Blackbird Team
 */
namespace Blackbird\MenuManager\Block\NodeType;

use Blackbird\MenuManager\Api\Data\NodeInterface;
use Magento\Backend\Block\Template\Context;

class Content extends AbstractNodeTypeFront implements NodeInterface
{
    protected $nodes;

    /**
     * @var \Blackbird\ContentManager\Model\ResourceModel\Content\CollectionFactory
     */
    protected $_contentCollectionFactory;

    /**
     * @var \Blackbird\ContentManager\Model\ContentFactory $_contentFactory
     */
    protected $_contentFactory;

    protected function getContentFactoryFactory()
    {
        if(!$this->_contentCollectionFactory){
            $this->_contentCollectionFactory = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Blackbird\ContentManager\Model\ResourceModel\Content\CollectionFactory::class);
        }
        return $this->_contentCollectionFactory;
    }

    /**
     * @param array $nodes
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function fetchData(array $nodes)
    {
        $localNodes = [];
        $contentIds = [];
        foreach ($nodes as $node) {
            $localNodes[$node->getId()] = $node;
            $contentIds[$node->getId()] = $node->getEntityId();
        }

        /**
         * Load only one time a collection of contents
         */
        $contentCollection = $this->getContentFactoryFactory()->create()
            ->addFieldToFilter('entity_id', ['in' => $contentIds])
            ->addFieldToSelect('url_key');

        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        foreach ($contentIds as $nodeId => $contentId) {
            $content = $contentCollection->getItemById($contentId);
            if ($content) {
                $url =  $baseUrl . $content->getUrl();
                if($url == '') {
                    $url = $baseUrl;
                }

                $localNodes[$nodeId]->setUrlPath($url);
            }
        }


        $this->nodes = $localNodes;
    }

    /**
     * @param $nodeId
     * @param $level
     * @param $classes
     * @param $childrenHtml
     * @param $childrenArray
     * @return string
     */
    public function getHtml($nodeId, $level, $classes, $childrenHtml, $childrenArray, $storeId)
    {
        $node = $this->nodes[$nodeId];

        $url = $node->getData('url_path');
        $this->setUrl($url); // Really set the url for the front node

        $classes = $this->getIsActiveClass($url, $node, $classes, $childrenArray);

        return parent::getHtml($nodeId, $level, $classes, $childrenHtml, $childrenArray, $storeId);
    }
}