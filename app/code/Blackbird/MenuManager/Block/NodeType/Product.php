<?php
/**
 * Blackbird MenuManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @product            Blackbird
 * @package		Blackbird_MenuManager
 * @copyright           Copyright (c) 2016 Blackbird (http://black.bird.eu)
 * @author		Blackbird Team
 */
namespace Blackbird\MenuManager\Block\NodeType;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\NoSuchEntityException;

class Product extends AbstractNodeTypeFront
{
    protected $nodes;

    /**
     * @var \Magento\Catalog\Helper\Product
     */
    protected $_catalogProductHelper;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var  \Magento\Catalog\Model\Product\Url
     */
    protected $_productUrl;

    /**
     * Product constructor.
     *
     * @param Context $context
     * @param \Magento\Catalog\Helper\Product $catalogProductHelper
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Url $productUrl
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Catalog\Helper\Product $catalogProductHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Url $productUrl,
        $data = []
    ) {
        $this->_catalogProductHelper = $catalogProductHelper;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productUrl = $productUrl;
        parent::__construct($context, $data);
    }


    /**
     * @param array $nodes
     * @throws NoSuchEntityException
     */
    public function fetchData(array $nodes)
    {
        $localNodes = [];
        $productSkus = [];
        foreach ($nodes as $node) {
            $localNodes[$node->getId()] = $node;
            $productSkus[$node->getId()] = $node->getEntityId();
        }

        /**
         * Load only one time a collection of products
         */
        $productCollection = $this->_productCollectionFactory->create()
            ->addFieldToFilter('sku', ['in' => array_values($productSkus)])
            ->addFieldToSelect('url_key');

        $params = [
            '_ignore_category' => 1,
        ];

        foreach ($productSkus as $nodeId => $productSku) {
            $product = $productCollection->getItemByColumnValue("sku", $productSku);

            if($product) {
                $url = $this->_productUrl->getUrl($product, $params);
                if($url == '') {
                    $url = $this->_storeManager->getStore()->getBaseUrl();
                }

                $localNodes[$nodeId]->setUrlPath($url);
            }
        }

        $this->nodes = $localNodes;
    }

    /**
     * @param $nodeId
     * @param $level
     * @param $classes
     * @param $childrenHtml
     * @param $childrenArray
     * @return string
     */
    public function getHtml($nodeId, $level, $classes, $childrenHtml, $childrenArray, $storeId)
    {
        $node = $this->nodes[$nodeId];

        $url = $node->getData('url_path');
        $this->setUrl($url); // Really set the url for the front node

        $classes = $this->getIsActiveClass($url, $node, $classes, $childrenArray);

        return parent::getHtml($nodeId, $level, $classes, $childrenHtml, $childrenArray, $storeId);
    }
}