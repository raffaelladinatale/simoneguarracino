<?php
/**
 * MassDelete
 *
 * @copyright Copyright © 2018 Blackbird. All rights reserved.
 * @author    etienne (Blackbird Team)
 */

namespace Blackbird\MenuManager\Controller\Adminhtml\Menu;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;

class MassDelete extends Action
{

    /**
     * @var Filter
     */
    private $_filter;

    /**
     * @var \Blackbird\MenuManager\Model\ResourceModel\Menu\CollectionFactory
     */
    private $_menuCollectionFactory;

    /**
     * @var \Blackbird\MenuManager\Model\MenuRepository
     */
    private $_menuRepository;

    /**
     * MassDelete constructor.
     *
     * @param Filter $filter
     * @param \Blackbird\MenuManager\Model\ResourceModel\Menu\CollectionFactory $menuCollectionFactory
     * @param \Blackbird\MenuManager\Model\MenuRepository $menuRepository
     * @param Context $context
     */
    public function __construct(
        Filter $filter,
        \Blackbird\MenuManager\Model\ResourceModel\Menu\CollectionFactory $menuCollectionFactory,
        \Blackbird\MenuManager\Model\MenuRepository $menuRepository,
        Context $context
    )
    {
        $this->_filter = $filter;
        $this->_menuCollectionFactory = $menuCollectionFactory;
        $this->_menuRepository = $menuRepository;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $menuCollection = $this->_filter->getCollection($this->_menuCollectionFactory->create());
        $collectionSize = $menuCollection->getSize();
        foreach($menuCollection as $menu) {
            $this->_menuRepository->delete($menu);
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 menu(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}