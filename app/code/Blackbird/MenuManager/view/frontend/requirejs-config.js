/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            blackbirdMenu: "Blackbird_MenuManager/js/menumanager-main-js"
        }
    }
};
