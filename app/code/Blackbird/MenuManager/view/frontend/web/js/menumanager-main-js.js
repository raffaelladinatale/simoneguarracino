define([
    'jquery',
    'matchMedia',
    'jquery/ui',
    'jquery/jquery.mobile.custom',
    'mage/translate',
    'mage/menu'
], function ($, mediaCheck) {
    'use strict';

    $.widget('blackbird.menu', $.mage.menu, {
        options: {
            responsive: false,
            expanded: false,
            showDelay: 42,
            hideDelay: 300,
            delay: 300,
            mediaBreakpoint: '(max-width: 768px)',
            additionalMenu: false
        },

        /**
         * Toggle.
         */
        toggle: function () {
            //Fix problem when this event listener is set twice on a page and prevented to close the mobile menu.
            if(!this.options.additionalMenu) {
                var html = $('html');

                if (html.hasClass('nav-open')) {
                    html.removeClass('nav-open');
                    setTimeout(function () {
                        html.removeClass('nav-before-open');
                    }, this.options.hideDelay);
                } else {
                    html.addClass('nav-before-open');
                    setTimeout(function () {
                        html.addClass('nav-open');
                    }, this.options.showDelay);
                }
            }
        },

        /**
         * @param {jQuery.Event} event
         */
        expand: function (event) {
            var realTarget = $(event.target).closest('.ui-menu-item').children('.ui-menu').first();

            if (realTarget && realTarget.length) {
                if (realTarget.closest('.ui-menu').is(':visible')) {
                    return;
                }

                // remove the active state class from the siblings
                this.active.siblings().children('.ui-state-active').removeClass('ui-state-active');

                this._open(realTarget);
            }
        },

        /**
         * @private
         */
        _toggleMobileMode: function () {
            var subMenus;

            $(this.element).off('mouseenter mouseleave');
            this._on({
                /**
                 * @param {jQuery.Event} event
                 */
                'click .ui-menu-item:has(a)': function (event) {
                    var target;

                    event.preventDefault();
                    target = $(event.target).closest('.ui-menu-item');

                    //add condition "!target.hasClass('no-link-to-first-child')" to not link to "undefined" url if the group item is not linking to url
                    if ((!target.hasClass('level-top') || !target.has('.ui-menu').length) && !target.hasClass('no-link-to-first-child')) {
                        window.location.href = target.find('> a').attr('href');
                    }
                },

                /**
                 * @param {jQuery.Event} event
                 */
                'click .ui-menu-item:has(.ui-state-active)': function (event) {
                    this.collapseAll(event, true);
                }
            });
        },


    });

    return $.blackbird.menu
});
