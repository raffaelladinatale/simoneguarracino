<?php
/**
 * MenuSingleton
 *
 * @copyright Copyright © 2018 Blackbird. All rights reserved.
 * @author    etienne (Blackbird Team)
 */

namespace Blackbird\MenuManager\Helper;


use Magento\Framework\App\Helper\AbstractHelper;

class MenuSingleton extends AbstractHelper
{
    private $hasBeenUsed = false;

    /**
     * @return bool
     */
    public function hasBeenUsed()
    {
        return $this->hasBeenUsed;
    }

    /**
     * @param $used
     * @return mixed
     */
    public function setHasBeenUsed($used)
    {
        return $this->hasBeenUsed = $used;
    }
}