<?php
/**
 * Blackbird MenuManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category            Blackbird
 * @package        Blackbird_MenuManager
 * @copyright           Copyright (c) 2016 Blackbird (http://black.bird.eu)
 * @author        Blackbird Team
 */

namespace Blackbird\MenuManager\Model\Menu\NodeTypes;

class Config extends \Magento\Framework\Config\Data
{
    /**
     * @var \Magento\Framework\Module\Manager;
     */
    private $_moduleManager;

    /**
     * @var \Magento\Framework\Module\PackageInfo
     */
    private $_packageInfo;

    /**
     * Config constructor.
     *
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Framework\Module\PackageInfo $packageInfo
     * @param Config\Reader $reader
     * @param \Magento\Framework\Config\CacheInterface $cache
     * @param string $cacheId
     */
    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\Module\PackageInfo $packageInfo,
        \Blackbird\MenuManager\Model\Menu\NodeTypes\Config\Reader $reader,
        \Magento\Framework\Config\CacheInterface $cache,
        $cacheId = 'menu_node_type'
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_packageInfo = $packageInfo;
        parent::__construct($reader, $cache, $cacheId);
    }

    /**
     * Get configuration of node type by name
     *
     * @param string $name
     * @return array
     */
    public function getField($name)
    {
        return $this->get($name, []);
    }

    /**
     * Get configuration of all registered node types
     *
     * @return array
     */
    public function getAll()
    {
        $nodeTypes = $this->get();

        foreach ($nodeTypes as $nodeType) {
            if (isset($nodeType['dependencies'])) {
                foreach ($nodeType['dependencies'] as $dependency) {
                    $moduleName = $dependency['module_name'];
                    $moduleVersion = $dependency['version'];
                    if ($this->_moduleManager->isEnabled($moduleName)) {
                        if (!version_compare($this->_packageInfo->getVersion($moduleName), $moduleVersion, '>=')) {
                            unset($nodeTypes[$nodeType['name']]);
                            break;
                        }
                    } else {
                        unset($nodeTypes[$nodeType['name']]);
                        break;
                    }
                }
            }
        }

        return $nodeTypes;
    }
}
