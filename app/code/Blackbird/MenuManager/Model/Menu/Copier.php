<?php
/**
 * Blackbird MenuManager Module
 *
 * NOTICE OF LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@bird.eu so we can send you a copy immediately.
 *
 * @category            Blackbird
 * @package		Blackbird_MenuManager
 * @copyright           Copyright (c) 2016 Blackbird (http://black.bird.eu)
 * @author		Blackbird Team
 */
namespace Blackbird\MenuManager\Model\Menu;

use Blackbird\MenuManager\Model\MenuRepository;
use Blackbird\MenuManager\Model\ResourceModel\Menu\Node\CollectionFactory as NodeCollectionFactory;
use Blackbird\MenuManager\Model\MenuFactory;
use Blackbird\MenuManager\Api\MenuRepositoryInterface;

class Copier
{
    /**
     * @var NodeCollectionFactory
     */
    private $nodeCollectionFactory;

    /**
     * @var MenuFactory
     */
    private $menuFactory;

    /**
     * @var MenuRepositoryInterface
     */
    private $menuRepository;

    /**
     * Copier constructor.
     *
     * @param \Blackbird\MenuManager\Model\ResourceModel\Menu\Node\CollectionFactory $nodeCollectionFactory
     * @param \Blackbird\MenuManager\Model\MenuFactory $menuFactory
     * @param \Blackbird\MenuManager\Model\MenuRepository $menuRepository
     */
    public function __construct(
        NodeCollectionFactory $nodeCollectionFactory,
        MenuFactory $menuFactory,
        MenuRepository $menuRepository
    )
    {
        $this->nodeCollectionFactory = $nodeCollectionFactory;
        $this->menuFactory = $menuFactory;
        $this->menuRepository = $menuRepository;
    }

    /**
     * @param int $menuId
     * @param array $data
     * @return mixed
     */
    public function copy(int $menuId, array $data)
    {
        $duplicateNodes = $this->nodeCollectionFactory->create()
            ->addFieldToFilter('menu_id', ['eq' => $menuId])
            ->addOrder('level', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);

        $newMenu = $this->menuFactory->create();
        $newMenu->setData($data);
        $newMenu->setId(null); //reset the id so we can have a new menu
        $newMenu->setIsActive(0); //set the menu as disabled

        //Save the menu entity as a new menu
        $this->menuRepository->save($newMenu);
        $newMenuId = $newMenu->getId();

        //init of the tmp array of nodes
        $oldNodesParents = [];

        //Creation of the new nodes
        foreach ($duplicateNodes as $duplicateNode) {
            $oldNodesParents[$duplicateNode->getId()] = $duplicateNode->getData('parent_id');
            $tmpId = $duplicateNode->getId();
            $duplicateNode->setId(null);
            $duplicateNode->setMenuId($newMenuId);

            //Change node parent_id if necessary
            $parentId = $duplicateNode->getParentId();
            if ($parentId) {
                $duplicateNode->setParentId($oldNodesParents[$parentId]);
            }

            $oldNodesParents[$tmpId] = $duplicateNode->save()->getId();
        }

        return $newMenuId;
    }
}