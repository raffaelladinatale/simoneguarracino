<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Ui\Component\Listing\Column;

use \Magento\Framework\View\Element\UiComponent\ContextInterface as Context;
use \Magento\Framework\View\Element\UiComponentFactory as UiComponent;
use \Magento\Tax\Model\Calculation\Rate;

/**
 * Class Size
 */
class TaxRate extends \Magento\Ui\Component\Listing\Columns\Column
{

    /**
     *
     * @var \Magento\Tax\Model\Calculation\Rate
     */
    private $rateModel;

    public function __construct(
        Context $context,
        UiComponent $uiComponent,
        Rate $rate,
        array $components = [],
        array $data = []
    ) {
        $this->rateModel = $rate;
        parent::__construct($context, $uiComponent, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['rate_id'])) {
                    $this->loadRate($item['rate_id']);
                    $item['rate_id'] = $this->rateModel->getCode();
                }
            }
        }

        return $dataSource;
    }

    private function loadRate($rate_id)
    {
        $this->rateModel->getResource()->load($this->rateModel, $rate_id);
    }
}
