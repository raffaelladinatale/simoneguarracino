<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Ui\Component\Listing\Column;

/**
 * Class Size
 */
class Status extends \Magento\Ui\Component\Listing\Columns\Column
{

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $names = $this->getDataNames();
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['status']) && isset($names[$item['status']])) {
                    $item['status'] = $names[$item['status']];
                }
            }
        }

        return $dataSource;
    }

    private function getDataNames()
    {
        return [
            \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_PENDING => __('Pending'),
            \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_RUNNING => __('Running'),
            \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_STOPPED => __('Stopped'),
            \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_SUCCESS => __('Success'),
            \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_UPLOADED => __('Uploaded')
        ];
    }
}
