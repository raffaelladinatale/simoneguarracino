<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class TestActions
 */
class PaymentsActions extends Column
{

    /**
     * Url path
     */
    const URL_PATH_EDIT = 'daneaeasyfatt/payments/edit';
    const URL_PATH_DELETE = 'daneaeasyfatt/payments/delete';

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {

        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['daneaeasyfatt_payments_id'])) {
                    $item[$this->getData('name')] = [
                        'import' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                ['daneaeasyfatt_payments_id' => $item['daneaeasyfatt_payments_id']]
                            ),
                            'label' => __('Edit')
                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                ['daneaeasyfatt_payments_id' => $item['daneaeasyfatt_payments_id']]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete "%1"', '${ $.$data.tipo_shop }'),
                                'message' =>
                                __('Are you sure you wan\'t to delete a "%1" record?', '${ $.$data.tipo_shop }')
                            ]
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
