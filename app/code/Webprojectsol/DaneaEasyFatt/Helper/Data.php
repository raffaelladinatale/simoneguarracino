<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const XML_FEEDBACK = 'wpbase/feedback/daneaeasyfatt';

    public $showFeedback = false;
    private $sfbGoUrl = 'http://www.webprojectsol.com/it/review/product/list/id/2/#review-form';

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     * @var Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    public function __construct(
    \Magento\Framework\App\Helper\Context $context, \Magento\Backend\Model\Session $backendSession, \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    )
    {
        $this->backendSession = $backendSession;
        $this->configWriter = $configWriter;
        $this->urlBuilder = $context->getUrlBuilder();
        parent::__construct(
            $context
        );

        if ($this->backendSession->getShowFeedback()) {
            return;
        }

        if ($this->canShowHelp()) {
            $date = $this->scopeConfig->getValue(self::XML_FEEDBACK, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if (!$date) {
                $this->configWriter->save(self::XML_FEEDBACK, date('Y-m-d', strtotime('+3 days')));
//	    } elseif ($date == 'writedOrCancelled') {
            } elseif ($date <= date('Y-m-d')) {
                $this->showFeedback = true;
            }
        }
    }

    public function canShowHelp()
    {
        return $this->_request->getModuleName() == 'daneaeasyfatt' || ($this->_request->getControllerName() == 'system_config' && ($this->_request->getParam('section') == 'importcatalog' || $this->_request->getParam('section') == 'exportorders'));
    }

    public function hideFeedback()
    {
        $this->backendSession->setShowFeedback(true);
        $this->configWriter->save(self::XML_FEEDBACK, date('Y-m-d', strtotime('+3 days')));
    }

    public function cancelFeedback()
    {
        $this->backendSession->setShowFeedback(true);
        $this->configWriter->save(self::XML_FEEDBACK, 'writedOrCancelled');
    }

    public function showFeeback()
    {
        return $this->showFeedback;
    }

    public function getSfbGoUrl()
    {
        return $this->sfbGoUrl;
    }

    public static function _convertByteSize($size)
    {
        if ($size > 1048576) {
            return number_format(round($size / 1048576, 1), 1) . ' MB';
        } elseif ($size > 1024) {
            return number_format(round($size / 1024)) . ' kB';
        }
        return number_format($size) . ' bytes';
    }

    public static function _parseXml($xml)
    {
        return json_decode(
            json_encode(
                new \SimpleXMLElement($xml)
            )
            , true);
    }

    public static function _SecondsToWords($tempo)
    {
        $ore = floor($tempo / 3600);
        $tempo %= 3600;
        $minuti = floor($tempo / 60);
        $tempo %= 60;
        $secondi = $tempo;
        return ($ore ? $ore . ($ore == 1 ? ' ora:' : ' ore:') : '') . ($minuti ? $minuti . ($minuti == 1 ? ' minuto:' : ' minuti:') : '') . ($secondi ? $secondi . ($secondi == 1 ? ' secondo' : ' secondi') : '');
    }

    public static function _JsonDecode($json, $assoc = false)
    {
        $decode = json_decode($json, $assoc);
        if (!$decode) {
            $decode = json_decode(stripslashes($json), $assoc);
        }
        return $decode;
    }

    public static function getExceptionMessage(\Exception $e)
    {
        return __('PHP ' . $e->getCode() . ': ' . $e->getMessage() . ' in ' . $e->getFile() . ' on line ' . $e->getLine());
    }
}
