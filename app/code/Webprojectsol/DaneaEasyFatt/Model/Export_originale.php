<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model;

class Export extends \Magento\Framework\Model\AbstractModel
{

    const XML_PATH_ACCESS_DATA_LOGIN = 'exportorders/access_data/login';
    const XML_PATH_ACCESS_DATA_PASSWORD = 'exportorders/access_data/password';
    const XML_PATH_EXTRA_AS_ROWS = 'exportorders/extra/extra_as_rows';
    const XML_PATH_COST_DESCRIPTION = 'exportorders/extra/cost_description';
    const XML_PATH_CF = 'exportorders/extra/cf';
    const XML_PATH_FE = 'exportorders/extra/fe';
    const XML_PATH_COMMENT = 'exportorders/extra/comment';
    const XML_PATH_DOC_TYPE = 'exportorders/doc/type';
    const XML_PATH_ORDER_STATUS_EXPORT = 'exportorders/order_status/export';
    const XML_PATH_USE_VARIANT = 'exportorders/varianti/use_variant';
    const XML_PATH_VARIANT_2TO1 = 'exportorders/varianti/variant_2to1';
    const XML_PATH_NAME_TAGLIA_COLORE = 'exportorders/varianti/name_taglia_colore';
    const XML_PATH_NAME_TAGLIA = 'exportorders/varianti/name_taglia';
    const XML_PATH_NAME_COLORE = 'exportorders/varianti/name_colore';

    /**
     * Codice aliquota fuori campo IVA di EasyFatt
     */
    const ALIQUOTA_FUORI_CAMPO_IVA = 'FC';

    private $config = array();
//    private $taxCollection = array();
    private $includeTax = false;
    private $getPrice = '';
    private $order_totals = array();
    private $currencyRates;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Tax\Model\ConfigFactory
     */
    protected $taxConfigFactory;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $directoryCurrencyFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $salesResourceModelOrderCollectionFactory;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $salesOrderFactory;

    /**
     * @var \Magento\Sales\Model\Order\AddressFactory
     */
    protected $salesOrderAddressFactory;

    /**
     * @var \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory
     */
    protected $taxResourceModelSalesOrderTaxCollectionFactory;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $directoryRegionFactory;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $directoryCountryFactory;

    /**
     * @var \Magento\Sales\Model\Order\ShipmentFactory
     */
    protected $salesOrderShipmentFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Tax\ItemFactory
     */
    protected $salesResourceModelOrderTaxItemFactory;

    /**
     * @var \Magento\Sales\Model\Order\TaxFactory
     */
    protected $salesOrderTaxFactory;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\Taxes
     */
    protected $taxesModel;

    /**
     * @var \Magento\Tax\Model\Calculation\Rate
     */
    protected $rateModel;

    public function __construct(
    \Magento\Framework\Model\Context $context, \Magento\Backend\App\Action\Context $contextAction, \Magento\Framework\Registry $registry, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Tax\Model\ConfigFactory $taxConfigFactory, \Magento\Directory\Model\CurrencyFactory $directoryCurrencyFactory, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesResourceModelOrderCollectionFactory, \Magento\Sales\Model\OrderFactory $salesOrderFactory, \Magento\Sales\Model\Order\AddressFactory $salesOrderAddressFactory, \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory $taxResourceModelSalesOrderTaxCollectionFactory, \Magento\Directory\Model\RegionFactory $directoryRegionFactory, \Magento\Directory\Model\CountryFactory $directoryCountryFactory, \Magento\Sales\Model\Order\ShipmentFactory $salesOrderShipmentFactory, \Magento\Sales\Model\ResourceModel\Order\Tax\ItemFactory $salesResourceModelOrderTaxItemFactory, \Magento\Sales\Model\Order\TaxFactory $salesOrderTaxFactory, \Webprojectsol\DaneaEasyFatt\Logger\Logger $logger, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Webprojectsol\DaneaEasyFatt\Model\Taxes $taxesModel, \Magento\Tax\Model\Calculation\Rate $rateModel, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->request = $contextAction->getRequest();
        $this->objectManager = $contextAction->getObjectManager();
        $this->taxConfigFactory = $taxConfigFactory;
        $this->directoryCurrencyFactory = $directoryCurrencyFactory;
        $this->salesResourceModelOrderCollectionFactory = $salesResourceModelOrderCollectionFactory;
        $this->salesOrderFactory = $salesOrderFactory;
        $this->salesOrderAddressFactory = $salesOrderAddressFactory;
        $this->taxResourceModelSalesOrderTaxCollectionFactory = $taxResourceModelSalesOrderTaxCollectionFactory;
        $this->directoryRegionFactory = $directoryRegionFactory;
        $this->directoryCountryFactory = $directoryCountryFactory;
        $this->salesOrderShipmentFactory = $salesOrderShipmentFactory;
        $this->salesResourceModelOrderTaxItemFactory = $salesResourceModelOrderTaxItemFactory;
        $this->salesOrderTaxFactory = $salesOrderTaxFactory;
        $this->logger = $logger;
        $this->taxesModel = $taxesModel;
        $this->rateModel = $rateModel;
        parent::__construct(
            $context, $registry, $resource, $resourceCollection, $data
        );

        $allStores = $this->storeManager->getStores();

        foreach ($allStores as $store) {
            $this->config[$store->getStoreId()] = new \stdClass();
            $this->config[$store->getStoreId()]->extra_as_rows = $this->scopeConfig->getValue(self::XML_PATH_EXTRA_AS_ROWS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->cost_descrition = $this->scopeConfig->getValue(self::XML_PATH_COST_DESCRIPTION, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->cf = $this->scopeConfig->getValue(self::XML_PATH_CF, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->fe = $this->scopeConfig->getValue(self::XML_PATH_FE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->comment = $this->scopeConfig->getValue(self::XML_PATH_COMMENT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            if ($this->request->getQuery('type')) {
                $this->config[$store->getStoreId()]->type = $this->request->getQuery('type');
            } else {
                $this->config[$store->getStoreId()]->type = $this->scopeConfig->getValue(self::XML_PATH_DOC_TYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            }

            $this->config[$store->getStoreId()]->use_variant = $this->scopeConfig->getValue(self::XML_PATH_USE_VARIANT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->variant_2to1 = $this->scopeConfig->getValue(self::XML_PATH_VARIANT_2TO1, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->name_taglia_colore = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA_COLORE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->name_taglia = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
            $this->config[$store->getStoreId()]->name_colore = $this->scopeConfig->getValue(self::XML_PATH_NAME_COLORE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getStoreId());
        }
        $this->config['default'] = new \stdClass();
        $this->config['default']->extra_as_rows = $this->scopeConfig->getValue(self::XML_PATH_EXTRA_AS_ROWS);
        $this->config['default']->cost_descrition = $this->scopeConfig->getValue(self::XML_PATH_COST_DESCRIPTION);
        $this->config['default']->cf = $this->scopeConfig->getValue(self::XML_PATH_CF);
        $this->config['default']->fe = $this->scopeConfig->getValue(self::XML_PATH_FE);
        $this->config['default']->comment = $this->scopeConfig->getValue(self::XML_PATH_COMMENT);
        if ($this->request->getQuery('type')) {
            $this->config['default']->type = $this->request->getQuery('type');
        } else {
            $this->config['default']->type = $this->scopeConfig->getValue(self::XML_PATH_DOC_TYPE);
        }

        $this->config['default']->use_variant = $this->scopeConfig->getValue(self::XML_PATH_USE_VARIANT);
        $this->config['default']->variant_2to1 = $this->scopeConfig->getValue(self::XML_PATH_VARIANT_2TO1);
        $this->config['default']->name_taglia_colore = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA_COLORE);
        $this->config['default']->name_taglia = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA);
        $this->config['default']->name_colore = $this->scopeConfig->getValue(self::XML_PATH_NAME_COLORE);
        $this->config['os_export'] = explode(',', $this->scopeConfig->getValue(self::XML_PATH_ORDER_STATUS_EXPORT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE));

        if ($this->taxConfigFactory->create()->priceIncludesTax()) {
            $this->includeTax = true;
            $this->getPrice = 'InclTax';
        }

        $base = $this->objectManager->get(\Magento\Sales\Model\Order\Invoice\Config::class);
        $know_totals = array('subtotal', 'discount', 'shipping', 'adjustment_positive', 'adjustment_negative', 'grand_total', 'cost_total', 'tax', 'weee');
        foreach ($base->getTotalModels() as $total) {
            if (!in_array($total->getCode(), $know_totals)) {
                $this->order_totals[$total->getCode()] = $total->getCode();
            }
        }

        $baseCurrencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode();

        $allowedCurrencies = $this->directoryCurrencyFactory->create()
            ->getConfigAllowCurrencies();
        $this->currencyRates = $this->directoryCurrencyFactory->create()
            ->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
    }

    public function export()
    {
        $orders = $this->salesResourceModelOrderCollectionFactory->create();

        if ($this->config['os_export']) {
            $orders->addAttributeToFilter('status', array('in' => $this->config['os_export']));
        }
        if ($this->request->getQuery('firstnum')) {
            $orders->addAttributeToFilter('increment_id', array('gteq' => $this->request->getQuery('firstnum')));
        }
        if ($this->request->getQuery('lastnum')) {
            $orders->addAttributeToFilter('increment_id', array('lteq' => $this->request->getQuery('lastnum')));
        }
        $data = array();
        if ($this->request->getQuery('firstdate')) {
            $data['from'] = $this->request->getQuery('firstdate');
        }
        if ($this->request->getQuery('lastdate')) {
            $data['to'] = $this->request->getQuery('lastdate');
        }
        if ($data) {
            $data['date'] = true;
            $orders->addAttributeToFilter('created_at', $data);
        }

        $orders->addAttributeToSort('created_at', 'desc');

        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" .
            '<EasyfattDocuments AppVersion="' . ($this->request->getQuery('AppVersion') ? $this->request->getQuery('AppVersion') : ($this->request->getQuery('type') ? '2' : 'DaneaEasyfatt.2006.17.00')) . '" Creator="Web Project Solutions" CreatorUrl="www.webprojectsol.com">' . "\n" .
            '<Documents>' . "\n";

        $numOrdini = $orders->getSize();
        foreach ($orders->getItems() as $order) {
            $shippingaddress = $this->salesOrderAddressFactory->create()->load($order['shipping_address_id'])->getData();
            $billingaddress = $this->salesOrderAddressFactory->create()->load($order['billing_address_id'])->getData();
            $payment = $order->getPayment()->getMethod();
            $items = $order->getAllItems();

            $taxes = $this->salesResourceModelOrderTaxItemFactory->create()->getTaxItemsByOrderId($order['entity_id']);
            $shipTax = $this->_getShippingTax($taxes);
            $itemsTaxes = $this->_getItemsTaxes($taxes);

            if ($order['order_currency_code'] != 'EUR') {
                if ($order['base_currency_code'] == 'EUR') {
                    $order['grand_total'] = $order['base_grand_total'];
                    $order['subtotal_incl_tax'] = $order['base_subtotal_incl_tax'];
                    $order['shipping_tax_amount'] = $order['base_shipping_tax_amount'];
                    $order['shipping_amount'] = $order['base_shipping_amount'];
                    $order['total_paid'] = $order['base_total_paid'];
                    $order['total_due'] = $order['base_total_due'];
                    foreach ($this->order_totals as $key => $value) {
                        if (isset($order[$key])) {
                            if (isset($order[preg_replace('/_/', '_base_', $key, 1)])) {
                                $order[$key] = $order[preg_replace('/_/', '_base_', $key, 1)];
                            } else {
                                $order[$key] = $this->_currencyConvert($order[$key], $order['order_currency_code'], 'EUR');
                            }
                        }
                    }
                } else {
                    $order['grand_total'] = $this->_currencyConvert($order['grand_total'], $order['order_currency_code'], 'EUR');
                    $order['subtotal_incl_tax'] = $this->_currencyConvert($order['subtotal_incl_tax'], $order['order_currency_code'], 'EUR');
                    $order['shipping_tax_amount'] = $this->_currencyConvert($order['shipping_tax_amount'], $order['order_currency_code'], 'EUR');
                    $order['shipping_amount'] = $this->_currencyConvert($order['shipping_amount'], $order['order_currency_code'], 'EUR');
                    $order['total_paid'] = $this->_currencyConvert($order['total_paid'], $order['order_currency_code'], 'EUR');
                    $order['total_due'] = $this->_currencyConvert($order['total_due'], $order['order_currency_code'], 'EUR');
                    foreach ($this->order_totals as $key => $value) {
                        if (isset($order[$key])) {
                            $order[$key] = $this->_currencyConvert($order[$key], $order['order_currency_code'], 'EUR');
                        }
                    }
                }
            }

            if (!$order['store_id']) {
                $order['store_id'] = 'default';
            }

            $xml .= '<Document>' . "\n";
            $xml .= '<DocumentType>' . $this->config[$order['store_id']]->type . '</DocumentType>' . "\n";
            $xml .= '<CustomerWebLogin>' . self::_setCharsW3c($order['customer_email']) . '</CustomerWebLogin>' . "\n";
            $CustomerName = ($billingaddress['company'] ? $billingaddress['company'] : $billingaddress['lastname'] . ' ' . $billingaddress['firstname']);
            $xml .= '<CustomerName>' . self::_ucWords($CustomerName) . '</CustomerName>' . "\n";
            $xml .= '<CustomerAddress>' . self::_ucWords($billingaddress['street']) . '</CustomerAddress>' . "\n";
            $xml .= '<CustomerPostcode>' . self::_setCharsW3c($billingaddress['postcode']) . '</CustomerPostcode>' . "\n";
            $xml .= '<CustomerCity>' . self::_ucWords($billingaddress['city']) . '</CustomerCity>' . "\n";
            $xml .= '<CustomerProvince>' . self::_setCharsW3c($this->directoryRegionFactory->create()->loadByName($billingaddress['region'], $billingaddress['country_id'])->getCode()) . '</CustomerProvince>' . "\n";

            $billingCountryName = '';
            if ($billingaddress['country_id']) {
                $billingCountryName = self::_ucWords($this->directoryCountryFactory->create()->loadByCode($billingaddress['country_id'])->getName());
            }

            $xml .= '<CustomerCountry>' . $billingCountryName . '</CustomerCountry>' . "\n";

            if ($this->config[$order['store_id']]->cf) {
                $codfisc = '';
                if (isset($order['customer_' . $this->config[$order['store_id']]->cf])) {
                    $codfisc = $order['customer_' . $this->config[$order['store_id']]->cf];
                }
                if (isset($billingaddress[$this->config[$order['store_id']]->cf])) {
                    $codfisc = $billingaddress[$this->config[$order['store_id']]->cf];
                }
                $xml .= '<CustomerFiscalCode>' . strtoupper(self::_setCharsW3c($codfisc)) . '</CustomerFiscalCode>' . "\n";
            }
            if ($this->config[$order['store_id']]->fe) {
                $coddestpec = '';
                if (isset($order['customer_' . $this->config[$order['store_id']]->fe])) {
                    $coddestpec = $order['customer_' . $this->config[$order['store_id']]->fe];
                }
                if (isset($billingaddress[$this->config[$order['store_id']]->fe])) {
                    $coddestpec = $billingaddress[$this->config[$order['store_id']]->fe];
                }
                $xml .= '<CustomerEInvoiceDestCode>' . self::_setCharsW3c($coddestpec) . '</CustomerEInvoiceDestCode>' . "\n";
            }
            $partita_iva = $billingaddress['vat_id'];
            if (isset($billingaddress['partita_iva']) && $billingaddress['partita_iva']) {
                $partita_iva = $billingaddress['partita_iva'];
            }

            $xml .= '<CustomerVatCode>' . strtoupper(self::_setCharsW3c($partita_iva)) . '</CustomerVatCode>' . "\n";
            $xml .= '<CustomerCellPhone>' . self::_setCharsW3c($billingaddress['telephone']) . '</CustomerCellPhone>' . "\n";
            $xml .= '<CustomerEmail>' . self::_setCharsW3c($billingaddress['email']) . '</CustomerEmail>' . "\n";
            $xml .= '<CustomerFax>' . self::_setCharsW3c($billingaddress['fax']) . '</CustomerFax>' . "\n";
            $carrier = '';
            $track_number = '';
            if ($order->hasShipments()) {
                foreach ($order->getShipmentsCollection() as $shipments) {
                    $tracks = $this->salesOrderShipmentFactory->create($order)->load($shipments->getId())->getAllTracks();
                    foreach ($tracks as $track) {
                        $carrier = $track->getTitle();
                        $track_number = $track->getTrackNumber();
                    }
                }
            }
            $xml .= '<TrackingNumber>' . self::_setCharsW3c($track_number) . '</TrackingNumber>' . "\n";
            $xml .= '<Carrier>' . self::_ucWords($carrier ? $carrier : $order['shipping_description']) . '</Carrier>' . "\n";
            $xml .= '<TransportedWeight>' . self::_setCharsW3c($order['weight']) . '</TransportedWeight>' . "\n";
            if ($shippingaddress) {
                $DeliveryName = ($shippingaddress['company'] ? $shippingaddress['company'] : $shippingaddress['lastname'] . ' ' . $shippingaddress['firstname']);
                $xml .= '<DeliveryName>' . self::_ucWords($DeliveryName) . '</DeliveryName>' . "\n";
                $xml .= '<DeliveryAddress>' . self::_ucWords($shippingaddress['street']) . '</DeliveryAddress>' . "\n";
                $xml .= '<DeliveryPostcode>' . self::_setCharsW3c($shippingaddress['postcode']) . '</DeliveryPostcode>' . "\n";
                $xml .= '<DeliveryCity>' . self::_ucWords($shippingaddress['city']) . '</DeliveryCity>' . "\n";
                $xml .= '<DeliveryProvince>' . self::_setCharsW3c($this->directoryRegionFactory->create()->loadByName($shippingaddress['region'], $shippingaddress['country_id'])->getCode()) . '</DeliveryProvince>' . "\n";

                $shippingCountryName = '';
                if ($shippingaddress['country_id']) {
                    $shippingCountryName = self::_ucWords($this->directoryCountryFactory->create()->loadByCode($shippingaddress['country_id'])->getName());
                }

                $xml .= '<DeliveryCountry>' . $shippingCountryName . '</DeliveryCountry>' . "\n";
            }

            $xml .= '<Date>' . self::_setCharsW3c(substr($order['created_at'], 0, 10)) . '</Date>' . "\n";
            $xml .= '<Number>' . self::_setCharsW3c(self::_getIncrementId($order['increment_id'])) . '</Number>' . "\n";

            if (!$this->config[$order['store_id']]->extra_as_rows) {
                $CostAmount = $order['grand_total'] - $order['subtotal_incl_tax'];
                if ($this->includeTax) {
                    $CostAmount -= $order['shipping_tax_amount'];
                }
                foreach ($this->order_totals as $key => $value) {
                    if (isset($order[$key])) {
                        $CostAmount += $order[$key];
                    }
                }
                $xml .= '<CostVatCode>' . $shipTax . '</CostVatCode>' . "\n";
                $xml .= '<CostAmount>' . self::_setCharsW3c(self::_NumberFormat($CostAmount)) . '</CostAmount>' . "\n";
                $xml .= '<CostDescription>' . self::_setCharsW3c($this->config[$order['store_id']]->cost_descrition) . '</CostDescription>' . "\n";
            }
            $xml .= '<Total>' . self::_setCharsW3c(self::_NumberFormat($order['grand_total'])) . '</Total>' . "\n";
            $xml .= '<PricesIncludeVat>' . ($this->includeTax ? 'true' : 'false') . '</PricesIncludeVat>' . "\n";
            $xml .= '<PaymentName>' . self::_setCharsW3c($this->_getPayment($payment)) . '</PaymentName>' . "\n";
            $xml .= '<PaymentBank>' . self::_setCharsW3c($this->_getPaymentDesc($payment)) . '</PaymentBank>' . "\n";

            if ($this->config[$order['store_id']]->type != 'R') {
                $xml .= '<Payments>' . "\n";
                if ($order['total_paid']) {
                    $xml .= '<Payment>' . "\n";
                    $xml .= '<Advance>false</Advance>' . "\n";
                    $xml .= '<Date>' . self::_setCharsW3c(substr($order['updated_at'], 0, 10)) . '</Date>' . "\n";
                    $xml .= '<Amount>' . self::_NumberFormat($order['total_paid']) . '</Amount>' . "\n";
                    $xml .= '<Paid>true</Paid>' . "\n";
                    $xml .= '</Payment>' . "\n";
                    if ($order['total_due']) {
                        $xml .= '<Payment>' . "\n";
                        $xml .= '<Advance>false</Advance>' . "\n";
                        $xml .= '<Date>' . self::_setCharsW3c(substr($order['updated_at'], 0, 10)) . '</Date>' . "\n";
                        $xml .= '<Amount>' . self::_NumberFormat($order['total_due']) . '</Amount>' . "\n";
                        $xml .= '<Paid>false</Paid>' . "\n";
                        $xml .= '</Payment>' . "\n";
                    }
                } else {
                    $xml .= '<Payment>' . "\n";
                    $xml .= '<Advance>false</Advance>' . "\n";
                    $xml .= '<Date>' . self::_setCharsW3c(substr($order['updated_at'], 0, 10)) . '</Date>' . "\n";
                    $xml .= '<Amount>' . self::_NumberFormat($order['grand_total']) . '</Amount>' . "\n";
                    $xml .= '<Paid>false</Paid>' . "\n";
                    $xml .= '</Payment>' . "\n";
                }
                $xml .= '</Payments>' . "\n";
            }

            if ($this->config[$order['store_id']]->comment) {
                $history = $order->getStatusHistoryCollection()->getData();
                krsort($history);
                $historyOrdered = array_values($history);
                if ($historyOrdered && isset($historyOrdered['0']) && isset($historyOrdered['0']['comment'])) {
                    $xml .= '<InternalComment>' . self::_setCharsW3c($historyOrdered['0']['comment']) . '</InternalComment>' . "\n";
                    $xml .= '<FootNotes>' . self::_setCharsW3c($historyOrdered['0']['comment']) . '</FootNotes>' . "\n";
                }
            }

            $configurables = array();
//	    $this->taxCollection = array();
            foreach ($items as $item) {
                if ($item->getProductType() == 'configurable') {
                    $configurables[] = $item->getItemId();
                }
                $parentItemId = $item->getParentItemId();
                if ($parentItemId && in_array($parentItemId, $configurables)) {
                    continue;
                }
                $xml .= '<Row>' . "\n";
                $Opzioni = $item->getProductOptions();
                $NomiOpzioni = ($Opzioni ? $this->_serializeOptions($Opzioni) : '');
                $xml .= '<Description>' . self::_setCharsW3c($item->getName() . ($NomiOpzioni ? ' (' . $NomiOpzioni . ')' : '')) . '</Description>' . "\n";
                $xml .= '<Qty>' . self::_setCharsW3c((int) $item->getQtyOrdered()) . '</Qty>' . "\n";
                $codeVariant = '';
                $nVariant = 0;
                if ($this->config[$order['store_id']]->use_variant) {
                    if ($this->config[$order['store_id']]->variant_2to1) {
                        $variants = $this->_findSizeColor($Opzioni, $this->config[$order['store_id']]->name_taglia_colore);
                        if ($variants) {
                            $variant = explode(' - ', $variants);
                            $variantiNelFile = '';
                            if (isset($variant[0]) && $variant[0]) {
                                $nVariant++;
                                $variantiNelFile .= '<Size>' . self::_setCharsW3c($variant[0]) . '</Size>' . "\n";
                                $codeVariant .= '/' . $variant[0];
                            } else {
                                $codeVariant .= '/-';
                                $variantiNelFile .= '<Size>-</Size>' . "\n";
                            }
                            if (isset($variant[1]) && $variant[1]) {
                                $nVariant++;
                                $variantiNelFile .= '<Color>' . self::_setCharsW3c($variant[1]) . '</Color>' . "\n";
                                $codeVariant .= '/' . substr($variant[1], 0, 4);
                            } else {
                                $codeVariant .= '/-';
                                $variantiNelFile .= '<Color>-</Color>' . "\n";
                            }
                            if ($nVariant == 0) {
                                $codeVariant = '';
                            } else {
                                $xml .= $variantiNelFile;
                            }
                        }
                    } else {
                        $size = $this->_findSizeColor($Opzioni, $this->config[$order['store_id']]->name_taglia);
                        $variantiNelFile = '';
                        if ($size) {
                            $nVariant++;
                            $variantiNelFile .= '<Size>' . self::_setCharsW3c($size) . '</Size>' . "\n";
                            $codeVariant .= '/' . $size;
                        } else {
                            $codeVariant .= '/-';
                            $variantiNelFile .= '<Size>-</Size>' . "\n";
                        }
                        $color = $this->_findSizeColor($Opzioni, $this->config[$order['store_id']]->name_colore);
                        if ($color) {
                            $nVariant++;
                            $variantiNelFile .= '<Color>' . self::_setCharsW3c($color) . '</Color>' . "\n";
                            $codeVariant .= '/' . substr($color, 0, 4);
                        } else {
                            $codeVariant .= '/-';
                            $variantiNelFile .= '<Color>-</Color>' . "\n";
                        }
                        if ($nVariant == 0) {
                            $codeVariant = '';
                        } else {
                            $xml .= $variantiNelFile;
                        }
                    }
                }
                if ($codeVariant) {
                    $xml .= '<Code>' . self::_setCharsW3c(strtr($item->getSku(), array($codeVariant => ''))) . '</Code>' . "\n";
                } else {
                    $xml .= '<Code>' . self::_setCharsW3c($item->getSku()) . '</Code>' . "\n";
                }
                $xml .= '<Um>pz</Um>' . "\n";
                $xml .= '<Discounts></Discounts>' . "\n"; #' . $this->setCharsW3c($this->formatNumber($item->getDiscountPercent())) . '
                if ($item->getProductType() == 'bundle') {
                    $price = '0';
                } elseif ($order['order_currency_code'] != 'EUR') {
                    if ($order['base_currency_code'] == 'EUR') {
                        $price = $item->{'getBasePrice' . $this->getPrice}(); #InclTax
                    } else {
                        $price = $this->_currencyConvert($item->{'getPrice' . $this->getPrice}(), $order['order_currency_code'], 'EUR'); #InclTax
                    }
                } else {
                    $price = $item->{'getPrice' . $this->getPrice}();
                }
                if ($item->getDiscountAmount()) {
                    $price *= 1 - $item->getDiscountAmount() * 0.01;
                }

                $taxProduct = isset($itemsTaxes[$item->getId()]) ? $itemsTaxes[$item->getId()] : self::ALIQUOTA_FUORI_CAMPO_IVA;

//		$taxProduct = '';
//		if ($taxItem) {
//		    $taxProduct = $this->_getTaxByPercentageAndCode($taxItem);
//		    if (!$taxProduct) {
//			$taxProduct = $this->_getTax($taxItem, $item->getTaxAmount());
//		    }
//		}
//		if (!$taxProduct) {
//		    $taxProduct = $this->_getTaxByPercentage($item->getTaxPercent());
//		}
//		if (!$taxProduct) {
//		    $taxProduct = 'FC';
//		}
                $xml .= '<VatCode>' . $taxProduct . '</VatCode>' . "\n";
                $xml .= '<Price>' . self::_setCharsW3c(self::_NumberFormat($price)) . '</Price>' . "\n";
                $xml .= '</Row>' . "\n";
            }

            if ($this->config[$order['store_id']]->extra_as_rows) {
                $xml .= '<Row>' . "\n";
                $xml .= '<Code></Code>' . "\n";
                $xml .= '<Description>' . __('Shipping') . ': ' . $order['shipping_description'] . '</Description>' . "\n";
                $xml .= '<Qty>1</Qty>' . "\n";
                $xml .= '<Um></Um>' . "\n";
                $xml .= '<Discounts></Discounts>' . "\n";
                $xml .= '<VatCode>' . $shipTax . '</VatCode>' . "\n";
                $xml .= '<Price>' . self::_setCharsW3c(self::_NumberFormat($this->includeTax ? $order['shipping_amount'] + $order['shipping_tax_amount'] : $order['shipping_amount'])) . '</Price>' . "\n";
                $xml .= '</Row>' . "\n";
                if ($order['discount_amount'] != 0) {
                    $xml .= '<Row>' . "\n";
                    $xml .= '<Code></Code>' . "\n";
                    $xml .= '<Description>' . __('Discount') . ($order['coupon_code'] ? ' (' . $order['coupon_code'] . ')' : '') . '</Description>' . "\n";
                    $xml .= '<Qty>1</Qty>' . "\n";
                    $xml .= '<Um></Um>' . "\n";
                    $xml .= '<Discounts></Discounts>' . "\n";
                    $xml .= '<VatCode>FC</VatCode>' . "\n";
                    $xml .= '<Price>' . self::_setCharsW3c(self::_NumberFormat($order['discount_amount'])) . '</Price>' . "\n";
                    $xml .= '</Row>' . "\n";
                }
                foreach ($this->order_totals as $key => $value) {
                    if (isset($order[$key]) || isset($order[$key . '_amount'])) {
                        $xml .= '<Row>' . "\n";
                        $xml .= '<Code></Code>' . "\n";
                        $xml .= '<Description>' . $value . '</Description>' . "\n";
                        $xml .= '<Qty>1</Qty>' . "\n";
                        $xml .= '<Um></Um>' . "\n";
                        $xml .= '<Discounts></Discounts>' . "\n";
                        $xml .= '<VatCode>FC</VatCode>' . "\n";
                        #$xml .= '<VatCode>' . $this->_getTotalTax($order,
                        #        $key) . '</VatCode>' . "\n";
                        $xml .= '<Price>' . self::_setCharsW3c(self::_NumberFormat($this->_getTotalPrice($order, $key))) . '</Price>' . "\n";
                        $xml .= '</Row>' . "\n";
                    }
                }
            }

            $xml .= '</Document>' . "\n";
        }

        $xml .= '</Documents>' . "\n" .
            '</EasyfattDocuments>';

        return ['xml' => $xml, 'numOrdini' => $numOrdini];
    }

    private function _getDaneaTax($tax)
    {
        $taxRate = $this->rateModel->getResourceCollection()->addFieldToSelect('tax_calculation_rate_id')
            ->addFieldToFilter('code', ['eq' => $tax['code']])
            ->addFieldToFilter('rate', ['eq' => $tax['tax_percent']])
            ->setPageSize(1)
            ->setCurPage(1)
            ->getFirstItem();

        $this->taxesModel->getResource()->load($this->taxesModel, $taxRate->getId(), 'rate_id');
    }

    private function _getShippingTax($taxes)
    {
        foreach ($taxes as $tax) {
            if ($tax['taxable_item_type'] != 'shipping') {
                continue;
            }
            $this->_getDaneaTax($tax);
            if ($this->taxesModel) {
                return $this->taxesModel->getTipoDanea();
            }
        }
        return self::ALIQUOTA_FUORI_CAMPO_IVA;
    }

    private function _getItemsTaxes($taxes)
    {
        $itemsTaxes = [];
        foreach ($taxes as $tax) {
            if ($tax['taxable_item_type'] != 'product' || isset($itemsTaxes[$tax['item_id']])) {
                continue;
            }
            $this->_getDaneaTax($tax);
            if ($this->taxesModel) {
                $itemsTaxes[$tax['item_id']] = $this->taxesModel->getTipoDanea();
            }
        }

        return $itemsTaxes;
    }

    private static function _getIncrementId($id)
    {
        if (strpos($id, '-') === false) {
            return $id;
        }
        $ids = explode('-', $id);
        return $ids[0];
    }

    private static function _ucWords($string)
    {
        return ucwords(strtolower(self::_setCharsW3c($string)));
    }

    private static function _setCharsW3c($string)
    {
        if (empty($string)) {
            return false;
        }
        return htmlspecialchars(($string), ENT_NOQUOTES, 'Windows-1252');
        #return mb_convert_encoding(htmlspecialchars(html_entity_decode($string), ENT_NOQUOTES, 'Windows-1252'), 'UTF-8', 'Windows-1252');
    }

    private static function _NumberFormat($numero)
    {
        return number_format($numero, 2, '.', '');
    }

    private static function _getOptions($options)
    {
        $result = array();
        if (isset($options['options'])) {
            $result = array_merge($result, $options['options']);
        }
        if (isset($options['additional_options'])) {
            $result = array_merge($result, $options['additional_options']);
        }
        if (!empty($options['attributes_info'])) {
            $result = array_merge($options['attributes_info'], $result);
        }
        return $result;
    }

    private function _serializeOptions($options)
    {
        $varianti = self::_getOptions($options);
        if (!$varianti) {
            return null;
        }
        if (is_array($varianti)) {
            $nomi = '';
            foreach ($varianti as $variante) {
                $nomi .= $variante['label'] . ': ' . $variante['value'] . ',';
            }
            return substr($nomi, 0, -1);
        }
    }

    private function _findSizeColor($options, $ID)
    {
        $varianti = self::_getOptions($options);
        if (!$varianti) {
            return null;
        }
        if (is_array($varianti)) {
            foreach ($varianti as $variante) {
                if ($variante['label'] == $ID) {
                    return $variante['value'];
                }
            }
        }
        return null;
    }

    private function _getTotalPrice($order, $key)
    {
        if ($this->includeTax) {
            return $order[$key . '_incl_tax'];
        }
        if (isset($order[$key])) {
            return $order[$key];
        } elseif (isset($order[$key . '_amount'])) {
            return $order[$key . '_amount'];
        }
    }

//    private function _getTotalTax($order, $key) {
//	$tipo_danea = 'FC';
//	$has_tax = false;
//	if ($this->scopeConfig->getValue('tax/classes/' . $key . '_tax_class', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
//	    $taxes = $this->_read->fetchAll("SELECT tcr.rate FROM " . $this->_readRes->getTableName('tax_calculation_rate') . " tcr INNER JOIN " . $this->_readRes->getTableName('tax_calculation') . " tc ON tc.tax_calculation_rate_id = tcr.tax_calculation_rate_id INNER JOIN " . $this->_readRes->getTableName('tax_class') . " tcl ON tcl.class_id = tc.product_tax_class_id where tcl.class_id  = '" . $this->scopeConfig->getValue('tax/classes/' . $key . '_tax_class', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) . "'");
//	    if (isset($taxes[0]['rate']) && $taxes[0]['rate']) {
//		$has_tax = trim($taxes[0]['rate']);
//	    }
//	} elseif ($this->scopeConfig->getValue('tax/classes/' . $key . '_taxclass', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
//	    $taxes = $this->_read->fetchAll("SELECT tcr.rate FROM " . $this->_readRes->getTableName('tax_calculation_rate') . " tcr INNER JOIN " . $this->_readRes->getTableName('tax_calculation') . " tc ON tc.tax_calculation_rate_id = tcr.tax_calculation_rate_id INNER JOIN " . $this->_readRes->getTableName('tax_class') . " tcl ON tcl.class_id = tc.product_tax_class_id where tcl.class_id  = '" . $this->scopeConfig->getValue('tax/classes/' . $key . '_taxclass', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) . "'");
//	    if (isset($taxes[0]['rate']) && $taxes[0]['rate']) {
//		$has_tax = trim($taxes[0]['rate']);
//	    }
//	} else {
//	    if (isset($order[$key])) {
//		$amount = $order[$key];
//	    } elseif (isset($order[$key . '_amount'])) {
//		$amount = $order[$key . '_amount'];
//	    }
//	    if ($order[$key . '_incl_tax'] - $amount > 0) {
//		$has_tax = round(($order[$key . '_incl_tax'] / $amount) - 1, 2) * 100;
//	    }
//	}
//
//	if ($has_tax) {
//	    $TDTassa = $this->_read->fetchAll("SELECT tipo_danea FROM " . $this->_readRes->getTableName('daneaeasyfatt_taxes') . " where tipo_shop  = '" . $has_tax . "'");
//	    if (isset($TDTassa[0]['tipo_danea']) && $TDTassa[0]['tipo_danea'] && $TDTassa[0]['tipo_danea'] != 'FC') {
//		$tipo_danea = trim($TDTassa[0]['tipo_danea']);
//	    }
//	}
//
//	return $tipo_danea;
//    }
//
//    private function _getTaxByPercentageAndCode($tasse) {
//	foreach ($tasse as $tassa) {
//	    $tax = $this->salesOrderTaxFactory->create()->load($tassa['tax_id']);
//	    $code = $tax->getCode();
//	    if (!$code) {
//		return '';
//	    }
//	    $TDTassa = $this->_read->fetchAll("SELECT tipo_danea FROM " . $this->_readRes->getTableName('daneaeasyfatt_taxes') . " where tipo_shop  = '" . $tassa['percent'] . "' and tipo_shop_code  = '" . $code . "'");
//	    if (isset($TDTassa[0]['tipo_danea']) && $TDTassa[0]['tipo_danea'] && $TDTassa[0]['tipo_danea'] != 'FC') {
//		return trim($TDTassa[0]['tipo_danea']);
//	    }
//	}
//	return '';
//    }
//
//    private function _getTax($tasse, $taxAmount = 0, $taxShipping = false) {
//	$tipo_danea = 'FC';
//	return $tipo_danea;
////	if ($taxShipping) {
////	    foreach ($tasse as $tassa) {
////		if (!isset($this->taxCollection[$tassa['tax_id']]) || $this->taxCollection[$tassa['tax_id']] != $tassa['amount']) {
////		    $TDTassa = $this->_read->fetchAll("SELECT tipo_danea FROM " . $this->_readRes->getTableName('daneaeasyfatt_taxes') . " where tipo_shop  = '" . $tassa['percent'] . "'");
////		    if (isset($TDTassa[0]['tipo_danea']) && $TDTassa[0]['tipo_danea'] && $TDTassa[0]['tipo_danea'] != 'FC') {
////			$tipo_danea = trim($TDTassa[0]['tipo_danea']);
////		    }
////		}
////	    }
////	} else {
////	    foreach ($tasse as $tassa) {
////		$this->taxCollection[$tassa['tax_id']] += $taxAmount;
////		$TDTassa = $this->_read->fetchAll("SELECT tipo_danea FROM " . $this->_readRes->getTableName('daneaeasyfatt_taxes') . " where tipo_shop  = '" . $tassa['tax_percent'] . "'");
////		if (isset($TDTassa[0]['tipo_danea']) && $TDTassa[0]['tipo_danea'] && $TDTassa[0]['tipo_danea'] != 'FC') {
////		    $tipo_danea = trim($TDTassa[0]['tipo_danea']);
////		}
////	    }
////	}
////	return $tipo_danea;
//    }
//
//    private function _getTaxByPercentage($percentuale) {
//	$TDTassa = $this->_read->fetchAll("SELECT tipo_danea FROM " . $this->_readRes->getTableName('daneaeasyfatt_taxes') . " where tipo_shop  = '" . $percentuale . "'");
//	if (isset($TDTassa[0]['tipo_danea']) && $TDTassa[0]['tipo_danea'] && $TDTassa[0]['tipo_danea'] != 'FC') {
//	    return trim($TDTassa[0]['tipo_danea']);
//	}
//
//	return 'FC';
//    }

    private function _getPayment($pagamento)
    {
        $payment = $this->objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\Payments::class);
        $payment->getResource()->load($payment, $pagamento, 'payment_code');
        return $payment->getTipoDanea();
    }

    private function _getPaymentDesc($pagamento)
    {
        $payment = $this->objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\Payments::class);
        $payment->getResource()->load($payment, $pagamento, 'payment_code');
        return $payment->getDescDanea();
    }

    /**
     *
     * @param type $costo
     * @param type $da
     * @param type $a
     * @return double
     * @throws \Magento\Framework\Exception\InputException
     */
    private function _currencyConvert($costo, $da, $a)
    {
        if (isset($this->currencyRates[$da]) && isset($this->currencyRates[$a])) {
            return $costo * $this->currencyRates[$a] / $this->currencyRates[$da];
        }
        $this->logger->debug('Le valute (' . $da . ', ' . $a . ') non sono definite');
        throw new \Magento\Framework\Exception\InputException('Le valute (' . $da . ', ' . $a . ') non sono definite');
    }
}
