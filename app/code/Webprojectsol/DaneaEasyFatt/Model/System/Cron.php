<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\System;

class Cron extends \Magento\Framework\Model\AbstractModel
{

    private $_readRes;
    private $_read;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\DirFactory
     */
    protected $daneaEasyFattDirFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Session\Generic
     */
    protected $generic;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directorylist;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     *
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $writer;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\Cron
     */
    protected $cronModel;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\Report
     */
    protected $reportModel;

    public function __construct(
    \Webprojectsol\DaneaEasyFatt\Model\Cron $cronModel, \Webprojectsol\DaneaEasyFatt\Model\Report $reportModel, \Magento\Framework\Model\Context $context, \Magento\Framework\App\Action\Context $contextAction, \Magento\Framework\Registry $registry, \Magento\Framework\App\ResourceConnection $resourceConnection, \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Session\Generic $generic, \Magento\Framework\App\Filesystem\DirectoryList $directorylist, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Magento\Framework\App\Config\Storage\WriterInterface $writer, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    )
    {
        $this->cronModel = $cronModel;
        $this->reportModel = $reportModel;
        $this->resourceConnection = $resourceConnection;
        $this->daneaEasyFattDirFactory = $daneaEasyFattDirFactory;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->generic = $generic;
        $this->directorylist = $directorylist;
        $this->transportBuilder = $transportBuilder;
        $this->messageManager = $contextAction->getMessageManager();
        $this->writer = $writer;
        parent::__construct(
            $context, $registry, $resource, $resourceCollection, $data
        );
        $this->_readRes = $this->resourceConnection;
        $this->_read = $this->_readRes->getConnection('core_read');
    }

    public function check($test = false)
    {
        return $this->_setCron($test);
    }

    private function _setCron($test = false)
    {
        if (function_exists('shell_exec')) {
            $output = shell_exec('crontab -l');
            $comandi = explode(PHP_EOL, $output);
            $newCron = $this->getCronDir();
            if (!in_array($newCron, $comandi)) {
                file_put_contents('/tmp/crontab.txt', $output . PHP_EOL . $newCron . PHP_EOL);
                shell_exec('crontab /tmp/crontab.txt');
            }
            if ($test) {
                file_put_contents($this->daneaEasyFattDirFactory->create()->dirImportExport() . 'testcron.txt', '1');
                $this->writer->save(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_AUTOCRON, 1);
                return false;
            }
            return true;
        }
        return false;
    }

    public function removeCron($filename)
    {
        $pendingOtherQuery = $this->_read->fetchAll("SELECT daneaeasyfatt_crons_id FROM " . $this->_readRes->getTableName('daneaeasyfatt_crons') . " WHERE status = 'pending' AND filename != '" . $filename . "'");
        if ($pendingOtherQuery) {
            return false;
        }
        if (function_exists('shell_exec') && function_exists('exec')) {
            $output = shell_exec('crontab -l');
            $comandi = explode(PHP_EOL, $output);
            $oldCron = $this->getCronDir();
            if (in_array($oldCron, $comandi)) {
                file_put_contents('/tmp/crontab.txt', trim(strtr($output, array($oldCron => ''))) . PHP_EOL);
                shell_exec('crontab /tmp/crontab.txt');
            }
            return true;
        }
        return false;
    }

    public function cronTest()
    {
        $emailData = new \Magento\Framework\DataObject();
        $emailData->setData('body', __('Cron job working properly. Date: ') . date('d-m-Y H:i:s'));
        $sender = [
            'name' => $this->scopeConfig->getValue('trans_email/ident_general/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId()),
            'email' => $this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId()),
        ];

        $transport = $this->transportBuilder
            ->setTemplateIdentifier('email_cron_test')
            ->setTemplateOptions([
                'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
            ])
            ->setTemplateVars(['data' => $emailData])
            ->setFrom($sender)
            ->addTo($this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId()))
            ->getTransport();

        try {
            $transport->sendMessage();
            $this->messageManager->addSuccessMessage(__('The e-mail has been sent successfully.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Unable to send e-mail. Error: ') . $e->getMessage());
        }
    }

    private static function _getPHPExecutable()
    {
        if (PHP_BINARY) {
            return PHP_BINARY;
        }
        if (PHP_BINDIR) {
            $phpBin = PHP_BINDIR . DIRECTORY_SEPARATOR . 'php' . PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION;
            if (file_exists($phpBin) && is_file($phpBin)) {
                return $phpBin;
            }
        }
        ob_start();
        phpinfo();
        $phpinfo = ob_get_contents();
        ob_end_clean();
        $nw = substr($phpinfo, strpos($phpinfo, 'bindir=') + 7, 100);
        $r = substr($nw, 0, strpos($nw, "&#039;"));

        if (is_file($r)) {
            return $r;
        } elseif (is_file($r . '/php')) {
            return $r . '/php';
        }

        return false;
    }

    public function getCronDir()
    {
        $path = self::_getPHPExecutable();
        return '* * * * * ' . ($path ? $path : '/usr/bin/php') . ' -q ' . $this->directorylist->getRoot() . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'magento cron:run';
    }

    public function getGeneralContact()
    {
        return $this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId());
    }

    public function process($filename)
    {
        $crons_running = $this->cronModel->getResourceCollection()
            ->addFieldToSelect('status')
            ->addFieldToSelect('function')
            ->addFieldToSelect('numbers')
            ->addFieldToFilter('filename', ['eq' => $filename])
            ->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
            ->getItems();

        if (!$crons_running) {
            return;
        }
        $n_crons = sizeof($crons_running);
        $unit_perc = (int) (100 / $n_crons);
        $n_success = 0;
        $function = '';
        $numbers = '';
        foreach ($crons_running as $value) {
            switch ($value->getStatus()) {
                case \Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_SUCCESS:
                    ++$n_success;
                    break;
                case \Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_RUNNING:
                    $function = $value->getFunction();
                    $numbers = $value->getNumbers();
                    break;
            }
        }
        $perc = $unit_perc * $n_success;
        if ($n_crons == $n_success) {
            $perc = 100;
        }

        if ($perc == 0) {
            $json_cron = $this->reportModel->getResourceCollection()
                ->addFieldToSelect('json_cron')
                ->addFieldToFilter('filename', ['eq' => $filename])
                ->setPageSize(1)
                ->setCurPage(1)
                ->getFirstItem();

            if ($json_cron) {
                $cron_jobs = json_decode($json_cron->getJsonCron(), true);
                if ($cron_jobs['send_image'] && !$cron_jobs['sended_image']) {
                    return array('0', __('Import images FTP in progress'));
                }
            }
        }

        $cronWorking = true;
        if (!$function && $perc != 100) {
            $crons_running = $this->cronModel->getResourceCollection()
                ->addFieldToSelect('function')
                ->addFieldToSelect('numbers')
                ->addFieldToFilter('filename', ['eq' => $filename])
                ->addFieldToFilter('status', ['eq' => \Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_PENDING])
                ->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
                ->setPageSize(1)
                ->setCurPage(1)
                ->getFirstItem();
            if ($crons_running) {
                $function = $crons_running->getFunction();
                $numbers = $crons_running->getNumbers();
                $cronWorking = false;
            }
        }
        $jsonProgress = array();
        $jsonFile = false;
        if ($cronWorking) {
            $file = $this->daneaEasyFattDirFactory->create()->dirImportExport() . trim($filename) . '_Progress.txt';
            if (file_exists($file)) {
                $jsonProgress = json_decode(file_get_contents($file), true);
            }
        }
        switch ($function) {
            case \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_IMPORT_PRODUCTS:
                $numbers = explode('-', $numbers);
                if ($jsonProgress && $jsonProgress[1] == \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_IMPORT_PRODUCTS) {
                    $perc += (int) ((((100 / $n_crons) / 100)) * (($jsonProgress[0] + 1) / (($numbers[1] + 1) / 100)));
                    $jsonFile = true;
                }
                if ($jsonFile) {
                    $message = sprintf(__('Import Product %s from %s to '), $jsonProgress[0], $numbers[0]);
                } else {
                    $message = sprintf(__('Import Products from %s to '), $numbers[0]);
                }
                return array($perc, $message . $numbers[1] . ' (' . ($cronWorking ? __('Working') : __('Waiting for a new cron job')) . ')');
            case \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_DELETE_PRODUCTS:
                $numbers = explode('-', $numbers);
                if ($jsonProgress && $jsonProgress[1] == \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_DELETE_PRODUCTS) {
                    $perc += (int) ((((100 / $n_crons) / 100)) * (($jsonProgress[0] + 1) / (($numbers[1] + 1) / 100)));
                    $jsonFile = true;
                }
                if ($jsonFile) {
                    $message = sprintf(__('Elimination Product %s does not exist in the catalog EasyFatt from %s to '), $jsonProgress[0], $numbers[0]);
                } else {
                    $message = sprintf(__('Elimination Products not in the catalog EasyFatt from %s a '), $numbers[0]);
                }
                return array($perc, $message . $numbers[1] . ' (' . ($cronWorking ? __('Working') : __('Waiting for a new cron job')) . ')');
            case \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_DELETE_INCREMENTAL_PRODUCTS:
                $numbers = explode('-', $numbers);
                if ($jsonProgress && $jsonProgress[1] == \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_DELETE_PRODUCTS) {
                    $perc += (int) ((((100 / $n_crons) / 100)) * (($jsonProgress[0] + 1) / (($numbers[1] + 1) / 100)));
                    $jsonFile = true;
                }
                if ($jsonFile) {
                    $message = sprintf(__('Elimination Product %s deleted from catalog EasyFatt from %s to '), $jsonProgress[0], $numbers[0]);
                } else {
                    $message = sprintf(__('Elimination Products deleted from catalog EasyFatt from %s a '), $numbers[0]);
                }
                return array($perc, $message . $numbers[1] . ' (' . ($cronWorking ? __('Working') : __('Waiting for a new cron job')) . ')');
            case \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_DELETE_CATEGORIES_REINDEX:
                $message = __('Delete Empty Categories and/or Indexing Information');
                if ($jsonProgress && $jsonProgress[1] == \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_DELETE_CATEGORIES) {
                    $perc += (int) (((((100 / $n_crons) / 100)) * ($jsonProgress[0] / ($jsonProgress[2] / 100))) / 2);
                    $jsonFile = true;
                    $message = __('Delete Empty Categories, deleted: ') . $jsonProgress[0];
                } elseif ($jsonProgress && $jsonProgress[1] == \Webprojectsol\DaneaEasyFatt\Model\Import::FUNCTION_REINDEX_PROCESS) {
                    $perc += (int) (100 / $n_crons / 2);
                    $jsonFile = true;
                    $message = __('Indexing Information');
                }
                return array($perc, $message . ' (' . ($cronWorking ? __('Working') : __('Waiting for a new cron job')) . ')');
            default:
                if ($perc == 100) {
                    return array($perc, __('Job Completed!'));
                }
                return array(0, __('Pending (Waiting for a new cron job)'));
        }
    }
}
