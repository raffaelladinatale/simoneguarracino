<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\System\Config\Backend;

class Encrypted extends \Magento\Framework\App\Config\Value
{

    /**
     * @var \Magento\User\Model\UserFactory
     */
    protected $userUserFactory;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface 
     */
    protected $encryptor;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    public function __construct(
    \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Config\ScopeConfigInterface $config, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\User\Model\UserFactory $userUserFactory, \Magento\Backend\Model\Session $backendSession, \Magento\Framework\Encryption\EncryptorInterface $encryptor, \Magento\Framework\Message\ManagerInterface $messageManager, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    )
    {
        $this->userUserFactory = $userUserFactory;
        $this->backendSession = $backendSession;
        $this->encryptor = $encryptor;
        $this->messageManager = $messageManager;
        $this->request = $request;
        parent::__construct(
            $context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data
        );
    }

    public function afterLoad()
    {
        $value = (string) $this->getValue();
        if (!empty($value) && ($decrypted = $this->encryptor->decrypt($value))) {
            $this->setValue($decrypted);
        }
        return parent::afterLoad();
    }

    public function beforeSave()
    {
        $value = (string) $this->getValue();
        if (preg_match('/^\*+$/', $this->getValue())) {
            $value = $this->getOldValue();
        }
        $groups = $this->request->getPost('groups');
        $username = $groups['access_data']['fields']['login']['value'];
        $user = $this->userUserFactory->create()->loadByUsername($username);

        if (!$this->encryptor->validateHash($value, $user->getPassword())) {
            $this->messageManager->addErrorMessage(__('Check Username and/or Password entered, you can not log in as admin with this data.'));
        }
        if (!empty($value) && ($encrypted = $this->encryptor->encrypt($value))) {
            $this->setValue($encrypted);
        }
        return parent::beforeSave();
    }

    public function getOldValue()
    {
        return $this->encryptor->decrypt(parent::getOldValue());
    }
}
