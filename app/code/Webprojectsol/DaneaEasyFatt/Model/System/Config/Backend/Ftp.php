<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\System\Config\Backend;

class Ftp extends \Magento\Framework\App\Config\Value
{

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     * @var \Magento\Store\Model\Store
     */
    protected $storeStore;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface 
     */
    protected $encryptor;

    public function __construct(
    \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $config, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Backend\Model\Session $backendSession, \Magento\Store\Model\Store $storeStore, \Magento\Framework\Message\ManagerInterface $messageManager, \Magento\Framework\Encryption\EncryptorInterface $encryptor, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    )
    {
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->backendSession = $backendSession;
        $this->storeStore = $storeStore;
        $this->messageManager = $messageManager;
        $this->encryptor = $encryptor;
        parent::__construct(
            $context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data
        );
    }

    public function beforeSave()
    {
        if (function_exists('ftp_connect')) {
            $groups = $this->request->getPost('groups');

            $username = $groups['ftp']['fields']['username']['value'];
            $password = $groups['ftp']['fields']['password']['value'];
            if (preg_match('/^\*+$/', $password)) {
                $password = $this->encryptor->decrypt($this->scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_FTP_PASSWORD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->getStoreId()));
            }

            $domain = (string) $this->getValue();
            $url = parse_url('http://' . $domain);

            $conn_id = ftp_connect($url['host']);
            if (!@ftp_login($conn_id, $username, $password)) {
                $this->messageManager->addErrorMessage(__('Please check your login information FTP, are incorrect.'));
            } elseif (isset($url['path']) && !ftp_chdir($conn_id, $url['path'])) {
                $this->messageManager->addErrorMessage(__('Please check the FTP path, it is probably wrong, or the directory does not exist. The FTP access data are correct. The directory must be /pub/media/import/'));
            }
            ftp_close($conn_id);
        }

        return parent::beforeSave();
    }

    public function getStoreId()
    {
        $storeCode = $this->request->getParam('store');
        $storeId = null;
        if ($storeCode) {
            $storeId = $this->storeStore->load($storeCode, 'code')->getId();
        }
        return $storeId;
    }
}
