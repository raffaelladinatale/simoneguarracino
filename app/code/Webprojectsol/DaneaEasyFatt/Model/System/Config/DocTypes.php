<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\System\Config;

class DocTypes
{

    public function toOptionArray()
    {
        $options = [];

        $options[] = [
            'label' => __('Notice of parcel'),
            'value' => 'A'
        ];

        $options[] = [
            'label' => __('Counter sales'),
            'value' => 'B'
        ];

        $options[] = [
            'label' => __('Sales Order'),
            'value' => 'C'
        ];

        $options[] = [
            'label' => __('Transport document'),
            'value' => 'D'
        ];

        $options[] = [
            'label' => __('Supplier Order'),
            'value' => 'E'
        ];

        $options[] = [
            'label' => __('Shipping Invoice'),
            'value' => 'F'
        ];

        $options[] = [
            'label' => __('Report intervention'),
            'value' => 'G'
        ];

        $options[] = [
            'label' => __('Invoice'),
            'value' => 'I'
        ];

        $options[] = [
            'label' => __('Proforma Invoice'),
            'value' => 'L'
        ];

        $options[] = [
            'label' => __('Self Billed'),
            'value' => 'M'
        ];

        $options[] = [
            'label' => __('Credit note'),
            'value' => 'N'
        ];

        $options[] = [
            'label' => __('Debit note'),
            'value' => 'O'
        ];

        $options[] = [
            'label' => __('Fee'),
            'value' => 'P'
        ];

        $options[] = [
            'label' => __('Quote'),
            'value' => 'Q'
        ];

        $options[] = [
            'label' => __('Fiscal receipt'),
            'value' => 'R'
        ];

        $options[] = [
            'label' => __('Quote supplier'),
            'value' => 'S'
        ];

        $options[] = [
            'label' => __('Arrivals products supplier'),
            'value' => 'H'
        ];

        return $options;
    }
}
