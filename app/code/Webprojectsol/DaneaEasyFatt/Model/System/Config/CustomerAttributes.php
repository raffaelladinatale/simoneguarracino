<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\System\Config;

class CustomerAttributes
{

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory
     */
    protected $eavResourceModelEntityAttributeCollectionFactory;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $customerResourceModelCustomer;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Address
     */
    protected $customerResourceModelAddress;

    public function __construct(
    \Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory $eavResourceModelEntityAttributeCollectionFactory, \Magento\Customer\Model\ResourceModel\Customer $customerResourceModelCustomer, \Magento\Customer\Model\ResourceModel\Address $customerResourceModelAddress
    )
    {
        $this->eavResourceModelEntityAttributeCollectionFactory = $eavResourceModelEntityAttributeCollectionFactory;
        $this->customerResourceModelCustomer = $customerResourceModelCustomer;
        $this->customerResourceModelAddress = $customerResourceModelAddress;
    }

    public function toOptionArray()
    {
        $customerAttributesArray = $this->eavResourceModelEntityAttributeCollectionFactory->create()
            ->setEntityTypeFilter($this->customerResourceModelCustomer->getEntityType())
            ->addFieldToFilter('attribute_code', array('nin' => 'all_children'));

        $outputAttributeArray = array();
        $outputAttributeArray['-0'] = array(
            'label' => __('-- Please Select --'),
            'value' => ''
        );

        foreach ($customerAttributesArray as $attribute) {
            $outputAttributeArray[$attribute['attribute_code']] = ($attribute['frontend_label'] ? $attribute['frontend_label'] : $attribute['attribute_code']);
        }


        $addressAttributesArray = $this->eavResourceModelEntityAttributeCollectionFactory->create()
            ->setEntityTypeFilter($this->customerResourceModelAddress->getEntityType())
            ->addFieldToFilter('attribute_code', array('nin' => 'all_children'));

        foreach ($addressAttributesArray as $attribute) {
            $outputAttributeArray[$attribute['attribute_code']] = ($attribute['frontend_label'] ? $attribute['frontend_label'] : $attribute['attribute_code']);
        }

        ksort($outputAttributeArray);
        return $outputAttributeArray;
    }
}
