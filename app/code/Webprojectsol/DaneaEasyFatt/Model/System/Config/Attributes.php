<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\System\Config;

class Attributes
{

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory
     */
    protected $eavResourceModelEntityAttributeCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $catalogResourceModelProduct;

    public function __construct(
    \Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory $eavResourceModelEntityAttributeCollectionFactory, \Magento\Catalog\Model\ResourceModel\Product $catalogResourceModelProduct
    )
    {
        $this->eavResourceModelEntityAttributeCollectionFactory = $eavResourceModelEntityAttributeCollectionFactory;
        $this->catalogResourceModelProduct = $catalogResourceModelProduct;
    }

    public function toOptionArray()
    {
        $attributesArray = $this->eavResourceModelEntityAttributeCollectionFactory->create()
            ->setEntityTypeFilter($this->catalogResourceModelProduct->getEntityType())
            ->addFieldToFilter('attribute_code', array('nin' => 'all_children'));
        $outputAttributeArray = array();
        $outputAttributeArray['-0'] = array(
            'label' => __('-- Please Select --'),
            'value' => ''
        );

        $outputAttributeArray['-functionStock'] = array(
            'label' => __('Import as product quantity'),
            'value' => 'functionStock'
        );

        $outputAttributeArray['-functionCategory'] = array(
            'label' => __('Import as first level category'),
            'value' => 'functionCategory'
        );

        $outputAttributeArray['-functionSubCategory'] = array(
            'label' => __('Import as categories of second-level and lower'),
            'value' => 'functionSubCategory'
        );

        $outputAttributeArray['-functionGetRemoteImg'] = array(
            'label' => __('Import as image (remote link)'),
            'value' => 'functionGetRemoteImg'
        );

        foreach ($attributesArray as $attribute) {
            if ($attribute['attribute_code'] != 'sku') {
                $outputAttributeArray[$attribute['attribute_code']] = ($attribute['frontend_label'] ? $attribute['frontend_label'] : $attribute['attribute_code']);
            }
        }
        ksort($outputAttributeArray);
        return $outputAttributeArray;
    }
}
