<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model;

/**
 * @method \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Cron _getResource()
 * @method \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Cron getResource()
 * @method int getId()
 * @method string getFilename()
 * @method string getStatus()
 * @method string getFunction()
 * @method string getNumbers()
 * @method int getSortOrder()
 */
class Cron extends \Magento\Framework\Model\AbstractModel
{

    const STATUS_UPLOADED = 'uploaded';
    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_STOPPED = 'stopped';
    const STATUS_SUCCESS = 'success';
    const CACHE_TAG = 'daneaeasyfatt_importcron';

    /**
     * @var string
     */
    protected $_cacheTag = 'daneaeasyfatt_importcron';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'daneaeasyfatt_importcron';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Cron');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }

    public function canBeStopped()
    {
        return $this->getStatus() == \Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_PENDING ||
            $this->getStatus() == \Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_RUNNING;
    }
}
