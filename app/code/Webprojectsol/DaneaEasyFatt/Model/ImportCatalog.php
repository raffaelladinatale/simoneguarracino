<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model;

/**
 * @method \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ImportCatalog _getResource()
 * @method \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ImportCatalog getResource()
 * @method int getId()
 * @method string getFilename()
 * @method int getSize()
 * @method string getStatus()
 * @method string getDate()
 */
use Magento\Framework\Exception\InputException;

class ImportCatalog extends \Magento\Framework\Model\AbstractModel
{

    const STATUS_UPLOADED = 'uploaded';
    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_STOPPED = 'stopped';
    const STATUS_SUCCESS = 'success';

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory
     */
    protected $daneaEasyFattSystemCronFactory;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\Dir
     */
    protected $daneaEasyFattDir;

    const CACHE_TAG = 'daneaeasyfatt_importcatalog';

    /**
     * @var string
     */
    protected $_cacheTag = 'daneaeasyfatt_importcatalog';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'daneaeasyfatt_importcatalog';

    /**
     *
     * @var \Magento\Framework\File\UploaderFactory 
     */
    protected $uploaderFactory;

    /**
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     *
     * @var array
     */
    protected $allowedExtensions = ['xml'];

    /**
     *
     * @var string
     */
    protected $fileId = 'file';

    public function __construct(
    \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Backend\App\Action\Context $contextAction, \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory, \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory, \Magento\Framework\File\UploaderFactory $uploaderFactory, \Magento\Framework\Stdlib\DateTime\DateTime $dateTime, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    )
    {
        $this->uploaderFactory = $uploaderFactory;
        $this->dateTime = $dateTime;
        $this->_objectManager = $contextAction->getObjectManager();
        $this->daneaEasyFattSystemCronFactory = $daneaEasyFattSystemCronFactory;
        $this->daneaEasyFattDir = $daneaEasyFattDirFactory->create();
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_construct();
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ImportCatalog');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * 
     * @return \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog
     * @throws LocalizedException
     */
    public function createImport()
    {
        $destinationPath = $this->daneaEasyFattDir->dirImportExport();
        $date = $this->dateTime->gmtDate();
        $newFileName = self::newFileName($date);
        
        

        $context = $this->_objectManager->get(\Magento\Backend\App\Action\Context::class);
        $request = $context->getRequest();
        
        $fileXML = $request->getFiles('file');
        
//        (
//        [name] => Prodotti.xml
//        [type] => text/xml
//        [tmp_name] => /var/www/virtual/simoneguarracino.eu/phptmp/phpAS1Slk
//        [error] => 0
//        [size] => 67816
//        )
//
        $result = move_uploaded_file($fileXML['tmp_name'],  $destinationPath . $newFileName);
//        echo '<pre>';
//        echo __LINE__ . __FILE__ . '<br/>';
//        print_r($request->getFiles('file'));
//        exit;


//        $uploader = $this->uploaderFactory->create(['fileId' => $this->fileId])
//            ->setAllowCreateFolders(true)
//            ->setAllowedExtensions($this->allowedExtensions)
//            ->addValidateCallback('validate', $this, 'validateFile');
//        
//        try {
//            $result = $uploader->save($destinationPath, $newFileName);
//        } catch (\BadMethodCallException $ex) {
//            #Baco di Magento
//        }

        if (!$result) {
            throw new \Magento\Framework\Exception\LocalizedException(
            __('File cannot be saved to path: %1', $destinationPath)
            );
        }
        $size = filesize($destinationPath . $newFileName);

        $this->setFilename($newFileName);
        $this->setDate($date);
        $this->setSize($size);
        $this->setStatus(self::STATUS_UPLOADED);

        return $this;
    }

    public function endImport($removeCron)
    {
        $this->setStatus(self::STATUS_SUCCESS);
        $this->_getResource()->save($this);

        if ($removeCron) {
            $this->daneaEasyFattSystemCronFactory->create()->removeCron(trim($this->getFilename()));
        }
        $this->deleteProgress(['groupedProducts', 'updateProducts', 'deleteProducts', 'Progress']);
    }

    public function stopImport($removeCron)
    {
        $this->setStatus(self::STATUS_STOPPED);
        $this->_getResource()->save($this);

        $collection = $this->_objectManager->create('Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Cron\Collection');

        $crons = $collection->addFieldToFilter('filename', ['eq' => $this->getFilename()])->getItems();
        foreach ($crons as $cron) {
            if ($cron->canBeStopped()) {
                $cron->setStatus(\Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_STOPPED);
                $cron->getResource()->save($cron);
            }
        }

        if ($removeCron) {
            $this->daneaEasyFattSystemCronFactory->create()->removeCron(trim($this->getFilename()));
        }
        $this->deleteProgress(['groupedProducts', 'updateProducts', 'deleteProducts', 'Progress']);
    }

    private function deleteProgress($var)
    {
        if (is_array($var)) {
            foreach ($var as $v) {
                $this->deleteProgress($v);
            }
        } else {
            $file = $this->daneaEasyFattDir->dirImportExport() . trim($this->getFilename()) . '_' . $var . '.txt';
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    public function canBeStopped()
    {
        return $this->getStatus() == self::STATUS_PENDING ||
            $this->getStatus() == self::STATUS_RUNNING;
    }

    public function isNotImported()
    {
        return $this->getStatus() == self::STATUS_UPLOADED;
    }

    private static function newFileName($date)
    {
        return 'danea_import' . strtr($date, array(' ' => '_', ':' => '-')) . '.xml';
    }

    public function validateFile($filePath)
    {
        $xml = \Webprojectsol\DaneaEasyFatt\Helper\Data::_parseXml(file_get_contents($filePath));

        $valid = isset($xml['@attributes']) && (isset($xml['Products']) || isset($xml['UpdatedProducts']) || isset($xml['DeletedProducts']));
        if (!$valid) {
            throw new InputException(
            __('File cannot be parsed as Xml Danea File')
            );
        }
    }
}
