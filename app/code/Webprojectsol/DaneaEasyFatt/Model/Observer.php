<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model;

final class Observer
{

    const XML_PATH_CRONTEST = 'importcatalog/cron/cron_test';

    private $cron_test;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory
     */
    private $daneaEasyFattSystemCronFactory;

    /**
     * @var \Magento\User\Model\UserFactory
     */
    private $userUserFactory;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    private $backendAuthSession;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Logger\Logger
     */
    private $logger;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\ImportFactory
     */
    private $daneaEasyFattImportFactory;

    /**
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface 
     */
    private $encryptor;

    /**
     *
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $writer;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\Cron
     */
    private $cronModel;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\Report
     */
    private $reportModel;

    /**
     *
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     *
     * @var \Magento\Framework\TranslateInterface
     */
    private $translate;

    /**
     * 
     * @param \Webprojectsol\DaneaEasyFatt\Model\Cron $cronModel
     * @param \Webprojectsol\DaneaEasyFatt\Model\Report $reportModel
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory
     * @param \Magento\User\Model\UserFactory $userUserFactory
     * @param \Magento\Backend\Model\Auth\Session $backendAuthSession
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Webprojectsol\DaneaEasyFatt\Logger\Logger $logger
     * @param \Webprojectsol\DaneaEasyFatt\Model\ImportFactory $daneaEasyFattImportFactory
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $writer
     * @param \Magento\Framework\TranslateInterface $translate
     */
    public function __construct(
    \Webprojectsol\DaneaEasyFatt\Model\Cron $cronModel, \Webprojectsol\DaneaEasyFatt\Model\Report $reportModel, \Magento\Framework\Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory, \Magento\User\Model\UserFactory $userUserFactory, \Magento\Backend\Model\Auth\Session $backendAuthSession, \Magento\Framework\Event\ManagerInterface $eventManager, \Webprojectsol\DaneaEasyFatt\Logger\Logger $logger, \Webprojectsol\DaneaEasyFatt\Model\ImportFactory $daneaEasyFattImportFactory, \Magento\Framework\Encryption\EncryptorInterface $encryptor, \Magento\Framework\App\Config\Storage\WriterInterface $writer, \Magento\Framework\TranslateInterface $translate
    )
    {
        $this->cronModel = $cronModel;
        $this->reportModel = $reportModel;
        $this->registry = $registry;
        $this->scopeConfig = $scopeConfig;
        $this->daneaEasyFattSystemCronFactory = $daneaEasyFattSystemCronFactory;
        $this->userUserFactory = $userUserFactory;
        $this->backendAuthSession = $backendAuthSession;
        $this->eventManager = $eventManager;
        $this->logger = $logger;
        $this->encryptor = $encryptor;
        $this->daneaEasyFattImportFactory = $daneaEasyFattImportFactory;
        $this->writer = $writer;
        $this->translate = $translate;
    }

    public function execute()
    {
        $this->cron_test = $this->scopeConfig->getValue(self::XML_PATH_CRONTEST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($this->cron_test) {
            $this->daneaEasyFattSystemCronFactory->create()->cronTest();
            $this->cron_test = 0;
            $this->writer->save(self::XML_PATH_CRONTEST, 0);
            return true;
        }

        @set_time_limit(0);
        if (@ini_get("max_input_time") < '1800') {
            @ini_set("max_input_time", "1800");
        }
        if (substr(@ini_get("memory_limit"), 0, -1) < '1024') {
            @ini_set("memory_limit", "16384M");
        }
        if (substr(@ini_get("upload_max_filesize"), 0, -1) < '32') {
            @ini_set("upload_max_filesize", "32M");
        }
        if (substr(@ini_get("post_max_size"), 0, -1) < '32') {
            @ini_set("post_max_size", '32M');
        }

        $cron = $this->cronModel->getResourceCollection()
            ->addFieldToSelect('daneaeasyfatt_crons_id')
            ->addFieldToSelect('filename')
            ->addFieldToSelect('function')
            ->addFieldToSelect('numbers')
            ->addFieldToFilter('status', ['eq' => \Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_PENDING])
            ->setOrder('daneaeasyfatt_crons_id', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
            ->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
            ->setPageSize(1)
            ->setCurPage(1)
            ->getFirstItem();

        try {
            if (!$cron->getId()) {
                return false;
            }

            $cron_running = $this->cronModel->getResourceCollection()
                ->addFieldToSelect('daneaeasyfatt_crons_id')
                ->addFieldToFilter('status', ['eq' => \Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_RUNNING])
                ->setPageSize(1)
                ->setCurPage(1)
                ->getSize();

            if ($cron_running) {
                return false;
            }

            $json_cron = $this->reportModel->getResourceCollection()
                ->addFieldToSelect('json_cron')
                ->addFieldToFilter('filename', ['eq' => $cron->getFilename()])
                ->getFirstItem();

            if ($json_cron) {
                $cron_jobs = json_decode($json_cron->getJsonCron(), true);
                if ($cron_jobs['send_image'] && !$cron_jobs['sended_image']) {
                    return false;
                }
            }

            $adminLogin = $this->scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_ACCESS_DATA_LOGIN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $cron_jobs['storeId']);
            $adminPassword = $this->encryptor->decrypt($this->scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_ACCESS_DATA_PASSWORD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $cron_jobs['storeId']));
            try {
                $user = $this->userUserFactory->create()->loadByUsername($adminLogin);

                if ($this->encryptor->validateHash($adminPassword, $user->getPassword())) {
                    $this->translate->setLocale($user->getInterfaceLocale());
                    $this->registry->register('isSecureArea', true);
                    $this->backendAuthSession->setIsFirstVisit(true);
                    $this->backendAuthSession->setUser($user);
                    $this->eventManager->dispatch('admin_session_user_login_success', array('user' => $user));
                }
            } catch (\Exception $e) {
                $message = __('Admin authentication failed: Please check Username and Password') . '. (' . $adminLogin . ':' . $adminPassword . ') ';
                $message .= 'Code= ' . $e->getCode() . ' - ';
                $message .= 'File= ' . $e->getFile() . ' - ';
                $message .= 'Line= ' . $e->getLine() . ' - ';
                $message .= 'Message= ' . $e->getMessage() . ' - ';
                $message .= 'Previous= ' . $e->getPrevious() . ' - ';
                $message .= 'TraceAsString= ' . $e->getTraceAsString();
                $this->logger->debug($message);
                throw new \Magento\Framework\Exception\LocalizedException(__($message));
            }

            if ($this->backendAuthSession->isLoggedIn()) {
                $cron->setStatus(\Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_RUNNING);
                $cron->getResource()->save($cron);

                $this->daneaEasyFattImportFactory->create()->import($cron->getFilename(), $cron->getFunction(), $cron->getNumbers());

                $cron->setStatus(\Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_SUCCESS);
                $cron->getResource()->save($cron);
            } else {
                $message = __('Admin authentication failed: Please check Username and Password') . '. (' . $adminLogin . ':' . $adminPassword . ') ';
                throw new \Magento\Framework\Exception\LocalizedException(__($message));
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }

        return $this;
    }
}
