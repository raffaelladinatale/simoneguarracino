<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model;

@ini_set('xdebug.scream', 0);

class Import extends \Magento\Framework\Model\AbstractModel
{

    const XML_PATH_ACCESS_DATA_LOGIN = 'importcatalog/access_data/login';
    const XML_PATH_ACCESS_DATA_PASSWORD = 'importcatalog/access_data/password';
    const XML_PATH_ONLY_UPDATE = 'importcatalog/mode/only_update';
    const XML_PATH_ARAPIDO = 'importcatalog/mode/arapido';
    const XML_PATH_NUMBERPRODUCT = 'importcatalog/cron/number_product';
    const XML_PATH_WEBSITE_IDS = 'importcatalog/catalog/website_ids';
    const XML_PATH_STORE_IDS = 'importcatalog/catalog/store_ids';
    const XML_PATH_TYPE_ID = 'importcatalog/catalog/type_id';
    const XML_PATH_TYPE_BYEASYFATT = 'importcatalog/catalog/type_byeasyfatt';
    const XML_PATH_ATTRIBUTE_SET_ID = 'importcatalog/catalog/attribute_set_id';
    const XML_PATH_STATO_PRODOTTI = 'importcatalog/catalog/stato_prodotti';
    const XML_PATH_VISIBILITY = 'importcatalog/catalog/visibility';
    const XML_PATH_STATO_CAT = 'importcatalog/catalog/stato_cat';
    const XML_PATH_CATID_DEF = 'importcatalog/catalog/catid_def';
    const XML_PATH_PRICE = 'importcatalog/catalog/price';
    const XML_PATH_PRICE1 = 'importcatalog/catalog/price1';
    const XML_PATH_PRICE2 = 'importcatalog/catalog/price2';
    const XML_PATH_PRICE3 = 'importcatalog/catalog/price3';
    const XML_PATH_PRICE4 = 'importcatalog/catalog/price4';
    const XML_PATH_PRICE5 = 'importcatalog/catalog/price5';
    const XML_PATH_PRICE6 = 'importcatalog/catalog/price6';
    const XML_PATH_PRICE7 = 'importcatalog/catalog/price7';
    const XML_PATH_PRICE8 = 'importcatalog/catalog/price8';
    const XML_PATH_PRICE9 = 'importcatalog/catalog/price9';
    const XML_PATH_SPECIAL_PRICE = 'importcatalog/catalog/special_price';
    const XML_PATH_CREA_CAT = 'importcatalog/function/crea_cat';
    const XML_PATH_QTY_ZERO = 'importcatalog/function/qty_zero';
    const XML_PATH_DELETE_P_OUT_LIST = 'importcatalog/function/delete_p_out_list';
    const XML_PATH_DELETE_CAT = 'importcatalog/function/delete_cat';
    const XML_PATH_NOTE_DESC = 'importcatalog/function/note_desc';
    const XML_PATH_NOTE_SHORTDESC = 'importcatalog/function/note_shortdesc';
    const XML_PATH_USE_VARIANT = 'importcatalog/varianti/use_variant';
    const XML_PATH_VARIANT_QTY_ZERO = 'importcatalog/varianti/variant_qty_zero';
    const XML_PATH_VARIANTS_AS_CONFIGURABLE = 'importcatalog/varianti/variants_as_configurable';
    const XML_PATH_VARIANT_2TO1 = 'importcatalog/varianti/variant_2to1';
    const XML_PATH_NAME_TAGLIA_COLORE_CONFIGURABLE = 'importcatalog/varianti/name_taglia_colore_configurable';
    const XML_PATH_NAME_TAGLIA_COLORE = 'importcatalog/varianti/name_taglia_colore';
    const XML_PATH_TYPE_TAGLIA_COLORE = 'importcatalog/varianti/type_taglia_colore';
    const XML_PATH_NAME_TAGLIA_CONFIGURABLE = 'importcatalog/varianti/name_taglia_configurable';
    const XML_PATH_NAME_TAGLIA = 'importcatalog/varianti/name_taglia';
    const XML_PATH_TYPE_TAGLIA = 'importcatalog/varianti/type_taglia';
    const XML_PATH_NAME_COLORE_CONFIGURABLE = 'importcatalog/varianti/name_colore_configurable';
    const XML_PATH_NAME_COLORE = 'importcatalog/varianti/name_colore';
    const XML_PATH_TYPE_COLORE = 'importcatalog/varianti/type_colore';
    const XML_PATH_FTP_ENABLE = 'importcatalog/ftp/enable';
    const XML_PATH_FTP_USERNAME = 'importcatalog/ftp/username';
    const XML_PATH_FTP_PASSWORD = 'importcatalog/ftp/password';
    const XML_PATH_FTP_DOMINIO = 'importcatalog/ftp/dominio';
    const XML_PATH_DANEA_CAMPI = 'importcatalog/danea_campi';
    const XML_PATH_AUTOCRON = 'importcatalog/cron/auto_cron';
    const MAX_QTY_VALUE = 99999999.9999;
    const ATTRIBUTE_SIZE = 'size';
    const ATTRIBUTE_COLOR = 'color';
    const ATTRIBUTE_SIZE_COLOR = 'size_color';

    /**
     * All import functions
     */
    const FUNCTION_IMPORT_PRODUCTS = 'importProducts';
    const FUNCTION_IMPORT_GROUPED_PRODUCTS = 'importGroupedProducts';
    const FUNCTION_DELETE_PRODUCTS = 'deleteProducts';
    const FUNCTION_DELETE_INCREMENTAL_PRODUCTS = 'deleteIncrementalProducts';
    const FUNCTION_DELETE_CATEGORIES_REINDEX = 'deleteCategories-Reindex';
    const FUNCTION_DELETE_CATEGORIES = 'deleteCategories';
    const FUNCTION_REINDEX_PROCESS = 'reindexProcess';

    /**
     * All type result of importation
     */
    const RESULT_ERROR = 'major';
    const RESULT_SUCCESS = 'notice';

    private $config;
    private $taxPrice;
    private $remote_img = false;
    private $qty_map = false;
    private $_CatUnique = [];
    private $danea_version = 0;
    private $report = [];
    private $initT;
    private $cron_jobs = [];
    private $direct;
    private $send_image = false;
    private $products_grouped = [];
    private $products_to_update = [];
    private $products_to_delete = [];
    private $products_to_check = [];
    private $_readRes;
    private $_read;
    private $_writeRes;
    private $_write;
    private $rootCategoryStoreIds = [];
    private $manufacturer_attribute_options_model;
    private $manufacturer_attribute_code;
    private $manufacturer_attribute;
    private $manufacturer_attribute_table;
    private $filename;
    private $tmp_catalog_product_dir = '';
    private $catalog_product_dir = '';
    private $import_dir = '';
    private $base_url = '';
    private $configurableApplicableAttribute = [];

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\Cron
     */
    protected $cronModel;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\Report
     */
    protected $reportModel;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Tax\Model\ConfigFactory
     */
    protected $taxConfigFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $eavEntityAttributeSourceTableFactory;

    /**
     * @var \Magento\Eav\Model\Entity\AttributeFactory
     */
    protected $eavEntityAttributeFactory;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\Dir
     */
    protected $daneaEasyFattDir;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $catalogResourceModelProductCollectionFactory;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory
     */
    protected $daneaEasyFattSystemCronFactory;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $catalogProductFactory;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\ConfigurableFactory
     */
    protected $configurableProductProductTypeConfigurableFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $catalogCategoryFactory;

    /**
     * @var \Magento\Indexer\Model\Processor
     */
    protected $processorModel;

    /**
     * @var \Magento\Catalog\Model\Product\Option
     */
    protected $catalogProductOption;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\ItemFactory
     */
    protected $catalogInventoryStockItemFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $catalogResourceModelCategoryCollectionFactory;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $eavResourceModelEntityAttributeSetCollectionFactory;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directorylist;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\ConfigurableProduct\Model\ConfigurableAttributeHandler
     */
    protected $configurableAttributeHandler;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    protected $attributeCollection;

    public function __construct(
    \Magento\Framework\Model\Context $context, \Magento\Backend\App\Action\Context $contextAction, \Magento\Framework\Registry $registry, \Webprojectsol\DaneaEasyFatt\Model\Cron $cronModel, \Webprojectsol\DaneaEasyFatt\Model\Report $reportModel, \Magento\Framework\App\ResourceConnection $resourceConnection, \Magento\Tax\Model\ConfigFactory $taxConfigFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $eavEntityAttributeSourceTableFactory, \Magento\Eav\Model\Entity\AttributeFactory $eavEntityAttributeFactory, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Webprojectsol\DaneaEasyFatt\Model\Dir $daneaEasyFattDir, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $catalogResourceModelProductCollectionFactory, \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory, \Magento\Catalog\Model\ProductFactory $catalogProductFactory, \Magento\ConfigurableProduct\Model\Product\Type\ConfigurableFactory $configurableProductProductTypeConfigurableFactory, \Magento\Catalog\Model\CategoryFactory $catalogCategoryFactory, \Magento\Indexer\Model\Processor $processorModel, \Magento\Catalog\Model\Product\Option $catalogProductOption, \Magento\CatalogInventory\Model\Stock\ItemFactory $catalogInventoryStockItemFactory, \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $catalogResourceModelCategoryCollectionFactory, \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $eavResourceModelEntityAttributeSetCollectionFactory, \Webprojectsol\DaneaEasyFatt\Logger\Logger $logger, \Magento\Framework\App\Filesystem\DirectoryList $directorylist, \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, \Magento\ConfigurableProduct\Model\ConfigurableAttributeHandler $configurableAttributeHandler, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    )
    {
        $this->catalogProductFactory = $catalogProductFactory;
        $this->catalogCategoryFactory = $catalogCategoryFactory;
        $this->resourceConnection = $resourceConnection;
        $this->cronModel = $cronModel;
        $this->reportModel = $reportModel;
        $this->taxConfigFactory = $taxConfigFactory;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->eavEntityAttributeSourceTableFactory = $eavEntityAttributeSourceTableFactory;
        $this->eavEntityAttributeFactory = $eavEntityAttributeFactory;
        $this->request = $request;
        $this->backendSession = $contextAction->getSession();
        $this->daneaEasyFattDir = $daneaEasyFattDir;
        $this->catalogResourceModelProductCollectionFactory = $catalogResourceModelProductCollectionFactory;
        $this->daneaEasyFattSystemCronFactory = $daneaEasyFattSystemCronFactory;
        $this->logger = $logger;
        $this->configurableProductProductTypeConfigurableFactory = $configurableProductProductTypeConfigurableFactory;
        $this->processorModel = $processorModel;
        $this->catalogProductOption = $catalogProductOption;
        $this->catalogInventoryStockItemFactory = $catalogInventoryStockItemFactory;
        $this->catalogResourceModelCategoryCollectionFactory = $catalogResourceModelCategoryCollectionFactory;
        $this->eavResourceModelEntityAttributeSetCollectionFactory = $eavResourceModelEntityAttributeSetCollectionFactory;
        $this->directorylist = $directorylist;
        $this->stockRegistry = $stockRegistry;
        $this->configurableAttributeHandler = $configurableAttributeHandler;
        $this->attributeCollection = $configurableAttributeHandler->getApplicableAttributes();
        $this->objectManager = $contextAction->getObjectManager();
        parent::__construct(
            $context, $registry, $resource, $resourceCollection, $data
        );
        set_error_handler(array($this, 'exceptionsErrorHandler'), E_ALL);
        $this->_readRes = $this->resourceConnection;
        $this->_read = $this->_readRes->getConnection('core_read');
        $this->_writeRes = $this->resourceConnection;
        $this->_write = $this->_writeRes->getConnection('core_write');
        if ($this->taxConfigFactory->create()->priceIncludesTax()) {
            $this->taxPrice = 'Gross';
        } else {
            $this->taxPrice = 'Net';
        }

        $this->config = new \stdClass();
    }

    private function _loadConfigs($storeId = null)
    {
        try {
            $this->config->only_update = $this->scopeConfig->getValue(self::XML_PATH_ONLY_UPDATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->arapido = $this->scopeConfig->getValue(self::XML_PATH_ARAPIDO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->number_product = $this->scopeConfig->getValue(self::XML_PATH_NUMBERPRODUCT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->website_ids = explode(',', $this->scopeConfig->getValue(self::XML_PATH_WEBSITE_IDS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId));
            $this->config->store_ids = explode(',', $this->scopeConfig->getValue(self::XML_PATH_STORE_IDS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId));
            $this->config->type_id = $this->scopeConfig->getValue(self::XML_PATH_TYPE_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->type_byeasyfatt = $this->scopeConfig->getValue(self::XML_PATH_TYPE_BYEASYFATT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->attribute_set_id = $this->scopeConfig->getValue(self::XML_PATH_ATTRIBUTE_SET_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->stato_prodotti = $this->scopeConfig->getValue(self::XML_PATH_STATO_PRODOTTI, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->visibility = $this->scopeConfig->getValue(self::XML_PATH_VISIBILITY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->stato_cat = $this->scopeConfig->getValue(self::XML_PATH_STATO_CAT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->catid_def = $this->scopeConfig->getValue(self::XML_PATH_CATID_DEF, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->note_desc = $this->scopeConfig->getValue(self::XML_PATH_NOTE_DESC, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->note_shortdesc = $this->scopeConfig->getValue(self::XML_PATH_NOTE_SHORTDESC, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->crea_cat = $this->scopeConfig->getValue(self::XML_PATH_CREA_CAT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->qty_zero = $this->scopeConfig->getValue(self::XML_PATH_QTY_ZERO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->delete_p_out_list = $this->scopeConfig->getValue(self::XML_PATH_DELETE_P_OUT_LIST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->delete_cat = $this->scopeConfig->getValue(self::XML_PATH_DELETE_CAT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->price = $this->scopeConfig->getValue(self::XML_PATH_PRICE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            for ($i = 1; $i < 10; ++$i) {
                $this->config->{'price' . $i} = $this->scopeConfig->getValue(constant('self::XML_PATH_PRICE' . $i), \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            }
            $this->config->special_price = $this->scopeConfig->getValue(self::XML_PATH_SPECIAL_PRICE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->use_variant = $this->scopeConfig->getValue(self::XML_PATH_USE_VARIANT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            if ($this->config->use_variant) {
                $this->config->variant_qty_zero = $this->scopeConfig->getValue(self::XML_PATH_VARIANT_QTY_ZERO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
                $this->config->variants_as_configurable = $this->scopeConfig->getValue(self::XML_PATH_VARIANTS_AS_CONFIGURABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
                $this->config->variant_2to1 = $this->scopeConfig->getValue(self::XML_PATH_VARIANT_2TO1, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
                $this->_loadVariantsConfigs($storeId);
            }
            $this->config->ftp_enable = $this->scopeConfig->getValue(self::XML_PATH_FTP_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->danea_campi = $this->scopeConfig->getValue(self::XML_PATH_DANEA_CAMPI, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            if ($this->config->danea_campi) {
                $this->_loadDaneaCampiConfigs();
            }
            $this->config->auto_cron = $this->scopeConfig->getValue(self::XML_PATH_AUTOCRON, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

            foreach ($this->config->store_ids as $store_id) {
                $this->rootCategoryStoreIds[$store_id] = $this->storeManager->getStore($store_id)->getRootCategoryId();
            }

            $this->manufacturer_attribute_options_model = $this->eavEntityAttributeSourceTableFactory->create();
            $this->manufacturer_attribute_code = $this->eavEntityAttributeFactory->create()->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, 'manufacturer');
            $this->manufacturer_attribute = $this->eavEntityAttributeFactory->create()->load($this->manufacturer_attribute_code);
            $this->manufacturer_attribute_table = $this->manufacturer_attribute_options_model->setAttribute($this->manufacturer_attribute);

            if ($this->config->use_variant && $this->config->variants_as_configurable) {
                $this->_loadConfigurableConfigs();
            }
        } catch (\Exception $e) {
            $this->backendSession->addError($e->getMessage());
        }
    }

    private function _loadDaneaCampiConfigs()
    {
        foreach ($this->config->danea_campi as $campo => $attribute) {
            if ($attribute == 'functionGetRemoteImg') {
                $this->remote_img = $campo;
            } elseif ($attribute == 'functionStock') {
                $this->qty_map = $campo;
            }
        }
    }

    private function _loadConfigurableConfigs()
    {
        if ($this->config->variant_2to1) {
            $this->{$this->config->name_taglia_colore_configurable . '_attribute_options_model'} = $this->eavEntityAttributeSourceTableFactory->create();
            $this->{$this->config->name_taglia_colore_configurable . '_attribute_code'} = $this->eavEntityAttributeFactory->create()->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, $this->config->name_taglia_colore_configurable);
            $this->{$this->config->name_taglia_colore_configurable . '_attribute'} = $this->eavEntityAttributeFactory->create()->load($this->{$this->config->name_taglia_colore_configurable . '_attribute_code'});
            $this->{$this->config->name_taglia_colore_configurable . '_attribute_table'} = $this->{$this->config->name_taglia_colore_configurable . '_attribute_options_model'}->setAttribute($this->{$this->config->name_taglia_colore_configurable . '_attribute'});
        } else {
            $this->{$this->config->name_taglia_configurable . '_attribute_options_model'} = $this->eavEntityAttributeSourceTableFactory->create();
            $this->{$this->config->name_taglia_configurable . '_attribute_code'} = $this->eavEntityAttributeFactory->create()->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, $this->config->name_taglia_configurable);
            $this->{$this->config->name_taglia_configurable . '_attribute'} = $this->eavEntityAttributeFactory->create()->load($this->{$this->config->name_taglia_configurable . '_attribute_code'});
            $this->{$this->config->name_taglia_configurable . '_attribute_table'} = $this->{$this->config->name_taglia_configurable . '_attribute_options_model'}->setAttribute($this->{$this->config->name_taglia_configurable . '_attribute'});

            $this->{$this->config->name_colore_configurable . '_attribute_options_model'} = $this->eavEntityAttributeSourceTableFactory->create();
            $this->{$this->config->name_colore_configurable . '_attribute_code'} = $this->eavEntityAttributeFactory->create()->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, $this->config->name_colore_configurable);
            $this->{$this->config->name_colore_configurable . '_attribute'} = $this->eavEntityAttributeFactory->create()->load($this->{$this->config->name_colore_configurable . '_attribute_code'});
            $this->{$this->config->name_colore_configurable . '_attribute_table'} = $this->{$this->config->name_colore_configurable . '_attribute_options_model'}->setAttribute($this->{$this->config->name_colore_configurable . '_attribute'});
        }

        foreach ($this->attributeCollection->getItems() as $attribute) {
            if ($this->configurableAttributeHandler->isAttributeApplicable($attribute)) {
                $this->configurableApplicableAttribute[] = [
                    'code' => $attribute->getAttributeCode(),
                    'id' => $attribute->getAttributeId()
                ];
            }
        }
    }

    private function _loadVariantsConfigs($storeId = null)
    {
        if ($this->config->variants_as_configurable) {
            $this->config->name_taglia_colore_configurable = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA_COLORE_CONFIGURABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->name_taglia_configurable = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA_CONFIGURABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->name_colore_configurable = $this->scopeConfig->getValue(self::XML_PATH_NAME_COLORE_CONFIGURABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        } else {
            $this->config->name_taglia_colore = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA_COLORE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->type_taglia_colore = $this->scopeConfig->getValue(self::XML_PATH_TYPE_TAGLIA_COLORE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->name_taglia = $this->scopeConfig->getValue(self::XML_PATH_NAME_TAGLIA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->type_taglia = $this->scopeConfig->getValue(self::XML_PATH_TYPE_TAGLIA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->name_colore = $this->scopeConfig->getValue(self::XML_PATH_NAME_COLORE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $this->config->type_colore = $this->scopeConfig->getValue(self::XML_PATH_TYPE_COLORE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        }
    }

    private function _setImportProduct(&$xmlArray, $arrayToFull)
    {
        if (!$this->send_image && $this->direct && $this->config->ftp_enable && isset($xmlArray['ImageFileName'])) {
            $this->send_image = true;
        }
        if ($this->config->type_id == 'byeasyfatt' && $xmlArray[$this->config->type_byeasyfatt] == 'grouped') {
            $this->products_grouped[] = $xmlArray['Code'];
        }
        if (in_array($xmlArray['Code'], $this->{$arrayToFull})) {
            unset($this->{$arrayToFull}[$xmlArray['Code']]);
            $this->products_to_update[] = $xmlArray['Code'];
        }
    }

    private function _setImportProducts(&$xml, $arrayToFull, $importIndex)
    {
        $n_total = 0;
        $productCollection = $this->catalogResourceModelProductCollectionFactory->create();
        if ($productCollection->getSize() > 0) {
            foreach ($productCollection->getData() as $controllo_pid) {
                $this->{$arrayToFull}[$controllo_pid['sku']] = $controllo_pid['sku'];
            }
        }
        if (is_array($xml[$importIndex]) && isset($xml[$importIndex]['Product'])) {
            if (!isset($xml[$importIndex]['Product'][0])) {
                $tmp = $xml[$importIndex]['Product'];
                unset($xml[$importIndex]['Product']);
                $xml[$importIndex]['Product'][0] = $tmp;
            }

            if (!is_array($xml[$importIndex]['Product'][0])) {
                return 0;
            }
            foreach ($xml[$importIndex]['Product'] as $xmlArray) {
                ++$this->report['Ptotali'];
                ++$n_total;
                $this->_setImportProduct($xmlArray, $arrayToFull);
            }
        }

        return $n_total;
    }

    private function _setDeleteProducts(&$xml)
    {
        $n_delete_incremental = 0;
        if (!isset($xml['DeletedProducts']['Product'][0])) {
            $tmp = $xml['DeletedProducts']['Product'];
            unset($xml['DeletedProducts']['Product']);
            $xml['DeletedProducts']['Product'][0] = $tmp;
        }
        if (isset($xml['DeletedProducts']['Product'][0]) && is_array($xml['DeletedProducts']['Product'][0])) {
            $n_delete = sizeof($xml['DeletedProducts']['Product']);
            $this->report['Ptotali'] += $n_delete;
            if ($this->config->delete_p_out_list) {
                $n_delete_incremental += $n_delete;
            } else {
                $this->report['Pnoneliminati'] += $n_delete;
            }
        }
        return $n_delete_incremental;
    }

    private function _generateUrlKey($id, $nome)
    {
        $chars = array('Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y');
        $string = strtr($nome, $chars);
        $urlKey = preg_replace('#^([-])|([-])$#', '', preg_replace('#[^a-zA-Z0-9]{1,}#', '-', strtolower($string)), 1);

        $collection = $this->catalogProductFactory->create()->getCollection()
            ->addAttributeToFilter('url_key', ['like' => $urlKey . '%'])
            ->addAttributeToSelect('url_key');

        if ($id) {
            $collection->addAttributeToFilter('entity_id', ['neq' => $id]);
        }

        $urlKeys = [];
        foreach ($collection->getItems() as $prodotto) {
            $urlKeys[] = $prodotto->getUrlKey();
        }

        if (!in_array($urlKey, $urlKeys)) {
            return $urlKey;
        }

        for ($i = 1; $i <= 100; $i++) {
            $checkUrlKey = $urlKey . '-' . $i;
            if (!in_array($checkUrlKey, $urlKeys)) {
                return $checkUrlKey;
            }
        }

        return null;
    }

    public function setCron($filename, $direct = false, $storeId = null)
    {
        $this->_loadConfigs($storeId);

        /* #import pilotato s
          $this->products_to_update = ['33982'];

          $this->report['Paggiornati'] = 0;
          $this->report['Peliminati'] = 0;
          $this->report['Pnoneliminati'] = 0;
          $this->report['Paggiunti'] = 0;
          $this->report['upload_img'] = 0;
          $this->report['categorie'] = 0;
          $this->cron_jobs['directly'] = true;
          $this->filename = $filename;
          echo $filename;
          $this->_checkAndImportProducts(0, 0);
          #$this->deleteIncrementalProducts(0, 0);
          echo 'dopo import<pre>';
          die();
          #import pilotato e */

        $this->filename = $filename;
        $this->direct = $direct;
        $this->initT = microtime(true);

        $this->report['data'] = date('d/m/Y H:i');
        $this->report['Ptotali'] = 0;
        $this->report['Paggiornati'] = 0;
        $this->report['Peliminati'] = 0;
        $this->report['Pnoneliminati'] = 0;
        $this->report['Paggiunti'] = 0;
        $this->report['Psaltati'] = 0;
        $this->report['categorie'] = 0;
        $this->report['Celiminate'] = 0;
        $this->report['upload_img'] = 0;

        $this->report['aggiornamento_rapido'] = $this->config->arapido;
        $this->report['note'] = $this->config->note_desc;
        $this->report['solo_aggiornamento'] = $this->config->only_update;
        $this->report['elimina_p'] = $this->config->delete_p_out_list;
        $this->report['elimina_c'] = $this->config->delete_cat;
        $this->report['crea_categorie'] = $this->config->crea_cat;
        $this->report['comp_qty'] = $this->config->qty_zero;
        $this->report['errori_det'] = [];
        $xml = $this->_parseXml(file_get_contents($this->daneaEasyFattDir->dirImportExport() . $filename));

        if ($xml['@attributes']['AppVersion'] == '2') {
            $this->danea_version = 2;
        } elseif ($xml['@attributes']['AppVersion'] == '3') {
            $this->danea_version = 3;
        }

        $n_total = 0;
        if (($xml['@attributes']['Mode'] == 'full') || ($this->danea_version == 0)) {
            $this->report['mode'] = ($this->direct ? __('From the management') : __('From admin')) . ': ' . __('Import complete from ') . $xml['@attributes']['Creator'];
            if ($this->danea_version == 0) {
                $this->report['mode'] = ($this->direct ? __('From the management') : __('From admin')) . ': ' . __('Import complete from ') . 'DaneaEasyfatt.2006.17.00';
            }
            $n_total += $this->_setImportProducts($xml, 'products_to_delete', 'Products');

            if (!$this->config->delete_p_out_list) {
                $this->products_to_delete = [];
            }
        } elseif ($xml['@attributes']['Mode'] == 'incremental') {
            $this->report['mode'] = ($this->direct ? __('From the management') : __('From admin')) . ': ' . __('Import incremental from ') . $xml['@attributes']['Creator'];
            $n_total += $this->_setImportProducts($xml, 'products_to_check', 'UpdatedProducts');
        }

        $n_delete = 0;
        $n_products_to_delete = sizeof($this->products_to_delete);
        if ($this->config->delete_p_out_list && $n_products_to_delete > 0) {
            $n_delete += $n_products_to_delete;
        }

        $n_delete_incremental = 0;
        if ($xml['@attributes']['Mode'] == 'incremental') {
            if (isset($xml['DeletedProducts']) && is_array($xml['DeletedProducts']) && isset($xml['DeletedProducts']['Product'])) {
                $n_delete_incremental = $this->_setDeleteProducts($xml);
            }
        }

        $this->cron_jobs['storeId'] = $storeId;
        $this->cron_jobs['filename'] = $filename;
        $this->cron_jobs['directly'] = $this->direct;
        $this->cron_jobs['danea_version'] = $this->danea_version;
        $this->cron_jobs['mode'] = $xml['@attributes']['Mode'];
        $this->cron_jobs['number_product_config'] = $this->config->number_product;
        $this->cron_jobs['number_product_total'] = $n_total;
        $this->cron_jobs['number_product_delete'] = $n_delete;
        $this->cron_jobs['number_product_delete_incremental'] = $n_delete_incremental;
        $this->cron_jobs['send_image'] = $this->send_image;
        $this->cron_jobs['sended_image'] = false;

        foreach ($this->cronModel->getResourceCollection()->addFieldToFilter('filename', ['eq' => trim($filename)])->getItems() as $report) {
            $report->getResource()->delete($report);
        }

        $this->cronModel->setFilename(trim($filename))
            ->setStatus(\Webprojectsol\DaneaEasyFatt\Model\Cron::STATUS_PENDING);

        $sort_order = 1;
        $saveCrons = [['numbers' => $this->_setNumberImport($this->cron_jobs['number_product_total']), 'function' => self::FUNCTION_IMPORT_PRODUCTS],
            ['numbers' => $this->_setNumberImport(sizeof($this->products_grouped)), 'function' => self::FUNCTION_IMPORT_GROUPED_PRODUCTS],
            ['numbers' => $this->_setNumberImport($this->cron_jobs['number_product_delete']), 'function' => self::FUNCTION_DELETE_PRODUCTS],
            ['numbers' => $this->_setNumberImport($this->cron_jobs['number_product_delete_incremental']), 'function' => self::FUNCTION_DELETE_INCREMENTAL_PRODUCTS],
            ['numbers' => ['0'], 'function' => self::FUNCTION_DELETE_CATEGORIES_REINDEX]];
        foreach ($saveCrons as $saveCron) {
            $sort_order = $this->_saveCrons($saveCron['numbers'], $saveCron['function'], $sort_order);
        }

        if (sizeof($this->report['errori_det']) < 1) {
            $this->report['esito'] = __('Import successful');
            $danea_esito = 'OK';
            $this->report['esito_det'] = self::RESULT_SUCCESS;
        } else {
            $this->report['esito'] = __('There were errors during import');
            $danea_esito = __('There were errors during importing, for more info see the report');
            $this->report['esito_det'] = self::RESULT_ERROR;
        }

        $TempoImpiegato = microtime(true) - $this->initT;
        $this->report['microtime'] = $TempoImpiegato;
        $this->report['tempo_impiegato'] = \Webprojectsol\DaneaEasyFatt\Helper\Data::_SecondsToWords($TempoImpiegato);

        $this->report['picco_memoria_byte'] = memory_get_peak_usage();
        $this->report['picco_memoria'] = \Webprojectsol\DaneaEasyFatt\Helper\Data::_convertByteSize($this->report['picco_memoria_byte']);

        if ($this->direct) {
            $this->report['tempo_ftp_iniziale'] = microtime(true);
        }
        $jsonReport = json_encode($this->report);
        $jsonCron = json_encode($this->cron_jobs);
        $jsonProductsGrouped = '';
        if ($this->products_grouped) {
            $jsonProductsGrouped = json_encode($this->products_grouped);
        }
        $jsonProductsToDelete = '';
        if ($this->products_to_delete) {
            $jsonProductsToDelete = json_encode(array_values($this->products_to_delete));
        }
        $jsonProductsToUpdate = '';

        if ($this->products_to_update) {
            $jsonProductsToUpdate = json_encode($this->products_to_update);
        }

        try {
            foreach ($this->reportModel->getResourceCollection()->addFieldToFilter('filename', ['eq' => trim($filename)])->getItems() as $report) {
                $report->getResource()->delete($report);
            }
            $this->reportModel->setFilename(trim($filename))
                ->setJsonReport(strtr($jsonReport, array('\'' => '&apos;')))
                ->setJsonCron(strtr($jsonCron, array('\'' => '&apos;')));

            $this->reportModel->getResource()->save($this->reportModel);

            $this->_saveImportData(['groupedProducts' => $jsonProductsGrouped,
                'deleteProducts' => $jsonProductsToDelete,
                'updateProducts' => $jsonProductsToUpdate]);

            if ($this->config->auto_cron) {
                $this->daneaEasyFattSystemCronFactory->create()->check();
            }
        } catch (\Exception $e) {
            $message = __('There are problems with saving the report. Error: ') . $e->getMessage() . "<br />(filename, json_report, json_cron) VALUES ('" . trim($filename) . "', '" . strtr($jsonReport, array('\'' => '&apos;')) . "', '" . strtr($jsonCron, array('\'' => '&apos;')) . "');";
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }

        if ($this->direct == true) {
            return array($danea_esito, $this->danea_version, $this->send_image);
        }
        return true;
    }

    private function _saveImportData(array $data)
    {
        foreach ($data as $name => $value) {
            if ($value) {
                file_put_contents($this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_' . $name . '.txt', stripslashes($value));
            }
        }
    }

    private function _saveCrons(array $numbers, $function, $sort_order)
    {
        $this->cronModel->setFunction($function);
        foreach ($numbers as $number) {
            $this->cronModel->setNumbers($number)
                ->setSortOrder($sort_order);

            $this->cronModel->getResource()->saveAndClear($this->cronModel);
            ++$sort_order;
        }
        return $sort_order;
    }

    private function _setNumberImport($n)
    {
        if (!$n) {
            return [];
        }
        $numeri = [];
        for ($i = -1; $i < $n; $i += $this->config->number_product) {
            $next = ($i + $this->config->number_product);
            if ($next > $n) {
                $next = $n - 1;
            }
            $numeri[] = ($i + 1) . '-' . $next;
        }
        return $numeri;
    }

    public function import($filename, $function, $numbers)
    {
        try {
            $this->initT = microtime(true);
            $this->filename = $filename;

            $report = $this->reportModel->getResourceCollection()
                ->addFieldToSelect('json_report')
                ->addFieldToSelect('json_cron')
                ->addFieldToFilter('filename', ['eq' => $this->filename])
                ->getFirstItem();

            $this->report = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode($report->getJsonReport(), true);
            $this->cron_jobs = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode($report->getJsonCron(), true);

            $this->_loadConfigs($this->cron_jobs['storeId']);

            switch ($function) {
                case self::FUNCTION_IMPORT_PRODUCTS:
                    $numbers = explode('-', $numbers);
                    $this->_checkAndImportProducts($numbers[0], $numbers[1]);
                    break;
                case self::FUNCTION_IMPORT_GROUPED_PRODUCTS:
                    $numbers = explode('-', $numbers);
                    $this->_checkAndImportProducts($numbers[0], $numbers[1], true);
                    break;
                case self::FUNCTION_DELETE_PRODUCTS:
                    $numbers = explode('-', $numbers);
                    $this->_deleteProducts($numbers[0], $numbers[1]);
                    break;
                case self::FUNCTION_DELETE_INCREMENTAL_PRODUCTS:
                    $numbers = explode('-', $numbers);
                    $this->_deleteIncrementalProducts($numbers[0], $numbers[1]);
                    break;
                case self::FUNCTION_DELETE_CATEGORIES_REINDEX:
                    $this->_finalizeImport();
                    break;
            }
            $this->_endCron();
        } catch (\Exception $e) {
            $message = __('Errors occurred during import. Error: ') . $e->getMessage();
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    private function _importGroupedProducts(&$xml, $start, $end, $importIndex)
    {
        if (!is_array($xml[$importIndex])) {
            return;
        }
        if (!isset($xml[$importIndex]['Product'][0])) {
            $tmp = $xml[$importIndex]['Product'];
            unset($xml[$importIndex]['Product']);
            $xml[$importIndex]['Product'][0] = $tmp;
        }

        if (isset($xml[$importIndex]['Product'][0]) && is_array($xml[$importIndex]['Product'][0])) {
            for ($j = $start; $j <= $end; ++$j) {
                if (!in_array($xml[$importIndex]['Product'][$j]['Code'], $this->products_grouped)) {
                    continue;
                }
                $this->_importProduct($xml[$importIndex]['Product'][$j], $j);
            }
        }
    }

    private function _importProducts(&$xml, $start, $end, $importIndex)
    {
        if (!is_array($xml[$importIndex])) {
            return;
        }
        if (!isset($xml[$importIndex]['Product'][0])) {
            $tmp = $xml[$importIndex]['Product'];
            unset($xml[$importIndex]['Product']);
            $xml[$importIndex]['Product'][0] = $tmp;
        }
        if (isset($xml[$importIndex]['Product'][0]) && is_array($xml[$importIndex]['Product'][0])) {
            for ($j = $start; $j <= $end; ++$j) {
                if ($this->config->type_id == 'byeasyfatt' && $xml[$importIndex]['Product'][$j][$this->config->type_byeasyfatt] == 'grouped') {
                    continue;
                }

                $this->_importProduct($xml[$importIndex]['Product'][$j], $j);
            }
        }
    }

    private function _checkAndImportProducts($start, $end, $isGrouped = false)
    {
        $this->daneaEasyFattDir->_checkImportImageDir();
        $media_dir = $this->directorylist->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->tmp_catalog_product_dir = $media_dir . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'catalog' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
        $this->catalog_product_dir = $media_dir . DIRECTORY_SEPARATOR . 'catalog' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
        $this->import_dir = $media_dir . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR;
        $this->base_url = $this->storeManager->getStore()->getBaseUrl();

        $xml = $this->_parseXml(file_get_contents($this->daneaEasyFattDir->dirImportExport() . $this->filename));
        $file = $this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_updateProducts.txt';
        if (file_exists($file)) {
            $this->products_to_update = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode(file_get_contents($file), true);
        }

        if ($xml['@attributes']['AppVersion'] == '2') {
            $this->danea_version = 2;
        } elseif ($xml['@attributes']['AppVersion'] == '3') {
            $this->danea_version = 3;
        }

        if ($isGrouped) {
            $file = $this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_groupedProducts.txt';
            if (!file_exists($file)) {
                return;
            }
            $this->products_grouped = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode(file_get_contents($file), true);

            if (($xml['@attributes']['Mode'] == 'full') || ($this->danea_version == 0)) {
                $this->_importGroupedProducts($xml, $start, $end, 'Products');
            } elseif ($xml['@attributes']['Mode'] == 'incremental') {
                $this->_importGroupedProducts($xml, $start, $end, 'UpdatedProducts');
            }
            return;
        }

        if (($xml['@attributes']['Mode'] == 'full') || ($this->danea_version == 0)) {
            $this->_importProducts($xml, $start, $end, 'Products');
        } elseif ($xml['@attributes']['Mode'] == 'incremental') {
            $this->_importProducts($xml, $start, $end, 'UpdatedProducts');
        }
    }

    private function _deleteProducts($start, $end)
    {
        $file = $this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_deleteProducts.txt';
        if (!file_exists($file)) {
            return;
        }
        $json = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode(file_get_contents($file), true);
        if ($this->config->delete_p_out_list && sizeof($json) > 0) {
            for ($j = $start; $j <= $end; ++$j) {
                if (!isset($json[$j])) {
                    continue;
                }
                $this->_deleteProduct($json[$j], self::FUNCTION_DELETE_PRODUCTS, $j);
            }
        }
    }

    private function _deleteIncrementalProducts($start, $end)
    {
        $xml = $this->_parseXml(file_get_contents($this->daneaEasyFattDir->dirImportExport() . $this->filename));

        if ($xml['@attributes']['Mode'] != 'incremental' || !isset($xml['DeletedProducts']) || !is_array($xml['DeletedProducts']) || !isset($xml['DeletedProducts']['Product'])) {
            return;
        }
        if (!isset($xml['DeletedProducts']['Product'][0])) {
            $tmp = $xml['DeletedProducts']['Product'];
            unset($xml['DeletedProducts']['Product']);
            $xml['DeletedProducts']['Product'][0] = $tmp;
        }
        if (!is_array($xml['DeletedProducts']['Product'][0])) {
            return;
        }
        if ($this->config->delete_p_out_list) {
            for ($j = $start; $j <= $end; ++$j) {
                $this->_deleteProduct($xml['DeletedProducts']['Product'][$j]['Code'], self::FUNCTION_DELETE_INCREMENTAL_PRODUCTS, $j);
            }
        }
    }

    private function _deleteProduct($sku, $function, $j)
    {
        try {
            $_product = $this->catalogProductFactory->create()->loadByAttribute('sku', $sku);
            if ($_product) {
                if ($_product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                    $this->_deleteConfigurableProduct($_product);
                }
                $_product->getResource()->delete($_product);
                ++$this->report['Peliminati'];
            } else {
                ++$this->report['Pnoneliminati'];
            }
            $json = array($j, $function);
            file_put_contents($this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_Progress.txt', json_encode($json));
        } catch (\Exception $e) {
            $message = sprintf(__('There are problems with the elimination of the product: % Error: '), $sku) . $e->getMessage();
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    private function _deleteConfigurableProduct($_product)
    {
        $configurable = $this->configurableProductProductTypeConfigurableFactory->create();
        $usedProducts = $configurable->getUsedProducts($_product);
        foreach ($usedProducts as $usedProduct) {
            if ($usedProduct->getData($this->config->name_taglia_colore_configurable)) {
                $equals = $_product->getSku() . '/';
                $vars = explode(' - ', $usedProduct->getAttributeText($this->config->name_taglia_colore_configurable));
                if (!isset($vars[1])) {
                    $sku = $usedProduct->getSku();
                    if ($sku == $equals . $vars[0] . '/-' || $sku == $equals . '-/' . substr($vars[0], 0, 4)) {
                        $usedProduct->getResource()->delete($usedProduct);
                    }
                } elseif ($usedProduct->getSku() == $equals . $vars[0] . '/' . substr($vars[1], 0, 4)) {
                    $usedProduct->getResource()->delete($usedProduct);
                }
            } else {
                $equals = $_product->getSku() . '/';
                if ($usedProduct->getData($this->config->name_taglia_configurable)) {
                    $equals .= $usedProduct->getAttributeText($this->config->name_taglia_configurable) . '/';
                } else {
                    $equals .= '-/';
                }
                if ($usedProduct->getData($this->config->name_colore_configurable)) {
                    $equals .= substr($usedProduct->getAttributeText($this->config->name_colore_configurable), 0, 4);
                } else {
                    $equals .= '-';
                }

                if ($usedProduct->getSku() == $equals) {
                    $usedProduct->getResource()->delete($usedProduct);
                }
            }
        }
    }

    private function _deleteCategories()
    {
        foreach ($this->rootCategoryStoreIds as $rootCategoryStoreId) {
            $root_category = $this->catalogCategoryFactory->create()->load($rootCategoryStoreId);

            $categories = $this->catalogCategoryFactory->create()->getResource()->getAllChildren($root_category);
            rsort($categories);
            $nCategories = sizeof($categories);
            foreach ($categories as $categoryId) {
                if ($categoryId == $rootCategoryStoreId || $categoryId == '1' || $categoryId == '2') {
                    continue;
                }
                $this->_deleteCategory($categoryId, $nCategories);
            }
        }
    }

    private function _deleteCategory($categoryId, $nCategories)
    {
        $category = $this->catalogCategoryFactory->create()->load($categoryId);
        if ($category->getChildrenCount() < 1 && $category->getProductCount() < 1) {
            try {
                $category->getResource()->delete($category);
                ++$this->report['Celiminate'];
                $json = array($this->report['Celiminate'], self::FUNCTION_DELETE_CATEGORIES, $nCategories);
                file_put_contents($this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_Progress.txt', json_encode($json));
            } catch (\Exception $e) {
                $message = sprintf(__('There are problems with the elimination of the category: % Error: '), $categoryId) . $e->getMessage();
                $this->logger->debug($message);
                $this->report['errori_det'][] = $message;
            }
        }
    }

    private function _finalizeImport()
    {
        if ($this->config->delete_cat && $this->rootCategoryStoreIds) {
            $this->_deleteCategories();
        }

        try {
            $json = [0, self::FUNCTION_REINDEX_PROCESS];
            file_put_contents($this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_Progress.txt', json_encode($json));

            $this->processorModel->reindexAllInvalid();

            $importCatalog = $this->objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::class);

            $importCatalog->getResource()->load($importCatalog, $this->filename, 'filename');
            $importCatalog->endImport($this->config->auto_cron);
        } catch (\Exception $e) {
            $message = __('Impossibile inizializzare il processo di indicizzazione') . __('. Error: ') . $e->getMessage();
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    private function _endCron()
    {
        if (sizeof($this->report['errori_det']) > 0) {
            $this->report['esito'] = __('There were errors during import');
            $this->report['esito_det'] = self::RESULT_ERROR;
        }

        $TempoImpiegato = microtime(true) - $this->initT;
        $this->report['microtime'] += $TempoImpiegato;
        $this->report['tempo_impiegato'] = \Webprojectsol\DaneaEasyFatt\Helper\Data::_SecondsToWords($this->report['microtime']);

        $picco_memoria_byte = memory_get_peak_usage();
        if ($picco_memoria_byte > $this->report['picco_memoria_byte']) {
            $this->report['picco_memoria'] = \Webprojectsol\DaneaEasyFatt\Helper\Data::_convertByteSize($picco_memoria_byte);
        }

        $jsonReport = json_encode($this->report);
        $jsonCron = json_encode($this->cron_jobs);

        try {
            $this->reportModel->getResource()->load($this->reportModel, $this->filename, 'filename');
            $this->reportModel->setFilename($this->filename);
            $this->reportModel->setJsonReport(strtr($jsonReport, array('\'' => '&apos;')));
            $this->reportModel->setJsonCron(strtr($jsonCron, array('\'' => '&apos;')));
            $this->reportModel->getResource()->save($this->reportModel);
        } catch (\Exception $e) {
            $message = __('There are problems with saving the report. Error: ') . $e->getMessage() . '<br />QUERY: ' . "REPLACE INTO daneaeasyfatt_reports (filename, json_report, json_cron) VALUES ('" . trim($this->filename) . "', '" . strtr($jsonReport, array('\'' => '&apos;')) . "', '" . strtr($jsonCron, array('\'' => '&apos;')) . "');";
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    private function _setPrices($_product, &$xml, $IdEsiste)
    {
        if (isset($xml[$this->taxPrice . 'Price' . $this->config->price])) {
            $_product->setPrice($xml[$this->taxPrice . 'Price' . $this->config->price]);
        } elseif (!$IdEsiste) {
            $_product->setPrice(0);
        }

        $tier_price = [];
        for ($p = 1; $p < 10; ++$p) {
            if (isset($xml[$this->taxPrice . 'Price' . $p]) && (!empty($this->config->{'price' . $p}) || $this->config->{'price' . $p} == '0')) {
                $tier_price[] = array(
                    'website_id' => 0,
                    'cust_group' => $this->config->{'price' . $p},
                    'price_qty' => 1,
                    'price' => $xml[$this->taxPrice . 'Price' . $p]
                );
            }
        }

        if ($tier_price || $IdEsiste) {
            $_product->setData('tier_price', $tier_price);
        }

        if ($this->config->special_price) {
            if (isset($xml[$this->taxPrice . 'Price' . $this->config->special_price])) {
                $_product->setSpecialPrice($xml[$this->taxPrice . 'Price' . $this->config->special_price])
                    ->setSpecialFromDate(strtotime('now'));
            } elseif ($IdEsiste) {
                $_product->setSpecialPrice('')
                    ->setSpecialFromDate('');
            }
        }
    }

    private function _importProduct(&$xml, $n = 0)
    {
        try {
            $_id = 0;
            if (in_array($xml['Code'], $this->products_to_update)) {
                $IdEsiste = 1;
                $_product = $this->catalogProductFactory->create();
                $_id = $_product->getIdBySku($xml['Code']);
                $_product->setData('_edit_mode', true);
                $_product->load($_id);
            } elseif ($this->config->only_update) {
                ++$this->report['Psaltati'];
                return true;
            } else {
                $IdEsiste = 0;
                $_product = $this->catalogProductFactory->create();
                $_product->setData('_edit_mode', true);
            }

            $this->catalogProductOption->unsetOptions();

            if (!isset($xml['AvailableQty'])) {
                $xml['AvailableQty'] = 0;
            }
            if (!isset($xml['OrderedQty'])) {
                $xml['OrderedQty'] = 0;
            }

            if (isset($xml[$this->qty_map])) {
                $xml['AvailableQty'] = $xml[$this->qty_map];
            }

            if (!isset($xml['ManageWarehouse'])) {
                $xml['ManageWarehouse'] = 'false';
            }
            if ($this->danea_version >= 3 && isset($xml['Variants'])) {
                $xml['Variant'] = $xml['Variants']['Variant'];
                unset($xml['Variants']);
            }

            $hasVariantsAndImportAsConfigurable = false;
            if ($this->config->use_variant && $this->config->variants_as_configurable && isset($xml['Variant']) && is_array($xml['Variant'])) {
                $this->type = \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE;
                $hasVariantsAndImportAsConfigurable = true;
            } elseif ($this->config->type_id == 'byeasyfatt') {
                $this->type = $xml[$this->config->type_byeasyfatt];
            } else {
                $this->type = $this->config->type_id;
            }
            $_product->setSku($xml['Code'])
                ->setAttributeSetId($this->config->attribute_set_id)
                ->setTypeId($this->type);

            $this->stockRegistry->getStockItem($_id);

//            if (!$IdEsiste || !$this->stockRegistry->getStockItem($_id)->getData()) {
                $_product->setStockData(array(
                    'use_config_manage_stock' => $xml['ManageWarehouse'] == 'true' ? 1 : 0,
                    'qty' => (float) min([$xml['AvailableQty'], self::MAX_QTY_VALUE]),
                    //'use_config_min_qty' => 1,
                    'use_config_min_sale_qty' => 1,
                    'use_config_max_sale_qty' => 1,
                    'is_qty_decimal' => 0,
                    'is_decimal_divided' => 0,
                    //'use_config_backorders' => 1,
                    'use_config_notify_stock_qty' => 1,
                    'use_config_enable_qty_increments' => 1,
                    'use_config_qty_increments' => 1,
                    'is_in_stock' => true,
//                    'is_in_stock' => (bool) $xml['AvailableQty'],
                ));
//            } else {
//                $_product->setUpdatedAt(strtotime('now'));
//                $this->_setStockItem($_id, $xml['AvailableQty'], $xml['ManageWarehouse']);
//            }

            if ($hasVariantsAndImportAsConfigurable) {
                $_product->setPrice(0);
            } else {
                $this->_setPrices($_product, $xml, $IdEsiste);
            }

            $tax = 0;
            if (!$this->config->arapido || (!$IdEsiste && !$this->config->only_update)) {
                $_product->setStoreIds($this->config->store_ids)
                    ->setWebsiteIds($this->config->website_ids);

                $_product->setUrlKey($this->_generateUrlKey($_id, $xml['Description']));

                if (!$IdEsiste) {
                    $_product->setVisibility($this->config->visibility);
                }

                $img = false;
                if (isset($xml['ImageFileName']) && $xml['ImageFileName']) {
                    $img = $xml['ImageFileName'];
                } elseif (isset($xml[$this->remote_img]) && $xml[$this->remote_img]) {
                    $image_type = substr(strrchr($xml[$this->remote_img], "."), 1);
                    $get_image = @file_get_contents(trim($xml[$this->remote_img]));
                    if ($get_image) {
                        $img = md5($xml[$this->remote_img] . $xml['Code']) . '.' . $image_type;
                        file_put_contents($this->import_dir . $img, $get_image);
                    }
                }

                if ($img) {
                    $imageCheck = strtolower(strtr($img, array(' (' => '_', ' ' => '_', ')' => '_')));
                    $a = substr($imageCheck, 0, 1);
                    $b = substr($imageCheck, 1, 1);
                    $path = strtolower($a . DIRECTORY_SEPARATOR . $b . DIRECTORY_SEPARATOR);
                    if (!is_dir($this->tmp_catalog_product_dir . $path)) {
                        mkdir($this->tmp_catalog_product_dir . $path, 0777, true);
                    }
                    $importImage = true;
                    $removeImages = [];
                    $checkRemoveImages = [];
                    $imgInProduct = false;
                    if ($IdEsiste && file_exists($this->import_dir . $img)) {
                        $gallery = $_product->getMediaGallery();
                        if (isset($gallery['images']) && $gallery['images']) {
                            if ($this->_fileExistsRegex($this->catalog_product_dir . $path . $imageCheck) > 0) {
                                foreach ($gallery['images'] as $image) {
                                    if ($this->_fileExistsRegex($this->catalog_product_dir . $path . $imageCheck, 'match', $this->catalog_product_dir . substr($image['file'], 1), false)) {
                                        $removeImages[] = array_merge($image, array('removed' => '1'));
                                        $checkRemoveImages[] = $image['file'];
                                    } elseif ($this->_fileExistsRegex($this->catalog_product_dir . $path . $imageCheck, 'match', $this->catalog_product_dir . substr($image['file'], 1), true)) {
                                        $imgInProduct = true;
                                        $importImage = false;
                                        if (md5(file_get_contents($this->catalog_product_dir . $path . $imageCheck)) != md5(file_get_contents($this->import_dir . $img))) {
                                            if ($this->cron_jobs['directly']) {
                                                ++$this->report['upload_img'];
                                            }
                                            rename($this->import_dir . $img, $this->catalog_product_dir . $path . $imageCheck);
                                        }
                                    } elseif (strtolower($image['file']) == DIRECTORY_SEPARATOR . $path . $imageCheck) {
                                        $removeImages[] = array_merge($image, array('removed' => '1'));
                                        $checkRemoveImages[] = $image['file'];
                                    }
                                }
                                $this->_fileExistsRegex($this->catalog_product_dir . $path . $imageCheck, 'delete', ($imgInProduct ? $this->catalog_product_dir . $path . $imageCheck : false));
                            }
                        } elseif ($this->_fileExistsRegex($this->catalog_product_dir . $path . $imageCheck) > 0) {
                            $this->_fileExistsRegex($this->catalog_product_dir . $path . $imageCheck, 'delete');
                        }
                    }

                    if ($imgInProduct || $importImage || sizeof($removeImages) > 0) {
                        do {
                            if ($importImage) {
                                if (!file_exists($this->import_dir . $img)) {
                                    continue;
                                }
                                $this->cron_jobs['directly'] = true;
                                if ($this->cron_jobs['directly']) {
                                    ++$this->report['upload_img'];
                                }
                                rename($this->import_dir . $img, $this->tmp_catalog_product_dir . $path . $imageCheck);
                                $images = array(
                                    array(
                                        'url' => $this->base_url . 'media/tmp/catalog/product/' . $path . $imageCheck,
                                        'file' => '/' . $path . $imageCheck,
                                        'label' => self::_charsW3cProducts($xml['Description']),
                                        'position' => '1',
                                        'disabled' => '0',
                                        'removed' => '0'
                                    )
                                );
                            }

                            if ($removeImages) {
                                $images = (isset($images) ? array_merge($images, $removeImages) : $removeImages);
                            }
                            $image = true;
                            $small_image = true;
                            $thumbnail = true;
                            if ($IdEsiste) {
                                if ($_product->getImage() != 'no_selection' && $_product->getImage() != 'true' && $_product->getImage() != null) {
                                    $image = false;
                                }
                                if ($_product->getSmallImage() != 'no_selection' && $_product->getSmallImage() != 'true' && $_product->getSmallImage() != null) {
                                    $small_image = false;
                                }
                                if ($_product->getThumbnail() != 'no_selection' && $_product->getThumbnail() != 'true' && $_product->getThumbnail() != null) {
                                    $thumbnail = false;
                                }
                                if ($checkRemoveImages) {
                                    if (in_array($_product->getImage(), $checkRemoveImages)) {
                                        $image = true;
                                    }
                                    if (in_array($_product->getSmallImage(), $checkRemoveImages)) {
                                        $small_image = true;
                                    }
                                    if (in_array($_product->getThumbnail(), $checkRemoveImages)) {
                                        $thumbnail = true;
                                    }
                                }
                            }

                            $values = [];
                            if ($image) {
                                $values['image'] = '/' . $path . $imageCheck;
                                $_product->setImage('/' . $path . $imageCheck);
                            }
                            if ($small_image) {
                                $values['small_image'] = '/' . $path . $imageCheck;
                                $_product->setSmallImage('/' . $path . $imageCheck);
                            }
                            if ($thumbnail) {
                                $values['thumbnail'] = '/' . $path . $imageCheck;
                                $_product->setThumbnail('/' . $path . $imageCheck);
                            }

                            $mediaGallery = [];
                            if (isset($images)) {
                                $mediaGallery['images'] = json_encode($images);
                            }
                            if ($values) {
                                $mediaGallery['values'] = json_encode($values);
                            }

                            $_product->setMediaGallery($mediaGallery);
                        } while (false);
                    }
                }

                if (!$IdEsiste) {
                    $_product->setCreatedAt(strtotime('now'));
                }

                if (isset($xml['NetWeight'])) {
                    $_product->setWeight($xml['NetWeight']);
                } elseif (!$IdEsiste) {
                    $_product->setWeight(0);
                }

                if ($this->config->qty_zero && $xml['AvailableQty'] < 1) {
                    $_product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
                } else {
                    $_product->setStatus($this->config->stato_prodotti);
                }

                if (isset($xml['Vat'])) {
                    $tax = $this->_getTax($xml['Vat']);
                    if (!is_numeric($tax)) {
                        $tax = 0;
                    }
                    $_product->setTaxClassId($tax);
                } elseif (!$IdEsiste) {
                    $_product->setTaxClassId($tax);
                }

                if (($this->danea_version == 0) && isset($xml['SupplierName']) && !empty($xml['SupplierName'])) {
                    $_product->setManufacturer($this->_addAttributeValue($xml['SupplierName']));
                } elseif (isset($xml['ProducerName']) && !empty($xml['ProducerName'])) {
                    $_product->setManufacturer($this->_addAttributeValue($xml['ProducerName']));
                }

                if (isset($xml['Description'])) {
                    $_product->setName(self::_charsW3cProducts($xml['Description']));
                } elseif (!$IdEsiste) {
                    $_product->setName($xml['Code']);
                }

                if ($this->danea_version >= 2 && !empty($xml['DescriptionHtml'])) {
                    $_product->setDescription(html_entity_decode($xml['DescriptionHtml'], ENT_NOQUOTES, 'UTF-8'));
                } elseif ($this->config->note_desc && !empty($xml['Notes'])) {
                    $_product->setDescription(nl2br(self::_charsW3cProducts($xml['Notes'])));
                }

                if ($this->config->note_shortdesc) {
                    $_product->setShortDescription(nl2br(self::_charsW3cProducts($xml['Notes'])));
                }
            }

            $_extraCategory = [];
            if ($this->config->danea_campi) {
                $xmlExtraCategories = [];
                foreach ($this->config->danea_campi as $campo => $attribute) {
                    if ($attribute && isset($xml[$campo])) {
                        switch ($attribute) {
                            case 'functionStock':
                            case 'functionGetRemoteImg':
                                break;
                            case 'functionCategory':
                                $xmlExtraCategories['Category'] = trim($xml[$campo]);
                                break;
                            case 'functionSubCategory':
                                $subCategories = explode(' >> ', $xml[$campo]);
                                $xmlExtraCategories['Subcategory'] = trim($subCategories[0]);
                                unset($subCategories[0]);
                                if ($subCategories) {
                                    foreach ($subCategories as $i => $subCategory) {
                                        $xmlExtraCategories['Subcategory' . ($i + 1)] = trim($subCategory);
                                    }
                                }
                                break;
                            default:
                                if ($xml[$campo] == '0') {
                                    $_product->setData($attribute, (string) self::_charsW3cProducts($xml[$campo]));
                                } else {
                                    $_product->setData($attribute, self::_charsW3cProducts($xml[$campo]));
                                }
                                break;
                        }
                    }
                }
                if ($xmlExtraCategories) {
                    $_extraCategory = $this->_importCategories($xmlExtraCategories, $IdEsiste);
                }

                /* $param = array(11 => array('position' => 1));

                  $_product->setUpSellLinkData($param);

                  if (isset($xml['CustomField1']) && $xml['CustomField1']) {
                  $codes = explode(',', $xml['CustomField1']);
                  $n_codes = sizeof($codes);
                  $upSellLinkData = [];
                  for ($i = 0; $i < $n_codes; $i++) {
                  $id = $_product->getIdBySku($codes[$i]);
                  $upSellLinkData[$id] = array('position' => $i);
                  }
                  $_product->setUpSellLinkData($upSellLinkData);
                  } */
            }

            $_category = $this->_importCategories($xml, $IdEsiste);
            if ($_category || $_extraCategory) {
                $_product->setCategoryIds(array_unique(array_merge($_category, $_extraCategory)));
            }

            if ($this->type == 'grouped') {
                if (isset($xml['Variant']) && is_array($xml['Variant'])) {
                    if (!isset($xml['Variant']['0'])) {
                        $tempVariant = $xml['Variant'];
                        unset($xml['Variant']);
                        $xml['Variant']['0'] = $tempVariant;
                        unset($tempVariant);
                    }

                    $optionValueRaw = 1;
                    $relation_data = [];
                    foreach ($xml['Variant'] as $variante) {
                        $associated_sku = $this->catalogProductFactory->create()->getIdBySku($variante['Color']);
                        if ($associated_sku) {
                            $relation_data[$associated_sku] = array('qty' => $this->catalogInventoryStockItemFactory->create()->loadByProduct($associated_sku)->getQty(),
                                'position' => $optionValueRaw
                            );
                            ++$optionValueRaw;
                        }
                    }
                    $_product->setGroupedLinkData($relation_data);
                }
            } elseif ($this->config->use_variant) {
                if ($this->config->variants_as_configurable && !$_product->getConfigurableReadonly()) {
                    if (isset($xml['Variant']) && is_array($xml['Variant'])) {
                        if (!isset($xml['Variant']['0'])) {
                            $tempVariant = $xml['Variant'];
                            unset($xml['Variant']);
                            $xml['Variant']['0'] = $tempVariant;
                            unset($tempVariant);
                        }

                        $simplesSkus = [];
                        $simplesSkusNotDelete = [];
                        $configurable = $this->configurableProductProductTypeConfigurableFactory->create();
                        if ($IdEsiste) {
                            $usedProducts = $configurable->getUsedProducts($_product);
                            foreach ($usedProducts as $usedProduct) {
                                $simplesSkus[$usedProduct->getSku()] = $usedProduct->getId();
                            }
                        }

                        $configurableAttributesId = null;
                        $configurableAttributesTagliaId = null;
                        $configurableAttributesColoreId = null;
                        if ($this->configurableApplicableAttribute) {
                            foreach ($this->configurableApplicableAttribute as $configurableAttribute) {
                                if ($this->config->variant_2to1) {
                                    if ($configurableAttribute['id'] == $this->{$this->config->name_taglia_colore_configurable . '_attribute_code'}) {
                                        $configurableAttributesId = $configurableAttribute['id'];
                                    } elseif ($configurableAttribute['code'] == self::ATTRIBUTE_SIZE || $configurableAttribute['code'] == self::ATTRIBUTE_COLOR) {
                                        $attributeEav = $this->objectManager->create(\Magento\Catalog\Model\ResourceModel\Eav\Attribute::class)->loadByCode(\Magento\Catalog\Model\Product::ENTITY, $configurableAttribute['code']);
                                        $attribute = $this->objectManager->create(\Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute::class)->loadByProductAndAttribute($_product, $attributeEav);
                                    }
                                } else {
                                    if ($configurableAttribute['id'] == $this->{$this->config->name_taglia_configurable . '_attribute_code'}) {
                                        $configurableAttributesTagliaId = $configurableAttribute['id'];
                                    } elseif ($configurableAttribute['id'] == $this->{$this->config->name_colore_configurable . '_attribute_code'}) {
                                        $configurableAttributesColoreId = $configurableAttribute['id'];
                                    } else {
                                        if ($configurableAttribute['code'] == self::ATTRIBUTE_SIZE_COLOR) {
                                            $attributeEav = $this->objectManager->create(\Magento\Catalog\Model\ResourceModel\Eav\Attribute::class)->loadByCode(\Magento\Catalog\Model\Product::ENTITY, $configurableAttribute['code']);
                                            $attribute = $this->objectManager->create(\Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute::class)->loadByProductAndAttribute($_product, $attributeEav);

                                            $attribute->deleteByProduct($_product);
                                        }
                                    }
                                }
                            }
                        }

                        if ($this->config->variant_2to1) {
                            $this->_importVariantsInConfigurable($xml, $tax, [$this->config->name_taglia_colore_configurable => $configurableAttributesId], $_product, $simplesSkus, $simplesSkusNotDelete, 'allInOne');
                        } else {
                            $this->_importVariantsInConfigurable($xml, $tax, [$this->config->name_taglia_configurable => $configurableAttributesTagliaId, $this->config->name_colore_configurable => $configurableAttributesColoreId], $_product, $simplesSkus, $simplesSkusNotDelete);
                        }

                        if ($simplesSkus) {
                            foreach ($simplesSkus as $simpleSku => $simpleId) {
                                $simpleProduct = $this->catalogProductFactory->create();
                                $simpleProduct->load($simpleId);
                                if ($simpleProduct) {
                                    $simpleProduct->delete();
                                    $simplesSkusNotDelete[] = $simpleSku;
                                }
                            }
                        }
                        if ($simplesSkusNotDelete) {
                            $file = $this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_deleteProducts.txt';
                            if (file_exists($file)) {
                                $delete = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode(file_get_contents($file), true);
                                $finallyDelete = json_encode(array_diff($delete, $simplesSkusNotDelete));
                                file_put_contents($file, stripslashes($finallyDelete));
                            }
                        }
                    }
                } elseif (!$_product->getOptionsReadonly()) {
                    $saveOptions = false;
                    if ($IdEsiste) {
                        $optionsCollection = $_product->getProductOptionsCollection();
                        if ($optionsCollection) {
                            foreach ($optionsCollection as $option) {
                                if ($option->getTitle() == $this->config->name_taglia_colore || $option->getTitle() == $this->config->name_taglia || $option->getTitle() == $this->config->name_colore) {
                                    $option->getResource()->delete($option);
                                }
                            }
                        }
                    }
                    if (isset($xml['Variant']) && is_array($xml['Variant'])) {
                        if (!isset($xml['Variant']['0'])) {
                            $tempVariant = $xml['Variant'];
                            unset($xml['Variant']);
                            $xml['Variant']['0'] = $tempVariant;
                            unset($tempVariant);
                        }
                        $optionRaw = 0;
                        $options = [];

                        if ($this->config->variant_2to1) {
                            $arraySizeColor = [];
                            foreach ($xml['Variant'] as $variante) {
                                $qty = isset($variante['AvailableQty']) ? $variante['AvailableQty'] : 0;
                                if ($qty > 0 || $this->config->variant_qty_zero) {
                                    $optionValues = [];
                                    if ($variante['Size'] != '-') {
                                        $optionValues[] = $variante['Size'];
                                    }
                                    if ($variante['Color'] != '-') {
                                        $optionValues[] = $variante['Color'];
                                    }
                                    $arraySizeColor[] = implode(' - ', $optionValues);
                                }
                            }
                            if ($arraySizeColor) {
                                $saveOptions = true;
                                $this->_importVariant($this->config->name_taglia_colore, $this->config->type_taglia_colore, $options, $optionRaw, $arraySizeColor);
                            }
                        } else {
                            $arraySize = ['-'];
                            $arrayColor = ['-'];
                            foreach ($xml['Variant'] as $variante) {
                                $qty = isset($variante['AvailableQty']) ? $variante['AvailableQty'] : 0;
                                if ($qty > 0 || $this->config->variant_qty_zero) {
                                    if (!in_array($variante['Size'], $arraySize)) {
                                        $arraySize[] = $variante['Size'];
                                    }
                                    if (!in_array($variante['Color'], $arrayColor)) {
                                        $arrayColor[] = $variante['Color'];
                                    }
                                }
                            }
                            unset($arraySize[0]);
                            unset($arrayColor[0]);

                            if ($arraySize) {
                                $saveOptions = true;
                                $this->_importVariant($this->config->name_taglia, $this->config->type_taglia, $options, $optionRaw, $arraySize);
                                ++$optionRaw;
                            }

                            if ($arrayColor) {
                                $saveOptions = true;
                                $this->_importVariant($this->config->name_colore, $this->config->type_colore, $options, $optionRaw, $arrayColor);
                            }
                        }
                    }
                    if ($saveOptions) {
                        if ($options) {
                            $customOptions = [];
                            $customOptionFactory = $this->objectManager->create('Magento\Catalog\Api\Data\ProductCustomOptionInterfaceFactory');
                            foreach ($options as $option) {
                                $customOption = $customOptionFactory->create(['data' => $option]);
                                $customOption->setProductSku($_product->getSku());
                                $customOptions[] = $customOption;
                            }

                            $_product->setCanSaveCustomOptions(true);
                            $_product->setOptions($customOptions);
                        }
                    }
                }
            }

            $_product->setIsMassupdate(true);
            $_product->setExcludeUrlRewrite(true);

            $_product->getResource()->save($_product);
            if ($IdEsiste) {
                ++$this->report['Paggiornati'];
            } else {
                ++$this->report['Paggiunti'];
            }
            $_product->reset();
            unset($_product);
            $json = array($n, 'importProducts');
            file_put_contents($this->daneaEasyFattDir->dirImportExport() . trim($this->filename) . '_Progress.txt', json_encode($json));
            return true;
        } catch (\Magento\Framework\Exception\AlreadyExistsException $e) {
            $message = __('The product %1 has name equals with one other, then Magento cannot generate the url, please edit it and try again', $xml['Code']);
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        } catch (\Exception $e) {
            $message = sprintf(__('There are problems with the save of the general product [%s]. Error: '), $xml['Code']) . $e->getMessage();
//	    $this->logger->debug($e->getTraceAsString());
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    private function _importVariant($name, $type, array &$options, $optionRaw, array &$arrayOption)
    {
        $options[$optionRaw] = array(
            'title' => $name,
            'type' => $type,
            'is_require' => 1,
            'sort_order' => $optionRaw
        );
        foreach ($arrayOption as $i => $value) {
            $options[$optionRaw]['values'][] = array(
                'title' => $value,
                'price' => 0.00,
                'price_type' => 'fixed',
                'sort_order' => $i
            );
        }
    }

    private function _importVariantAsSimple(&$xml, $simpleProduct, $qty, $tax, &$variante, $opzioneValueString, &$value_index)
    {
        $id = $simpleProduct->getIdBySku($variante['Barcode']);
        if ($id) {
            $simpleProduct->load($id);
            $this->_setStockItem($id, $qty, $xml['ManageWarehouse']);

            $stockItem = $this->stockRegistry->getStockItemBySku($variante['Barcode']);
            $stockItem->setQty($qty);
            $stockItem->setIsInStock((bool) $qty);
            $this->stockRegistry->updateStockItemBySku($variante['Barcode'], $stockItem);

            $this->_setPrices($simpleProduct, $xml, true);
            $simpleProduct->setTaxClassId($tax);
            foreach ($value_index as $code => $value) {
                $simpleProduct->{'set' . ucwords($code)}($value);
            }

            if (isset($this->config->danea_campi['Barcode']) && isset($variante['Barcode'])) {
                $simpleProduct->{'set' . ucwords($this->config->danea_campi['Barcode'])}($variante['Barcode']);
            }
            $simpleProduct->setWeight(isset($xml['NetWeight']) ? $xml['NetWeight'] : 0);
            $simpleProduct->save();
        } else {
            $dataSimple = array_merge(['name' => self::_charsW3cProducts($xml['Description'] . ' (' . $opzioneValueString . ')'),
                'qty' => $qty,
                'manage_stock' => $xml['ManageWarehouse'],
                'tax' => $tax,
                'sku' => $variante['Barcode'],
                'weight' => (isset($xml['NetWeight']) ? $xml['NetWeight'] : 0)], $value_index);

            $id = $this->_setSimpleProduct($simpleProduct, $dataSimple, $xml);
        }
        return $id;
    }

    private function _importVariantsInConfigurable(&$xml, $tax, $configAttributeIds, $_product, &$simplesSkus, &$simplesSkusNotDelete, $howIsAttribute = 'separated')
    {
        $attributeValues = [];
        $associatedProductIds = [];
        foreach ($xml['Variant'] as $variante) {
            $qty = isset($variante['AvailableQty']) ? $variante['AvailableQty'] : 0;
            if ($qty <= 0 && !$this->config->variant_qty_zero) {
                continue;
            }
            $configAttribute = [];
            if ($variante['Size'] != '-') {
                $configAttribute[$this->config->name_taglia_configurable] = $variante['Size'];
            }
            if ($variante['Color'] != '-') {
                $configAttribute[$this->config->name_colore_configurable] = $variante['Color'];
            }
            $opzioneValueString = implode(' - ', $configAttribute);

            if (!$opzioneValueString) {
                continue;
            }

            if ($howIsAttribute == 'allInOne') {
                $configAttribute = [$this->config->name_taglia_colore_configurable => $opzioneValueString];
            }
            $simpleProduct = $this->catalogProductFactory->create();
            $value_index = [];
            foreach ($configAttribute as $code => $value) {
                $value_index[$code] = $this->_addAttributeValue($value, $code);
            }

            $_idSimple = $this->_importVariantAsSimple($xml, $simpleProduct, $qty, $tax, $variante, $opzioneValueString, $value_index);
            foreach ($value_index as $code => $value) {
                $attributeValues[] = [
                    'label' => $configAttribute[$code],
                    'attribute_id' => $this->{$code . '_attribute_code'},
                    'value_index' => $value,
                ];
            }

            $associatedProductIds[] = $_idSimple;

            $simpleProduct->reset();
            if (isset($simplesSkus[$variante['Barcode']])) {
                unset($simplesSkus[$variante['Barcode']]);
                $simplesSkusNotDelete[] = $variante['Barcode'];
            }
        }

        if ($attributeValues) {
            /** @var \Magento\ConfigurableProduct\Helper\Product\Options\Factory $optionsFactory */
            $optionsFactory = $this->objectManager->get(\Magento\ConfigurableProduct\Helper\Product\Options\Factory::class);

            $configurableAttributesData = [];
            foreach ($configAttributeIds as $code => $id) {
                $configurableAttributesData[] = [
                    'attribute_id' => $id,
                    'code' => $code,
                    'label' => $this->{$code . '_attribute'}->getFrontendLabel(),
                    'position' => '0',
                    'values' => $attributeValues,
                ];
            }

            $configurableOptions = $optionsFactory->create($configurableAttributesData);
            $extensionConfigurableAttributes = $_product->getExtensionAttributes();
            $extensionConfigurableAttributes->setConfigurableProductOptions($configurableOptions);
            $extensionConfigurableAttributes->setConfigurableProductLinks($associatedProductIds);
            $_product->setExtensionAttributes($extensionConfigurableAttributes);
        }
    }

    private function _setSimpleProduct($product, $data, &$xml)
    {
        try {
            $product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
            $product->setTaxClassId($data['tax']);
            $product->setWebsiteIds($this->config->website_ids);
            $product->setAttributeSetId($this->config->attribute_set_id);
            $product->setSku($data['sku']);
            $product->setName($data['name']);
            $product->setDescription($data['name']);
            $this->_setPrices($product, $xml, false);
            $product->setShortDescription($data['name']);
            $product->setWeight($data['weight']);
            $product->setStatus($this->config->stato_prodotti);
            $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE);
            $product->setStockData(array(
                'use_config_manage_stock' => $data['manage_stock'] == 'true' ? 1 : 0,
                'qty' => (float) $data['qty'] > self::MAX_QTY_VALUE ? self::MAX_QTY_VALUE : $data['qty'],
                //'use_config_min_qty' => 1,
                'use_config_min_sale_qty' => 1,
                'use_config_max_sale_qty' => 1,
                'is_qty_decimal' => 0,
                'is_decimal_divided' => 0,
                //'use_config_backorders' => 1,
                'use_config_notify_stock_qty' => 1,
                'use_config_enable_qty_increments' => 1,
                'use_config_qty_increments' => 1,
                'is_in_stock' => true,
//                'is_in_stock' => ($data['qty'] > 0 ? 1 : 0),
            ));

            if (isset($this->config->danea_campi['Barcode'])) {
                $product->{'set' . ucwords($this->config->danea_campi['Barcode'])}($data['sku']);
            }
            if ($this->config->variant_2to1) {
                $product->{'set' . ucwords($this->config->name_taglia_colore_configurable)}($data[$this->config->name_taglia_colore_configurable]);
            } else {
                $product->{'set' . ucwords($this->config->name_taglia_configurable)}($data[$this->config->name_taglia_configurable]);
                $product->{'set' . ucwords($this->config->name_colore_configurable)}($data[$this->config->name_colore_configurable]);
            }
            $product->setIsMassupdate(true);
            $product->setExcludeUrlRewrite(true);

            $product->save();
            $id = $product->getId();
            $product->reset();
            unset($product);
            return $id;
        } catch (\Exception $e) {
            $message = sprintf(__('There are problems with the save of the simple product [%s]. Error: '), $data['sku']) . $e->getMessage();
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    private function _createCategory($category, $id, $store_id, $catName, $rootPath, $Path)
    {
        if (!$id) {
            $category->setStoreId($store_id);
            ++$this->report['categorie'];
            $category->setPath($Path);
            if ($Path != $rootPath) {
                $category->setParentId(substr($Path, 1 + strrpos($Path, '/')));
            }

            $category->setName($catName)
                ->setDisplayMode('PRODUCTS_AND_PAGE')
                ->setIsActive($this->config->stato_cat)
                ->setMetaTitle($catName)
                ->setImage(null);
        }

        if (!$id) {
            try {
                $category->getResource()->save($category);
                $id = $category->getId();
            } catch (\Magento\Framework\Exception\AlreadyExistsException $e) {
                $message = __('The category %1 has name equals with one other, then Magento cannot generate the url, please edit it and try again', $category->getName());
                $this->logger->debug($message);
                $this->report['errori_det'][] = $message;
            }
        }
        unset($category);

        return $id;
    }

    private function _createCategories($categories)
    {
        $rCategories = [];
        foreach ($this->rootCategoryStoreIds as $store_id => $rootId) {
            $rootPath = '1/' . $rootId;
            $Path = '1/' . $rootId;
            $rCategories[] = $rootId;

            $catNamesArr = explode(' >> ', $categories);
            foreach ($catNamesArr as $catName) {
                $_categoryCollection = $this->catalogResourceModelCategoryCollectionFactory->create()
                    ->addAttributeToFilter('name', $catName)
                    ->addAttributeToFilter('parent_id', substr($Path, 1 + strrpos($Path, '/')));
                $_categoryCollection->load();
                $category = $this->catalogCategoryFactory->create()
                    ->load($_categoryCollection->getFirstItem()->getId());
                if ($category) {
                    $id = $category->getId();
                } else {
                    $id = null;
                    $category = $this->catalogCategoryFactory->create();
                }

                $id = $this->_createCategory($category, $id, $store_id, $catName, $rootPath, $Path);

                $Path .= '/' . $id;
                $rCategories[] = $id;
            }
        }
        return $rCategories;
    }

    private function _importCategories(&$xml, $IdEsiste)
    {
        if ($this->config->crea_cat && (!$this->config->arapido || (!$IdEsiste && !$this->config->only_update))) {
            $category = (isset($xml['Category']) ? $xml['Category'] : '');
            $subcategory = (isset($xml['Subcategory']) ? $xml['Subcategory'] : '');
            if (empty($category)) {
                return array($this->config->catid_def);
            }
            $categories = $category . (!empty($subcategory) ? ' >> ' . $subcategory : '');
            for ($c = 2; $c < 10; ++$c) {
                $categories .= (isset($xml['Subcategory' . $c]) ? ' >> ' . $xml['Subcategory' . $c] : '');
            }
            if (isset($this->_CatUnique[$categories])) {
                return $this->_CatUnique[$categories];
            }
            if (!$this->rootCategoryStoreIds) {
                $this->_CatUnique[$categories] = [];
                return [];
            }

            try {
                $rCategories = $this->_createCategories($categories);
            } catch (\Exception $e) {
                $message = sprintf(__('There are problems with the category: %s. Error: '), $categories) . $e->getMessage();
                $this->logger->debug($message);
                $this->report['errori_det'][] = $message;
                return false;
            }

            $this->_CatUnique[$categories] = $rCategories;
            return $rCategories;
        } else {
            if (!$this->config->arapido || (!$IdEsiste && !$this->config->only_update)) {
                return array($this->config->catid_def);
            }
        }
    }

    private function _setStockItem($productId, $qty, $manage_stock)
    {
        $stockItem = $this->stockRegistry->getStockItem($productId);

        $stockItem->setData('is_in_stock', true);
//        $stockItem->setData('is_in_stock', (bool) $qty);
        $stockItem->setData('qty', (float) min([$qty, self::MAX_QTY_VALUE]));
        $stockItem->setData('use_config_manage_stock', $manage_stock == 'true' ? 1 : 0);
#$stockItem->setData('original_inventory_qty', $stockItem->getQty());

        try {
            $stockItem->save();
            unset($stockItem);
        } catch (\Exception $e) {
            $message = sprintf(__('There are problems with saving the quantity of the product [%s]. Error: '), $productId) . $e->getMessage();
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    public function tryimport()
    {
        $xmlFile = '<?xml version="1.0" encoding="UTF-8"?>
<EasyfattProducts AppVersion="2" Creator="Danea Easyfatt Professional  2013.21" CreatorUrl="http://www.danea.it/software/easyfatt" Mode="full">
  <Products>
    <Product>
      <InternalID>2</InternalID>
      <Code>TRYMAGDANEA</Code>
      <Description>Test Server Modulo Danea EasyFatt</Description>
      <DescriptionHtml>Test Server Modulo Danea EasyFatt</DescriptionHtml>
      <Category>MAGDANEACAT1</Category>
      <Subcategory>MAGDANEACAT2</Subcategory>
      <Subcategory2>MAGDANEACAT3</Subcategory2>
      <Vat Perc="21" Class="Imponibile" Description="Aliquota 21%">21</Vat>
      <Um>pz</Um>
      <NetPrice1>9.02</NetPrice1>
      <GrossPrice1>10.91</GrossPrice1>
      <NetPrice2>8.57</NetPrice2>
      <GrossPrice2>10.37</GrossPrice2>
      <NetPrice3>7.67</NetPrice3>
      <GrossPrice3>9.28</GrossPrice3>
      <SizeUm>cm</SizeUm>
      <NetSizeX>0</NetSizeX>
      <NetSizeY>0</NetSizeY>
      <WeightUm>kg</WeightUm>
      <NetWeight>0</NetWeight>
      <ManageWarehouse>true</ManageWarehouse>
      <AvailableQty>30</AvailableQty>
      <Notes>Test Server Modulo Danea EasyFatt</Notes>
      <Variant>
        <Size>XL</Size>
        <Color>Blu</Color>
        <Barcode>TRYMAGDANEA/XL/Blu</Barcode>
        <AvailableQty>5</AvailableQty>
      </Variant>
      <Variant>
        <Size>XL</Size>
        <Color>Rosso</Color>
        <Barcode>TRYMAGDANEA/XL/Ross</Barcode>
        <AvailableQty>6</AvailableQty>
      </Variant>
      <Variant>
        <Size>L</Size>
        <Color>Blu</Color>
        <Barcode>TRYMAGDANEA/L/Blu</Barcode>
        <AvailableQty>4</AvailableQty>
      </Variant>
      <Variant>
        <Size>S</Size>
        <Color>Blu</Color>
        <Barcode>TRYMAGDANEA/S/Blu</Barcode>
        <AvailableQty>5</AvailableQty>
      </Variant>
      <Variant>
        <Size>M</Size>
        <Color>Verde</Color>
        <Barcode>TRYMAGDANEA/M/Verd</Barcode>
        <AvailableQty>3</AvailableQty>
      </Variant>
      <Variant>
        <Size>M</Size>
        <Color>Rosso</Color>
        <Barcode>TRYMAGDANEA/M/Ross</Barcode>
        <AvailableQty>7</AvailableQty>
      </Variant>
    </Product>
  </Products>
</EasyfattProducts>';

        $media_dir = $this->directorylist->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->tmp_catalog_product_dir = $media_dir . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'catalog' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
        $this->catalog_product_dir = $media_dir . DIRECTORY_SEPARATOR . 'catalog' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
        $this->import_dir = $media_dir . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR;
        $this->base_url = $this->storeManager->getStore()->getBaseUrl();
        $this->_loadConfigs();
        $this->config->only_update = false;
        $this->config->type_id = \Magento\Catalog\Model\Product\Type::DEFAULT_TYPE;
        $this->config->attribute_set_id = $this->eavResourceModelEntityAttributeSetCollectionFactory->create()
            ->setEntityTypeFilter($this->catalogProductFactory->create()->getResource()->getEntityType()->getId())
            ->addFieldToFilter('attribute_set_name', 'Default')
            ->load()
            ->getFirstItem()
            ->getData('attribute_set_id');
        $this->config->price = 1;
        $this->config->price1 = 2;
        $this->config->price2 = 3;
        $this->config->special_price = null;
        $this->config->arapido = false;
        $this->config->website_ids = [];
        foreach ($this->storeManager->getWebsites() as $website) {
            $id = $website->getId();
            if ($id != 0) {
                $this->config->website_ids[] = $id;
            }
        }
        $this->config->store_ids = [];
        foreach ($this->storeManager->getStores() as $store) {
            $this->config->store_ids[] = $store->getId();
        }

        $this->config->stato_prodotti = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
        $this->config->visibility = \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH;
        $this->config->note_desc = false;
        $this->config->note_shortdesc = true;
        $this->config->danea_campi = [];
        $this->config->use_variant = true;
        $this->config->name_taglia_colore = 'Taglia - Colore';
        $this->config->type_taglia_colore = 'drop_down';
        $this->config->variant_2to1 = true;
        $this->config->variants_as_configurable = true;
        foreach ($this->config->store_ids as $store_id) {
            $this->rootCategoryStoreIds[$store_id] = $this->storeManager->getStore($store_id)->getRootCategoryId();
        }
        $this->config->stato_cat = 0;

        $this->cron_jobs = [];
        $this->cron_jobs['directly'] = false;

        $this->report = [];
        $this->report['Ptotali'] = 0;
        $this->report['Paggiornati'] = 0;
        $this->report['Peliminati'] = 0;
        $this->report['Pnoneliminati'] = 0;
        $this->report['Paggiunti'] = 0;
        $this->report['Psaltati'] = 0;
        $this->report['categorie'] = 0;
        $this->report['Celiminate'] = 0;
        $this->report['upload_img'] = 0;

        $this->filename = 'prova.xml';

        $xml = $this->_parseXml($xmlFile);

        $this->danea_version = 2;

        $timeEnd = time() + 60;
        $i = 0;
        do {
            $productId = $this->catalogProductFactory->create()->getIdBySku('TRYMAGDANEA');
            if ($productId) {
                $product = $this->catalogProductFactory->create();
                $productResource = $product->getResource();
                $productResource->load($product, $productId);
                $this->_deleteConfigurableProduct($product);
                $productResource->delete($product);
            }

            $this->_importProduct($xml['Products']['Product']);

            file_put_contents($this->daneaEasyFattDir->dirImportExport() . 'tryimport.txt', stripslashes($i));
            $i++;
        } while (time() < $timeEnd);

        $productId = $this->catalogProductFactory->create()->getIdBySku('TRYMAGDANEA');
        if ($productId) {
            $product = $this->catalogProductFactory->create();
            $productResource = $product->getResource();
            $productResource->load($product, $productId);
            $this->_deleteConfigurableProduct($product);
            $productResource->delete($product);
        }

        $_categoryCollection = $this->catalogResourceModelCategoryCollectionFactory->create()
            ->addAttributeToFilter('name', 'MAGDANEACAT1');
        $_categoryCollection->load();
        $category = $this->catalogCategoryFactory->create();
        $category->getResource()->load($category, $_categoryCollection->getFirstItem()->getId());
        $category->getResource()->delete($category);
    }

    private function _attributeValueExists($arg_value, $code)
    {
        $options = $this->{$code . '_attribute_options_model'}->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                return $option['label'];
            }
        }

        return false;
    }

    /**
     * Crea, se non esiste, l'attributo con codice assegnato. Ritorna il lavore dell'opzione creata
     * @param string $arg_value valore dell'attributo
     * @param string $code codice dell'attributo
     * @return string
     */
    private function _addAttributeValue($arg_value, $code = 'manufacturer')
    {
        if (!$this->_attributeValueExists($arg_value, $code)) {
            $value['option'] = array($arg_value, $arg_value);
            $result = array('value' => $value);
            $this->{$code . '_attribute'}->setData('option', $result);
            $this->{$code . '_attribute'}->save();

            $this->{$code . '_attribute_options_model'} = $this->eavEntityAttributeSourceTableFactory->create();
            $this->{$code . '_attribute_table'} = $this->{$code . '_attribute_options_model'}->setAttribute($this->{$code . '_attribute'});
        }

        $options = $this->{$code . '_attribute_options_model'}->getAllOptions(false);
        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                return $option['value'];
            }
        }
    }

    private static function _charsW3cProducts($string)
    {
        if (empty($string) && $string != '0') {
            return false;
        }
        return htmlentities($string, ENT_NOQUOTES, 'UTF-8');
    }

    private function _getTax($tassa)
    {

        /**
         * @var \Webprojectsol\DaneaEasyFatt\Model\Taxes
         */
        // protected $taxesModel;

        /**
         * @var \Magento\Tax\Model\Calculation\Rate
         */
        //protected $rateModel;
//        $base = $this->objectManager->get(\Magento\Sales\Model\Order\Invoice\Config::class);
//        $taxesModel = $this->objectManager->get(\Magento\Tax\Model\Calculation\Rate::class);
//        $rateModel = $this->objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\Taxes::class);
//
//
//        $taxRate = $rateModel->getResourceCollection()->addFieldToSelect('tax_calculation_rate_id')
//            ->addFieldToFilter('code', ['eq' => $tax['code']])
//            ->setPageSize(1)
//            ->setCurPage(1)
//            ->getFirstItem();
//
//
//        $joinConditions = 'e.entity_id = store_price.product_id';
//        $collection->addAttributeToSelect('*');
//        $collection->getSelect()->join(
//                ['store_price'], $joinConditions, []
//            )->columns("store_price.product_price")
//            ->where("store_price.store_id=1");
//
//
//
//        $this->taxesModel->getResource()->load($this->taxesModel, $taxRate->getId(), 'rate_id');
//        return 'FC';
        try {
            $TDTassa = $this->_read->fetchAll("SELECT tc.product_tax_class_id FROM " . $this->_readRes->getTableName('tax_calculation_rate') . " tcr INNER JOIN " . $this->_readRes->getTableName('tax_calculation') . " tc ON tc.tax_calculation_rate_id = tcr.tax_calculation_rate_id INNER JOIN " . $this->_writeRes->getTableName('daneaeasyfatt_taxes') . " dt ON dt.rate_id = tcr.tax_calculation_rate_id WHERE dt.tipo_danea  = '" . $tassa . "' LIMIT 1");

            return trim(isset($TDTassa[0]['product_tax_class_id']) ? $TDTassa[0]['product_tax_class_id'] : '0');
        } catch (\Exception $e) {
            $message = __('There were errors trying some type of tax. Error: ') . $e->getMessage();
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    private function _fileExistsRegex($img, $action = 'count', $other = false, $check = false)
    {
        $name = substr($img, 0, strrpos($img, '.'));
        $extension = substr($img, strrpos($img, '.'));
        $files = glob($name . "*" . $extension);

        if ($action == 'match') {
            if ($check) {
                $match = '';
            } else {
                $match = '_[0-9]+';
            }
            if (is_array($files)) {
                foreach ($files as $file) {
                    if (preg_match('#' . $name . '(' . $match . ')' . $extension . '#', $other)) {
                        return true;
                    }
                }
            }
        } elseif ($action == 'count') {
            return sizeof($files);
        } elseif ($action == 'delete') {
            if (is_array($files)) {
                foreach ($files as $file) {
                    if (!$other || ($other && $file != $img)) {
                        @unlink($file);
                        @unlink(strtr($file, array($this->catalog_product_dir => $this->tmp_catalog_product_dir)));
                    }
                }
            }
        }
    }

    private function _parseXml($xmlContent)
    {
        try {
            return \Webprojectsol\DaneaEasyFatt\Helper\Data::_parseXml($xmlContent);
        } catch (\Exception $e) {
            $message = $e->getTraceAsString() . "\n" . $e->getCode() . ': ' . $e->getMessage() . ' on ' . $e->getFile() . ' on line ' . $e->getLine();
            $this->logger->debug($message);
            $this->report['errori_det'][] = $message;
        }
    }

    public function exceptionsErrorHandler($errno, $errstr, $errfile, $errline)
    {
        if (error_reporting() == 0) {
            return;
        }
        switch ($errno) {
            case E_NOTICE:
            case E_USER_NOTICE:
                $errors = "Notice";
                break;
            case E_WARNING:
            case E_USER_WARNING:
                $errors = "Warning";
                break;
            case E_ERROR:
            case E_USER_ERROR:
                $errors = "Fatal Error";
                break;
            default:
                $errors = "Unknown";
                break;
        }
        $this->logger->debug('PHP ' . $errors . ': ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
        $this->report['errori_det'][] = '<b>' . $errors . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';

        throw new \Exception('<b>' . $errors . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>');
    }
}
