<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model;

/**
 * @method \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Taxes _getResource()
 * @method \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Taxes getResource()
 * @method int getId()
 * @method string getTipoShop()
 * @method string getTipoShopCode()
 * @method string getTipoDanea()
 * @method string getDescDanea()
 */
class Taxes extends \Magento\Framework\Model\AbstractModel
{

    const CACHE_TAG = 'daneaeasyfatt_taxes';

    /**
     * @var string
     */
    protected $_cacheTag = 'daneaeasyfatt_taxes';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'daneaeasyfatt_taxes';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Taxes');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }
}
