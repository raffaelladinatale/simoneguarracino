<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Report;

/**
 * Aion Test collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'daneaeasyfatt_reports_id';

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
    \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory, \Psr\Log\LoggerInterface $logger, \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy, \Magento\Framework\Event\ManagerInterface $eventManager, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\DB\Adapter\AdapterInterface $connection = null, \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    )
    {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->storeManager = $storeManager;
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webprojectsol\DaneaEasyFatt\Model\Report', 'Webprojectsol\DaneaEasyFatt\Model\ResourceModel\Report');
    }
}
