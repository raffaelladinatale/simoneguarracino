<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Model\ResourceModel;

/**
 * Aion Test resource model
 */
class ExportOrders extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\Dir
     */
    protected $daneaEasyFattDir;

    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context, \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory, $connectionName = null)
    {
        $this->daneaEasyFattDir = $daneaEasyFattDirFactory->create();
        parent::__construct($context, $connectionName);
    }

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('daneaeasyfatt_export_filename', 'filename_id');
    }

    /**
     * Delete the object
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     * @throws \Exception
     */
    public function delete(\Magento\Framework\Model\AbstractModel $object)
    {
        $dir = $this->daneaEasyFattDir->dirImportExport();
        if (file_exists($dir . $object->getFilename())) {
            @unlink($dir . $object->getFilename());
        }
        return parent::delete($object);
    }
}
