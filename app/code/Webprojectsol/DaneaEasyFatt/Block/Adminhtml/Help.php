<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml;

class Help extends \Magento\Framework\View\Element\Text
{

    const XML_PATH_DISABLEHELP = 'wpsbase/daneaeasyfatt/help';

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    public function __construct(
    \Magento\Framework\View\Element\Context $context, \Magento\Backend\Model\Session $backendSession, array $data = []
    )
    {
        $this->backendSession = $backendSession;
        parent::__construct(
            $context, $data
        );
    }

    protected function _toHtml()
    {
        $this->setText('');
        if ($this->_scopeConfig->getValue(self::XML_PATH_DISABLEHELP, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            return parent::_toHtml();
        }
        foreach ($this->getChildNames() as $name) {
            if ($this->backendSession->{'get' . $name}()) {
                continue;
            }
            $block = $this->getLayout()->getBlock($name);
            if (!$block) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Invalid block: %1', $name));
            }

            $this->addText(strtr($block->toHtml(), array('$HELP_TEXT' => $this->getHelpText($name), '$HELP_NAME' => $name)));
        }

        return parent::_toHtml();
    }

    private function getHelpText($name)
    {
        switch ($name) {
            case 'daneaeasyfatt_importcatalog_index_help':
                return __('This page contains all the catalogs imported.<br />To import a catalog directly from EasyFatt you open the software and go to <em>Strumenti-&gt;E-commerce-&gt;Aggiorna prodotti-&gt;Impostazioni</em> <small><span style="color:red">*</span></small>, copy the link from the button <u>Link to the Import/Update directed by EasyFatt</u>, enter the access data set in the module configuration and press <u>Invia</u>. Then the system will split virtually the catalog, depending on the capabilities of the server, and avvier&egrave; import via cron jobs.<br />From here you can perform actions on individual catalogs:<ul><li>&nbsp;Import/Re-import a catalog by choosing <b>Import</b></li><li>&nbsp;Stop the importation in course choosing <b>Stop Import</b></li><li>&nbsp;Check the detailed report of import if the status is completed or monitor the progress of the import if the state is in progress, choosing <b>Report</b></li><li>&nbsp;Delete an imported catalog by choosing <b>Delete</b></li></ul>Or actions for multiple catalogs simultaneously:<ul><li>&nbsp;Download catalogs selected by choosing <b>Download</b></li><li>&nbsp;Remove catalogs selected by choosing <b>Delete</b></li></ul><small><span style="color:red">*</span> If there isn\'t an option called <em>E-commerce</em> go <em>Strumenti-&gt;Opzioni-&gt;Moduli</em> and check the box E-commerce</small>');
            case 'daneaeasyfatt_importcatalog_report_help':
                return __('This page contains the details of the import of the catalog and the main configurations used during the latter.<br />There are details such as: time spent, total products in the catalog, updated products, products added, deleted products, categories added, errors, etc.<br />You can re-import the catalog in question by pressing <u>Import</u>.');
            case 'daneaeasyfatt_importcatalog_process_help':
                return __('On this page you can check the progress of the import. When the process is complete you will be redirected to the Report of the import.');
            case 'daneaeasyfatt_importcatalog_upload_help':
                return __('On this page you can upload a new XML EasyFatt catalog.');
            case 'daneaeasyfatt_exportorders_index_help':
                return __('This page contains all orders exported.<br />To export orders directly from EasyFatt you open the software and go to <em>Strumenti-&gt;E-commerce-&gt;Scarica ordini-&gt;Impostazioni</em> <small><span style="color:red">*</span></small>, copy the link from the button <u>Link to the Export Orders/Customers directed by EasyFatt</u>, enter the access data set in the module configuration and press <u>Scarica</u>.<br />Or you can export orders manually by pressing the button <u>New export</u>, then download the generated file and import it into EasyFatt by <em>Strumenti-&gt;E-commerce-&gt;Scarica ordini-&gt;Importa da file</em><br />From here you can perform actions on individual exports:<ul><li>&nbsp;Download a file generated with orders choosing <b>Download</b></li><li>&nbsp;Delete a file generated with orders choosing <b>Delete</b></li></ul>Or more actions for exports at the same time:<ul><li>&nbsp;Download the selected files by choosing <b>Download</b></li><li>&nbsp;Delete the selected files by choosing <b>Delete</b></li></ul><small><span style="color:red">*</span> If there isn\'t an option called <em>E-commerce</em> go <em>Strumenti->Opzioni->Moduli</em> and check the box E-commerce</small>');
            case 'daneaeasyfatt_exportorders_new_help':
                return __('This page allows you to generate an xml file with the orders according to the chosen parameters. <br /> To generate a file simply fill in the form below (the parameters are optional) and then click <u>Save new export</u>.');
            case 'daneaeasyfatt_taxes_index_help':
                return __('This page contains a list of VAT rates of EasyFatt and their association with the taxes of Magento.<br />Each of EasyFatt VAT rate is associated with the percentage of tax choice.<br />To insert a new association must click <u>New Rate</u>.<br />You also can modify or delete the submitted rates.');
            case 'daneaeasyfatt_taxes_edit_help':
                return __('From this page you change the association between a VAT rate of EasyFatt and that of Magento.');
            case 'daneaeasyfatt_payments_index_help':
                return __('This page contains a list of the types of payment EasyFatt and their association with the payment methods in Magento.<br />Each type of payment EasyFatt is associated with the chosen payment method.<br />To insert a new association must click <u>New Payment Type</u>.<br />You also can edit or delete payment types entered.');
            case 'daneaeasyfatt_payments_edit_help':
                return __('From this page you change the association between a type of payment and EasyFatt to Magento.');
            case 'daneaeasyfatt_license_index_help':
                return __('If you see this page most likely has not yet activated the license key module for this domain. To do so, simply click here: <a target="_blank" href="http://www.webprojectsol.com/clients/?languages=en">http://www.webprojectsol.com/clients/</a> and enter the details of the order made to buy this module and the name of the domain. After obtaining the license key, you can refresh the page and proceed to the next step.');
            case 'daneaeasyfatt_cron_index_help':
                return __('In questa pagina si verifica se i cron jobs possono essere impostati automaticamente o manualmente. Se &egrave; possibile automaticamente il sistema ne imposter&agrave; uno immediamente per verificarne il corretto funzionamento, maggiori info nel messaggio sotto. Se il cron job viene eseguito correttamente sarai reindirizzato al prossimo step.');
            case 'daneaeasyfatt_tryimport_index_help':
                return __('When importing the catalog EasyFatt, the module will split virtually Magento because it was unable to import many products all at once. For this reason, this page is used to verify how many products Magento unable to import at once. To get started click on the verification <u>Get the value </u>.');
            case 'daneaeasyfatt_tryimport_try_help':
                return __('On this page you start the process of verification of the import capacity of the server. Once the upload is finished, you can click on the button <u>Save the value obtained</u> to save the value obtained.');
            case 'daneaeasyfatt_config_help':
                if ($this->_request->getParam('section') == 'importcatalog') {
                    return __('In this section you can configure the module for the operation of import catalog');
                } elseif ($this->_request->getParam('section') == 'exportorders') {
                    return __('In this section you can configure the module for the operation of export document (orders/customers/invoices/etc)');
                }
        }
    }
}
