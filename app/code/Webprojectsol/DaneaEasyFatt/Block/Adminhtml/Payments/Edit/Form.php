<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml\Payments\Edit;

/**
 * Adminhtml Aion item edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Payment\Model\Config
     */
    protected $paymentConfig;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Data\FormFactory $formFactory, \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, \Magento\Payment\Model\Config $paymentConfig, \Magento\Store\Model\System\Store $systemStore, array $data = []
    )
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_systemStore = $systemStore;
        $this->paymentConfig = $paymentConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('daneaeasyfatt_payments_form');
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('daneaeasyfatt_payment');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('item_');

        $fieldset = $form->addFieldset(
            'base_fieldset', ['legend' => __('Payment information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('daneaeasyfatt_payments_id', 'hidden', ['name' => 'daneaeasyfatt_payments_id']);
        }

        $fieldset->addField(
            'payment_code', 'select', [
            'name' => 'payment_code',
            'label' => __('Payment in Magento'),
            'title' => __('Payment in Magento'),
            'required' => true,
            'options' => $this->_getPaymentAsOptions($model->getPaymentCode())
            ]
        );

        $fieldset->addField(
            'tipo_danea', 'text', [
            'name' => 'tipo_danea',
            'label' => __('Payment Type Danea'),
            'title' => __('Payment Type Danea'),
            'required' => true,
            ]
        );

        $fieldset->addField(
            'desc_danea', 'text', [
            'name' => 'desc_danea',
            'label' => __('Payment Description Danea'),
            'title' => __('Payment Description Danea'),
            'required' => false,
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    private function _getPaymentAsOptions($paymentCodeEditing)
    {

        $paymentCollection = $this->_coreRegistry->registry('daneaeasyfatt_payment')->getResourceCollection()
            ->addFieldToSelect('payment_code');

        if ($paymentCodeEditing) {
            $paymentCollection->addFieldToFilter('payment_code', ['neq' => $paymentCodeEditing]);
        }

        $daneaPayments = [];
        foreach ($paymentCollection->getItems() as $payment) {
            $daneaPayments[] = $payment->getPaymentCode();
        }

        $payments = $this->paymentConfig->getActiveMethods();
        $options = [];
        $options[''] = __('-- Please Select --');
        foreach (array_keys($payments) as $paymentCode) {
            if (in_array($paymentCode, $daneaPayments)) {
                continue;
            }
            $paymentTitle = $this->_scopeConfig
                ->getValue('payment/' . $paymentCode . '/title');
            $options[$paymentCode] = $paymentTitle;
        }

        return $options;
    }
}
