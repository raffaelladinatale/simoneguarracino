<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml\ExportOrders\Export;

/**
 * Adminhtml Aion item edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Data\FormFactory $formFactory, \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, \Magento\Store\Model\System\Store $systemStore, array $data = []
    )
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('exportorders_form');
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'enctype' => 'multipart/form-data', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('item_');

        $fieldset = $form->addFieldset(
            'base_fieldset', ['legend' => __('Export New Document'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'type', 'select', [
            'name' => 'type',
            'label' => __('Type'),
            'title' => __('Type'),
            'required' => true,
            'options' => $this->_docTypes()
            ]
        );

        $fieldset->addField(
            'firstdate', 'date', [
            'name' => 'firstdate',
            'label' => __('Date minimum order for export (ie "2008-01-01") [Optional]:'),
            'title' => __('Date minimum order for export (ie "2008-01-01") [Optional]:'),
            'date_format' => 'yyyy-MM-dd',
            'showsTime' => false,
            'class' => 'validate-date'
            ]
        );

        $fieldset->addField(
            'lastdate', 'date', [
            'name' => 'lastdate',
            'label' => __('Date maximum order for export (ie "2008-12-31") [Optional]:'),
            'title' => __('Date maximum order for export (ie "2008-12-31") [Optional]:'),
            'date_format' => 'yyyy-MM-dd',
            'showsTime' => false,
            'class' => 'validate-date'
            ]
        );

        $fieldset->addField(
            'firstnum', 'text', [
            'name' => 'firstnum',
            'label' => __('First no. order to export (eg "1") [Optional]:'),
            'title' => __('First no. order to export (eg "1") [Optional]:'),
            ]
        );

        $fieldset->addField(
            'lastnum', 'text', [
            'name' => 'lastnum',
            'label' => __('Second nr. order to export (eg "45") [Optional]:'),
            'title' => __('Second nr. order to export (eg "45") [Optional]:'),
            ]
        );

        $form->setValues(['type' => $this->_scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Export::XML_PATH_DOC_TYPE)]);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    private function _docTypes()
    {
        $docTypes = new \Webprojectsol\DaneaEasyFatt\Model\System\Config\DocTypes();

        $options = [];
        foreach ($docTypes->toOptionArray() as $docType) {
            $options[$docType['value']] = $docType['label'];
        }
        return $options;
    }
}
