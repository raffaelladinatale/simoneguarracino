<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml\Taxes\Edit;

/**
 * Adminhtml Aion item edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Tax\Model\Calculation\Rate
     */
    protected $rateModel;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Data\FormFactory $formFactory, \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, \Magento\Tax\Model\Calculation\Rate $rateModel, \Magento\Store\Model\System\Store $systemStore, array $data = []
    )
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_systemStore = $systemStore;
        $this->rateModel = $rateModel;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('daneaeasyfatt_taxes_form');
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('daneaeasyfatt_tax');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('item_');

        $fieldset = $form->addFieldset(
            'base_fieldset', ['legend' => __('Tax information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('daneaeasyfatt_taxes_id', 'hidden', ['name' => 'daneaeasyfatt_taxes_id']);
        }

        $fieldset->addField(
            'rate_id', 'select', [
            'name' => 'rate_id',
            'label' => __('Shop Tax Rate Code'),
            'title' => __('Shop Tax Rate Code'),
            'required' => true,
            'options' => $this->_getRateAsOptions($model->getRateId())
            ]
        );

        $fieldset->addField(
            'tipo_danea', 'text', [
            'name' => 'tipo_danea',
            'label' => __('Danea Tax Code'),
            'title' => __('Danea Tax Code'),
            'required' => true,
            ]
        );

        $fieldset->addField(
            'desc_danea', 'text', [
            'name' => 'desc_danea',
            'label' => __('Danea Tax Description'),
            'title' => __('Danea Tax Description'),
            'required' => false,
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    private function _getRateAsOptions($rateId)
    {

        $taxCollection = $this->_coreRegistry->registry('daneaeasyfatt_tax')
            ->getResourceCollection()
            ->addFieldToSelect('rate_id');

        if ($rateId) {
            $taxCollection->addFieldToFilter('rate_id', ['neq' => $rateId]);
        }

        $ids = [];
        foreach ($taxCollection->getItems() as $tax) {
            $ids[] = $tax->getRateId();
        }

        $rateCollection = $this->rateModel->getResourceCollection()
            ->addFieldToSelect('code')
            ->addFieldToSelect('tax_calculation_rate_id');
        if ($ids) {
            $rateCollection->addFieldToFilter('tax_calculation_rate_id', ['nin' => $ids]);
        }

        $options = [];
        $options[''] = __('-- Please Select --');
        foreach ($rateCollection->getItems() as $rate) {
            $options[$rate->getId()] = $rate->getCode();
        }

        return $options;
    }
}
