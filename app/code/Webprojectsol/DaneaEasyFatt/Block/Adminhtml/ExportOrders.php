<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml;

class ExportOrders extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('add');
        $this->addButton('copy_export_url', array(
            'label' => __('Link to the Export Orders/Customers directed by EasyFatt'),
            'onclick' => 'window.alert(\'' . strtr($this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB) . 'danea/direct.php?app=Export', array('\'' => '&apos;')) . '\')',
            'class' => 'save primary'
        ));
    }

    protected function _prepareLayout()
    {
        $this->toolbar->pushButtons($this, $this->buttonList);
        return $this;
    }
}
