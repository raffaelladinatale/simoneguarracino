<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml\ImportCatalog;

class Report extends \Magento\Backend\Block\Widget\Grid\Container
{

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog
     */
    protected $importCatalog;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\Report
     */
    protected $report;

    public function __construct(\Magento\Backend\Block\Widget\Context $context, \Webprojectsol\DaneaEasyFatt\Model\ReportFactory $reportFactory, \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog $importCatalog, array $data = array())
    {
        $this->report = $reportFactory->create();
        $this->importCatalog = $importCatalog;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('add');
        $this->addButton('back', array(
            'label' => __('Back'),
            'onclick' => 'setLocation(\'' . $this->getUrl('*/*/index') . '\')',
            'class' => 'back'
        ));
        $this->addButton('import', array(
            'label' => __('Import'),
            'onclick' => 'setLocation(\'' . $this->getUrl('*/*/import', array('filename_id' => $this->getRequest()->getParam('filename_id'))) . '\')',
            'class' => 'save primary'
        ));
    }

    protected function _prepareLayout()
    {
        $this->toolbar->pushButtons($this, $this->buttonList);
        return $this;
    }

    public function getReport()
    {
        $id = $this->getRequest()->getParam('filename_id');
        $this->importCatalog->getResource()->load($this->importCatalog, $id);

        if ($this->importCatalog->isNotImported()) {
            return null;
        }
        $this->report->getResource()->load($this->report, $this->importCatalog->getFilename(), 'filename');
        return $this->report;
    }
}
