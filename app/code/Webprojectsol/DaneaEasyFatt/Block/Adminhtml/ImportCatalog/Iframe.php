<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml\ImportCatalog;

class Iframe extends \Magento\Framework\View\Element\Template
{ //\Magento\Backend\Block\Template {

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */

    protected $_objectManager;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Backend\App\Action\Context $contextAction, array $data = [])
    {
        $this->_objectManager = $contextAction->getObjectManager();

        parent::__construct($context, $data);
    }

    public function getCronProcess()
    {
        $id = $this->getRequest()->getParam('filename_id');
        $importCatalog = $this->_objectManager->create('\Webprojectsol\DaneaEasyFatt\Model\ImportCatalog');
        $importCatalog->getResource()->load($importCatalog, $id);
        return $this->_objectManager->create('\Webprojectsol\DaneaEasyFatt\Model\System\Cron')->process($importCatalog->getFilename());
    }
}
