<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Block\Adminhtml;

use Magento\Framework\Module\ModuleListInterface;

class Extensions extends \Magento\Config\Block\System\Config\Form\Fieldset
{

    protected $_fieldRenderer;

    /**
     * @var \Magento\Config\Block\System\Config\Form\Field
     */
    protected $configSystemConfigFormField;

    /**
     * @var Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    public function __construct(
    \Magento\Config\Block\System\Config\Form\Field $configSystemConfigFormField, ModuleListInterface $moduleList, \Magento\Backend\Block\Context $context, \Magento\Backend\Model\Auth\Session $authSession, \Magento\Framework\View\Helper\Js $jsHelper, array $data = []
    )
    {
        $this->configSystemConfigFormField = $configSystemConfigFormField;
        $this->moduleList = $moduleList;
        parent::__construct($context, $authSession, $jsHelper, $data);
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = $this->_getHeaderHtml($element);

        $modules = array_keys((array) $this->moduleList->getAll());
        sort($modules);

        foreach ($modules as $moduleName) {
            if (strstr($moduleName, 'Webprojectsol_') === false || $moduleName == 'Webprojectsol_Base') {
                continue;
            }

            $html .= $this->_getFieldHtml($element, $moduleName);
        }
        $html .= $this->_getFooterHtml($element);

        return $html;
    }

    protected function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $this->_fieldRenderer = $this->configSystemConfigFormField;
        }
        return $this->_fieldRenderer;
    }

    protected function _getFieldHtml($fieldset, $moduleCode)
    {
        $module = $this->moduleList->getOne($moduleCode);

        $currentVer = $module['setup_version'];
        if (!$currentVer) {
            return '';
        }

        $moduleName = (isset($module['sequence'][1]) ? $module['sequence'][1] : substr($moduleCode, strpos($moduleCode, '_') + 1));

        $aggiornabile = false;
        $lastVer = '';
        if (isset($module['sequence'][0])) {
            $lastVer = $this->openRemoteUrl('http://www.webprojectsol.com/extern/CheckVersion.php?code=' . $module['sequence'][0] . '&version=' . $currentVer);
            if (version_compare($lastVer, $currentVer, '>')) {
                $aggiornabile = true;
            }
        }

        $field = $fieldset->addField($moduleCode, 'label', array(
                'name' => 'dummy',
                'label' => $moduleName,
                'value' => $currentVer . ($aggiornabile ? ' => ' . $lastVer : ''),
            ))->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }

    private function openRemoteUrl($u, $post = null)
    {
        if (function_exists('curl_init')) {
            $c = curl_init($u);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            if ($post) {
                curl_setopt($c, CURLOPT_POST, 1);
                curl_setopt($c, CURLOPT_POSTFIELDS, $post);
            }
            $result = curl_exec($c);
            curl_close($c);
            return $result;
        }
    }
}
