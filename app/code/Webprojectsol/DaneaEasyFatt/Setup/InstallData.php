<?php

/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */

namespace Webprojectsol\DaneaEasyFatt\Setup;

/* use Aion\Test\Model\Test;
  use Aion\Test\Model\TestFactory; */

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * Test factory
     *
     * @var TestFactory
     * /
      private $testFactory;

      /**
     * Init
     *
     * @param TestFactory $testFactory
     * /
      public function __construct(TestFactory $testFactory) {
      $this->testFactory = $testFactory;
      }

      /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup,
            ModuleContextInterface $context)
    {
        /* $testItems = [
          [
          'name' => 'John Doe',
          'email' => 'john.doe@example.com',
          'is_active' => 1,
          ],
          [
          'name' => 'Jane Doe',
          'email' => 'jane.doe@example.com',
          'is_active' => 0,
          ],
          [
          'name' => 'Steve Test',
          'email' => 'steve.test@example.com',
          'is_active' => 1,
          ],
          ];

          /**
         * Insert default items
         * /
          foreach ($testItems as $data) {
          $this->createTest()->setData($data)->save();
          } */

        $setup->endSetup();
    }
    /**
     * Create Test item
     *
     * @return Test

      public function createTest() {
      return $this->testFactory->create();
      }
     */
}
