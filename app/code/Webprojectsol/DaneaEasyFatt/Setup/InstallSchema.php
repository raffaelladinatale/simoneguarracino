<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * Install table
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'daneaeasyfatt_taxes'
         */
        $connection = $installer->getConnection();

        $connection->dropTable($connection->getTableName('daneaeasyfatt_taxes'));
        $daneaeasyfatt_taxes_table = $connection->newTable(
            $installer->getTable('daneaeasyfatt_taxes')
        );

        $daneaeasyfatt_taxes_table->addColumn(
            'daneaeasyfatt_taxes_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true],
            'DaneaEasyFatt Tax ID'
        )->addColumn(
            'rate_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Tax Rate Magento'
        )->addColumn(
            'tipo_danea',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            128,
            ['nullable' => false, 'default' => ''],
            'Tax EasyFatt'
        )->addColumn(
            'desc_danea',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            128,
            ['nullable' => true],
            'Description Tax EasyFatt'
        )->setComment(
            'DaneaEasyFatt Tax Table'
        )->addForeignKey(
            'magento_rate_id',
            'rate_id',
            $installer->getTable('tax_calculation_rate'),
            'tax_calculation_rate_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $connection->createTable($daneaeasyfatt_taxes_table);

        /**
         * Create table 'daneaeasyfatt_payments'
         */
        $connection->dropTable($connection->getTableName('daneaeasyfatt_payments'));
        $daneaeasyfatt_payments_table = $connection->newTable(
            $installer->getTable('daneaeasyfatt_payments')
        );
        $daneaeasyfatt_payments_table->addColumn(
            'daneaeasyfatt_payments_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true],
            'DaneaEasyFatt Payment ID'
        )->addColumn(
            'payment_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            128,
            ['nullable' => false],
            'Payment Magento'
        )->addColumn(
            'tipo_danea',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            128,
            ['nullable' => false, 'default' => ''],
            'Payment EasyFatt'
        )->addColumn(
            'desc_danea',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            128,
            ['nullable' => true],
            'Description Payment EasyFatt'
        )->setComment(
            'DaneaEasyFatt Payment Table'
        );
        $connection->createTable($daneaeasyfatt_payments_table);

        /**
         * Create table 'daneaeasyfatt_import_filename'
         */
        $connection->dropTable($connection->getTableName('daneaeasyfatt_import_filename'));
        $daneaeasyfatt_import_filename_table = $connection->newTable(
            $installer->getTable('daneaeasyfatt_import_filename')
        );
        $daneaeasyfatt_import_filename_table->addColumn(
            'filename_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true],
            'DaneaEasyFatt Xml ID'
        )->addColumn(
            'filename',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            35,
            ['nullable' => false, 'default' => ''],
            'Xml file'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            8,
            ['nullable' => false, 'default' => \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_PENDING],
            'Status Importation'
        )->addColumn(
            'size',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            64,
            ['nullable' => false, 'default' => ''],
            'Size file'
        )->addColumn(
            'date',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Date uploaded'
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('daneaeasyfatt_import_filename'),
                ['filename'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['filename'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'DaneaEasyFatt Import Xml Table'
        );
        $connection->createTable($daneaeasyfatt_import_filename_table);

        /**
         * Create table 'daneaeasyfatt_reports'
         */
        $connection->dropTable($connection->getTableName('daneaeasyfatt_reports'));
        $daneaeasyfatt_reports_table = $connection->newTable(
            $installer->getTable('daneaeasyfatt_reports')
        );
        $daneaeasyfatt_reports_table->addColumn(
            'daneaeasyfatt_reports_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true],
            'DaneaEasyFatt Report ID'
        )->addColumn(
            'filename',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            35,
            ['nullable' => false, 'default' => ''],
            'Xml file'
        )->addColumn(
            'json_report',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Report json'
        )->addColumn(
            'json_cron',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Cron job json'
        )->setComment(
            'DaneaEasyFatt Report Table'
        )->addForeignKey(
            'daneaeasyfatt_reports_filename',
            'filename',
            $installer->getTable('daneaeasyfatt_import_filename'),
            'filename',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $connection->createTable($daneaeasyfatt_reports_table);

        /**
         * Create table 'daneaeasyfatt_crons'
         */
        $connection->dropTable($connection->getTableName('daneaeasyfatt_crons'));
        $daneaeasyfatt_crons_table = $connection->newTable(
            $installer->getTable('daneaeasyfatt_crons')
        );
        $daneaeasyfatt_crons_table->addColumn(
            'daneaeasyfatt_crons_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true],
            'DaneaEasyFatt Cron ID'
        )->addColumn(
            'filename',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            35,
            ['nullable' => false, 'default' => ''],
            'Xml file'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            7,
            ['nullable' => false, 'default' => 'pending'],
            'Status Cron job'
        )->addColumn(
            'function',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            64,
            ['nullable' => false, 'default' => ''],
            'Function Cron job'
        )->addColumn(
            'numbers',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            24,
            ['nullable' => false, 'default' => '0'],
            'Numbers Cron job'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '0'],
            'Orders Cron job'
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('daneaeasyfatt_crons'),
                ['filename'],
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            ['filename'],
            ['type' => AdapterInterface::INDEX_TYPE_INDEX]
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('daneaeasyfatt_crons'),
                ['filename', 'function', 'numbers'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['filename', 'function', 'numbers'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'DaneaEasyFatt Cron job Table'
        )->addForeignKey(
            'daneaeasyfatt_crons_filename',
            'filename',
            $installer->getTable('daneaeasyfatt_import_filename'),
            'filename',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $connection->createTable($daneaeasyfatt_crons_table);

        /**
         * Create table 'daneaeasyfatt_export_filename'
         */
        $connection->dropTable($connection->getTableName('daneaeasyfatt_export_filename'));
        $daneaeasyfatt_export_filename_table = $connection->newTable(
            $installer->getTable('daneaeasyfatt_export_filename')
        );
        $daneaeasyfatt_export_filename_table->addColumn(
            'filename_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true],
            'DaneaEasyFatt Xml ID'
        )->addColumn(
            'filename',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            35,
            ['nullable' => false, 'default' => ''],
            'Xml file'
        )->addColumn(
            'number',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'default' => '0'],
            'Number of documents'
        )->addColumn(
            'size',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            64,
            ['nullable' => false, 'default' => ''],
            'Size file'
        )->addColumn(
            'date',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Date uploaded'
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('daneaeasyfatt_export_filename'),
                ['filename'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['filename'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'DaneaEasyFatt Export Xml Table'
        );
        $connection->createTable($daneaeasyfatt_export_filename_table);

        $installer->endSetup();
    }
}
