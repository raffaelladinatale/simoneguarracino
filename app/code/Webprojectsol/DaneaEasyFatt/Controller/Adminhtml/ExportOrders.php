<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml;

abstract class ExportOrders extends DaneaEasyFatt
{

    const ADMIN_RESOURCE = 'Webprojectsol_DaneaEasyFatt::exportorders';

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\Dir
     */
    protected $daneaEasyFattDir;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\ExportFactory
     */
    protected $daneaEasyFattExportFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\ExportFactory $daneaEasyFattExportFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $coreRegistry,
        \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory,
        \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory,
        \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory,
        \Webprojectsol\DaneaEasyFatt\Model\ExportFactory $daneaEasyFattExportFactory
    ) {
        $this->daneaEasyFattDir = $daneaEasyFattDirFactory->create();
        $this->daneaEasyFattExportFactory = $daneaEasyFattExportFactory;
        parent::__construct(
            $context,
            $scopeConfig,
            $coreRegistry,
            $daneaEasyFattSystemLicenseFactory,
            $daneaEasyFattSystemCronFactory
        );
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Webprojectsol_DaneaEasyFatt::exportorders')
            ->addBreadcrumb(__('Danea EasyFatt'), __('Danea EasyFatt'))
            ->addBreadcrumb(__('Export Orders/Customers'), __(''));
        return parent::initPage($resultPage);
    }
}
