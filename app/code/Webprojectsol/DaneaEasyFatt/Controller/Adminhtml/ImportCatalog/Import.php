<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ImportCatalog;

class Import extends \Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ImportCatalog
{

    /**
     * Edit item
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('filename_id');
        $model = $this->_objectManager->create(\Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::class);

        // 2. Initial checking
        if ($id) {
            $model->getResource()->load($model, $id);

            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This catalog no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $resource = $this->_objectManager->create(\Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ImportCatalog\Collection::class);

            $imports = $resource->addFieldToSelect('filename')
                    ->addFieldToFilter(['status', 'status'], [
                        ['eq' => \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_PENDING],
                        ['eq' => \Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_RUNNING]
                    ])->getData();

            try {
                if ($imports) {
                    $files = '';
                    foreach ($imports as $import) {
                        $files .= $import['filename'] . ', ';
                    }

                    throw new \Magento\Framework\Exception\LocalizedException(__('The following files are still being imported: %1. Before importing other catalogs wait until the end of the importation or stop those already underway.', [substr($files, 0, -2)]));
                }

                $this->daneaEasyFattImportFactory->create()->setCron($model->getFilename());
                $model->setStatus(\Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_PENDING);
                $model->getResource()->save($model);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
//	    } catch (Exception $e) {
                $this->messageManager->addErrorMessage(
                    \Webprojectsol\DaneaEasyFatt\Helper\Data::getExceptionMessage($e)
                );
                return $resultRedirect->setPath('*/*/');
            }

            $this->messageManager->addSuccessMessage(__('The file is being imported.'));

            return $resultRedirect->setPath('*/*/process', array('filename_id' => $id));
        }

        $this->messageManager->addErrorMessage(__('This catalog no longer exists.'));
        return $resultRedirect->setPath('*/*/');
    }
}
