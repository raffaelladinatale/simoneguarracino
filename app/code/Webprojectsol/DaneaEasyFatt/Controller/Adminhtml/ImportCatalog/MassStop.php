<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ImportCatalog;

use Magento\Ui\Component\MassAction\Filter;

class MassStop extends \Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ImportCatalog
{

    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ImportCatalog\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\ImportFactory $daneaEasyFattImportFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ImportCatalog\CollectionFactory $collectionFactory
     * @param Filter $filter
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $coreRegistry,
        \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory,
        \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory,
        \Webprojectsol\DaneaEasyFatt\Model\ImportFactory $daneaEasyFattImportFactory,
        \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ImportCatalog\CollectionFactory $collectionFactory,
        Filter $filter
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->filter = $filter;
        parent::__construct(
            $context,
            $scopeConfig,
            $coreRegistry,
            $daneaEasyFattSystemLicenseFactory,
            $daneaEasyFattSystemCronFactory,
            $daneaEasyFattImportFactory
        );
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $stopped = 0;

        foreach ($collection as $item) {
            if ($item->canBeStopped()) {
                $stopped++;
                $item->stopImport(
                    $this->scopeConfig->getValue(
                        \Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_AUTOCRON,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    )
                );
            }
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been stopped.', $stopped));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
