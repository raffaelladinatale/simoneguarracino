<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\Taxes;

class Edit extends \Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\Taxes
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Registry $coreRegistry, \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory, \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $scopeConfig, $coreRegistry, $daneaEasyFattSystemLicenseFactory, $daneaEasyFattSystemCronFactory);
    }

    /**
     * Edit item
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('daneaeasyfatt_taxes_id');
        $tax = $this->_objectManager->create(\Webprojectsol\DaneaEasyFatt\Model\Taxes::class);

        // 2. Initial checking
        if ($id) {
            $tax->load($id);
            if (!$tax->getId()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        // 3. Set entered data if was error when we do save
        $data = $this->_objectManager->get(\Magento\Backend\Model\Session::class)->getFormData(true);
        if (!empty($data)) {
            $tax->setData($data);
        }

        // 4. Register model to use later in blocks
        $this->_coreRegistry->register('daneaeasyfatt_tax', $tax);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        // 5. Build edit form
        $this->initPage($resultPage)->addBreadcrumb(
            $tax->getId() ? __('Edit Tax') : __('New Tax'), $id ? __('Edit Tax') : __('New Tax')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Taxes'));
        $resultPage->getConfig()->getTitle()->prepend($tax->getId() ? __('Edit Tax') : __('New Tax'));
        return $resultPage;
    }
}
