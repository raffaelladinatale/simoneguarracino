<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\TryImport;

class SaveTryImport extends \Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\TryImport
{

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\Dir
     */
    protected $daneaEasyFattDir;

    /**
     *
     * @var  \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $writer;

    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\Dir $daneaEasyFattDir
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $writer
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Registry $coreRegistry, \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory, \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory, \Webprojectsol\DaneaEasyFatt\Model\Dir $daneaEasyFattDir, \Magento\Framework\App\Config\Storage\WriterInterface $writer
    )
    {
        $this->daneaEasyFattDir = $daneaEasyFattDir;
        $this->writer = $writer;
        parent::__construct($context, $scopeConfig, $coreRegistry, $daneaEasyFattSystemLicenseFactory, $daneaEasyFattSystemCronFactory);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $file_progress = $this->daneaEasyFattDir->dirImportExport() . 'prova.xml_Progress.txt';
        if (file_exists($file_progress)) {
            unlink($file_progress);
        }
        $this->writer->save(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_NUMBERPRODUCT, $this->getRequest()->getPost('tryimportvalue'));
        $this->writer->save(\Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\DaneaEasyFatt::XML_PATH_TRYIMPORT, 0);
        $this->messageManager->addSuccessMessage(__('The configuration has been saved.'));
        return $resultRedirect->setPath('adminhtml/system_config/edit', array('section' => 'importcatalog'));
    }
}
