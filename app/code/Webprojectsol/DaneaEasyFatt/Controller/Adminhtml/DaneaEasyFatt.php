<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml;

abstract class DaneaEasyFatt extends \Magento\Backend\App\Action
{

    const XML_PATH_CRONTEST = 'importcatalog/cron/cron_test';
    const XML_PATH_TRYIMPORT = 'importcatalog/cron/tryimport';
    const ADMIN_RESOURCE = 'Webprojectsol_DaneaEasyFatt::menu';

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory
     */
    protected $daneaEasyFattSystemLicenseFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory
     */
    protected $daneaEasyFattSystemCronFactory;

    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Registry $coreRegistry, \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory, \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory
    )
    {
        $this->daneaEasyFattSystemLicenseFactory = $daneaEasyFattSystemLicenseFactory;
        $this->daneaEasyFattSystemCronFactory = $daneaEasyFattSystemCronFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function initPage($resultPage)
    {
        if (!$this->daneaEasyFattSystemLicenseFactory->create()->check()) {
            $this->_redirect('*/license/index');
        } elseif ($this->scopeConfig->getValue(self::XML_PATH_CRONTEST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            if (!$this->daneaEasyFattSystemCronFactory->create()->check(true)) {
                $this->_redirect('*/cron/index');
            }
        } elseif ($this->scopeConfig->getValue(self::XML_PATH_TRYIMPORT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            $this->_redirect('*/tryimport/index');
        }

        return $resultPage;
    }
}
