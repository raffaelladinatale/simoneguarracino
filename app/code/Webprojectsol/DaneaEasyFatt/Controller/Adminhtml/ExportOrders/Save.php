<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ExportOrders;

class Save extends \Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ExportOrders
{

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if data sent
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $dati = $this->daneaEasyFattExportFactory->create()->export();

            $model = $this->_objectManager->create(\Webprojectsol\DaneaEasyFatt\Model\ExportOrders::class);

            $model->createExport($dati['xml'], $dati['numOrdini']);

            // try to save it
            try {
                // save the data

                $model->getResource()->save($model);
                // display success message
                $this->messageManager->addSuccessMessage(__('The file has been successfully exported.'));
                // clear previously saved data from session
                $this->_objectManager->get(\Magento\Backend\Model\Session::class)->setFormData(false);

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // save data in session
                $this->_objectManager->get(\Magento\Backend\Model\Session::class)->setFormData($data);
                // redirect to edit form
                return $resultRedirect->setPath('*/*/export');
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
