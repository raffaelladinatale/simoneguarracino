<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ExportOrders;

class Download extends \Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ExportOrders
{

    /**
     * Edit item
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('filename_id');
        $model = $this->_objectManager->create(\Webprojectsol\DaneaEasyFatt\Model\ExportOrders::class);
        $model->getResource()->load($model,
            $id);

        $file = $this->daneaEasyFattDir->dirImportExport() . $model->getFilename();
        // 2. Initial checking
        if ($id) {
            $model->getResource()->load($model,
                $id);

            if (!$model->getId() || !file_exists($file)) {
                $this->messageManager->addErrorMessage(__('This document no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->getResponse()->setHeader('content-type',
            'application/force-download',
            true);
        $this->getResponse()->setHeader('content-type',
            'application/x-octet-stream');
        $this->getResponse()->setHeader('content-disposition',
            'attachment; filename="' . urldecode($model->getFilename()) . '"',
            true);

        $this->getResponse()->setBody(file_get_contents(urldecode($file)));
        $this->getResponse()->sendResponse();
    }
}
