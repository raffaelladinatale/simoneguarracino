<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ExportOrders;

use Magento\Ui\Component\MassAction\Filter;

class MassDelete extends \Webprojectsol\DaneaEasyFatt\Controller\Adminhtml\ExportOrders
{

    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     *
     * @var \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ExportOrders\CollectionFactory
     */
    protected $collectionFactory;

    /**
     *
     * @var string
     */
    protected $fileId = 'filename';

    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\ExportFactory $daneaEasyFattExportFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ExportOrders\CollectionFactory $collectionFactory
     * @param Filter $filter
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Registry $coreRegistry, \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory, \Webprojectsol\DaneaEasyFatt\Model\System\CronFactory $daneaEasyFattSystemCronFactory, \Webprojectsol\DaneaEasyFatt\Model\DirFactory $daneaEasyFattDirFactory, \Webprojectsol\DaneaEasyFatt\Model\ExportFactory $daneaEasyFattExportFactory, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Webprojectsol\DaneaEasyFatt\Model\ResourceModel\ExportOrders\CollectionFactory $collectionFactory, Filter $filter
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->collectionFactory = $collectionFactory;
        $this->filter = $filter;
        parent::__construct($context, $scopeConfig, $coreRegistry, $daneaEasyFattSystemLicenseFactory, $daneaEasyFattSystemCronFactory, $daneaEasyFattDirFactory, $daneaEasyFattExportFactory);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $importCatalog) {
            $importCatalog->getResource()->delete($importCatalog);
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
