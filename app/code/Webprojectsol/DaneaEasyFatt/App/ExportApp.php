<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\App;

class ExportApp extends AbstractApp
{

    public function run()
    {
        $export = $this->_objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\Export::class);
        try {
            $dati = $export->export();
            $exportOrders = $this->_objectManager->create(\Webprojectsol\DaneaEasyFatt\Model\ExportOrders::class);

            $exportOrders->createExport($dati['xml'],
                $dati['numOrdini']);
            $exportOrders->getResource()->save($exportOrders);

            $this->_response->setHeader('content-type',
                'application/xml',
                true);
            $this->_response->setBody($dati['xml']);
        } catch (\Exception $ex) {
            $this->logger->debug($ex->getTraceAsString());
        }
        $this->_response->sendResponse();
    }

    protected function _getDaneaDataLogin()
    {
        $data = new \stdClass();
        $data->user = \Webprojectsol\DaneaEasyFatt\Model\Export::XML_PATH_ACCESS_DATA_LOGIN;
        $data->password = \Webprojectsol\DaneaEasyFatt\Model\Export::XML_PATH_ACCESS_DATA_PASSWORD;

        return $data;
    }
}
