<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\App;

class ImageApp extends AbstractApp
{

    public function run()
    {
        try {
            $file = $this->_request->getQuery('file');

            $report = $this->_objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\Report::class)->getByFilename(trim($file));
            if ($report) {
                $jsonOldCron = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode($report->getJsonCron(), true);
                $jsonOldReport = \Webprojectsol\DaneaEasyFatt\Helper\Data::_JsonDecode($report->getJsonReport(), true);

                $jsonOldReport['tempo_ftp_impiegato'] = \Webprojectsol\DaneaEasyFatt\Helper\Data::_SecondsToWords((microtime(true) - $jsonOldReport['tempo_ftp_iniziale']));
                unset($jsonOldReport['tempo_ftp_iniziale']);
                $jsonReport = json_encode($jsonOldReport);
                unset($jsonOldCron['sended_image']);
                $jsonOldCron['sended_image'] = true;
                $jsonCron = json_encode($jsonOldCron);

                $report->setJsonCron(strtr($jsonCron, array('\'' => '&apos;')));
                $report->setJsonReport(strtr($jsonReport, array('\'' => '&apos;')));

                $report->getResource()->save($report);
            }
            $this->_response->setBody('OK');
        } catch (\Exception $e) {
            $this->logger->debug(\Webprojectsol\DaneaEasyFatt\Helper\Data::getExceptionMessage($e));
        }
        $this->_response->sendResponse();
    }

    protected function _getDaneaDataLogin()
    {
        return null;
    }

    static public function needLogin()
    {
        return false;
    }
}
