<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\App;

abstract class AbstractApp extends \Magento\Framework\App\Http implements \Magento\Framework\AppInterface
{

    /**
     * @var \Magento\User\Model\UserFactory
     */
    protected $userUserFactory;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $backendAuthSession;

    /**
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface 
     */
    protected $encryptor;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory
     */
    protected $daneaEasyFattSystemLicenseFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Webprojectsol\DaneaEasyFatt\Logger\Logger
     */
    protected $logger;

    public function __construct(
    \Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Framework\Event\Manager $eventManager, \Magento\Framework\App\AreaList $areaList, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Response\Http $response, \Magento\Framework\ObjectManager\ConfigLoaderInterface $configLoader, \Magento\Framework\App\State $state, \Magento\Framework\Registry $registry, \Magento\User\Model\UserFactory $userUserFactory, \Magento\Backend\Model\Auth\Session $backendAuthSession, \Magento\Framework\Encryption\EncryptorInterface $encryptor, \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Webprojectsol\DaneaEasyFatt\Logger\Logger $logger
    ) {
        $this->userUserFactory = $userUserFactory;
        $this->backendAuthSession = $backendAuthSession;
        $this->encryptor = $encryptor;
        $this->daneaEasyFattSystemLicenseFactory = $daneaEasyFattSystemLicenseFactory;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        parent::__construct($objectManager,
            $eventManager,
            $areaList,
            $request,
            $response,
            $configLoader,
            $state,
            $registry);
    }

    public function launch()
    {
        if (!self::needLogin()) {
            $this->run();
            return $this->_response;
        }

        if (!$this->_adminLogin()) {
            $this->_response
                ->setStatusCode(\Magento\Framework\App\Response\Http::STATUS_CODE_403)
                ->setReasonPhrase(__('HTTP/1.1 403 Admin authentication failed: Please check Username and Password'));

            return $this->_response;
        }

        if ($this->backendAuthSession->isLoggedIn()) {
            if (!$this->registry->registry('isSecureArea')) {
                $this->registry->register('isSecureArea',
                    true);
            }

            if (!$this->_checkDanea()) {
                return $this->_response;
            }
            if (!$this->_daneaLogin()) {
                $this->_response
                    ->setStatusCode(\Magento\Framework\App\Response\Http::STATUS_CODE_403)
                    ->setReasonPhrase(__('HTTP/1.1 403 Authentication failed: Please check Username and Password'));
                return $this->_response;
            }

            $this->run();
        }

        return $this->_response;
    }

    static public function needLogin()
    {
        return true;
    }

    abstract public function run();

    /**
     * @return \stdClass Oggetto con user e password per il login di EasyFatt
     */
    abstract protected function _getDaneaDataLogin();

    public function catchException(\Magento\Framework\App\Bootstrap $bootstrap, \Exception $exception): bool
    {
        return false;
    }

    /**
     * @return bool Ritorna true se il login va a buon fine, false altrimenti
     */
    protected function _adminLogin()
    {
        $access_data = explode(':',
            base64_decode($this->_request->getServer('HTTP_X_AUTHORIZATION')));

        $adminLogin = $access_data[0];
        $adminPassword = $access_data[1];

        $user = $this->userUserFactory->create()->loadByUsername($adminLogin);

        if ($this->encryptor->validateHash($adminPassword,
                $user->getPassword())) {
            $this->registry->register('isSecureArea',
                true);
            $this->backendAuthSession->setIsFirstVisit(true);
            $this->backendAuthSession->setUser($user);
            $this->_eventManager->dispatch('admin_session_user_login_success',
                array('user' => $user));
            return true;
        }
        return false;
    }

    protected function _checkDanea()
    {
        if (!$this->daneaEasyFattSystemLicenseFactory->create()->check()) {
            $this->_response
                    ->setStatusCode(\Magento\Framework\App\Response\Http::STATUS_CODE_403)
                    ->setReasonPhrase(__('HTTP/1.1 403 The module do not has an active license key'));
            return false;
        }
        return true;
    }

    protected function _daneaLogin()
    {
        return true;
        $data = $this->_getDaneaDataLogin();
        $storeId = $this->storeManager->getStore()->getId();
        $user = $this->scopeConfig->getValue($data->user,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId);
        $password = $this->encryptor->decrypt($this->scopeConfig->getValue($data->password,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId));
        if (base64_decode($this->_request->getServer('HTTP_X_AUTHORIZATION')) == $user . ":" . $password) {
            return true;
        }
        return false;
    }
    
    public static function getAllowedApps() {
        return ['Export', 'Import', 'Image'];
    }
}
