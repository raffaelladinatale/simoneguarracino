<?php
/**
 * Copyright (c) 2017 Web Project Solutions (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 * @author Antonello Venturino <info@webprojectsol.com>
 * @copyright  2017 Web Project Solutions
 * @version  Release: 2.1.7 $
 * @license    http://www.webprojectsol.com/license.php
 * @url  http://www.webprojectsol.com/it/modulo-interfacciamento-danea-easyfatt-ed-open-source-e-commerce.html
 */
namespace Webprojectsol\DaneaEasyFatt\App;

class ImportApp extends AbstractApp
{

    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    public function __construct(
    \Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Framework\Event\Manager $eventManager, \Magento\Framework\App\AreaList $areaList, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Response\Http $response, \Magento\Framework\ObjectManager\ConfigLoaderInterface $configLoader, \Magento\Framework\App\State $state, \Magento\Framework\Registry $registry, \Magento\User\Model\UserFactory $userUserFactory, \Magento\Backend\Model\Auth\Session $backendAuthSession, \Magento\Framework\Encryption\EncryptorInterface $encryptor, \Webprojectsol\DaneaEasyFatt\Model\System\LicenseFactory $daneaEasyFattSystemLicenseFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Webprojectsol\DaneaEasyFatt\Logger\Logger $logger, \Magento\Framework\UrlInterface $urlInterface
    )
    {
        $this->urlInterface = $urlInterface;
        parent::__construct($objectManager, $eventManager, $areaList, $request, $response, $configLoader, $state, $registry, $userUserFactory, $backendAuthSession, $encryptor, $daneaEasyFattSystemLicenseFactory, $storeManager, $scopeConfig, $logger);
    }

    public function run()
    {
        $importCatalog = $this->_objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::class);
        try {
            $importCatalog->createImport();
            $importCatalog->getResource()->save($importCatalog);

            $import = $this->_objectManager->get(\Webprojectsol\DaneaEasyFatt\Model\Import::class);
            $result = $import->setCron($importCatalog->getFilename(), true, $this->storeManager->getStore()->getId());
            $importCatalog->setStatus(\Webprojectsol\DaneaEasyFatt\Model\ImportCatalog::STATUS_PENDING);
            $importCatalog->getResource()->save($importCatalog);
            $content = $result[0];
            if ($result[1] >= 2 && $result[2]) {
                $content .= $this->_printFtpCommand($importCatalog->getFilename());
            }
            $this->_response->setBody($content);
        } catch (\Exception $e) {
            $this->logger->debug(\Webprojectsol\DaneaEasyFatt\Helper\Data::getExceptionMessage($e));
            $this->logger->debug($e->getTraceAsString());
        }
        $this->_response->sendResponse();
    }

    private function _printFtpCommand($filename)
    {
        $content = '';
        $storeId = $this->storeManager->getStore()->getId();
        $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        if ($this->scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_FTP_ENABLE, $scope, $storeId)) {
            $ftp_user = $this->scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_FTP_USERNAME, $scope, $storeId);
            $ftp_pass = $this->scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_FTP_PASSWORD, $scope, $storeId);
            $ftp_dominio = $this->scopeConfig->getValue(\Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_FTP_DOMINIO, $scope, $storeId);
            $content .= "\n" . 'ImageSendURL=ftp://' . urlencode($ftp_user) . ':' . urlencode($ftp_pass) . '@' . $ftp_dominio;
            $content .= "\n" . 'ImageSendFinishURL=' . $this->storeManager->getStore($storeId)->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB) . 'danea/direct.php?app=Image&file=' . $filename;
        }
        return $content;
    }

    protected function _getDaneaDataLogin()
    {
        $data = new \stdClass();
        $data->user = \Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_ACCESS_DATA_LOGIN;
        $data->password = \Webprojectsol\DaneaEasyFatt\Model\Import::XML_PATH_ACCESS_DATA_PASSWORD;

        return $data;
    }
}
