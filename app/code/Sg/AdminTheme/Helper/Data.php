<?php

namespace Sg\AdminTheme\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const CONFIG_IMAGE_DIR_PREFIX = 'sg/admintheme/';

    protected $_storeManager;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getConfig($config_path, $section = 'sg_admintheme_section') {
        return $this->scopeConfig->getValue(
                        $section . '/' . $config_path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getBaseUrlConfigImage() {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . SELF::CONFIG_IMAGE_DIR_PREFIX;
    }

    public function getLogoImage() {
        if ($this->getConfig("general/logo_image")) {
            return $this->getBaseUrlConfigImage() . $this->getConfig("general/logo_image");
        } else {
            return null;
        }
    }

    public function getLogoIconImage() {
        if ($this->getConfig("general/logo_icon_image")) {
            return $this->getBaseUrlConfigImage() . $this->getConfig("general/logo_icon_image");
        } else {
            return null;
        }
    }

    public function getBgImage() {
        if ($this->getConfig("general/bg_image")) {
            return $this->getBaseUrlConfigImage() . $this->getConfig("general/bg_image");
        } else {
            return null;
        }
    }

    public function getExtraStyle() {
        return $this->getConfig("general/extra_style");
    }

}
