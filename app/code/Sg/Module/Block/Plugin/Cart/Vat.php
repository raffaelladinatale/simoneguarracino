<?php

namespace Sg\Module\Block\Plugin\Cart;

use Magento\Framework\View\LayoutInterface;

class Vat
{
    /** @var LayoutInterface */
    protected $_layout;

    public function __construct(LayoutInterface $layout)
    {
        $this -> _layout = $layout;
    }

    public function afterGetConfig(\Magento\Checkout\Block\Cart\Sidebar $subject, array $result)
    {
        $cmsBlockVat = $this -> _layout -> createBlock('Magento\Cms\Block\Block') -> setBlockId('label_vat');

        if ($cmsBlockVat) {
            $result['vatMinicart'] = $cmsBlockVat -> toHtml();
        }

        return $result;
    }
}
