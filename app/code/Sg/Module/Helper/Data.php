<?php
namespace Sg\Module\Helper;

use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */
    protected $stockState;
    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var \Magento\Catalog\Model\CategoryRepository
     */
    protected $_categoryRepository;

    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $_mathRandom;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $_resourceModelProduct;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    protected $_categoryFactory;

    protected $_timezoneInterface;

    /**
     * @var Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param \Magento\Catalog\Model\ResourceModel\Product $resourceModelProduct
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\CategoryRepository $categoryRepository
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockState
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
     * @param \Magento\Catalog\Block\Product\ListProduct $listBlock
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     */
    public function __construct(
        \Magento\Framework\Math\Random $mathRandom,
        \Magento\Catalog\Model\ResourceModel\Product $resourceModelProduct,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Block\Product\ListProduct $listBlock,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\Session $customerSession
    ){
        $this->_mathRandom = $mathRandom;
        $this->_categoryFactory = $categoryFactory;
        $this->_resourceModelProduct = $resourceModelProduct;
        $this->_storeManager = $storeManager;
        $this->_categoryRepository = $categoryRepository;
        $this->_productRepository = $productRepository;
        $this->_filterProvider = $filterProvider;
        $this->stockState = $stockState;
        $this->_timezoneInterface = $timezoneInterface;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_listBlock = $listBlock;
        $this->_countryFactory = $countryFactory;
        $this->priceCurrency = $priceCurrency;
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
    }

    public function getContentUrl($linkType, $linkCustom, $linkCategory, $linkProduct)
    {
        $_url = null;

        switch ($linkType) {
            case 'default':
            case 'none':
                $_url = '';
                break;
            case 'custom':
                $_url = $this->getFilteredContent($linkCustom);
                break;

            case 'category':
                $_url = $this->getCategoryUrlById($linkCategory);
                break;

            case 'product':
                $_url = $this->getProductUrlBySku($linkProduct);
                break;
        }

        return trim($_url);
    }

    /**
     * @param $categoryId
     * @return |null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCategoryUrlById($categoryId) {
        return ( $categoryId ) ? $this->_categoryRepository->get($categoryId)->getUrl() : null;
    }

    /**
     * @param $string
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getFilteredContent($string) {
        return $this->_filterProvider->getBlockFilter()->setStoreId($this->_storeManager->getStore()->getId())->filter($string);
    }

    /**
     * @param $productSku
     * @return |null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductUrlBySku($productSku) {
        $productId = $this->_resourceModelProduct->getIdBySku($productSku);
        if ($productId) {
            $product = $this->_productRepository->getById($productId);
            return $product->getUrlModel()->getUrl($product);
        } else {
            return null;
        }
    }

    /**
     * @param $config_path
     * @param $section
     * @return mixed
     */
    public function getConfig($config_path, $section) {
        return $this->scopeConfig->getValue(
            $section . '/' . $config_path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $length
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getRandomString($length) {
        return $this->_mathRandom->getRandomString($length);
    }

    public function getUrlMedia($path) {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $path;
    }

    public function getProductsCollection($categoryId) {
        $category = $this->_categoryFactory->create()->load($categoryId);
        $collection = $category->getProductCollection()->addAttributeToSelect(['id','product_url', 'image' ,'small_image', 'name', 'price', 'special_price', 'final_price', 'news_from_date', 'product_status', 'shipping_from', 'extension_attributes']);
        $collection->addAttributeToFilter('home_bestseller', 1);
        $collection->setOrder('position', 'ASC');
        $collection->setPageSize(16);
        return $collection;
    }

    public function getPercentageDiscount($basePrice, $specialPrice) {
        try {
            $diff = ($basePrice - $specialPrice);
            if ($basePrice > 0) {
                $percentVal = round((($diff * 100) / $basePrice), 0);
            } else {
                $percentVal = 0;
            }

            return $percentVal;
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getStockQty($productId, $websiteId = null){
        return $this->stockState->getStockQty($productId, $websiteId);
    }

    public function getDate($_date) {
        $_dateTimeAsTimeZone = $this->_timezoneInterface->date(new \DateTime($_date));
        return $_dateTimeAsTimeZone;
    }

    public function getAddToCartPostParams($product){
        return $this->_listBlock->getAddToCartPostParams($product);
    }

    public function getCountryName($countryCode){
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }

    public function getCurrencySymbol($storeId)
    {
        $currencySymbol = $this->priceCurrency->getCurrencySymbol()->getCurrencyCode();
        return $currencySymbol;
    }

    public function getCurrentCurrencyCode()
    {
        return $this->priceCurrency->getCurrency()->getCurrencyCode();
    }

    public function getStoreCode()
    {
        return substr($this->_storeManager->getStore()->getCode(), -2);
    }

    public function getCustomerFirstname()
    {
        if($this->customerSession->getData('customer_id'))
        {
            $customer = $this->customerRepository->getById($this->customerSession->getData('customer_id'));
            return $customer->getFirstname();
        }
    }
}
