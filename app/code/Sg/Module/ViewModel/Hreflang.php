<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Smtp
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Sg\Module\ViewModel;

use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Cms\Model\ResourceModel\Page as PageResource;
use Magento\CmsUrlRewrite\Model\CmsPageUrlPathGenerator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class AccountManagement
 * @package Mageplaza\Smtp\Plugin
 */
class Hreflang  implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    protected $storeManager;
    protected $productUrlPathGenerator;

    protected $categoryRepository;
    /**
     * @var CategoryUrlPathGenerator
     */
    protected $categoryUrlPathGenerator;

    protected $productRepository;
    /**
     * @param ProductUrlPathGenerator $productUrlPathGenerator
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ProductUrlPathGenerator $productUrlPathGenerator,
        CategoryUrlPathGenerator $categoryUrlPathGenerator,
        StoreManagerInterface $storeManager,
        \Magento\Store\Block\Switcher $storeSwitcher,
        CategoryRepositoryInterface $categoryRepository,
        ProductRepositoryInterface $productRepository,
        PageRepositoryInterface $pageRepository,
        CmsPageUrlPathGenerator $cmsPageUrlPathGenerator,
        PageResource $pageResource
    ) {
        $this->productUrlPathGenerator = $productUrlPathGenerator;
        $this->categoryUrlPathGenerator = $categoryUrlPathGenerator;
        $this->storeManager = $storeManager;
        $this->storeSwitcher = $storeSwitcher;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository= $productRepository;
        $this->pageRepository = $pageRepository;
        $this->cmsPageUrlPathGenerator = $cmsPageUrlPathGenerator;
        $this->pageResource = $pageResource;
    }

    public function getStores(){
        return $this->storeSwitcher->getStores();
    }

    public function getProductUrl($product, $store)
    {
        $path = $this->productUrlPathGenerator->getUrlPathWithSuffix($product, $store->getId());
        return $store->getBaseUrl() . $path;
    }

    public function getStoreCodeCustom($storeCode){
        if($storeCode){
            $first = substr($storeCode, 0 , -3);
            $last = substr($storeCode, -2);
            if(!$first){
                $first = $last;
            }
            if($first == "en"){
                $first = "gb";
            }
            if($storeCode == "uk"){
                $first = "gb";
                $last = "en";
            }
            if($storeCode == "IE"){
                $last = "en";
            }
            if($storeCode == "AT" || $storeCode == "CH"){
                $last = "de";
            }
            return strtolower($last . "-" ) . strtoupper($first);
        }
    }

    public function getProductUrlByStoreView($productId, $storeId)
    {
        $productUrl = null;
        try {
            $product = $this->productRepository->getById($productId, false, $storeId);
            $productUrl = $product->getProductUrl();
        } catch (NoSuchEntityException $exception) {
            throw new $exception;
        }

        return $productUrl;
    }

    public function getCategortyUrlByStoreView($category, $store)
    {
        $categoryUrl = null;
        try {
            $category = $this->categoryRepository->get($category->getId(), $store->getId());
            $path =   $this->categoryUrlPathGenerator->getUrlPathWithSuffix($category);
            $categoryUrl = $store->getBaseUrl() . $path;
        } catch (NoSuchEntityException $exception) {
            throw new $exception;
        }

        return $categoryUrl;
    }

    public function getCmsUrlByStoreView($cmsPageId, $store)
    {
        try {
            $page = $this->pageRepository->getById($cmsPageId);
            $pageId = $this->pageResource->checkIdentifier($page->getIdentifier(), (int)$store->getId());
            $storePage = $this->pageRepository->getById($pageId);
            $path = $this->cmsPageUrlPathGenerator->getUrlPath($storePage);
            if($path == "home"){
                return $store->getBaseUrl();
            }
            return $store->getBaseUrl() . $path;
        } catch (LocalizedException $e) {
            return '';
        }
    }
}
