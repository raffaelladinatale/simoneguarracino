<?php

namespace Sg\Checkout\Plugin;

use Magento\Checkout\Model\Session as CheckoutSession;


class QuoteItemToOrderItemPlugin
{
    public function __construct(
        CheckoutSession $checkoutSession)
    {
        $this->checkoutSession = $checkoutSession;
    }

    public function aroundConvert(\Magento\Quote\Model\Quote\Item\ToOrderItem $subject, callable $proceed, $quoteItem, $data)
    {

        // get order item
        $orderItem = $proceed($quoteItem, $data);

        //set custom attribute to sales_order_item
        $orderItem->setInfoStock($quoteItem->getInfoStock());
        $orderItem->setInfoShippingfrom($quoteItem->getInfoShippingfrom());
        $orderItem->setInfoShippingfromDate($quoteItem->getInfoShippingfromDate());

        return $orderItem;
    }
}
