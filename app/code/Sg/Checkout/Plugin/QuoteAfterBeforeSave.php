<?php


namespace Sg\Checkout\Plugin;

use Magento\Quote\Model\Quote as MagentoQuote;


class QuoteAfterBeforeSave
{
    public function afterBeforeSave(MagentoQuote $subject, MagentoQuote $result): MagentoQuote
    {
        if ($result->getCustomerId()) {
            $result->setCustomerIsGuest(false);
        }

        return $result;
    }
}

