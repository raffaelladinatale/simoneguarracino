<?php
namespace Sg\Checkout\Plugin\Checkout\Model;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\LayoutInterface;

class DefaultConfigProvider
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * Constructor
     *
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        LayoutInterface $layout,
        \Sg\Module\Helper\Data $_helper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->_layout = $layout;
        $this->_helper = $_helper;
    }

    public function afterGetConfig(
        \Magento\Checkout\Model\DefaultConfigProvider $subject,
        array $result
    ) {
        $items = $result['totalsData']['items'];

        foreach ($items as $index => $item) {
            $quoteItem = $this->checkoutSession->getQuote()->getItemById($item['item_id']);

            if($quoteItem->getInfoStock() != null){
                $result['quoteItemData'][$index]['info_stock'] = $quoteItem->getInfoStock();
            }

            if($quoteItem->getInfoShippingFrom() != null){
                $result['quoteItemData'][$index]['info_shippingfrom'] = $quoteItem->getInfoShippingfrom();
            }

            if($quoteItem->getInfoShippingFromDate() != null){
                $result['quoteItemData'][$index]['info_shippingfrom_date'] = $quoteItem->getInfoShippingfromDate();
            }
        }
        return $result;
    }

}
