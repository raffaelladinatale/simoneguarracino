<?php
namespace Sg\Checkout\Plugin\Checkout\CustomerData;
use Magento\Checkout\Model\Session;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\LayoutInterface;

class DefaultItemPlugin
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */
    protected $stockState;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        Session $session,
        LayoutInterface $layout,
        \Sg\Module\Helper\Data $_helper,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState
    ) {
        $this->productRepository = $productRepository;
        $this->session = $session;
        $this->_layout = $layout;
        $this->_helper = $_helper;
        $this->stockState = $stockState;
    }
    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item $item) {
        $data = $proceed($item);
        $_product = $item->getProduct();
        $result = [];
        $existingQty = (int)$item->getProduct()->getData('extension_attributes')->getStockItem()->getData('qty');
        $orderedQty = $item->getQty();
        $shippingFrom = $this->_helper->getDate($item->getProduct()->getShippingFrom())->format('j-m-y');
        $orderedQtyFromDate = (int)$item->getProduct()->getResource()->getAttribute('ordered_qty')->getFrontend()->getValue($item->getProduct());
        $backOrders = $item->getData('backorders') ? $item->getData('backorders') : 0;

        $result['info_stock'] = '';
        $result['info_shippingfrom_date'] = '';
        $result['info_shippingfrom'] = '';

        $existingQtyMore0 = $existingQty < 0 ? 0 - $existingQty : 0;

        $qtyAvailable = $existingQty > $orderedQty ? $orderedQty : $existingQty;
        $backOrdersForShipping = ($orderedQtyFromDate - $existingQtyMore0) >= $backOrders ? $backOrders :  ($orderedQtyFromDate - $existingQtyMore0);

        if($existingQty - $orderedQty < 0) {
            if($existingQty > 0 ){
                $result['info_stock'] = '<div class="availability status-instock"><span>'. (int)($qtyAvailable) . ' x </span>' . $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId('info_stock')->toHtml() . '</div>';
            }
            if($backOrders != 0){
                if($shippingFrom != null &&  ($orderedQtyFromDate - $existingQtyMore0) > 0){
                    $result['info_shippingfrom_date'] = '<div class="availability status-preorder"><span>' . $backOrdersForShipping . ' x </span>' .  $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId('info_shippingfrom_date')->toHtml() . $this->_helper->getDate($_product->getShippingFrom())->format('j-m-y') . '</div>';
                }else{
                    $result['info_shippingfrom'] = '<div class="availability status-120gg"><span>' . $backOrders . ' x </span>' . $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId('info_shippingfrom')->toHtml() . '</div>';
                }
                if($shippingFrom != null && ($orderedQtyFromDate - $existingQtyMore0) > 0 &&  ($orderedQtyFromDate - $existingQtyMore0) < $backOrders){
                    $result['info_shippingfrom'] = '<div class="availability status-120gg"><span>' . ($backOrders - ($orderedQtyFromDate - $existingQtyMore0)) . ' x </span>' . $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId('info_shippingfrom')->toHtml() . '</div>';
                }
            }
        }else{
            $result['info_stock'] = '<div class="availability status-instock"><span>'. (int)($qtyAvailable) . ' x </span>' . $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId('info_stock')->toHtml() . '</div>';
        }

        $item->setData('info_stock', $result['info_stock'])->save();
        $item->setData('info_shippingfrom', $result['info_shippingfrom'])->save();
        $item->setData('info_shippingfrom_date', $result['info_shippingfrom_date'])->save();

        return array_merge($data,$result);
    }
}
