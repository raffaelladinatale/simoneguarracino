<?php
namespace Sg\Checkout\Plugin\Checkout;

class SgProcessorPlugin
{
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $jsLayout
    )
    {
        $shippingConfiguration =  $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['street'];
        $billingConfiguration = '';

            if (isset($shippingConfiguration)) {
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['children'][0]['label'] = __('Address');

                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['children'][1]['label'] = __('Address Number') .  ' *';
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['children'][1]['validation'] = 'required-entry';
            }

            if (isset($billingConfiguration)) {
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']['afterMethods']['children']
                ['billing-address-form']['children']['form-fields']['children']['street']['children'][0]['label'] = __('Address');


                $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']['afterMethods']['children']
                ['billing-address-form']['children']['form-fields']['children']['street']['children'][1]['label'] = __('Address Number') . ' *';
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']['afterMethods']['children']
                ['billing-address-form']['children']['form-fields']['children']['street']['children'][1]['validation'] = 'required-entry';

            }

        return $jsLayout;
    }
}
