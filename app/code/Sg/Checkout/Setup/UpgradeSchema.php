<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket Cart Reservation v2.x.x
 * @copyright   Copyright (c) 2017 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Sg\Checkout\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        try {
            $tableName = $setup->getTable('quote_item');
            $connection = $setup->getConnection();
            if ($connection->isTableExists($tableName) == true) {
                $connection->dropColumn(
                    $tableName,
                    'shipping_order_date'
                );
                $connection->dropColumn(
                    $tableName,
                    'order_quantity'
                );
                $connection->dropColumn(
                    $tableName,
                    'shipping_order_date_qty'
                );
                $connection->addColumn(
                    $tableName,
                    'info_stock',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'unsigned' => true, 'comment' => 'Label stock']
                );
                $connection->addColumn(
                    $tableName,
                    'info_shippingfrom_date',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'unsigned' => true, 'comment' => 'Label Shipping from date']
                );
                $connection->addColumn(
                    $tableName,
                    'info_shippingfrom',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'unsigned' => true, 'comment' => 'Label Shipping 120gg']
                );
            }
            $tableNameOrder = $setup->getTable('sales_order_item');
            if ($connection->isTableExists($tableNameOrder) == true) {
                $connection->dropColumn(
                    $tableNameOrder,
                    'shipping_order_date'
                );
                $connection->dropColumn(
                    $tableNameOrder,
                    'order_quantity'
                );
                $connection->dropColumn(
                    $tableNameOrder,
                    'shipping_order_date_qty'
                );
                $connection->addColumn(
                    $tableNameOrder,
                    'info_stock',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'unsigned' => true, 'comment' => 'Label stock']
                );
                $connection->addColumn(
                    $tableNameOrder,
                    'info_shippingfrom_date',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'unsigned' => true, 'comment' => 'Label Shipping from date']
                );
                $connection->addColumn(
                    $tableNameOrder,
                    'info_shippingfrom',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'unsigned' => true, 'comment' => 'Label Shipping 120gg']
                );
            }
        } catch (\Exception $e) {
        }

        $setup->endSetup();
    }
}
