<?php

declare(strict_types=1);

namespace Sg\Checkout\Model\CheckoutAgreements\Checkout\Plugin;

use Magento\Checkout\Api\PaymentInformationManagementInterface;
use Magento\CheckoutAgreements\Model\Checkout\Plugin\Validation as BaseValidation;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\PaymentInterface;

class Validation extends BaseValidation
{
    /**
     * @inheritDoc
     */
    public function beforeSavePaymentInformation(
        PaymentInformationManagementInterface $subject,
                                              $cartId,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    ) {
        // no need to check Checkout Agreements validation when set payment method from review step
    }
}
