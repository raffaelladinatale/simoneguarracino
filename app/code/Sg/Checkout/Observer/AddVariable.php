<?php
namespace Sg\Checkout\Observer;
use Magento\Sales\Model\Order;
use Magento\Framework\Event\ObserverInterface;

class AddVariable implements ObserverInterface
{
    protected $order;
    public function __construct(
        \Magento\Directory\Model\CountryFactory $countryFactory,
        Order $order
    ) {
        $this->order= $order;
        $this->_countryFactory = $countryFactory;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Framework\App\Action\Action $controller */
        $transport = $observer->getEvent()->getTransport();
        if($transport->getOrder() != null)
        {
            $order = $this->order->load($transport->getOrder()->getId());
            $shippingAddress = $order->getShippingAddress();
            $transport['shipping_firstname'] = $shippingAddress->getData('firstname');
            $transport['shipping_lastname'] = $shippingAddress->getData('lastname');
            $transport['shipping_street'] = $shippingAddress->getData('street');
            $transport['shipping_city'] = $shippingAddress->getData('city');
            $transport['shipping_region'] = $shippingAddress->getData('region');
            $transport['shipping_postcode'] = $shippingAddress->getData('postcode');
            $transport['shipping_country_id'] = $this->getCountryname($shippingAddress->getData('country_id'));
            $transport['shipping_telephone'] = $shippingAddress->getData('telephone');

            // Payment method
            $transport['payment_method_name'] = $order->getPayment()->getMethodInstance()->getTitle();
            $transport['payment_method'] = $order->getPayment()->getMethodInstance()->getCode();
            $transport['payment_method_cashondelivery'] = $order->getPayment()->getMethodInstance()->getCode() === 'cashondelivery' ? 'cashondelivery': '';
            $transport['payment_method_banktransfer'] = $order->getPayment()->getMethodInstance()->getCode() === 'banktransfer' ? 'banktransfer': '';
        }
    }

    public function getCountryname($countryCode){
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
}
