<?php
namespace Sg\Checkout\Observer;
use Magento\Framework\Event\ObserverInterface;

class QuoteSubmitBefore implements ObserverInterface
{
    protected $order;
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Stdlib\DateTime $dateTime
    ) {
        $this->localeDate = $localeDate;
        $this->dateTime = $dateTime;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();
        $createDate = $observer->getQuote()->getCreatedAt();
        $updateDate = $observer->getQuote()->getUpdatedAt();
        if ($quote && $order) {
            $order->setCreatedAt($this->localeDate->date(new \DateTime($createDate))->format('Y-m-d H:i:s'));
            $order->setUpdatedAt($this->localeDate->date(new \DateTime($updateDate))->format('Y-m-d H:i:s'));
        }
    }
}
