<?php

namespace Sg\Countdown\Block\Widget;

class Countdown extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('widget/countdown.phtml');
    }
}
