<?php
namespace Sg\Countdown\Block\Adminhtml\Widget;

class DateField extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_elementFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        array $data = []
    )
    {
        $this->_elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }

    public function prepareElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $input = $this->_elementFactory->create("date", ['data' => $element->getData()]);
        $input->setId($element->getId());
        $input->setForm($element->getForm());
        $input->setFormat(\Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT);
        $input->setTimeFormat('HH:mm:ss');
        $input->setShowsTime(true);
        $input->setClass("widget-option input-textarea admin__control-text");
        if ($element->getRequired()) {
            $input->addClass('required-entry');
        }

        $customStyle = "<style>.control-value {margin-right: 10px;}</style>";

        $element->setData('after_element_html',  $customStyle . $input->getElementHtml());
        return $element;
    }
}
