define([
    'jquery',
    'matchMedia',
    'Magento_Ui/js/modal/modal',
], function ($, mediaCheck, modal) {
    'use strict';

    $.widget('sg.countdown', {
        options: {
            title: false,
            popupContainer: false,
            finalDate: false
        },

        _create: function(){

            var self = this;
            self._super();
            var now = new Date().getTime();
            var countDownDate = new Date(this.options.finalDate).getTime();
            var distance = countDownDate - now;

            this._countDown(this.options.finalDate);

            if(distance > 0){
                self.initPopup(self.element, self.element);
                $(this.element).addClass("show");
            }else{
                $(this.element).removeClass("show");
            }
        },

        initPopup: function (el, container) {
            var self = this;

            //modal options
            var modalOptions = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                modalClass: 'countdown_popup',
                title: '',
                buttons: [],
                opened: function () {
                    $("body").addClass("countdown-popup-showing");
                    $('.modal-overlay').fadeIn();
                    $('.modals-wrapper').addClass("active");
                },
                closed: function () {
                    $('.modal-overlay').fadeOut();
                    $('.modals-wrapper').removeClass("active");
                    $("body").removeClass("countdown-popup-showing");
                }
            };

            var popup = modal(modalOptions, $(container));

            setTimeout(function(){
                    $(container).modal('openModal');
                }, 500);
            },

        _countDown: function (date){
            var self = this;

            var countDownDate = new Date(date).getTime();

            var x = setInterval(function() {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                document.getElementById("days").innerText = Math.floor(distance / (1000 * 60 * 60 * 24));
                document.getElementById("hours").innerText = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                document.getElementById("minutes").innerText = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                document.getElementById("seconds").innerText =  Math.floor((distance % (1000 * 60)) / 1000);


                /*if (distance < 0) {
                    clearInterval(x);
                    //$(".countdown-popup-showing .modals-wrapper").removeClass('active');
                    $(self.element).modal('closeModal');
                }*/
            }, 1000);
        }
    });
    return $.sg.countdown;
});
