<?php

namespace Amasty\CashOnDelivery\Model;

class ConfigProvider extends \Amasty\Base\Model\ConfigProviderAbstract
{
    protected $pathPrefix = 'payment/';

    const CASH_ON_DELIVERY_BLOCK = 'cashondelivery/';

    const CASH_ON_DELIVERY_ENABLED = 'active';

    const PAYMENT_FEE_ENABLED = 'enable_payment_fee';
    const PAYMENT_FEE_LABEL = 'payment_fee_label';
    const PAYMENT_FEE = 'payment_fee';
    const PAYMENT_FEE_TYPE = 'payment_fee_type';

    const PERCENT_CALCULATE_BASED_ON = 'payment_calculate_based_on';
    const FIXED_CALCULATE_BASED_ON = 'payment_calculate_based_on_fixed';
    const TAX_CLASS_FOR_FIXED = 'tax_class_for_fixed';
    const ALLOWED_SHIPPING = 'payment_shipping_allowspecific';
    const SPECIFIC_SHIPPING = 'payment_shipping_specificcountry';

    const CODE_VERIFICATION_ENABLED = 'enable_postcode_verification';
    const VERIFICATION_TYPE = 'postcode_verification_type';
    const ALLOWED_POSTAL_CODES = 'allowed_postal_codes';

    const ORDER_STATUS = 'order_status';

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isPaymentFeeEnabled($storeId = null)
    {
        return $this->isSetFlag(self::CASH_ON_DELIVERY_BLOCK . self::PAYMENT_FEE_ENABLED, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isCashOnDeliveryEnabled($storeId = null)
    {
        return $this->isSetFlag(self::CASH_ON_DELIVERY_BLOCK . self::CASH_ON_DELIVERY_ENABLED, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isCodeVerificationEnabled($storeId = null)
    {
        return $this->isSetFlag(self::CASH_ON_DELIVERY_BLOCK . self::CODE_VERIFICATION_ENABLED, $storeId);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getPaymentFeeLabel()
    {
        $paymentFeeLabel = $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::PAYMENT_FEE_LABEL)
            ?: 'Cash on Delivery Fee';

        return __($paymentFeeLabel);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getPaymentFee($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::PAYMENT_FEE, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getAllowedShipping($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::ALLOWED_SHIPPING, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getSpecificShipping($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::SPECIFIC_SHIPPING, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getVerificationType($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::VERIFICATION_TYPE, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getAllowedPostalCodes($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::ALLOWED_POSTAL_CODES, $storeId);
    }

    /**
     * @return string
     */
    public function getOrderStatus()
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::ORDER_STATUS);
    }

    /**
     * @param int|null $storeId
     * @return int
     */
    public function getPaymentFeeType($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::PAYMENT_FEE_TYPE, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return int
     */
    public function getPercentCalculateBasedOn($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::PERCENT_CALCULATE_BASED_ON, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return int
     */
    public function getFixedCalculateBasedOn($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::FIXED_CALCULATE_BASED_ON, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return int
     */
    public function getTaxClassForFixedFee($storeId = null)
    {
        return $this->getValue(self::CASH_ON_DELIVERY_BLOCK . self::TAX_CLASS_FOR_FIXED, $storeId);
    }
}
