<?php

namespace Amasty\CashOnDelivery\Model\Quote;

use Amasty\CashOnDelivery\Api\PaymentFeeRepositoryInterface;
use Amasty\CashOnDelivery\Model\Config\Source\FixedCalculateBasedOn;
use Amasty\CashOnDelivery\Model\Config\Source\PaymentFeeTypes;
use Amasty\CashOnDelivery\Model\Config\Source\PercentCalculateBasedOn;
use Amasty\CashOnDelivery\Model\ConfigProvider;
use Amasty\CashOnDelivery\Model\PaymentFeeFactory;
use Amasty\CashOnDelivery\Model\PaymentValidator;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Tax\Model\Calculation;
use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;
use Psr\Log\LoggerInterface;

class FeeCollector extends AbstractTotal
{
    const HUNDRED_PERCENT = 100;

    /**
     * @var PaymentValidator
     */
    private $paymentValidator;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var PaymentFeeRepositoryInterface
     */
    private $paymentFeeRepository;

    /**
     * @var PaymentFeeFactory
     */
    private $paymentFeeFactory;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Calculation
     */
    private $calculationTool;

    public function __construct(
        PaymentValidator $paymentValidator,
        ConfigProvider $configProvider,
        PaymentFeeRepositoryInterface $paymentFeeRepository,
        PaymentFeeFactory $paymentFeeFactory,
        PriceCurrencyInterface $priceCurrency,
        LoggerInterface $logger,
        Calculation $calculationTool
    ) {
        $this->paymentValidator = $paymentValidator;
        $this->configProvider = $configProvider;
        $this->paymentFeeRepository = $paymentFeeRepository;
        $this->paymentFeeFactory = $paymentFeeFactory;
        $this->priceCurrency = $priceCurrency;
        $this->logger = $logger;
        $this->calculationTool = $calculationTool;
    }

    /**
     * @inheritdoc
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        try {
            if ($total->getBaseSubtotal() || $total->getBaseShippingAmount()) {
                $paymentFeeData = [
                    'quote_id' => $quote->getId(),
                    'base_amount' => 0,
                    'amount' => 0,
                ];

                $total->setBaseTotalAmount($this->getCode(), 0);
                $total->setTotalAmount($this->getCode(), 0);

                try {
                    /** @var \Amasty\CashOnDelivery\Model\PaymentFee $paymentFee */
                    $paymentFee = $this->paymentFeeRepository->getByQuoteId($quote->getId());
                } catch (NoSuchEntityException $exception) {
                    $paymentFee = $this->paymentFeeFactory->create();
                }

                if ($this->paymentValidator->validatePaymentFee($quote)) {
                    $address = $shippingAssignment->getShipping()->getAddress();
                    $storeId = $quote->getStoreId();
                    $paymentFeeData['base_amount'] = $this->getPaymentFee($quote, $total, $address);
                    $paymentFeeData['amount'] = $this->getStoreCurrencyAmount($paymentFeeData['base_amount'], $storeId);
                    $total->setBaseTotalAmount($this->getCode(), $paymentFeeData['base_amount']);
                    $total->setTotalAmount($this->getCode(), $paymentFeeData['amount']);
                }

                if ($paymentFee->getAmount() != $paymentFeeData['amount']) {
                    $paymentFee->addData($paymentFeeData);
                    $this->paymentFeeRepository->save($paymentFee);
                }
            }
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }

        return $this;
    }

    /**
     * Assign subtotal amount and label to address object
     *
     * @param Quote $quote
     * @param Total $total
     *
     * @return array
     * @codingStandardsIgnoreStart
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(Quote $quote, Total $total)
    {
        try {
            /** @var \Amasty\CashOnDelivery\Model\PaymentFee $paymentFee */
            $paymentFee = $this->paymentFeeRepository->getByQuoteId($quote->getId());
        } catch (NoSuchEntityException $exception) {
            return null;
        }

        if ($paymentFee && $paymentFee->getAmount()) {
            return [
                'code' => $this->getCode(),
                'title' => __($this->getLabel()),
                'value' => $paymentFee->getAmount()
            ];
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getLabel()
    {
        return $this->configProvider->getPaymentFeeLabel();
    }

    /**
     * @param Quote $quote
     * @param Total $total
     * @param AddressInterface $address
     *
     * @return float|int
     */
    private function getPaymentFee(Quote $quote, Total $total, $address)
    {
        $fee = $this->configProvider->getPaymentFee($quote->getStoreId());

        $paymentFeeType = $this->configProvider->getPaymentFeeType($quote->getStoreId());
        if ($paymentFeeType == PaymentFeeTypes::PERCENT) {
            $fee = $this->calculatePercentFee($total, $fee, $quote->getStoreId());
        } elseif ($paymentFeeType == PaymentFeeTypes::FIXED_AMOUNT) {
            $fee = $this->calculateFixedFee($quote, $fee, $address, $quote->getStoreId());
        }

        return $fee;
    }

    /**
     * @param Total $total
     * @param float|int $fee
     * @param int $storeId
     * @return float|int
     */
    private function calculatePercentFee(Total $total, $fee, $storeId)
    {
        $calculateBasedOn = $this->configProvider->getPercentCalculateBasedOn($storeId);
        if ($calculateBasedOn == PercentCalculateBasedOn::EXCLUDING_TAX) {
            $fee = $total->getBaseSubtotal() / self::HUNDRED_PERCENT * $fee;
        } elseif ($calculateBasedOn == PercentCalculateBasedOn::INCLUDING_TAX) {
            $fee = $total->getBaseSubtotalInclTax() / self::HUNDRED_PERCENT * $fee;
        }

        return $fee;
    }

    /**
     * @param Quote $quote
     * @param float|int $fee
     * @param AddressInterface $address
     * @param int $storeId
     * @return float|int
     */
    private function calculateFixedFee(Quote $quote, $fee, $address, $storeId)
    {
        if ($taxClassId = $this->configProvider->getTaxClassForFixedFee($storeId)) {
            $calculateBasedOn = $this->configProvider->getFixedCalculateBasedOn($storeId);
            if ($calculateBasedOn == FixedCalculateBasedOn::EXCLUDING_TAX) {
                $this->addFeeTax($address, $fee, $taxClassId, $quote->getStoreId());
            } elseif ($calculateBasedOn == FixedCalculateBasedOn::INCLUDING_TAX) {
                $rate = $this->calculationTool->getRate($this->getTaxRateRequest($quote, $taxClassId));
                $taxAmount = $this->calculationTool->calcTaxAmount($fee, $rate, true, false);
                $baseFee = $fee;
                $fee -= $taxAmount;
                $this->addFeeTax($address, $baseFee, $taxClassId, $quote->getStoreId(), true);
            }
        }

        return $fee;
    }

    /**
     * @param AddressInterface $address
     * @param float|int $baseFeeAmount
     * @param int $taxClassId
     * @param int $storeId
     * @param bool $priceIncludeTax
     */
    private function addFeeTax($address, $baseFeeAmount, $taxClassId, $storeId, $priceIncludeTax = false)
    {
        $associatedTaxables = $address->getAssociatedTaxables();
        if (!$associatedTaxables) {
            $associatedTaxables = [];
        }

        $feeAmount = $this->getStoreCurrencyAmount($baseFeeAmount, $storeId);

        $associatedTaxables[] = [
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_TYPE => 'fee',
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_CODE => $this->getCode(),
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_UNIT_PRICE => $feeAmount,
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_BASE_UNIT_PRICE => $baseFeeAmount,
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_QUANTITY => 1,
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_TAX_CLASS_ID => $taxClassId,
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_PRICE_INCLUDES_TAX => $priceIncludeTax,
            CommonTaxCollector::KEY_ASSOCIATED_TAXABLE_ASSOCIATION_ITEM_CODE
            => CommonTaxCollector::ASSOCIATION_ITEM_CODE_FOR_QUOTE,
        ];

        $address->setAssociatedTaxables($associatedTaxables);
    }

    /**
     * @param float $amount
     * @param int $storeId
     * @return float
     */
    private function getStoreCurrencyAmount($amount, $storeId)
    {
        return $this->priceCurrency->convert($amount, $storeId);
    }

    /**
     * @param Quote $quote
     * @param int $taxClassId
     * @return DataObject
     */
    private function getTaxRateRequest(Quote $quote, $taxClassId)
    {
        return $this->calculationTool->getRateRequest(
            $quote->getShippingAddress(),
            $quote->getBillingAddress(),
            $quote->getCustomerTaxClassId(),
            $quote->getStore(),
            $quote->getCustomerId()
        )->setProductClassId($taxClassId);
    }
}
