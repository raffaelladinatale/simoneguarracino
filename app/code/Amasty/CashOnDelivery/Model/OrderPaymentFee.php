<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Model;

use Amasty\CashOnDelivery\Api\Data\OrderPaymentFeeInterface;
use Amasty\CashOnDelivery\Model\ResourceModel\OrderPaymentFee as OrderPaymentFeeResource;
use Magento\Framework\Model\AbstractModel;

class OrderPaymentFee extends AbstractModel implements OrderPaymentFeeInterface
{
    public function _construct()
    {
        $this->_init(OrderPaymentFeeResource::class);
    }

    /**
     * @return int
     */
    public function getEntityFeeId(): int
    {
        return (int)$this->_getData(OrderPaymentFeeInterface::ENTITY_ID);
    }

    /**
     * @param int $entityId
     */
    public function setEntityFeeId(int $entityId): void
    {
        $this->setData(OrderPaymentFeeInterface::ENTITY_ID, $entityId);
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return (int)$this->_getData(OrderPaymentFeeInterface::ORDER_ID);
    }

    /**
     * @param int $orderId
     */
    public function setOrderId(int $orderId): void
    {
        $this->setData(OrderPaymentFeeInterface::ORDER_ID, $orderId);
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return (float)$this->_getData(OrderPaymentFeeInterface::AMOUNT);
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->setData(OrderPaymentFeeInterface::AMOUNT, $amount);
    }

    /**
     * @return float
     */
    public function getBaseAmount(): float
    {
        return (float)$this->_getData(OrderPaymentFeeInterface::BASE_AMOUNT);
    }

    /**
     * @param float $baseAmount
     */
    public function setBaseAmount(float $baseAmount): void
    {
        $this->setData(OrderPaymentFeeInterface::BASE_AMOUNT, $baseAmount);
    }
}
