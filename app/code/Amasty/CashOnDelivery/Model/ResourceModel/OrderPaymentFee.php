<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Model\ResourceModel;

use Amasty\CashOnDelivery\Api\Data\OrderPaymentFeeInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OrderPaymentFee extends AbstractDb
{
    const TABLE_NAME = 'amasty_cash_on_delivery_fee_order';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, OrderPaymentFeeInterface::ENTITY_ID);
    }
}
