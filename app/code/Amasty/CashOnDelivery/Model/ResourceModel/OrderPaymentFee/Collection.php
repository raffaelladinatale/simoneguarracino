<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Model\ResourceModel\OrderPaymentFee;

use Amasty\CashOnDelivery\Api\Data\OrderPaymentFeeInterface;
use Amasty\CashOnDelivery\Model\OrderPaymentFee;
use Amasty\CashOnDelivery\Model\ResourceModel\OrderPaymentFee as OrderPaymentFeeResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = OrderPaymentFeeInterface::ENTITY_ID;

    public function _construct()
    {
        $this->_init(OrderPaymentFee::class, OrderPaymentFeeResource::class);
    }
}
