<?php

namespace Amasty\CashOnDelivery\Model\Order\Invoice\Total;

use Amasty\CashOnDelivery\Api\PaymentFeeRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class FeeCollector extends AbstractTotal
{
    /**
     * @var PaymentFeeRepositoryInterface
     */
    private $paymentFeeRepository;

    public function __construct(
        PaymentFeeRepositoryInterface $paymentFeeRepository,
        array $data = []
    ) {
        parent::__construct($data);
        $this->paymentFeeRepository = $paymentFeeRepository;
    }

    /**
     * @param Invoice $invoice
     *
     * @return $this
     */
    public function collect(Invoice $invoice)
    {
        try {
            $feeTax = 0;
            $feeBaseTax = 0;

            /** @var \Magento\Sales\Model\Order $order */
            $order = $invoice->getOrder();

            /** @var \Amasty\CashOnDelivery\Model\PaymentFee $paymentFee */
            $paymentFee = $this->paymentFeeRepository->getByQuoteId($order->getQuoteId());

            if ($order->getTaxAmount() != $invoice->getTaxAmount()
                || $order->getBaseTaxAmount() != $invoice->getBaseTaxAmount()
            ) {
                $feeTax = $order->getTaxAmount() - $invoice->getTaxAmount();
                $feeBaseTax = $order->getBaseTaxAmount() - $invoice->getBaseTaxAmount();

                $invoice->setTaxAmount($order->getTaxAmount());
                $invoice->setBaseTaxAmount($order->getBaseTaxAmount());
            }

            $invoice->setGrandTotal($invoice->getGrandTotal() + $paymentFee->getAmount() + $feeTax);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $paymentFee->getBaseAmount() + $feeBaseTax);
        } catch (NoSuchEntityException $exception) {
            return $this;
        }

        return $this;
    }
}
