<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Model\Sales\Pdf;

use Amasty\CashOnDelivery\Api\OrderPaymentFeeRepositoryInterface;
use Amasty\CashOnDelivery\Model\ConfigProvider;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Pdf\Total\DefaultTotal;
use Magento\Tax\Helper\Data;
use Magento\Tax\Model\Calculation;
use Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory;

class PaymentFee extends DefaultTotal
{
    /**
     * @var OrderPaymentFeeRepositoryInterface
     */
    private $paymentFeeRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(
        Data $taxHelper,
        Calculation $taxCalculation,
        CollectionFactory $ordersFactory,
        OrderPaymentFeeRepositoryInterface $paymentFeeRepository,
        OrderRepositoryInterface $orderRepository,
        ConfigProvider $configProvider,
        array $data = []
    ) {
        parent::__construct($taxHelper, $taxCalculation, $ordersFactory, $data);
        $this->paymentFeeRepository = $paymentFeeRepository;
        $this->orderRepository = $orderRepository;
        $this->configProvider = $configProvider;
    }

    /**
     * @return float|null
     */
    public function getAmount()
    {
        try {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->orderRepository->get($this->getSource()->getOrderId());

            /** @var \Amasty\CashOnDelivery\Model\PaymentFee $paymentFee */
            $paymentFee = $this->paymentFeeRepository->getByOrderId((int)$order->getEntityId());

            return $paymentFee->getAmount();
        } catch (NoSuchEntityException $exception) {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->configProvider->getPaymentFeeLabel();
    }
}
