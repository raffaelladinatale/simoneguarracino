<?php

namespace Amasty\CashOnDelivery\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Shipping implements OptionSourceInterface
{
    const ALL_SHIPPING = 0;
    const SPECIFIC_SHIPPING = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('All Allowed Shipping Methods'),
                'value' => self::ALL_SHIPPING
            ],
            [
                'label' => __('Specific Shipping Methods'),
                'value' => self::SPECIFIC_SHIPPING
            ]
        ];
    }
}
