<?php

namespace Amasty\CashOnDelivery\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class FixedCalculateBasedOn implements OptionSourceInterface
{
    const EXCLUDING_TAX = 0;
    const INCLUDING_TAX = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Excluding Tax'),
                'value' => self::EXCLUDING_TAX
            ],
            [
                'label' => __('Including Tax'),
                'value' => self::INCLUDING_TAX
            ]
        ];
    }
}
