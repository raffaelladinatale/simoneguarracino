<?php

namespace Amasty\CashOnDelivery\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class PaymentFeeTypes implements OptionSourceInterface
{
    const FIXED_AMOUNT = 0;
    const PERCENT = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Fixed Amount'),
                'value' => self::FIXED_AMOUNT
            ],
            [
                'label' => __('Percent'),
                'value' => self::PERCENT
            ]
        ];
    }
}
