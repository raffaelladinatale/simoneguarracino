<?php

namespace Amasty\CashOnDelivery\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class PercentCalculateBasedOn implements OptionSourceInterface
{
    const EXCLUDING_TAX = 0;
    const INCLUDING_TAX = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Cart Subtotal Excluding Tax'),
                'value' => self::EXCLUDING_TAX
            ],
            [
                'label' => __('Cart Subtotal Including Tax'),
                'value' => self::INCLUDING_TAX
            ]
        ];
    }
}
