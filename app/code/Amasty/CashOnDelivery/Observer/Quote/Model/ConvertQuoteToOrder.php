<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Observer\Quote\Model;

use Amasty\CashOnDelivery\Api\OrderPaymentFeeRepositoryInterface;
use Amasty\CashOnDelivery\Api\PaymentFeeRepositoryInterface;
use Amasty\CashOnDelivery\Model\OrderPaymentFee;
use Amasty\CashOnDelivery\Model\OrderPaymentFeeFactory;
use Amasty\CashOnDelivery\Model\PaymentFee;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;

class ConvertQuoteToOrder implements ObserverInterface
{
    /**
     * @var OrderPaymentFeeFactory
     */
    private $orderFeeFactory;

    /**
     * @var PaymentFeeRepositoryInterface
     */
    private $quoteFeeRepository;

    /**
     * @var OrderPaymentFeeRepositoryInterface
     */
    private $orderFeeRepository;

    public function __construct(
        OrderPaymentFeeFactory $orderFeeFactory,
        PaymentFeeRepositoryInterface $quoteFeeRepository,
        OrderPaymentFeeRepositoryInterface $orderFeeRepository
    ) {
        $this->orderFeeFactory = $orderFeeFactory;
        $this->quoteFeeRepository = $quoteFeeRepository;
        $this->orderFeeRepository = $orderFeeRepository;
    }

    /**
     * Event 'sales_model_service_quote_submit_success'
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');

        try {
            /** @var PaymentFee $paymentFee */
            $quoteFee = $this->quoteFeeRepository->getByQuoteId((int)$order->getQuoteId());
        } catch (NoSuchEntityException $exception) {
            unset($exception);

            return;
        }

        /** @var OrderPaymentFee $orderFee */
        $orderFee = $this->orderFeeFactory->create();
        $orderFee->setOrderId((int)$order->getEntityId());
        $orderFee->setAmount($quoteFee->getAmount());
        $orderFee->setBaseAmount($quoteFee->getBaseAmount());
        $this->orderFeeRepository->save($orderFee);
    }
}
