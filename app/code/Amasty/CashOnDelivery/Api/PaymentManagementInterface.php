<?php

namespace Amasty\CashOnDelivery\Api;

interface PaymentManagementInterface
{

    /**
     * Check cash on delivery for available
     *
     * @param string $postalCode ZIP code.
     * @return bool
     */
    public function checkAvailable($postalCode);
}
