<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Api\Data;

interface OrderPaymentFeeInterface
{
    /**
     * Constants defined for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const ORDER_ID = 'order_id';
    const AMOUNT = 'amount';
    const BASE_AMOUNT = 'base_amount';
    /**

    /**
     * @return int
     */
    public function getEntityFeeId(): int;

    /**
     * @param int $entityId
     * @return void
     */
    public function setEntityFeeId(int $entityId): void;

    /**
     * @return int
     */
    public function getOrderId(): int;

    /**
     * @param int $orderId
     * @return void
     */
    public function setOrderId(int $orderId): void;

    /**
     * @return float
     */
    public function getAmount(): float;

    /**
     * @param float $amount
     * @return void
     */
    public function setAmount(float $amount): void;

    /**
     * @return float
     */
    public function getBaseAmount(): float;

    /**
     * @param float $baseAmount
     * @return void
     */
    public function setBaseAmount(float $baseAmount): void;
}
