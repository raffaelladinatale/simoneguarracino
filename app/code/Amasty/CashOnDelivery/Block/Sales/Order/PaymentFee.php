<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Block\Sales\Order;

use Amasty\CashOnDelivery\Api\OrderPaymentFeeRepositoryInterface;
use Amasty\CashOnDelivery\Model\ConfigProvider;
use Amasty\CashOnDelivery\Model\OrderPaymentFee;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\View\Element\Template\Context;

class PaymentFee extends AbstractBlock
{
    /**
     * @var OrderPaymentFeeRepositoryInterface
     */
    private $paymentFeeRepository;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(
        Context $context,
        OrderPaymentFeeRepositoryInterface $paymentFeeRepository,
        ConfigProvider $configProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->paymentFeeRepository = $paymentFeeRepository;
        $this->configProvider = $configProvider;
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        /** @var \Magento\Sales\Block\Adminhtml\Order\Totals $parent */
        $parent = $this->getParentBlock();
        $source = $this->getSource();

        if (!$parent || !method_exists($parent, 'getOrder')) {
            return $this;
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $parent->getOrder();

        if (!($order instanceof \Magento\Sales\Api\Data\OrderInterface)) {
            return $this;
        }

        try {
            /** @var OrderPaymentFee $paymentFee */
            $paymentFee = $this->paymentFeeRepository->getByOrderId((int)$order->getEntityId());
        } catch (NoSuchEntityException $exception) {
            return $this;
        }

        if ($paymentFee->getAmount()) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => $this->getNameInLayout(),
                    'label' => $this->configProvider->getPaymentFeeLabel(),
                    'value' => +$paymentFee->getAmount(),
                    'base_value' => +$paymentFee->getBaseAmount()
                ]
            );

            $totalDeposite = new \Magento\Framework\DataObject(
                [
                    'code' => 'cashondelivery_deposite_10',
                    'label' => __('Deposite Cash on delivery percentage'),
                    'value' => +(10 / 100) * $order->getBaseSubtotalInclTax(),
                    'base_value' => +(10 / 100) * $order->getBaseSubtotalInclTax()
                ]
            );

            $totalPrice = new \Magento\Framework\DataObject(
                [
                    'code' => 'cashondelivery_deposite',
                    'label' => __('Deposite Cash on delivery total'),
                    'value' => +$order->getBaseSubtotalInclTax() - ((10 / 100) * $order->getBaseSubtotalInclTax()),
                    'base_value' => +$order->getBaseSubtotalInclTax() - ((10 / 100) * $order->getBaseSubtotalInclTax())
                ]
            );

            $newGrandTotal = new \Magento\Framework\DataObject(
                [
                    'code' => 'new_grand_total',
                    'field' => 'new_grand_total',
                    'strong' => true,
                    'value' => $totalPrice->getData('value') + $order->getShippingInclTax() + $paymentFee->getBaseAmount(),
                    'label' => __('Grand Total')
                ]
            );
            $parent->removeTotal('grand_total');
            $parent->addTotal($total, 'shipping');
            $parent->addTotalBefore($totalDeposite, 'shipping');
            $parent->addTotalBefore($totalPrice, 'shipping');
            $parent->addTotal($newGrandTotal, $this->getNameInLayout());
        }

        return $this;
    }
}
