<?php

namespace Amasty\CashOnDelivery\Block\Adminhtml\Config;

use Amasty\Base\Helper\Module;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\Manager;

class FeeComment extends Field
{
    /**
     * @var string
     */
    const AMASTY_FEE_URL = 'https://amasty.com/extra-fee-for-magento-2.html?utm_source=extension&utm_medium=backend'
    . '&utm_campaign=cash-on-delivery-fee';

    /**
     * @var string
     */
    const MARKETPLACE_FEE_URL = 'https://marketplace.magento.com/amasty-module-extra-fee.html';

    /**
     * @var string
     */
    private $feeComment = "Floating point numbers only. Up to 2 digits after the point. Should be greater than 0. "
    . "To set more flexible fees, use <a href='%1' target='_blank'>Extra Fee</a> extension.";

    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * @var Module
     */
    private $moduleHelper;

    public function __construct(
        Context $context,
        Manager $moduleManager,
        Module $moduleHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->moduleManager = $moduleManager;
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * @inheritdoc
     */
    public function render(AbstractElement $element)
    {
        if ($this->moduleManager->isEnabled('Amasty_Extrafee')) {
            $url = $this->getUrl('amasty_extrafee/index/new');
            $element->setComment(__($this->feeComment, $url));
        } else {
            if ($this->moduleHelper->isOriginMarketplace()) {
                $element->setComment(__($this->feeComment, self::MARKETPLACE_FEE_URL));
            } else {
                $element->setComment(__($this->feeComment, self::AMASTY_FEE_URL));
            }
        }

        return parent::render($element);
    }
}
