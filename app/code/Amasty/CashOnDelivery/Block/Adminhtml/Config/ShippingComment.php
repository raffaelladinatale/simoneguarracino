<?php

namespace Amasty\CashOnDelivery\Block\Adminhtml\Config;

use Amasty\Base\Helper\Module;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\Manager;

class ShippingComment extends Field
{
    /**
     * @var string
     */
    const AMASTY_PAYMENT_RESTRICTION_URL = 'https://amasty.com/payment-restrictions-for-magento-2.html?utm_source'
    . '=extension&utm_medium=backend&utm_campaign=cash-on-delivery-allowed-shipping-methods';

    /**
     * @var string
     */
    const MARKETPLACE_PAYMENT_RESTRICTION_URL = 'https://marketplace.magento.com/amasty-payrestriction.html';

    /**
     * @var string
     */
    //phpcs:ignore Magento2.SQL.RawQuery.FoundRawSql
    private $paymentComment = "Select all or clear the selection to allow all shipping methods. If you want to apply"
    . " more restrictions, please use <a href='%1' target='_blank'>Payment Restrictions</a> extension.";

    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * @var Module
     */
    private $moduleHelper;

    public function __construct(
        Context $context,
        Manager $moduleManager,
        Module $moduleHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->moduleManager = $moduleManager;
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        if ($this->moduleManager->isEnabled('Amasty_Payrestriction')) {
            $url = $this->getUrl('amasty_payrestriction/rule/newAction');
            $element->setComment(__($this->paymentComment, $url));
        } else {
            if ($this->moduleHelper->isOriginMarketplace()) {
                $element->setComment(__($this->paymentComment, self::MARKETPLACE_PAYMENT_RESTRICTION_URL));
            } else {
                $element->setComment(__($this->paymentComment, self::AMASTY_PAYMENT_RESTRICTION_URL));
            }
        }

        return parent::render($element);
    }
}
