<?php
declare(strict_types=1);

namespace Amasty\CashOnDelivery\Setup;

use Amasty\CashOnDelivery\Model\ResourceModel\OrderPaymentFee;
use Amasty\CashOnDelivery\Model\ResourceModel\PaymentFee;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

class Uninstall implements UninstallInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->getConnection()
            ->dropTable($setup->getTable(PaymentFee::TABLE_NAME));
        $setup->getConnection()
            ->dropTable($setup->getTable(OrderPaymentFee::TABLE_NAME));
    }
}
