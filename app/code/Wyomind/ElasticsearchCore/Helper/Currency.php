<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Helper;

/**
 * Frontend currency utilities
 */
class Currency extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Framework\App\Helper\Context $context)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context);
    }
    /**
     * Get the currency conversion rate
     * @param null $store
     * @return float
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrentCurrencyRate($store = null)
    {
        $store = $this->_storeManager->getStore($store);
        return (float) $store->getCurrentCurrencyRate();
    }
}