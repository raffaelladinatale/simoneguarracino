<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Helper;

/**
 * Theme utilities
 */
class Theme extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Code of the theme used on the frontend
     * @var string
     */
    private $_themeModel = '';
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Framework\App\Helper\Context $context)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->_themeModel = $this->design->getDesignParams()['themeModel']->getCode();
        parent::__construct($context);
    }
    /**
     * Get the code of the theme used on the frontend
     * @return string
     */
    public function getThemeModel()
    {
        return $this->_themeModel;
    }
    public function getMediaBaseUrl($storeId)
    {
        return $this->_urlBuilder->getBaseUrl(['_scope' => $storeId, '_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]);
    }
}