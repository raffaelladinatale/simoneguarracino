<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Model\Index;

class MappingBuilder
{
    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Indexer
     */
    protected $_indexerHelperFactory = null;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Wyomind\ElasticsearchCore\Helper\IndexerFactory $indexerHelperFactory)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->_indexerHelperFactory = $indexerHelperFactory;
    }
    /**
     * {@inheritdoc}
     */
    public function build($storeId, $type)
    {
        $mapping = [];
        $indexer = $this->_indexerHelperFactory->create()->getIndexer($type);
        $compatibility = $this->_configHelper->getCompatibility($storeId);
        if ($compatibility >= 6) {
            $mapping[$type] = ['properties' => $indexer->getProperties($storeId)];
        } elseif ($compatibility < 6) {
            $mapping[$type] = ['_all' => ['analyzer' => $indexer->getLanguageAnalyzer($storeId)], 'properties' => $indexer->getProperties($storeId)];
        }
        return $mapping;
    }
}