<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Model\Indexer;

/**
 * 
 */
abstract class AbstractIndexer
{
    /**
     * @var string
     */
    public $name = 'Abstract';

    /**
     * @var string
     */
    public $type = 'abstract';

    public $updatedAt = null;

    public $lastIndexDate = null;

    public $reindexed = null;

    /** @var int */
    public $updateMode = \Wyomind\ElasticsearchCore\Helper\Indexer::UPDATE_MODE_ON_SAVE;
    /** @var string */
    public $comment = 'Abstract indexer';

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface|null
     */
    protected $_encryptor = null;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager = null;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $_messageManager = null;

    /**
     * @var \Magento\Framework\Module\ModuleList
     */
    protected $_moduleList = null;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_coreDate = null;

    /**
     * @var \Magento\Framework\UrlInterface|null
     */
    protected $_urlBuilder = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager = null;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutputFactory
     */
    protected $_consoleOutputFactory = null;

    /**
     * @var \Wyomind\Framework\Helper\Module
     */
    public $_framework = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Attribute
     */
    protected $_attributeHelper = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Config
     */
    protected $_configHelper = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\IndexerFactory
     */
    protected $_indexerHelperFactory = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\JsonConfig
     */
    protected $_jsonConfigHelper = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Log
     */
    protected $_logHelper = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Session
     */
    protected $_sessionHelper = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\Adapter
     */
    protected $_adapter = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\Index
     */
    protected $_indexModel = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\ToReindexFactory
     */
    protected $_toReindexModelFactory = null;

    /**
     * @var Wyomind\ElasticsearchCore\Model\ResourceModel\Config|null
     */
    protected $_resourceModelConfig = null;

    /**
     * @var \Magento\InventoryIndexer\Model\StockIndexTableNameResolver\Proxy|null
     */
    protected $_stockIndexTableNameResolver;

    /**
     * @var \Magento\InventorySalesApi\Api\StockResolverInterface\Proxy|null
     */
    protected $_stockResolver;

    /**
     * @var mixed|null
     */
    protected $_selectBuilder = null;
    /**
     * @var mixed|null
     */
    protected $_sourceCollection;

    protected $storesRatio = 0;
    protected $additionalPercent = 0;
    protected $nbEntitiesIndexed = 0;

    /**
     * @var array
     * @link https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html
     */
    protected $languages = [
        'arabic', 'armenian', 'basque', 'brazilian', 'bulgarian', 'catalan', 'cjk', 'czech', 'danish', 'dutch',
        'english', 'finnish', 'french', 'galician', 'german', 'greek', 'hindi', 'hungarian', 'indonesian', 'irish',
        'italian', 'latvian', 'lithuanian', 'norwegian', 'persian', 'portuguese', 'romanian', 'russian', 'sorani',
        'spanish', 'swedish', 'turkish', 'thai'
    ];

    /**
     * @var string
     */
    public $error = "License error.\nPlease run bin/magento wyomind:license:status to check your license status.";

    public $forceIds = [];

    /**
     * @var Wyomind\ElasticsearchCore\Helper\Progress
     */
    protected $_progressHelper;

    /**
     * AbstractIndexer constructor.
     * @param Context $context
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Context $context
    )
    {
        $this->_framework = $context->getFramework();
        $this->_encryptor = $context->getEncryptor();
        $this->_eventManager = $context->getEventManager();
        $this->_messageManager = $context->getMessageManager();
        $this->_moduleList = $context->getModuleList();
        $this->_objectManager = $context->getObjectManager();
        $this->_coreDate = $context->getCoreDate();
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->_storeManager = $context->getStoreManager();
        $this->_consoleOutputFactory = $context->getConsoleOutputFactory();
        $this->_attributeHelper = $context->getAttributeHelper();
        $this->_configHelper = $context->getConfigHelper();
        $this->_indexerHelperFactory = $context->getIndexerHelperFactory();
        $this->_jsonConfigHelper = $context->getJsonConfigHelper();
        $this->_logHelper = $context->getLogHelper();
        $this->_sessionHelper = $context->getSessionHelper();
        $this->_adapter = $context->getAdapter();
        $this->_indexModel = $context->getIndexModel();
        $this->_toReindexModelFactory = $context->getToReindexModelFactory();
        $this->_resourceModelConfig = $context->getResourceModelConfig();
        $this->_stockResolver = $context->getStockResolver();
        $this->_stockIndexTableNameResolver = $context->getStockIndexTableNameResolver();
        $this->_selectBuilder = $context->getSelectBuilder();
        $this->_sourceCollection = $context->getSourceItemCollection();
        $this->_progressHelper = $context->getProgressHelper();
    }

    /**
     * @return int
     */
    public function getStoresRatio()
    {
        return $this->storesRatio;
    }

    /**
     * @param int $nbStoresTotal
     */
    public function setStoresRatio($nbStoresTotal)
    {
        $this->storesRatio = $nbStoresTotal;
    }

    /**
     * @return int
     */
    public function getNbEntitiesIndexed(): int
    {
        return $this->nbEntitiesIndexed;
    }

    /**
     * @return int
     */
    public function getAdditionalPercent(): int
    {
        return $this->additionalPercent;
    }

    /**
     * @param int $additionalPercent
     */
    public function setAdditionalPercent(int $additionalPercent)
    {
        $this->additionalPercent = $additionalPercent;
    }


    /**
     * @param int $nbEntitiesIndexed
     */
    public function setNbEntitiesIndexed(int $nbEntitiesIndexed)
    {
        $this->nbEntitiesIndexed = $nbEntitiesIndexed;
    }


    /**
     * @param int $storeId
     * @param array $ids
     * @return \Generator
     */
    abstract public function export($storeId, $ids = []);

    /**
     * @param mixed $store
     * @param bool $withBoost
     * @return array
     */
    abstract public function getProperties($store = null, $withBoost = false);

    /**
     * Get dynamic config groups
     * @return array
     */
    abstract public function getDynamicConfigGroups();

    /**
     * Get events list related to the indexer
     * Format example ['event_name' => [['indexer' => $this->type, 'action' => 'reindex']]]
     * @return array
     */
    abstract public function getEvents();

    /**
     * @param string $type
     * @param string $field
     * @param string $title
     * @param int $sortOrder
     * @return array
     */
    public function addBrowseColumn($type, $field, $title, $sortOrder)
    {
        $column = [];
        if ($type == 'html') {
            $column = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => 'text',
                            'component' => 'Magento_Ui/js/grid/columns/column',
                            'componentType' => 'column',
                            'filter' => 'text',
                            'bodyTmpl' => 'ui/grid/cells/html',
                            'sorting' => 'asc',
                            'label' => $title,
                            'sortOrder' => $sortOrder,
                            'sortable' => false
                        ]
                    ]
                ],
                'attributes' => [
                    'class' => 'Magento\Ui\Component\Listing\Columns\Column',
                    'component' => 'Magento_Ui/js/grid/columns/column',
                    'name' => $field
                ],
                'children' => []
            ];
        } elseif ($type == 'json') {
            $column = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => 'text',
                            'component' => 'Magento_Ui/js/grid/columns/column',
                            'componentType' => 'column',
                            'filter' => 'text',
                            'bodyTmpl' => 'Wyomind_ElasticsearchCore/listing/browse/json',
                            'sorting' => 'asc',
                            'label' => $title,
                            'sortOrder' => $sortOrder,
                            'sortable' => false
                        ]
                    ]
                ],
                'attributes' => [
                    'class' => 'Magento\Ui\Component\Listing\Columns\Column',
                    'component' => 'Magento_Ui/js/grid/columns/column',
                    'name' => $field
                ],
                'children' => []
            ];
        } elseif ($type == 'price') {
            $column = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => 'text',
                            'component' => 'Magento_Ui/js/grid/columns/column',
                            'componentType' => 'column',
                            'filter' => 'textRange',
                            'sorting' => 'asc',
                            'label' => $title,
                            'sortOrder' => $sortOrder,
                            'sortable' => false
                        ]
                    ]
                ],
                'attributes' => [
                    'class' => 'Magento\Catalog\Ui\Component\Listing\Columns\Price',
                    'component' => 'Magento_Ui/js/grid/columns/column',
                    'name' => $field
                ],
                'children' => []
            ];
        } elseif ($type == 'image') {
            $column = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'altField' => $field,
                            'add_field' => true,
                            'dataType' => 'text',
                            'component' => 'Magento_Ui/js/grid/columns/column',
                            'componentType' => 'column',
                            'bodyTmpl' => 'Wyomind_ElasticsearchCore/listing/browse/image',
                            'has_preview' => 1,
                            'label' => $title,
                            'sortOrder' => $sortOrder,
                            'sortable' => false
                        ]
                    ]
                ],
                'attributes' => [
                    'class' => 'Magento\Ui\Component\Listing\Columns\Column',
                    'name' => $field
                ],
                'children' => []
            ];
        } elseif ($type == 'url') {
            $column = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'altField' => $field,
                            'add_field' => true,
                            'dataType' => 'text',
                            'component' => 'Magento_Ui/js/grid/columns/column',
                            'componentType' => 'column',
                            'bodyTmpl' => 'Wyomind_ElasticsearchCore/listing/browse/url',
                            'has_preview' => 1,
                            'filter' => 'text',
                            'label' => $title,
                            'sortOrder' => $sortOrder,
                            'sortable' => false
                        ]
                    ]
                ],
                'attributes' => [
                    'class' => 'Magento\Ui\Component\Listing\Columns\Column',
                    'name' => $field
                ],
                'children' => []
            ];
        }
        return $column;
    }

    /**
     * @param array $item
     * @param string $name
     * @return array
     */
    public function getBrowseActions(&$item, $name)
    {
        return [];
    }

    /**
     * @param int $storeId
     * @return array
     */
    public function getBrowseColumns($storeId)
    {
        $columns = [];
        $attributesConfig = $this->_configHelper->getEntitySearchableAttributes($this->type, $storeId);
        foreach (array_keys($attributesConfig) as $attribute) {
            $columns[$attribute] = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => 'text',
                            'component' => 'Magento_Ui/js/grid/columns/column',
                            'componentType' => 'column',
                            'filter' => 'text',
                            'sorting' => 'asc',
                            'label' => ucfirst($attribute),
                            'sortOrder' => '20',
                            'sortable' => false
                        ]
                    ]
                ],
                'attributes' => [
                    'class' => 'Magento\Ui\Component\Listing\Columns\Column',
                    'component' => 'Magento_Ui/js/grid/columns/column',
                    'name' => $attribute,
                ],
                'children' => []
            ];
        }
        return $columns;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Is a module enabled?
     * @param string $moduleName
     * @return boolean
     */
    public function moduleIsEnabled($moduleName)
    {
        return $this->_moduleList->has($moduleName);
    }

    /**
     * @param string $type
     * @param string|null $storeCode
     * @return array
     */
    public function reindex($type, $storeCode = null)
    {


        $report = [];
        $stores = $this->_storeManager->getStores();
        $index = $this->_indexModel->loadByIndexerId($type);
        $index->setIndexerId($type);
        $index->setUpdateMode('schedule');
        $index->setReindexed(1);
        $datetime = $this->_coreDate->date('Y-m-d H:i:s', $this->_coreDate->gmtTimestamp());
        $index->setLastIndexDate($datetime);


        $storeIds = [];
        foreach ($stores as $store) {
            if ($storeCode != null && $store->getCode() != $storeCode) continue;
            $storeId = $store->getId();
            if ($this->_configHelper->isIndexationEnabled($type, $storeId)) {
                $storeIds[] = $storeId;
            } else {
                $report[] = '<error>Store ' . $storeId . ': ' . $type . __(' indexer disabled') . '</error>';
            }
        }


        $this->_progressHelper->startObservingProgress(true, $this->type, $this->type);
        //$this->_progressHelper->log(__("Starting %1 index", $this->name), true, \Wyomind\Framework\Helper\Progress::PROCESSING, 0);

        $storeCounter = 1;
        $this->setStoresRatio(1 / (count($storeIds) > 0 ? count($storeIds) : 1));
        foreach ($storeIds as $storeId) {
            $this->setAdditionalPercent((($storeCounter - 1) / count($storeIds)) * 100);
            $storeCounter++;
            $documents = $this->export($storeId);
            $this->_adapter->addDocs($documents, $type, true, $storeId);
//            $report[] = "\n<info>Store " . $storeId . ': ' . $type . __(' indexer enabled') . '</info>';
            $index->save();
            $this->_jsonConfigHelper->saveConfig($store->getCode());
            $this->_progressHelper->log(__("%1: %2 entit%3 indexed", $this->name, $this->getNbEntitiesIndexed(), $this->getNbEntitiesIndexed() > 1 ? 'ies' : 'y'), true, \Wyomind\Framework\Helper\Progress::PROCESSING, 100);
        }

        /** @var \Wyomind\ElasticsearchCore\Model\ToReindex $toReindexModel */
        $toReindexModel = $this->_toReindexModelFactory->create();
        // remove the indexer lines in the "buffer" table wyomind_elasticsearchcore_to_reindex
        $toReindexModel->deleteIndexerToReindex($type);

        $this->_progressHelper->log(__("%1: %2 entit%3 indexed", $this->name, $this->getNbEntitiesIndexed(), $this->getNbEntitiesIndexed() > 1 ? 'ies' : 'y'), true, \Wyomind\Framework\Helper\Progress::SUCCEEDED, 100);
        $this->_progressHelper->stopObservingProgress();

        return $report;
    }

    /**
     * Execute partial indexation by ID
     * @param int $id
     * @return void
     */
    public function executeRow($id)
    {
        $this->execute([$id]);
    }


    /**
     * Execute materialization on ids entities
     * @param int[] $ids
     * @return void
     * @throws \Exception
     */
    public function execute($ids)
    {

        $storeIds = array_keys($this->_storeManager->getStores());

        $datetime = $this->_coreDate->date('Y-m-d H:i:s', $this->_coreDate->gmtTimestamp());
        $index = $this->_indexModel->loadByIndexerId($this->getType());
        $index->setIndexerId($this->getType());
        $index->setLastIndexDate($datetime);
        $index->save();

        $this->setStoresRatio(count($storeIds));
        $this->_progressHelper->startObservingProgress(true, $this->type, $this->type);

        foreach ($storeIds as $storeId) {
            $documents = $this->export($storeId, $ids);
            $this->_adapter->deleteDocs($this->type, $storeId, $ids);
            $this->_adapter->addDocs($documents, $this->type, false, $storeId);
        }


        $this->_progressHelper->log(__("Last execution: %1 entit%2 indexed", $this->getNbEntitiesIndexed(), $this->getNbEntitiesIndexed() > 1 ? 'ies' : 'y'), true, \Wyomind\Framework\Helper\Progress::SUCCEEDED, 100);
        $this->_progressHelper->stopObservingProgress();
    }

    /**
     * Deletes an unique document
     *
     * @param int $id
     */
    public function deleteRow($id)
    {
        $this->delete([$id]);
    }

    /**
     * Deletes multiple documents
     *
     * @param array $ids
     */
    public function delete($ids)
    {
        $storeIds = array_keys($this->_storeManager->getStores());

        foreach ($storeIds as $storeId) {
            $this->_adapter->deleteDocs($this->type, $storeId, $ids);
        }
    }

    public function getAttributeProperties($attributeCode, $attributeInfo, $store = null, $withBoost = false)
    {
        $properties = [];
        $analyzer = $this->getLanguageAnalyzer($store);

        $type = $attributeInfo['b']; // backend_type

        $compatibility = $this->_configHelper->getCompatibility($store);

        if ($compatibility >= 6) {
            if ($type === 'option') {
                // Attribute options
                $properties = [
                    'type' => 'text',
                    'analyzer' => $analyzer,
                    'index_options' => 'docs', // do not use tf/idf for options
                    'norms' => ['enabled' => false] // useless for options
                ];
                //if ((bool)$attribute->getData('is_searchable')) {
                $properties['copy_to'] = 'all';
                //}
            } elseif ($type !== 'string') {
                // Non-string types
                $properties = [
                    'type' => 'text',
                    'index' => 'false' // do not analyze integers, decimals, dates and booleans
                ];
                if ($attributeCode == 'visibility') {
                    $properties['index'] = 'true';
                }
            } else {
                // String type
                $properties = [
                    'type' => 'text',
                    'analyzer' => $analyzer
                ];
                //if ((bool)$attribute->getData('is_searchable')) {
                $properties['copy_to'] = 'all';
                //}

                if ($attributeCode == 'sku') {
                    $properties['analyzer'] = 'std';
                    $properties['fields']['prefix'] = [
                        'type' => 'text',
                        'analyzer' => 'sku_prefix',
                        'search_analyzer' => 'std'
                    ];
//                    $properties['fields']['suffix'] = [
//                        'type' => 'text',
//                        'analyzer' => 'sku_suffix',
//                        'search_analyzer' => 'std'
//                    ];
                } elseif ($attributeCode == 'name') {
                    $properties['analyzer'] = 'std';
                    $properties['fields']['prefix'] = [
                        'type' => 'text',
                        'analyzer' => 'name_prefix',
                        'search_analyzer' => 'std'
                    ];
//                    $properties['fields']['suffix'] = [
//                        'type' => 'text',
//                        'analyzer' => 'name_suffix',
//                        'search_analyzer' => 'std'
//                    ];
                } else {
                    $properties['analyzer'] = 'std';
                    $properties['fields']['prefix'] = [
                        'type' => 'text',
                        'analyzer' => 'text_prefix',
                        'search_analyzer' => 'std'
                    ];
//                    $properties['fields']['suffix'] = [
//                        'type' => 'text',
//                        'analyzer' => 'text_suffix',
//                        'search_analyzer' => 'std'
//                    ];
                }

            }
        } elseif ($compatibility < 6) {
            if ($type === 'option') {
                // Attribute options
                $properties = [
                    'type' => 'string',
                    'analyzer' => $analyzer,
                    'index_options' => 'docs', // do not use tf/idf for options
                    'norms' => ['enabled' => false], // useless for options
                    'include_in_all' => true//(bool)$attribute->getData('is_searchable')
                ];
            } elseif ($type !== 'string') {
                // Non-string types
                $properties = [
                    'type' => $type,
                    'index' => 'not_analyzed', // do not analyze integers, decimals, dates and booleans
                    'include_in_all' => false // do not include this kind of field in _all field (used in fuzzy queries)
                ];
                if ($type === 'integer') {
                    $properties['ignore_malformed'] = true;
                }
            } else {
                // String type
                $properties = [
                    'type' => 'string',
                    'analyzer' => $analyzer,
                    'include_in_all' => true//(bool)$attribute->getData('is_searchable')
                ];

                if ($attributeCode == 'sku'/* || $attributeCode == 'name' || $attributeCode == 'title'*/) {
                    $properties['fields']['prefix'] = [
                        'type' => 'string',
                        'analyzer' => 'sku_prefix',
                        'search_analyzer' => 'std'
                    ];
//                    $properties['fields']['suffix'] = [
//                        'type' => 'string',
//                        'analyzer' => 'sku_suffix',
//                        'search_analyzer' => 'std'
//                    ];
                }
            }
        }

        if ($withBoost) {
            if (isset($attributeInfo['w'])) {
                $boost = (int)$attributeInfo['w'];
                if ($boost > 1) {
                    $properties['boost'] = $boost;
                }
            }
        }
        return $properties;
    }

    /**
     * Returns store language if handled
     *
     * @param mixed $store
     * @return string|false
     */
    protected function getLanguage($store = null)
    {
        $language = strtolower($this->_configHelper->getLanguage($store));

        if (!in_array($language, $this->languages)) {
            // try with potential first string
            $parts = explode(' ', $language);
            $language = $parts[0];
            if (!in_array($language, $this->languages)) {
                // language not present by default
                $language = false;
            }
        }

        return $language;
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getLanguageAnalyzer($store = null)
    {
        $language = $this->getLanguage($store); // use built-in language analyzer if possible

        return $language ?: 'std';
    }

    /**
     * Returns attribute type for indexation
     *
     * @param \Magento\Eav\Model\Entity\Attribute $attribute
     * @return string
     */
    protected function getAttributeType(\Magento\Eav\Model\Entity\Attribute $attribute)
    {
        $type = 'string';
        if ($this->_attributeHelper->isDecimal($attribute)) {
            $type = 'double';
        } elseif ($this->_attributeHelper->isBool($attribute)) {
            $type = 'boolean';
        } elseif ($this->_attributeHelper->isDate($attribute)) {
            $type = 'date';
        } elseif ($this->_attributeHelper->isAttributeUsingOptions($attribute)) {
            // custom type
            $type = 'option';
        } elseif ($this->_attributeHelper->isInteger($attribute)) {
            $type = 'integer';
        }

        return $type;
    }

    /**
     * @param null $store
     * @param bool $withBoost
     * @param null $compatibility
     * @return array
     */
    public function getSearchFields($store = null, $withBoost = false, $compatibility = null)
    {
        $fields = [];
        foreach ($this->getProperties($store, $withBoost) as $fieldName => $property) {

            if ($compatibility >= 6) {
                if (!isset($property['copy_to'])) {
                    continue;
                }
            } elseif ($compatibility < 6) {
                if (!isset($property['include_in_all']) || !$property['include_in_all']) {
                    continue;
                }
            }

            $boost = false;
            if ($withBoost && isset($property['boost'])) {
                $boost = (int)$property['boost'];
            }

            $fields[] = $fieldName . ($boost ? "^" . $boost : '');

            if (isset($property['fields'])) {
                foreach ($property['fields'] as $key => $field) {
                    $fields[] = $fieldName . '.' . $key . ($boost ? "^" . ($key == "prefix" ? "1" : $boost) : '');
                }
            }
        }

        return $fields;
    }

    /**
     * @param string $message
     */
    public function handleLog($message)
    {
        $this->_logHelper->log(strip_tags($message));

        if (php_sapi_name() === 'cli') {
            $this->_consoleOutputFactory->create()->writeln($message);
        }
    }

    public function handleError($message)
    {
        $this->_logHelper->log(strip_tags($message));

        if (php_sapi_name() === 'cli') {
            $this->_consoleOutputFactory->create()->writeln($message);
            throw new \Magento\Framework\Exception\LocalizedException(__($message));
        }
    }

    /**
     * Get reindex event observer ID parameter
     * @param \Magento\Framework\Event\Observer $observer
     * @return string
     */
    public function getObserverId($observer)
    {
        return $observer->getId();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getLastIndexDate()
    {
        return $this->lastIndexDate;
    }

    public function setLastIndexDate($lastIndexDate)
    {
        $this->lastIndexDate = $lastIndexDate;
    }

    public function getReindexed()
    {
        return $this->reindexed;
    }

    public function setReindexed($reindexed)
    {
        $this->reindexed = $reindexed;
    }

    public function getUpdateMode()
    {
        return $this->updateMode;
    }

    public function getComment()
    {
        return $this->comment;
    }
}