<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Model\Adapter;

use Magento\Catalog\Model\Layer\Search\FilterableAttributeList;
use Magento\Elasticsearch\SearchAdapter\Aggregation\Builder as AggregationBuilder;
use Magento\Elasticsearch\SearchAdapter\ConnectionManager;
use Magento\Elasticsearch\SearchAdapter\QueryContainerFactory;
use Magento\Elasticsearch\SearchAdapter\ResponseFactory;
use Magento\Elasticsearch7\SearchAdapter\Mapper;
use Magento\Framework\Search\RequestInterface;
use Magento\Framework\Search\Response\QueryResponse;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Wyomind\ElasticsearchCore\Helper\Config;
use Wyomind\ElasticsearchCore\Helper\Data;
if (!class_exists("\\Magento\\Elasticsearch7\\SearchAdapter\\Adapter")) {
    class Elasticsearch7
    {
    }
    return;
}
/**
 * Elasticsearch Adapter
 * Rewrites the products collection retrieved when performing a search (for the search results page)
 */
class Elasticsearch7 extends \Magento\Elasticsearch7\SearchAdapter\Adapter
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;
    /**
     * @var \Magento\Framework\Search\Adapter\Mysql\Aggregation\Builder
     */
    private $aggregationBuilder;
    /**
     * @var \Magento\Framework\Search\Adapter\Mysql\DocumentFactory
     */
    protected $documentFactory;
    /**
     * @var \Magento\Framework\Search\Adapter\Mysql\Mapper
     */
    protected $mapper;
    /**
     * @var \Magento\Framework\Search\Adapter\Mysql\ResponseFactory
     */
    protected $responseFactory;
    /**
     * @var \Magento\Framework\Search\Adapter\Mysql\TemporaryStorageFactory
     */
    private $temporaryStorageFactory;
    private $connectionManager;
    private $queryContainerFactory;
    private $logger;
    private $empty = ["hits" => ["hits" => []], "aggregations" => ["price_bucket" => [], "category_bucket" => ["buckets" => []]]];
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, ConnectionManager $connectionManager, Mapper $mapper, ResponseFactory $responseFactory, AggregationBuilder $aggregationBuilder, QueryContainerFactory $queryContainerFactory, LoggerInterface $logger)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $construct = "__construct";
        parent::$construct($connectionManager, $mapper, $responseFactory, $aggregationBuilder, $queryContainerFactory, $logger);
        $this->mapper = $mapper;
        $this->responseFactory = $responseFactory;
        $this->aggregationBuilder = $aggregationBuilder;
        $this->queryContainerFactory = $queryContainerFactory;
    }
    /**
     * @inheritdoc
     */
    public function query(RequestInterface $request) : QueryResponse
    {
        $query = $request->getQuery();
        // category page?
        if ($query->getName() == 'catalog_view_container' || $query->getName() == 'quick_order_suggestions_search_container') {
            return parent::query($request);
        }
        // ES down?
        if (!$this->configHelper->getServerStatus()) {
            return parent::query($request);
        }
        try {
            $filters = $this->getFiltersFromQuery($query);
            $sort = $this->getSortFromQuery($request);
            $rawResponse = $this->requestElastic($request, $filters, $sort);
        } catch (\Exception $e) {
            // config file not found => fallback to mysql
            return parent::query($request);
        }
        $rawDocuments = isset($rawResponse['raw']) && !empty($rawResponse['raw']) ? $rawResponse['raw'] : $this->empty;
        $query = $this->mapper->buildQuery($request);
        $aggregationBuilder = $this->aggregationBuilder;
        $aggregationBuilder->setQuery($this->queryContainerFactory->create(['query' => $query]));
        $aggregations = $aggregationBuilder->build($request, $rawDocuments);
        $customerGroupId = $this->dataHelper->getCustomerGroupId();
        foreach ($aggregations as $bucket => $agg) {
            foreach ($agg as $option => $info) {
                $aggregations[$bucket][$option]['count'] = 0;
            }
        }
        $filterableAttributes = $this->filterableAttributeList->getList();
        foreach ($filterableAttributes as $att) {
            if (!isset($aggregations[$att->getAttributeCode()]) && $att->getAttributeCode() != "price") {
                $aggregations[$att->getAttributeCode() . '_bucket'] = [];
            }
        }
        if (count($rawDocuments)) {
            foreach ($rawDocuments['hits']['hits'] as $product) {
                $product = $product['_source'];
                if ($product['visibility'] > 1) {
                    foreach ($product as $key => $value) {
                        $bucket = str_replace('_ids', '', $key) . '_bucket';
                        if ($key != 'price' && isset($aggregations[$bucket])) {
                            $values = $product[$key];
                            if (!is_array($values)) {
                                $value = $values;
                                if (isset($aggregations[$bucket][$value]['count'])) {
                                    $aggregations[$bucket][$value]['count']++;
                                } else {
                                    $aggregations[$bucket][$value]['count'] = 1;
                                    $aggregations[$bucket][$value]['value'] = $value;
                                }
                            } else {
                                foreach ($values as $value) {
                                    if (isset($aggregations[$bucket][$value]['count'])) {
                                        $aggregations[$bucket][$value]['count']++;
                                    } else {
                                        $aggregations[$bucket][$value]['count'] = 1;
                                        $aggregations[$bucket][$value]['value'] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            foreach ($rawDocuments['hits']['hits'] as $product) {
                $product = $product['_source'];
                if ($product['visibility'] > 1) {
                    foreach ($aggregations as $bucket => $agg) {
                        if ($bucket != 'price_bucket' && $bucket != 'category_bucket') {
                            if (isset($product[str_replace('_bucket', '', $bucket) . '_ids'])) {
                                $values = $product[str_replace('_bucket', '', $bucket) . '_ids'];
                                if (!is_array($values)) {
                                    $value = $values;
                                    if (isset($aggregations[$bucket][$value]['count'])) {
                                        $aggregations[$bucket][$value]['count']++;
                                    }
                                } else {
                                    foreach ($values as $value) {
                                        if (isset($aggregations[$bucket][$value]['count'])) {
                                            $aggregations[$bucket][$value]['count']++;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($bucket == 'category_bucket') {
                                if (isset($product['categories_ids'])) {
                                    $values = $product['categories_ids'];
                                    if (!is_array($values)) {
                                        $value = $values;
                                        if (isset($aggregations[$bucket][$value]['count'])) {
                                            $aggregations[$bucket][$value]['count']++;
                                        } else {
                                            $aggregations[$bucket][$value]['count'] = 1;
                                            $aggregations[$bucket][$value]['value'] = $value;
                                        }
                                    } else {
                                        foreach ($values as $value) {
                                            if (isset($aggregations[$bucket][$value]['count'])) {
                                                $aggregations[$bucket][$value]['count']++;
                                            } else {
                                                $aggregations[$bucket][$value]['count'] = 1;
                                                $aggregations[$bucket][$value]['value'] = $value;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($bucket == 'price_bucket') {
                                    if (isset($product['prices_' . $customerGroupId])) {
                                        $values = $product['prices_' . $customerGroupId]['final_price'];
                                        foreach ($agg as $interval) {
                                            $int = explode('_', $interval['value']);
                                            if ($int[0] === '*') {
                                                $int[0] = 0;
                                            }
                                            if (!isset($int[1]) || $int[1] === '*') {
                                                $int[1] = INF;
                                            }
                                            if ($values >= $int[0] && $values <= $int[1] - 0.01) {
                                                $aggregations[$bucket][$interval['value']]['count']++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach ($aggregations as $bucket => $agg) {
            //if ($bucket != "price_bucket" && $bucket != "category_bucket") {
            foreach ($agg as $option => $info) {
                if ($aggregations[$bucket][$option]['count'] == 0) {
                    unset($aggregations[$bucket][$option]);
                }
            }
            //}
        }
        $rawDocuments = $rawDocuments['hits']['hits'] ?? [];
        foreach ($rawDocuments as $key => $doc) {
            unset($rawDocuments[$key]['_source']);
        }
        $queryResponse = $this->responseFactory->create(['documents' => $rawDocuments, 'aggregations' => $aggregations, 'total' => $rawResponse['raw']['hits']['total']['value'] ?? 0]);
        return $queryResponse;
    }
    /**
     * Perform the request using Elasticsearch
     * @param \Magento\Framework\Search\RequestInterface $request
     * @param array $filters
     * @return array
     * @throws \Exception when the config file for the storeview is not found
     */
    public function requestElastic(\Magento\Framework\Search\RequestInterface $request, $filters, $sort)
    {
        $dimension = current($request->getDimensions());
        if ($dimension && $dimension->getName() == 'scope') {
            $storeId = $dimension->getValue();
        } else {
            $storeId = $this->getCurrentStore()->getId();
        }
        $storeCode = $this->storeManager->getStore($storeId)->getCode();
        try {
            $config = new \Wyomind\ElasticsearchCore\Helper\Autocomplete\Config($storeCode);
        } catch (\Exception $e) {
            throw $e;
        }
        if (!$config->getData()) {
            throw new \Exception(__('Could not find config for autocomplete'));
        }
        $boolQuery = $request->getQuery();
        $should = $boolQuery->getShould();
        $matchQuery = $should['search'];
        $client = new \Wyomind\ElasticsearchCore\Model\Client($config);
        $client->init($storeId);
        $cache = new \Wyomind\ElasticsearchCore\Helper\Cache\FileSystem();
        $synonymsHelper = new \Wyomind\ElasticsearchCore\Helper\Synonyms();
        $requester = new \Wyomind\ElasticsearchCore\Helper\Requester($client, $config, $cache, $synonymsHelper);
        $customerGroupId = $this->dataHelper->getCustomerGroupId();
        if ($sort) {
            $order = $sort['order'];
            $direction = $sort['direction'];
        } else {
            $order = "relevance";
            $direction = "desc";
        }
        $result = $requester->getProducts($storeCode, $customerGroupId, -1, $matchQuery->getValue(), 0, 10000, $order, $direction, $filters, true, true, true, 1, true);
        return $result;
    }
    private function getSortFromQuery($query)
    {
        $sort = $query->getSort();
        if ($sort) {
            foreach ($sort as $sortEntity) {
                $order = $sortEntity['field'];
                $direction = strtolower($sortEntity['direction']);
                if ($order != "entity_id") {
                    return ["order" => $order, "direction" => $direction];
                }
            }
        }
        return null;
    }
    /**
     * Extract the filters to apply from the query
     * @param \Magento\Framework\Search\Request\Query $query
     * @return array
     */
    private function getFiltersFromQuery($query)
    {
        $filters = [];
        $should = $query->getShould();
        $must = $query->getMust();
        foreach ($should as $key => $info) {
            if ($info instanceof \Magento\Framework\Search\Request\Query\Filter) {
                $reference = $info->getReference();
                if ($reference instanceof \Magento\Framework\Search\Request\Filter\Term) {
                    $field = $reference->getField();
                    if ($field == 'category_ids') {
                        $field = 'categories';
                    }
                    if ($field == 'visibility') {
                        continue;
                    }
                    $value = $reference->getValue();
                    if (isset($value['in'])) {
                        $value = $value['in'];
                    }
                    $filters[$field . '_ids'] = [$value];
                } elseif ($reference instanceof \Magento\Framework\Search\Request\Filter\Range) {
                    $field = $reference->getField();
                    if ($field == 'price') {
                        $field = 'final_price';
                    }
                    $filters[$field] = ['min' => $reference->getFrom(), 'max' => $reference->getTo()];
                }
            }
        }
        foreach ($must as $key => $info) {
            if ($info instanceof \Magento\Framework\Search\Request\Query\Filter) {
                $reference = $info->getReference();
                if ($reference instanceof \Magento\Framework\Search\Request\Filter\Term) {
                    $field = $reference->getField();
                    if ($field == 'category_ids') {
                        $field = 'categories';
                    }
                    if ($field == 'visibility') {
                        continue;
                    }
                    $value = $reference->getValue();
                    if (isset($value['in'])) {
                        $value = $value['in'];
                    }
                    $filters[$field . '_ids'] = [$value];
                } elseif ($reference instanceof \Magento\Framework\Search\Request\Filter\Range) {
                    $field = $reference->getField();
                    if ($field == 'price') {
                        $field = 'final_price';
                    }
                    $filters[$field] = ['min' => $reference->getFrom(), 'max' => $reference->getTo()];
                }
            }
        }
        return $filters;
    }
    /**
     * @inheritdoc
     */
    private function getDocuments(\Magento\Framework\DB\Ddl\Table $table)
    {
        $connection = $this->getConnection();
        $select = $connection->select();
        $select->from($table->getName(), ['entity_id', 'score']);
        return $connection->fetchAssoc($select);
    }
    /**
     * @inheritdoc
     */
    private function getConnection()
    {
        return $this->resource->getConnection();
    }
}