<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Model;

class Adapter
{
    /**
     * @var Client
     */
    protected $_client = null;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    public function getClient()
    {
        if (!$this->_client) {
            $this->_client = new Client($this->_configHelper);
        }
        return $this->_client;
    }
    /**
     * @param int $storeId
     * @param string $type
     * @return array
     */
    protected function getIndexParams($storeId, $type)
    {
        $compatibility = $this->_configHelper->getCompatibility($storeId);
        $settings = json_decode($this->_configHelper->getIndexSettings($storeId), true);
        $settings['index.mapping.total_fields.limit'] = 10000;
        //        $settings['analysis']["char_filter"] = [
        //            "search_in_name_filter" => [
        //                "type" => "mapping",
        //                "mappings" => [
        //                    "- => _"
        //                ]
        //            ]
        //        ];
        //        $settings['analysis']['analyzer']["search_in_name_analyzer"] = [
        //            "tokenizer" => "standard",
        //            "char_filter" => [
        //                "search_in_name_filter"
        //            ],
        //            "filter" => [
        //                "elision", "asciifolding", "lowercase", "length"
        //            ]
        //        ];
        // ELS 7 filter "standard" doesn't exist anymore + no more "custom"
        if ($compatibility == 7) {
            foreach ($settings['analysis']['analyzer'] as $analyzer => $data) {
                if (isset($data['filter'])) {
                    $newA = array_diff($data['filter'], ['standard']);
                    $settings['analysis']['analyzer'][$analyzer]['filter'] = $newA;
                }
            }
            $result = ['body' => ['settings' => $settings, 'mappings' => $this->_mappingBuilder->build($storeId, $type)[$type]]];
        } else {
            $result = [
                //'custom' => ['update_all_types' => true],
                'body' => ['settings' => $settings, 'mappings' => [$type => $this->_mappingBuilder->build($storeId, $type)]],
            ];
        }
        return $result;
    }
    /**
     * @param \Traversable $documents
     * @param string $type
     * @param boolean $full
     * @param int $storeId
     */
    public function addDocs(\Traversable $documents, $type, $full, $storeId)
    {
        // Initialize some variables
        $new = false;
        $this->getClient()->init($storeId);
        // Create a new index if full product reindexation is needed
        if ($full && $this->_configHelper->isSafeReindex($storeId)) {
            $new = true;
        }
        // Retrieve store index (create it if not exists)
        $index = $this->getIndex($storeId, $new, $type);
        // Index documents (this is a bulk indexation according to indexer batch size)
        foreach ($documents as $docs) {
            $this->getClient()->index($docs, $index, $type);
        }
        if ($new) {
            // Switch alias to the new index when indexation has ended
            $this->switchIndex($index, $storeId, $type);
        }
    }
    /**
     * @param string $type
     * @param int $storeId
     * @param array $ids
     */
    public function deleteDocs($type, $storeId, $ids)
    {
        $this->getClient()->init($storeId);
        $index = $this->getIndex($storeId, false, $type);
        $this->getClient()->delete($ids, $index, $type);
    }
    /**
     * @param string $index
     * @param int $storeId
     * @param string $type
     */
    protected function switchIndex($index, $storeId, $type)
    {
        $alias = $this->getIndexAlias($storeId, $type);
        $indices = $this->getClient()->getIndicesWithAlias($alias);
        foreach ($indices as $indexName) {
            if ($indexName != $index) {
                // remove old index that was linked to the alias
                $this->getClient()->deleteIndex($indexName);
            }
        }
        $this->getClient()->createAlias($index, $alias);
    }
    /**
     * @param int $storeId
     * @param bool $new
     * @param string $type
     * @return string
     */
    protected function getIndex($storeId, $new, $type)
    {
        $index = $this->getIndexName($storeId, $new, $type);
        // Delete index if exists and if we are indexing all documents
        $indexExists = $this->getClient()->existsIndex($index);
        if ($new && $indexExists) {
            $this->getClient()->deleteIndex($index);
            $indexExists = false;
        }
        // If index doesn't exist, create it
        if (!$indexExists) {
            $this->_eventManager->dispatch('wyomind_elasticsearchcore_create_index_before', ['index' => $index, 'store' => $storeId]);
            $this->getClient()->createIndex($index, $this->getIndexParams($storeId, $type));
            if (!$new) {
                $this->getClient()->createAlias($index, $this->getIndexAlias($storeId, $type));
            }
            $this->_eventManager->dispatch('wyomind_elasticsearchcore_create_index_after', ['index' => $index, 'store' => $storeId]);
        }
        return $index;
    }
    /**
     * @param int $storeId
     * @param boolean $new
     * @param string $type
     * @return string
     */
    protected function getIndexName($storeId, $new, $type)
    {
        $store = $this->_storeManager->getStore($storeId);
        return strtolower($this->getClient()->getIndexName($store->getCode(), $new, $type));
    }
    /**
     * @param int $storeId
     * @param string $type
     * @return string
     */
    protected function getIndexAlias($storeId, $type)
    {
        $store = $this->_storeManager->getStore($storeId);
        return strtolower($this->getClient()->getIndexAlias($store->getCode(), $type));
    }
    public function getConfigHelper()
    {
        return $this->_configHelper;
    }
}