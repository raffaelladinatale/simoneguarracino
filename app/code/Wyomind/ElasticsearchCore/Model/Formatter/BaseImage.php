<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Model\Formatter;

use Magento\Catalog\Helper\Image;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Design\Theme\FlyweightFactory;
use Wyomind\ElasticsearchCore\Helper\Config;
class BaseImage
{
    /**
     * @var FlyweightFactory
     */
    protected $themeFactory;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, FlyweightFactory $themeFactory)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->themeFactory = $themeFactory;
    }
    /**
     * @param mixed $value
     * @param mixed $store
     * @return mixed
     */
    public function format($value, $store = null)
    {
        if ($value == 'no_selection') {
            $value = null;
        } else {
            try {
                $image = $this->imageHelper->init(null, 'base', ['type' => 'base', 'width' => 240, 'height' => 300]);
                $image->setImageFile($value);
                $value = $image->getUrl();
            } catch (\Exception $e) {
                // placeholder image
                $mediaBaseUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $placeholderPath = $this->_configHelper->getStoreConfig('catalog/placeholder/image_placeholder', $store->getId());
                //Base Image
                if ($placeholderPath != '') {
                    $value = $mediaBaseUrl . 'catalog/product/placeholder/' . $placeholderPath;
                } else {
                    $themeId = $this->config->getTheme($store);
                    $value = $this->assetRepo->createAsset('Magento_Catalog::images/product/placeholder/image.jpg', ['area' => 'frontend', 'theme' => $this->themeFactory->create($themeId)->getThemePath()])->getUrl();
                }
            }
            if (strpos($value, '://') === false) {
                // if the url doesn't contain :// this means probably it is a relative url
                $baseUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $value = substr($value, strpos($value, '/media') + 7);
                $value = $baseUrl . $value;
            }
            if ($store->isFrontUrlSecure()) {
                $value = str_replace("http://", "https://", $value);
            }
            if (!$this->config->getEnablePubFolder()) {
                $value = str_replace("pub/", "", $value);
            }
        }
        return $value;
    }
}