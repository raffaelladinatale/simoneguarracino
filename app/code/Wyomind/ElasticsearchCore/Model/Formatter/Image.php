<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Model\Formatter;

use Magento\Catalog\Helper\Image as HelperImage;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Design\Theme\FlyweightFactory;
use Magento\Store\Model\Store;
use Wyomind\ElasticsearchCore\Helper\Config;
class Image
{
    /**
     * @var FlyweightFactory
     */
    protected $_themeFactory = null;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, FlyweightFactory $themeFactory)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->_themeFactory = $themeFactory;
    }
    /**
     * @param mixed $value
     * @param Store $store
     * @return mixed
     */
    public function format($value, $store = null)
    {
        if ($value == 'no_selection') {
            $value = null;
        } else {
            try {
                $imageSize = $this->_configHelper->getStoreConfig('wyomind_elasticsearchcore/types/product/image_size', $store);
                $image = $this->_imageHelper->init(null, 'thumbnail', ['type' => 'thumbnail', 'width' => $imageSize, 'height' => $imageSize]);
                $image->setImageFile($value);
                $value = $image->getUrl();
            } catch (\Exception $e) {
                $themeId = $this->_configHelper->getTheme($store);
                $value = $this->_assetRepository->createAsset('Magento_Catalog::images/product/placeholder/image.jpg', ['area' => 'frontend', 'theme' => $this->_themeFactory->create($themeId)->getThemePath()])->getUrl();
            }
            if (strpos($value, '://') === false) {
                $baseUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $value = substr($value, strpos($value, '/media') + 7);
                $value = $baseUrl . $value;
            }
            if ($store->isFrontUrlSecure()) {
                $value = str_replace('http://', 'https://', $value);
            }
            if (!$this->_configHelper->getEnablePubFolder()) {
                $value = str_replace('pub/', '', $value);
            }
        }
        return $value;
    }
}