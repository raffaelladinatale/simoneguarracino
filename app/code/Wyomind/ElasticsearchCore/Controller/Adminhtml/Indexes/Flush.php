<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Controller\Adminhtml\Indexes;

/**
 * @package Wyomind\ElasticsearchCore\Controller\Adminhtml\Indexes
 */
class Flush extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Wyomind_ElasticsearchCore::indexer';

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory|null
     */
    protected $_resultRedirectFactory = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Indexer|null
     */
    protected $_indexerHelper = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\Adapter
     */
    protected $_adapter = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Progress
     */
    protected $_progressHelper;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_coreDate;
    /**
     * @var \Wyomind\ElasticsearchCore\Model\Index
     */
    protected $_indexModel;

    /**
     * Flush constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Wyomind\ElasticsearchCore\Helper\Indexer $indexerHelper
     * @param \Wyomind\ElasticsearchCore\Model\Index $indexModel
     * @param \Wyomind\ElasticsearchCore\Model\Adapter $adapter
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $coreDate
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Wyomind\ElasticsearchCore\Helper\Indexer $indexerHelper,
        \Wyomind\ElasticsearchCore\Model\Index $indexModel,
        \Wyomind\ElasticsearchCore\Model\Adapter $adapter,
        \Magento\Framework\Stdlib\DateTime\DateTime $coreDate
    )
    {
        $this->_resultRedirectFactory = $context->getResultRedirectFactory();
        $this->_storeManager = $storeManager;
        $this->_indexerHelper = $indexerHelper;
        $this->_adapter = $adapter;
        $this->_progressHelper = $context->getObjectManager()->create('\Wyomind\ElasticsearchCore\Helper\Progress');
        parent::__construct($context);
        $this->_coreDate = $coreDate;
        $this->_indexModel = $indexModel;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        if (php_sapi_name() != "cli") {
            session_write_close();
        }



        $indexerId = $this->getRequest()->getParam('indexer_id');

        $this->_progressHelper->startObservingProgress(true, $indexerId, $indexerId);

        $stores = $this->_storeManager->getStores();
        $counter = 0;
        foreach ($stores as $store) {
            $this->_progressHelper->log(__("Flushing index '%1'", $indexerId), true, \Wyomind\Framework\Helper\Progress::PROCESSING, $counter / count($stores) * 100);
            $counter++;
            $this->_adapter->deleteDocs($indexerId, $store->getStoreId(), []);
        }

        $this->messageManager->addSuccess(__('The indexer ' . $indexerId . ' has been flushed'));

        $result = $this->_resultRedirectFactory->create()->setPath(\Wyomind\ElasticsearchCore\Helper\Url::MANAGE_INDEXES);

        $this->_progressHelper->log(__("'$1' index has been flushed", $indexerId), true, \Wyomind\Framework\Helper\Progress::SUCCEEDED, 100);
        $this->_progressHelper->stopObservingProgress();

        $index = $this->_indexModel->loadByIndexerId($indexerId);
        $datetime = $this->_coreDate->date('Y-m-d H:i:s', $this->_coreDate->gmtTimestamp());
        $index->setLastIndexDate($datetime);
        $index->save();

        return $result;
    }
}