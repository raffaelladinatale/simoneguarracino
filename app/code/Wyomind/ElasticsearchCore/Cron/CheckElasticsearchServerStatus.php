<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Cron;

/**
 * Class CheckElasticsearchServerStatus
 */
class CheckElasticsearchServerStatus
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    /**
     * @param \Magento\Cron\Model\Schedule $schedule
     */
    public function execute(\Magento\Cron\Model\Schedule $schedule)
    {
        // Global scope
        $cleanCache = $this->_serverHelper->updateServerCron();
        // Other scopes
        foreach ($this->_storeManager->getStores() as $store) {
            $storeId = $store->getStoreId();
            $storeCode = $store->getCode();
            $cleanCache |= $this->_serverHelper->updateServerCron($storeId, $storeCode, true);
        }
        if ($cleanCache) {
            $this->_configHelper->cleanCache();
        }
    }
}