<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Observer;

class SaveConfig implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $storeId = $observer->getStore();
        $storeCodes = [];
        if ($storeId) {
            // Store view
            $store = $this->_storeManager->getStore($storeId);
            $storeCodes = [$store->getCode()];
        } else {
            // Default Config
            $stores = $this->_storeManager->getStores();
            foreach ($stores as $store) {
                $storeCodes[] = $store->getCode();
            }
        }
        foreach ($storeCodes as $code) {
            $this->_jsonConfigHelper->saveConfig($code);
        }
    }
}