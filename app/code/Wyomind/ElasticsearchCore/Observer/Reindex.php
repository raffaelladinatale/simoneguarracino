<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Observer;

class Reindex implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Wyomind\ElasticsearchCore\Helper\ConfigFactory
     */
    protected $_configHelperFactory = null;
    /**
     * @var \Wyomind\ElasticsearchCore\Helper\IndexerFactory
     */
    protected $_indexerHelperFactory;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Wyomind\ElasticsearchCore\Helper\ConfigFactory $configHelperFactory, \Wyomind\ElasticsearchCore\Helper\IndexerFactory $indexerHelperFactory)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->_configHelperFactory = $configHelperFactory;
        $this->_indexerHelperFactory = $indexerHelperFactory;
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var \Wyomind\ElasticsearchCore\Helper\Config $configHelper */
            $configHelper = $this->_configHelperFactory->create();
            /** @var \Wyomind\ElasticsearchCore\Helper\Indexer $indexerHelper */
            $indexerHelper = $this->_indexerHelperFactory->create();
            $storeId = $this->_storeManager->getStore()->getId();
            $eventsList = $indexerHelper->getEventsList();
            $event = $observer->getEvent();
            $eventName = $event->getName();
            if (array_key_exists($eventName, $eventsList)) {
                foreach ($eventsList[$eventName] as $eventDetails) {
                    $type = $eventDetails['indexer'];
                    $action = $eventDetails['action'];
                    $eventObject = $observer->getDataObject();
                    // Check if the indexation is enabled
                    if ($configHelper->isIndexationEnabled($type, $storeId)) {
                        $indexer = $indexerHelper->getIndexer($type);
                        if (array_key_exists('getId', $eventDetails)) {
                            $objectId = $indexer->{$eventDetails['getId']}($observer);
                        } else {
                            $objectId = $eventObject->getId();
                        }
                        $indexer->{$action}($objectId);
                        if (isset($eventDetails['fallback'])) {
                            $this->_eventManager->dispatch($eventDetails['fallback'], ['id' => $objectId]);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
        }
    }
}