<?php

namespace Wyomind\ElasticsearchCore\Ui\Model;

class Manager_2_1 extends \Magento\Ui\Model\Manager
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Framework\View\Element\UiComponent\Config\Provider\Component\Definition $componentConfigProvider, \Magento\Framework\View\Element\UiComponent\Config\DomMergerInterface $domMerger, \Magento\Framework\View\Element\UiComponent\Config\ReaderFactory $readerFactory, \Magento\Framework\View\Element\UiComponent\ArrayObjectFactory $arrayObjectFactory, \Magento\Framework\View\Element\UiComponent\Config\FileCollector\AggregatedFileCollectorFactory $aggregatedFileCollectorFactory, \Magento\Framework\Config\CacheInterface $cache, \Magento\Framework\Data\Argument\InterpreterInterface $argumentInterpreter)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $construct = "__construct";
        // in order to bypass the compiler
        parent::$construct($componentConfigProvider, $domMerger, $readerFactory, $arrayObjectFactory, $aggregatedFileCollectorFactory, $cache, $argumentInterpreter);
    }
    public function prepareData($name)
    {
        parent::prepareData($name);
        if ($name == 'elasticsearchcore_browse') {
            $data = $this->getData($name);
            list($type, $indice, $storeCode, $storeId) = $this->_sessionHelper->getBrowseData();
            if ($indice == null) {
                return $this;
            }
            $columns =& $data['elasticsearchcore_browse']['children']['columns']['children'];
            $indexer = $this->_indexerHelper->getIndexer($type);
            $columns = array_merge($columns, $indexer->getBrowseColumns($storeId));
            $this->componentsData->offsetSet($name, $data);
        }
        return $this;
    }
}