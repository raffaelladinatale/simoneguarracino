<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Ui\Component\Listing\Indexes\Column;

class Actions extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Framework\View\Element\UiComponent\ContextInterface $context, \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory, array $components = [], array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    /**
     * Prepare Data Source
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['indexer_id'])) {
                    $item[$name]['flush'] = ['href' => $this->_urlBuilder->getUrl(\Wyomind\ElasticsearchCore\Helper\Url::FLUSH_URL, ['indexer_id' => $item['indexer_id']]), 'label' => __('Flush'), 'confirm' => ['title' => __('Flush'), 'message' => __("Are you sure you want to flush the index <b>%1</b> now?", $item['indexer_id'])]];
                    $item[$name]['run'] = ['href' => $this->_urlBuilder->getUrl(\Wyomind\ElasticsearchCore\Helper\Url::RUN_URL, ['indexer_id' => $item['indexer_id']]), 'label' => __('Run'), 'confirm' => ['title' => __('Run'), 'message' => __("Are you sure you want to run the index <b>%1</b> now?", $item['indexer_id'])]];
                }
            }
        }
        return $dataSource;
    }
}