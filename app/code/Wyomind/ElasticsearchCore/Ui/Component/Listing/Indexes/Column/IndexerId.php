<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Ui\Component\Listing\Indexes\Column;

class IndexerId implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var array 
     */
    public $options = null;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    /**
     * Get all options available
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $collection = $this->_indexerHelper->getTypes();
            foreach ($collection as $type) {
                $this->options[] = ['label' => $type, 'value' => $type];
            }
            $this->options = array_map('unserialize', array_unique(array_map('serialize', $this->options)));
        }
        return $this->options;
    }
}