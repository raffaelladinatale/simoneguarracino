<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Ui\Component\Listing\Browse\Column;

class Actions extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Framework\View\Element\UiComponent\ContextInterface $context, \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory, array $components = [], array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    /**
     * Prepare Data Source
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        list($type, $indice, $store, $storeId) = $this->_sessionHelper->getBrowseData();
        if (isset($dataSource['data']['items'])) {
            $index = $this->_indexerHelper->getIndexer($type);
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                $item[$name]['raw'] = ['href' => "javascript:void(require(['elasticsearchcore_browse'], function (browse) { browse.raw('" . $this->_urlBuilder->getUrl(\Wyomind\ElasticsearchCore\Helper\Url::RAW_DATA_URL, ['indice' => $indice, 'store' => $store, 'storeId' => $storeId, 'type' => $type, 'id' => $item['id']]) . "'); }))", 'label' => __('Raw data'), 'hidden' => false];
                $index->getBrowseActions($item, $name);
            }
        }
        return $dataSource;
    }
}