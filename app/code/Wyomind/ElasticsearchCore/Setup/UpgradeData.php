<?php

namespace Wyomind\ElasticsearchCore\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Store\Model\StoreManager;
use Wyomind\ElasticsearchCore\Helper\JsonConfig;


class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Wyomind\Framework\Helper\Module
     */
    protected $framework;
    /**
     * @var StoreManager
     */
    protected $storeManager;
    /**
     * @var JsonConfig
     */
    protected $jsonConfigHelper;

    /** @var EavSetupFactory $eavSetupFactory */
    private $_eavSetupFactory;

    /**
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param \Wyomind\Framework\Helper\Module $framework
     * @param StoreManager $storeManager
     * @param JsonConfig $jsonConfigHelper
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Wyomind\Framework\Helper\Module $framework,
        StoreManager $storeManager,
        JsonConfig $jsonConfigHelper
    )
    {
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->framework = $framework;
        $this->storeManager = $storeManager;
        $this->jsonConfigHelper = $jsonConfigHelper;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    )
    {

        if (version_compare($context->getVersion(), '3.2.1') < 0) {
            $setup->startSetup();

            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'elasticsearchcore_ignore',
                [
                    'group' => "Wyomind Elasticsearch Core",
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Ignore the product for indexation',
                    'input' => 'boolean',
                    'class' => '',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible' => true,
                    'required' => true,
                    'user_defined' => false,
                    'default' => '0',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
            $setup->endSetup();
        }

        if (version_compare($context->getVersion(), '4.1.17') < 0) {
            $oldIndexSettings = "{\"number_of_shards\":1,\"number_of_replicas\":0,\"analysis\":{\"analyzer\":{\"std\":{\"tokenizer\":\"standard\",\"char_filter\":\"html_strip\",\"filter\":[\"standard\",\"elision\",\"asciifolding\",\"lowercase\",\"length\"]},\"keyword\":{\"tokenizer\":\"keyword\",\"filter\":[\"asciifolding\",\"lowercase\",\"length\"]},\"keyword_prefix\":{\"tokenizer\":\"keyword\",\"filter\":[\"asciifolding\",\"lowercase\",\"edge_ngram_front\"]},\"text_prefix\":{\"tokenizer\":\"standard\",\"char_filter\":\"html_strip\",\"filter\":[\"standard\",\"elision\",\"asciifolding\",\"lowercase\",\"edge_ngram_front\"]},\"text_suffix\":{\"tokenizer\":\"standard\",\"char_filter\":\"html_strip\",\"filter\":[\"standard\",\"elision\",\"asciifolding\",\"lowercase\",\"edge_ngram_back\"]}},\"filter\":{\"edge_ngram_front\":{\"type\":\"edgeNGram\",\"min_gram\":1,\"max_gram\":20,\"side\":\"front\"},\"edge_ngram_back\":{\"type\":\"edgeNGram\",\"min_gram\":1,\"max_gram\":20,\"side\":\"back\"},\"length\":{\"type\":\"length\",\"min\":1,\"min_subword_size\":1}}}}";
            $newIndexSettings = "{
   \"number_of_shards\": 1,
   \"number_of_replicas\": 0,
   \"analysis\": {
       \"analyzer\": {
           \"std\": {
               \"tokenizer\": \"standard\",
               \"char_filter\": \"html_strip\",
               \"filter\": [\"standard\", \"elision\", \"asciifolding\", \"lowercase\", \"length\"]
           },
           \"keyword\": {
               \"tokenizer\": \"keyword\",
               \"filter\": [\"asciifolding\", \"lowercase\", \"length\"]
           },
           \"keyword_prefix\": {
               \"tokenizer\": \"keyword\",
               \"filter\": [\"asciifolding\", \"lowercase\", \"edge_ngram_front\"]
           },
           \"text_prefix\": {
               \"tokenizer\": \"standard\",
               \"char_filter\": \"html_strip\",
               \"filter\": [\"standard\", \"elision\", \"asciifolding\", \"lowercase\", \"edge_ngram_front\"]
           },
           \"text_suffix\": {
               \"tokenizer\": \"standard\",
               \"char_filter\": \"html_strip\",
               \"filter\": [\"reverse\",\"standard\", \"elision\", \"asciifolding\", \"lowercase\", \"edge_ngram_front\",\"reverse\"]
           },
           \"sku_prefix\": {
               \"tokenizer\": \"whitespace\",
               \"filter\": [\"lowercase\", \"edge_ngram_front\"]
           },
           \"sku_suffix\": {
               \"tokenizer\": \"whitespace\",
               \"filter\": [\"reverse\",\"lowercase\", \"edge_ngram_front\",\"reverse\"]
           },
           \"name_prefix\": {
               \"tokenizer\": \"whitespace\",
               \"filter\": [\"lowercase\", \"edge_ngram_front\"]
           },
           \"name_suffix\": {
               \"tokenizer\": \"whitespace\",
               \"filter\": [\"reverse\",\"lowercase\", \"edge_ngram_front\",\"reverse\"]
           }
       },
       \"filter\": {
           \"edge_ngram_front\": {
               \"type\": \"edge_ngram\",
               \"min_gram\": 3,
               \"max_gram\": 20,
               \"side\": \"front\"
           },
           \"edge_ngram_back\": {
               \"type\": \"edge_ngram\",
               \"min_gram\": 3,
               \"max_gram\": 20,
               \"side\": \"back\"
           },
           \"length\": {
               \"type\": \"length\",
               \"min\": 1,
               \"min_subword_size\": 1
           }
       }
   }
}";

            $currentIndexSettings = $this->framework->getStoreConfig("wyomind_elasticsearchcore/configuration/index_settings");
            $currentIndexSettings = preg_replace("/\s+/", "", $currentIndexSettings);
            if ($oldIndexSettings == $currentIndexSettings) {
                $this->framework->setStoreConfig("wyomind_elasticsearchcore/configuration/index_settings", $newIndexSettings);
            }

        }


        if (version_compare($context->getVersion(), '4.5.12') < 0) {
            foreach ($this->storeManager->getStores() as $store) {
                $storeCode = $store->getCode();
                $this->jsonConfigHelper->saveConfig($storeCode);
            }
        }
    }
}
