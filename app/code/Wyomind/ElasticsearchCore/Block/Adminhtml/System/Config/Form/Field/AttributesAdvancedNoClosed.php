<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field;

/**
 * Config field for the selection of the product attributes filterable in the layered navigation
 *  with "closed by default"
 */
class AttributesAdvancedNoClosed extends \Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\AttributesAdvanced
{
    protected $closed = false;
}