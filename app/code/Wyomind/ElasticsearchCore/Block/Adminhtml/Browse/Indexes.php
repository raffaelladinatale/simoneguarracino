<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Block\Adminhtml\Browse;

/**
 * Class Indexes
 */
class Indexes extends \Magento\Framework\View\Element\Template
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Framework\View\Element\Template\Context $context, array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $data);
    }
    /**
     * Get all indexers
     * @return array
     */
    public function getIndexers()
    {
        return $this->_indexerHelper->getAllIndexers();
    }
    /**
     * Get all indices for a specific type
     * @param $type
     * @return array
     */
    public function getIndices($type)
    {
        return $this->_indexerHelper->getIndices($type);
    }
    /**
     * Get the current indice (from the $request object)
     * @return mixed
     */
    public function getSelectedIndice()
    {
        list($type, $indice, $store, $storeId) = $this->_sessionHelper->getBrowseData();
        return $indice;
    }
}