<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Block\Product\Renderer;

/**
 * Class Configurable for Magento 2.1
 */
class Configurable_2_1 extends \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable
{
    /**
     * @var \Magento\Catalog\Helper\Image|null
     */
    protected $_imageHelper = null;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Catalog\Block\Product\Context $context, \Magento\Framework\Stdlib\ArrayUtils $arrayUtils, \Magento\Framework\Json\EncoderInterface $jsonEncoder, \Magento\ConfigurableProduct\Helper\Data $helper, \Magento\Catalog\Helper\Product $catalogProduct, \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, \Magento\ConfigurableProduct\Model\ConfigurableAttributeData $configurableAttributeData, array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $arrayUtils, $jsonEncoder, $helper, $catalogProduct, $currentCustomer, $priceCurrency, $configurableAttributeData, $data);
        $this->_imageHelper = $context->getImageHelper();
    }
    /**
     * @return array
     */
    protected function getOptionImages()
    {
        $images = [];
        foreach ($this->getAllowProducts() as $product) {
            //            $productImages = $this->helper->getGalleryImages($product) ?: [];
            //            foreach ($productImages as $image) {
            if (null !== $product->getImage()) {
                $imageObj = $this->_imageHelper->init(null, 'base', ['type' => 'base', 'width' => 240, 'height' => 300]);
                $imageObj->setImageFile($product->getImage());
                $value = $imageObj->getUrl();
                if ($this->getCurrentStore()->isFrontUrlSecure()) {
                    $value = str_replace("http://", "https://", $value);
                }
                if (!$this->_config->getEnablePubFolder()) {
                    $value = str_replace("pub/", "", $value);
                }
                $images[$product->getId()] = ['img' => $value];
            }
            //            }
        }
        return $images;
    }
}