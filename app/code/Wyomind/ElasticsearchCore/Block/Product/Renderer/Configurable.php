<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Block\Product\Renderer;

/**
 * Class Configurable for Magento 2.2
 */
class Configurable extends \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable
{
    /**
     * @var \Magento\Catalog\Helper\Image|null
     */
    protected $_imageHelper = null;
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind, \Magento\Catalog\Block\Product\Context $context, \Magento\Framework\Stdlib\ArrayUtils $arrayUtils, \Magento\Framework\Json\EncoderInterface $jsonEncoder, \Magento\ConfigurableProduct\Helper\Data $helper, \Magento\Catalog\Helper\Product $catalogProduct, \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, \Magento\ConfigurableProduct\Model\ConfigurableAttributeData $configurableAttributeData, array $data = [], \Magento\Framework\Locale\Format $localeFormat = null, \Magento\Customer\Model\Session $customerSession = null)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $arrayUtils, $jsonEncoder, $helper, $catalogProduct, $currentCustomer, $priceCurrency, $configurableAttributeData, $data, $localeFormat, $customerSession);
        $this->_imageHelper = $context->getImageHelper();
    }
    public function getConfig()
    {
        $currentProduct = $this->getProduct();
        $options = $this->helper->getOptions($currentProduct, $this->getAllowProducts());
        $config = ['images' => $this->getOptionImages(), 'index' => isset($options['index']) ? $options['index'] : []];
        return $config;
    }
    /**
     * @return array
     */
    protected function getOptionImages()
    {
        $images = [];
        $secure = $this->getCurrentStore()->isFrontUrlSecure();
        $enablePubFolder = $this->_config->getEnablePubFolder();
        foreach ($this->getAllowProducts() as $product) {
            //            $productImages = $this->helper->getGalleryImages($product) ?: [];
            //            foreach ($productImages as $image) {
            if (null !== $product->getImage()) {
                $imageObj = $this->_imageHelper->init(null, 'base', ['type' => 'base', 'width' => 240, 'height' => 300]);
                $imageObj->setImageFile($product->getImage());
                $value = $imageObj->getUrl();
                if ($secure) {
                    $value = str_replace("http://", "https://", $value);
                }
                if (!$enablePubFolder) {
                    $value = str_replace("pub/", "", $value);
                }
                $images[$product->getId()] = ['img' => $value];
            }
            //            }
        }
        return $images;
    }
}