<?php

namespace Wyomind\ElasticsearchCore\Plugin\Catalog\Model\Product;

class Action
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    /**
     * @param Interceptor $interceptor
     * @param \Closure $closure
     * @param $productIds
     * @param $attrData
     * @param $storeId
     * @return Interceptor
     */
    public function aroundUpdateAttributes(\Magento\Catalog\Model\Product\Action\Interceptor $interceptor, \Closure $closure, $productIds, $attrData, $storeId)
    {
        $result = $closure($productIds, $attrData, $storeId);
        $this->_productPriceIndexerRows->execute($productIds);
        $this->_eventManager->dispatch("catalog_product_attribute_update_after", ['id' => $productIds]);
        return $result;
    }
}