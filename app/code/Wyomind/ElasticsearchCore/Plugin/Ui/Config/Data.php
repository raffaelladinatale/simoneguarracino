<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchCore\Plugin\Ui\Config;

class Data
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    /**
     * @param $subject
     * @param $proceed
     * @param null $path
     * @param null $default
     * @return mixed
     */
    public function aroundGet($subject, $proceed, $path = null, $default = null)
    {
        $data = $proceed($path, $default);
        if ($path == 'elasticsearchcore_browse') {
            list($type, $indice, $storeCode, $storeId) = $this->_sessionHelper->getBrowseData();
            // is still no indice, no change
            if ($indice == null) {
                return $data;
            }
            $columns =& $data['children']['columns']['children'];
            $indexer = $this->_indexerHelper->getIndexer($type);
            $columns = array_merge($columns, $indexer->getBrowseColumns($storeId));
        }
        return $data;
    }
}