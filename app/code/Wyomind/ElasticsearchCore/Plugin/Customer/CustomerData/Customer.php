<?php

namespace Wyomind\ElasticsearchCore\Plugin\Customer\CustomerData;

use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Customer\Helper\View;
/**
 * Class Customer
 */
class Customer
{
    public function __construct(\Wyomind\ElasticsearchCore\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    /**
     * {@inheritdoc}
     */
    public function afterGetSectionData($subject, $result)
    {
        if (!empty($result)) {
            $customer = $this->currentCustomer->getCustomer();
            $result['cgi'] = $customer->getGroupId();
        }
        return $result;
    }
}