<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\ElasticsearchAutocomplete\Block\Search;

/**
 * Display the first suggestion in search results page
 * @package Wyomind\ElasticsearchAutocomplete\Block\Search
 */
class DidYouMean extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Search\Model\QueryFactory
     */
    protected $queryFactory = null;
    public function __construct(\Wyomind\ElasticsearchAutocomplete\Helper\Delegate $wyomind, \Magento\Framework\View\Element\Template\Context $context, \Magento\Search\Model\QueryFactory $queryFactory, array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->queryFactory = $queryFactory;
        parent::__construct($context, $data);
    }
    public function getDataHelper()
    {
        return $this->dataHelper;
    }
    /**
     * Get the suggestion term matching the search term
     * @param string $storeCode code of the store
     * @return string the suggestion
     */
    public function getSuggestion($storeCode)
    {
        try {
            $config = new \Wyomind\ElasticsearchCore\Helper\Autocomplete\Config($storeCode);
            $config->getData();
        } catch (\Exception $e) {
            return "";
        }
        $client = new \Wyomind\ElasticsearchCore\Model\Client($config, new \Psr\Log\NullLogger());
        $client->init($storeCode);
        $cache = new \Wyomind\ElasticsearchCore\Helper\Cache\FileSystem();
        $synonymsHelper = new \Wyomind\ElasticsearchCore\Helper\Synonyms();
        $requester = new \Wyomind\ElasticsearchCore\Helper\Requester($client, $config, $cache, $synonymsHelper);
        $query = $this->queryFactory->get();
        $suggests = $requester->getSuggestions($storeCode, $query->getQueryText(), 1)['docs'];
        if (!empty($suggests)) {
            $suggest = array_pop($suggests);
            return $suggest['text'];
        } else {
            return "";
        }
    }
    /**
     * Get the search url for the suggest term
     * @param string $suggestion the suggest term
     * @return string the url
     */
    public function getQueryUrl($suggestion)
    {
        return $this->searchDataHelper->getResultUrl() . "?q=" . $suggestion;
    }
    /**
     * Are suggestions enabled?
     * @return boolean true if enabled, else false
     */
    public function isSuggestionEnabled()
    {
        return $this->configHelper->isDidYouMeanEnableSearch();
    }
    public function _toHtml()
    {
        if (!$this->_scopeConfig->isSetFlag('wyomind_elasticsearchautocomplete/settings/enable')) {
            return '';
        }
        return parent::_toHtml();
    }
}