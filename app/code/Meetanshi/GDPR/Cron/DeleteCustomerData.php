<?php
namespace Meetanshi\GDPR\Cron;

use Magento\Sales\Model\ResourceModel\Order\Collection;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\DeleteRequests;
use Meetanshi\GDPR\Model\DeleteRequests as DeleteRequestsModel;

/**
 * Class DeleteCustomerData
 */
class DeleteCustomerData
{
    /**
     * @var DeleteRequests
     */
    protected $objectRepository;
    /**
     * @var
     */
    protected $customer;
    /**
     * @var Collection
     */
    protected $orderCollection;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var DeleteRequestsModel
     */
    protected $model;

    /**
     * DeleteCustomerData constructor.
     * @param DeleteRequests $objectRepository
     * @param DeleteRequestsModel $model
     * @param Collection $orderCollection
     * @param Data $helper
     */
    public function __construct(
        DeleteRequests $objectRepository,
        DeleteRequestsModel $model,
        Collection $orderCollection,
        Data $helper
    ) {
        $this->objectRepository = $objectRepository;
        $this->helper = $helper;
        $this->model = $model;
        $this->orderCollection = $orderCollection;
    }

    /**
     * @return string
     */
    public function execute()
    {
        if ($this->helper->isGdprEnabled()) {
            if ($this->helper->isAllowAutomateDeleteData()) {
                $period = $this->helper->getAutomateDeleteDataPeriod();
                $orders = $this->orderCollection->getData();
                $now = time();
                foreach ($orders as $order) {
                    $createdDate = strtotime(date('Y-m-d', strtotime($order['created_at'])));
                    $datediff = $now - $createdDate;
                    $duration = round($datediff / (60 * 60 * 24));
                    if ($order['customer_id']) {
                        if ($duration == $period) {
                            $data = [
                                'request_id' => null,
                                'customer_id' => $order['customer_id'],
                                'customer_name' => $order['customer_firstname'] . ' ' . $order['customer_lastname'],
                                'email' => $order['customer_email'],
                                'intiator_of_deletion' => 'admin',
                                'completed_orders' => $this->helper->getOrderDetails($order['customer_id'])['complete'],
                                'pending_orders' => $this->helper->getOrderDetails($order['customer_id'])['pending'],
                                'request_date' => null
                            ];
                            if ($data) {
                                $data['request_id'] = null;
                                if (isset($data['request_id'])) {
                                    if (empty($data['request_id'])) {
                                        $data['request_id'] = null;
                                    }
                                }

                                if ($this->helper->isManagerNotifyOnDeletionRequest()) {
                                    $sendTo = $this->helper->getSenderEmailsForDeletionRequest();
                                    $subject = "Customer account deletion request email.";
                                    $template = $this->helper->getManagerNotificationEmailTemplates();
                                    $sender = $this->helper->getSenderName($this->helper->getManagerEmailOnDeletionRequest());
                                    $senderEmail = $this->helper->getSenderEmail($this->helper->getManagerEmailOnDeletionRequest());
                                    if ($sendTo != null) {
                                        $sentToArray = explode(';', $sendTo);
                                        foreach ($sentToArray as $item) {
                                            $this->helper->sendMail($template, $subject, $senderEmail, $sender, trim($item), null, trim($item), null, $order['customer_firstname'] . ' ' . $order['customer_lastname']);
                                        }
                                    }
                                }
                                $this->model->setData($data);
                                try {
                                    $this->objectRepository->save($this->model);
                                } catch (\Exception $e) {
                                    return $e->getMessage();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
