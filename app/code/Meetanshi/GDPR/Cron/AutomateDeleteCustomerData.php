<?php

namespace Meetanshi\GDPR\Cron;

use Magento\Customer\Model\Customer;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Meetanshi\GDPR\Helper\Data;
use Magento\Sales\Model\ResourceModel\Order\Collection;

/**
 * Class AutomateDeleteCustomerData
 */
class AutomateDeleteCustomerData
{
    /**
     * @var Customer
     */
    protected $customer;
    /**
     * @var Order
     */
    protected $orderModel;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var Collection
     */
    protected $orderCollection;
    /**
     * @var State
     */
    protected $state;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTimeZone;


    /**
     * AutomateDeleteCustomerData constructor.
     * @param Registry $registry
     * @param Data $helper
     * @param Order $orderModel
     * @param OrderRepositoryInterface $orderRepository
     * @param Collection $orderCollection
     * @param Customer $customer
     * @param State $state
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTimeZone
     */
    public function __construct(
        Registry $registry,
        Data $helper,
        Order $orderModel,
        OrderRepositoryInterface $orderRepository,
        Collection $orderCollection,
        Customer $customer,
        State $state,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTimeZone
    ) {
        $this->registry = $registry;
        $this->helper = $helper;
        $this->orderModel = $orderModel;
        $this->customer = $customer;
        $this->state = $state;
        $this->orderCollection = $orderCollection;
        $this->orderRepository = $orderRepository;
        $this->dateTimeZone = $dateTimeZone;
    }

    /**
     * @return string
     */
    public function execute()
    {
        if ($this->helper->isGdprEnabled()) {
            $isDelete = $this->helper->getPreventDataDeletion();
            $period = $this->helper->getPreventDataDeletionPeriod();
            $orders = $this->orderCollection->getData();
            $this->registry->register('isSecureArea', 'true');
            $currentDate = $this->dateTimeZone->date('Y-m-d');
            $nextdate = $this->dateTimeZone->date('Y-m-d', strtotime($currentDate." +".$period." days"));
            $currentDate = strtotime(date('Y-m-d', strtotime($currentDate)));
            $nextdate = strtotime(date('Y-m-d', strtotime($nextdate)));
            $datediff = $currentDate - $nextdate;
            $duration = round($datediff / (60 * 60 * 24));

            foreach ($orders as $order) {
                $customerId = $order['customer_id'];
                $orderEntityId = $order['entity_id'];
                $modelOrder = $this->orderModel->load($orderEntityId);
                $modelCustomer = $this->customer->load($customerId);
                if ($customerId) {
                    if ($isDelete) {
                        try {
                            if (!empty($modelOrder)) {
                                $this->orderRepository->delete($modelOrder);
                            }
                            $modelCustomer->delete();
                        } catch (\Exception $e) {
                            return $e->getMessage();
                        }
                    } else {
                        if ($duration == $period) {
                            try {
                                if (!empty($modelOrder)) {
                                    $this->orderRepository->delete($modelOrder);
                                }
                                $modelCustomer->delete();
                            } catch (\Exception $e) {
                                return $e->getMessage();
                            }
                        }
                    }
                }
            }
        }
    }
}
