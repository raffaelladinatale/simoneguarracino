<?php

namespace Meetanshi\GDPR\Cron;

use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\CookieConsentLogs\Collection;
use Meetanshi\GDPR\Model\CookieConsentLogsFactory;

/**
 * Class AutomateDeleteCookieConsents
 */
class AutomateDeleteCookieConsents
{
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var State
     */
    protected $state;
    /**
     * @var Collection
     */
    private $cookieConsents;
    /**
     * @var CookieConsentLogsFactory
     */
    private $cookieModel;

    /**
     * AutomateDeleteCookieConsents constructor.
     * @param Registry $registry
     * @param Data $helper
     * @param Collection $cookieConsents
     * @param CookieConsentLogsFactory $cookieModel
     * @param State $state
     */
    public function __construct(
        Registry $registry,
        Data $helper,
        Collection $cookieConsents,
        CookieConsentLogsFactory $cookieModel,
        State $state
    ) {
        $this->registry = $registry;
        $this->helper = $helper;
        $this->state = $state;
        $this->cookieConsents = $cookieConsents;
        $this->cookieModel = $cookieModel;
    }

    /**
     * @return string
     */
    public function execute()
    {
        if ($this->helper->isGdprEnabled()) {
            if ($this->helper->getCookieLogCleaningPeriod()) {
                $period = $this->helper->getCookieLogCleaningPeriod();
                $consents = $this->cookieConsents->getData();
                $this->registry->register('isSecureArea', 'true');
                $now = time();
                foreach ($consents as $consent) {
                    $createdDate = strtotime(date('Y-m-d', strtotime($consent['created_at'])));
                    $datediff = $now - $createdDate;
                    $duration = round($datediff / (60 * 60 * 24));
                    if ($duration == $period) {
                        try {
                            $model = $this->cookieModel->create();
                            $model->load($consent['concents_log_id']);
                            $model->delete();
                        } catch (\Exception $e) {
                            return $e->getMessage();
                        }
                    }
                }
            }
        }
    }
}
