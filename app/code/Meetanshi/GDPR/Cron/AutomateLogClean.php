<?php

namespace Meetanshi\GDPR\Cron;

use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\ActionLogs\Collection;
use Meetanshi\GDPR\Model\ActionLogs;

/**
 * Class AutomateLogClean
 */
class AutomateLogClean
{
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var State
     */
    protected $state;
    /**
     * @var Collection
     */
    private $logConsents;
    /**
     * @var ActionLogs
     */
    private $logModel;

    /**
     * AutomateLogClean constructor.
     * @param Registry $registry
     * @param Data $helper
     * @param Collection $logConsents
     * @param ActionLogs $logModel
     * @param State $state
     */
    public function __construct(
        Registry $registry,
        Data $helper,
        Collection $logConsents,
        ActionLogs $logModel,
        State $state
    ) {
        $this->registry = $registry;
        $this->helper = $helper;
        $this->state = $state;
        $this->logConsents = $logConsents;
        $this->logModel = $logModel;
    }

    /**
     * @return string
     */
    public function execute()
    {
        if ($this->helper->isGdprEnabled()) {
            if ($this->helper->isLogAutoCleaning() && $this->helper->logAutoCleaningPeriod()) {
                $period = $this->helper->logAutoCleaningPeriod();
                $consents = $this->logConsents->getData();
                $this->registry->register('isSecureArea', 'true');
                $now = time();
                foreach ($consents as $consent) {
                    $createdDate = strtotime(date('Y-m-d', strtotime($consent['created_at'])));
                    $datediff = $now - $createdDate;
                    $duration = round($datediff / (60 * 60 * 24));

                    if ($duration == $period) {

                        try {
                            $model = $this->logModel->load($consent['action_log_id']);
                            $model->delete();
                        } catch (\Exception $e) {
                            return $e->getMessage();
                        }
                    }
                }
            }
        }
    }
}
