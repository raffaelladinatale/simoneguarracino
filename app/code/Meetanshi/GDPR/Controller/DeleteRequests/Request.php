<?php

namespace Meetanshi\GDPR\Controller\DeleteRequests;

use Magento\Customer\Model\Authentication;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\State\UserLockedException;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\DeleteRequests;

/**
 * Class Request
 */
class Request extends Action
{
    /**
     * @var
     */
    protected $accountManagement;
    /**
     * @var DeleteRequests
     */
    protected $objectRepository;
    /**
     * @var
     */
    protected $customer;
    /**
     * @var Authentication
     */
    protected $authentication;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var \Meetanshi\GDPR\Model\DeleteRequests
     */
    protected $model;

    /**
     * Request constructor.
     * @param Context $context
     * @param Authentication $authentication
     * @param DeleteRequests $objectRepository
     * @param \Meetanshi\GDPR\Model\DeleteRequests $model
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        Authentication $authentication,
        DeleteRequests $objectRepository,
        \Meetanshi\GDPR\Model\DeleteRequests $model,
        Data $helper
    ) {
        parent::__construct($context);
        $this->objectRepository = $objectRepository;
        $this->authentication = $authentication;
        $this->model = $model;
        $this->helper = $helper;
        $this->messageManager = $context->getMessageManager();
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute()
    {
        $resultFactory = $this->resultRedirectFactory->create();
        $customerData = $this->_request->getParams();
        $password = $customerData['current_password'];
        $reqiestId = null;
        $id = $customerData['id'];
        $name = $customerData['name'];
        $email = $customerData['email'];
        $isAllow = $this->helper->isAllowAnonyDeleteAccount($id);
        try {
            if ($this->authentication->authenticate($id, $password)) {
                if ($isAllow) {
                    $data = [
                        'request_id' => null,
                        'customer_id' => $id,
                        'customer_name' => $name,
                        'email' => $email,
                        'intiator_of_deletion' => 'admin',
                        'completed_orders' => $this->helper->getOrderDetails($id)['complete'],
                        'pending_orders' => $this->helper->getOrderDetails($id)['pending'],
                        'request_date' => null
                    ];
                    if ($data) {

                        $data['request_id'] = null;
                        $model = $this->model;

                        if ($this->helper->isManagerNotifyOnDeletionRequest()) {
                            $sendTo = $this->helper->getSenderEmailsForDeletionRequest();
                            $template = $this->helper->getManagerNotificationEmailTemplates();
                            $subject = "Customer account deletion request email.";
                            $sender = $this->helper->getSenderName($this->helper->getManagerEmailOnDeletionRequest());
                            $senderEmail = $this->helper->getSenderEmail($this->helper->getManagerEmailOnDeletionRequest());

                            if ($sendTo != null) {
                                $sentToArray = explode(';', $sendTo);
                                foreach ($sentToArray as $item) {
                                    $this->helper->sendMail($template, $subject, $senderEmail, $sender, trim($item), null, trim($item), null, $name);
                                }
                            }
                        }

                        $model->setData($data);

                        try {
                            $this->objectRepository->save($model);
                            $message = __('Your request is in progress.');
                            $this->messageManager->addSuccessMessage($message);
                        } catch (\Exception $e) {
                            $this->messageManager->addErrorMessage($e->getMessage());
                        }
                    }
                } else {
                    $message = __('We are unable to process your request right now. Please complete your order(s) and try again');
                    $this->messageManager->addErrorMessage($message);
                }
            }
        } catch (InvalidEmailOrPasswordException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (UserLockedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $resultFactory->setPath('*/privacypolicy/settings');
    }
}
