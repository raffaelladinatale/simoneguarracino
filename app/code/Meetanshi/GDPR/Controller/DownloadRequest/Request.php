<?php

namespace Meetanshi\GDPR\Controller\DownloadRequest;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Authentication;
use Magento\Customer\Model\ResourceModel\Customer\Collection as Collection;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\GDPR\Helper\Data;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\File\Csv ;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Request
 */
class Request extends Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;
    /**
     * @var Csv
     */
    protected $csvProcessor;
    /**
     * @var DirectoryList
     */
    protected $directoryList;
    /**
     * @var Collection
     */
    private $customerFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Authentication
     */
    private $authentication;

    /**
     * Request constructor.
     * @param Context $context
     * @param Collection $customerFactory
     * @param Authentication $authentication
     * @param StoreManagerInterface $storeManager
     * @param Data $helper
     * @param FileFactory $fileFactory
     * @param Csv $csvProcessor
     * @param DirectoryList $directoryList
     */
    public function __construct(
        Context $context,
        Collection $customerFactory,
        Authentication $authentication,
        StoreManagerInterface $storeManager,
        Data $helper,
        FileFactory $fileFactory,
        Csv $csvProcessor,
        DirectoryList $directoryList
    ) {
        $this->fileFactory = $fileFactory;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->authentication = $authentication;
        $this->helper = $helper;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();

        $id = $params['id'];
        $password = $params['current_password'];
        $customerItem = $this->customerFactory->addFieldToFilter('entity_id', $id);
        try {
            if ($this->authentication->authenticate($id, $password)) {

                $customerData = $customerItem->getData();

                $titles = [
                    'firstname' =>  __('First Name'),
                    'lastname' =>  __('Last Name'),
                    'email' =>  __('Email'),
                    'group_id' =>  __('Group'),
                    'dob' =>  __('Date of Birth'),
                    'gender' =>  __('Gender'),
                    'taxvat' => __('Tax VAT'),
                    'store_id' =>  __('Store'),
                    'website_id' =>  __('Website'),
                ];
                $userdata = [];
                foreach ($customerData as $customer) {
                    $userdata = $customer;
                }
                $customerName = $userdata['firstname'] . '_' . $userdata['lastname'];
                $outputFile = "$customerName.csv";
                $filePath =  $this->directoryList->getPath(DirectoryList::MEDIA) . "/" . $outputFile;
                $userdata['store_id'] = $this->storeManager->getStore()->getName();
                $userdata['website_id'] = $this->storeManager->getWebsite()->getName();

                $data = [];
                foreach ($titles as $key => $value) {
                    if ($this->helper->getAllowDownloadEmptyFields()) {
                        if ($userdata[$key] == null) {
                            continue;
                        }
                    }
                    $data[] = [
                        __($titles[$key]),
                        __($userdata[$key])
                    ];
                }
                $this->csvProcessor->setEnclosure('"')->setDelimiter(',')->saveData($filePath, $data);
                return $this->fileFactory->create(
                    $outputFile,
                    [
                        'type'  => "filename",
                        'value' => $outputFile,
                        'rm'    => true,
                    ],
                    DirectoryList::MEDIA,
                    'text/csv',
                    null
                );
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
