<?php

namespace Meetanshi\GDPR\Controller\Consent;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\Cookie\CollectionFactory;
use Meetanshi\GDPR\Model\ResourceModel\CookieConsentLogs;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\App\ResourceConnection;

/**
 * Class CookieConsentsLogEntry
 */
class CookieConsentsLogEntry extends Action
{
    /**
     * @var CookieConsentLogs
     */
    protected $objectRepository;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;
    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;
    /**
     * @var Collection
     */
    private $cookieCollection;
    /**
     * @var bool
     */
    private $isAllAccepted;
    /**
     * @var null
     */
    private $cookiesAccepted;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var \Meetanshi\GDPR\Model\CookieConsentLogs
     */
    protected $model;

    /**
     * CookieConsentsLogEntry constructor.
     * @param Context $context
     * @param Data $helper
     * @param Collection $cookieCollection
     * @param CookieConsentLogs $objectRepository
     * @param CookieManagerInterface $cookieManager
     * @param SerializerInterface $serializer
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param \Meetanshi\GDPR\Model\CookieConsentLogs $model
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Context $context,
        Data $helper,
        CollectionFactory $cookieCollection,
        CookieConsentLogs $objectRepository,
        CookieManagerInterface $cookieManager,
        SerializerInterface $serializer,
        CookieMetadataFactory $cookieMetadataFactory,
        \Meetanshi\GDPR\Model\CookieConsentLogs $model,
        ResourceConnection $resourceConnection
    ) {
        $this->objectRepository  = $objectRepository;
        $this->helper  = $helper;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieCollection = $cookieCollection->create();
        $this->serializer = $serializer;
        $this->resourceConnection = $resourceConnection;
        $this->model = $model;
        $this->isAllAccepted = false;
        $this->cookiesAccepted = null;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute()
    {
        if ($this->helper->isGdprEnabled()) {
            $data = $this->_request->getParams();
            $userData = [];
            if (isset($data['userData'])) {
                $userData = $data['userData'];
            }
            if (isset($data['all_accepted'])) {
                $this->isAllAccepted = $data['all_accepted'];
            }
            if ($userData) {
                $userData['concents_log_id'] = null;
                $customerName = $userData['customer_name'];
                $model = $this->model;

                if (isset($data['concents_log_id'])) {
                    if (empty($userData['concents_log_id'])) {
                        $data['concents_log_id'] = null;
                    }
                }
                if ($this->isAllAccepted == 'true') {
                    $this->setCookiesToBrowser();
                } else {
                    if (isset($data['cookies_accepted'])) {
                        $this->cookiesAccepted = $this->getAllowCookies($data['cookies_accepted']);
                        $userData['status'] = $this->serializer->serialize($this->cookiesAccepted);
                        $this->setCookiesToBrowser();
                    } else {
                        $userData['status'] = "All Rejected";
                    }
                }
                $model->setData($userData);

                try {
                    $isLog = true;

                    if (!$this->helper->isGuestLogCookieConsent()) {
                        if ($customerName === 'Guest') {
                            $isLog = false;
                        }
                    }
                    if ($isLog) {
                        $this->objectRepository->save($model);
                    }

                } catch (AlreadyExistsException $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                }
            }
        }
    }

    /**
     *
     * @throws \Zend_Db_Statement_Exception
     */
    private function setCookiesToBrowser()
    {
        $allCookies = [];

        if ($this->isAllAccepted == 'true') {
            $allCookies = $this->cookieCollection->getData();
        } else {
            if ($this->cookiesAccepted != null) {
                foreach ($this->cookiesAccepted as $groupName => $value) {
                    if ($value == 'Allowed') {
                        $cookies = $this->cookieCollection->addFieldToFilter('cookie_group_name', $groupName)->getData();
                        foreach ($cookies as $cookie) {
                            $allCookies[] = $cookie;
                        }
                    }
                }
            }
        }
        foreach ($allCookies as $allCookie) {
            $cookieName = $allCookie['cookie_name'];
            $cookieLifeTime = $allCookie['cookie_lifetime'];
            $cookieProvider = $allCookie['cookie_provider'];

            $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
            $metadata->setPath('/');
            $metadata->setDuration($cookieLifeTime);
            $metadata->setHttpOnly(true);

            try {
                $this->cookieManager->setPublicCookie(
                    $cookieName,
                    $cookieProvider,
                    $metadata
                );
            } catch (InputException $e) {
            } catch (CookieSizeLimitReachedException $e) {
            } catch (FailureToSendException $e) {
            }
        }
    }

    /**
     * @param $cookies_accepted
     * @return array
     */
    private function getAllowCookies($cookies_accepted)
    {
        $accepted = [];

        foreach ($cookies_accepted as $cookie) {
            $accepted[$cookie] = 'Allowed';
        }
        return $accepted;
    }
}
