<?php

namespace Meetanshi\GDPR\Controller\Consent;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Meetanshi\GDPR\Helper\Data;
use Psr\Log\LoggerInterface as Logger;
use Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes\Collection;

/**
 * Class CheckoutConsentEntry
 */
class CheckoutConsentEntry extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs
     */
    protected $objectRepository;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var \Meetanshi\GDPR\Model\ConsentLogs
     */
    protected $model;
    /**
     * @var \Meetanshi\GDPR\Block\PrivacySettings\PolicySettings
     */
    protected $policySettings;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Collection
     */
    private $checkboxes;

    /**
     * CheckoutConsentEntry constructor.
     * @param Context $context
     * @param \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs $objectRepository
     * @param \Meetanshi\GDPR\Model\ConsentLogs $model
     * @param \Meetanshi\GDPR\Block\PrivacySettings\PolicySettings $policySettings
     * @param Logger $logger
     * @param Collection $checkboxes
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs $objectRepository,
        \Meetanshi\GDPR\Model\ConsentLogs $model,
        \Meetanshi\GDPR\Block\PrivacySettings\PolicySettings $policySettings,
        Logger $logger,
        Collection $checkboxes,
        Data $helper
    ) {
        $this->objectRepository  = $objectRepository;
        $this->helper  = $helper;
        $this->model  = $model;
        $this->policySettings  = $policySettings;
        $this->logger = $logger;
        $this->checkboxes = $checkboxes;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->helper->isGdprEnabled()) {
            $params = $this->_request->getParams();
            $policyDetails = [];
            $consents = [];
            $customerDetails = [];
            $isLog = [];
            $isHide = [];
            $consentsCollection = $this->getConsents();

            if (isset($params['consents'])) {
                $consents = $params['consents'];
            }

            if ($consents) {
                $model = $this->model;
                $consentBlock = $this->policySettings;

                if (is_array($consentBlock->getCustomerDetails()) && $consentBlock->getCustomerDetails() != null) {
                    $customerDetails = $consentBlock->getCustomerDetails();
                }
                foreach ($consentsCollection as $item) {
                    if ($item['is_enabled']) {
                        if ($this->helper->getCheckboxCountries($item['countries_restricted'], $item['countries'])) {
                            $isHide[$item['checkbox_code']] = $item['is_hide'];
                            $isLog[$item['checkbox_code']] = $item['log_status'];
                        }
                    }
                }
                foreach ($consents as $consent => $value) {
                    $status = true;
                    if (!$isHide[$consent]) {
                        if (!$isLog[$consent]) {
                            $status = false;
                        }
                    }
                    if ($status) {
                        $data = [
                            'concents_log_id ' => null,
                            'customer_id' => $customerDetails['id'],
                            'customer_name' => $customerDetails['name'],
                            'remote_ip' => $customerDetails['ip_address'],
                            'email' => $customerDetails['email'],
                            'login_date' => $customerDetails['logged_in_date'],
                            'checkbox_location' => 'Checkout',
                            'checkbox_code' => $consent,
                            'policy_version' => '---',
                            'website_id' => $customerDetails['website_id'],
                            'action' => ($value) ? "Accepted" : "Rejected"
                        ];

                        $model->setData($data);
                        try {
                            $this->objectRepository->save($model);
                        } catch (AlreadyExistsException $e) {
                            $this->logger->info(__($e->getMessage()));
                        } catch (\Exception $e) {
                            $this->logger->info(__($e->getMessage()));
                        }
                    }
                }
            }
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
    }

    public function getConsents()
    {
        $consents = $this->checkboxes
            ->addFieldToSelect('*')
            ->setOrder('checkbox_position', 'asc')
            ->getData();
        if ($consents!=null) {
            return $consents;
        } else {
            return [];
        }
    }
}
