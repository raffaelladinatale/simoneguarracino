<?php

namespace Meetanshi\GDPR\Controller\Consent;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Meetanshi\GDPR\Helper\Data;
use Magento\Customer\Model\Customer;

/**
 * Class ConsentLogEntry
 */
class ConsentLogEntry extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs
     */
    protected $objectRepository;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Customer
     */
    private $customer;
    /**
     * @var \Meetanshi\GDPR\Model\ConsentLogs
     */
    protected $model;

    /**
     * ConsentLogEntry constructor.
     * @param Context $context
     * @param \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs $objectRepository
     * @param \Meetanshi\GDPR\Model\ConsentLogs $model
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param CookieManagerInterface $cookieManager
     * @param Data $helper
     * @param Customer $customer
     */
    public function __construct(
        Context $context,
        \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs $objectRepository,
        \Meetanshi\GDPR\Model\ConsentLogs $model,
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManager,
        Data $helper,
        Customer $customer
    ) {
        $this->objectRepository  = $objectRepository;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieManager = $cookieManager;
        $this->helper = $helper;
        $this->customer = $customer;
        $this->model = $model;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        if ($this->helper->isGdprEnabled()) {
            $data = $this->_request->getParams();

            if ($data) {
                $data['concents_log_id'] = null;
                $model = $this->model;
                if (isset($data['concents_log_id'])) {
                    if (empty($data['concents_log_id'])) {
                        $data['concents_log_id'] = null;
                    }
                }
                $model->setData($data);
                try {
                    $this->objectRepository->save($model);
                } catch (AlreadyExistsException $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                }
            }
        }
    }
}
