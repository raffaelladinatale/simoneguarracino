<?php
namespace Meetanshi\GDPR\Controller\PrivacyPolicy;

use Magento\Customer\Model\Session;

/**
 * Class Settings
 */
class Settings extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Session
     */
    private $session;

    /**
     * Settings constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param Session $session
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        Session $session
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->session = $session;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultFactory = $this->resultRedirectFactory->create();
        if ($this->session->isLoggedIn()) {
            return $this->resultPageFactory->create();
        } else {
            return $resultFactory->setPath('customer/account/login');
        }
    }
}
