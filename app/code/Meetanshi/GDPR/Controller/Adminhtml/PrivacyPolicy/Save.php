<?php
namespace Meetanshi\GDPR\Controller\Adminhtml\PrivacyPolicy;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy\CollectionFactory;
use Meetanshi\GDPR\Model\PrivacyPolicy as PolicyModel;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /**#@+
     * Constants
     */
    const ADMIN_RESOURCE = 'Meetanshi_GDPR::config';

    /**
     * @var PrivacyPolicy\Collection
     */
    protected $collection;
    /**
     * @var PrivacyPolicy
     */
    protected $objectRepository;
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;
    /**
     * @var PolicyModel
     */
    private $model;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param PrivacyPolicy $objectRepository
     * @param PrivacyPolicy\Collection $collection
     * @param PolicyModel $model
     * @param \Magento\Backend\Model\Auth\Session $authSession
     */
    public function __construct(
        Action\Context $context,
        PrivacyPolicy $objectRepository,
        PrivacyPolicy\Collection $collection,
        PolicyModel $model,
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        $this->collection = $collection;
        $this->objectRepository  = $objectRepository;
        $this->authSession = $authSession;
        $this->model = $model;

        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            if (empty($data['policy_id'])) {
                $data['policy_id'] = null;
            }

            /** @var \Meetanshi\GDPR\Model\PrivacyPolicy $model */
            $model = $this->model;
            $user = $this->authSession->getUser()->getData();

            $data['edited_by'] = $user['firstname'].' '.$user['lastname'];

            if (array_key_exists('status', $data)) {
                $policies = $this->collection->getData();
                foreach ($policies as $policy) {
                    if ($data['status'] == 'Enable') {
                        if (array_key_exists('status', $policy)) {
                            if ($policy['status'] == 'Enable') {
                                $policy['status'] = 'Draft';
                                $model->setData($policy);
                                try {
                                    $this->objectRepository->save($model);
                                } catch (AlreadyExistsException $e) {
                                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                                } catch (\Exception $e) {
                                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                                }
                            }
                        }
                    }
                }
            }

            $model->setData($data);

            try {
                $this->objectRepository->save($model);
                $this->messageManager->addSuccess(__('You saved the policy.'));

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['policy_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError('Policy version should not be same');
                return $resultRedirect->setPath('*/*/new');
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['policy_id' => $this->getRequest()->getParam('policy_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_GDPR::privacy_policy');
    }
}
