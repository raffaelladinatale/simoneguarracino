<?php

namespace Meetanshi\GDPR\Controller\Adminhtml\DeleteRequests;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Sales\Model\Order;
use Magento\Customer\Model\Customer;
use Magento\Ui\Component\MassAction\Filter;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\ActionLogs;
use Meetanshi\GDPR\Model\ResourceModel\DeleteRequests\CollectionFactory;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Framework\Registry;
use Magento\Framework\App\State;
use Magento\Framework\App\ResourceConnection;
use Psr\Log\LoggerInterface as Logger;
use Meetanshi\GDPR\Model\ActionLogs as ActionModel;

/**
 * Class MassApproveRequest
 */
class MassApproveRequest extends \Magento\Backend\App\Action
{
    /**
     * @var Customer
     */
    protected $customer;
    /**
     * @var Order
     */
    protected $orderModel;
    /**
     * @var Filter
     */
    protected $filter;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var ActionLogs
     */
    protected $objectRepository;
    /**
     * @var RemoteAddress
     */
    protected $remote;
    /**
     * @var Collection
     */
    protected $orderCollection;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var State
     */
    private $state;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var ActionModel
     */
    private $model;

    /**
     * MassApproveRequest constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Filter $filter
     * @param ActionLogs $objectRepository
     * @param RemoteAddress $remote
     * @param CollectionFactory $collectionFactory
     * @param Collection $orderCollection
     * @param Order $orderModel
     * @param Customer $customer
     * @param State $state
     * @param Data $helper
     * @param Logger $logger
     * @param ResourceConnection $resourceConnection
     * @param ActionModel $model
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Filter $filter,
        ActionLogs $objectRepository,
        RemoteAddress $remote,
        CollectionFactory $collectionFactory,
        Collection $orderCollection,
        Order $orderModel,
        Customer $customer,
        State $state,
        Data $helper,
        Logger $logger,
        ResourceConnection $resourceConnection,
        ActionModel $model
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->remote = $remote;
        $this->orderModel = $orderModel;
        $this->customer = $customer;
        $this->objectRepository = $objectRepository;
        $this->orderCollection = $orderCollection;
        $this->registry = $registry;
        $this->state = $state;
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
        $this->model = $model;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        $userData = $collection->getData();
        $email = null;
        $customerName = null;
        $customerId = null;

        foreach ($userData as $data) {
            $email = $data['email'];
            $customerName = $data['customer_name'];
            $customerId = $data['customer_id'];
        }

        foreach ($collection as $block) {
            $block->delete();
            $model = $this->model;
            $data = [
                "action_log_id" => null,
                "customer_id"=>$customerId,
                "customer_name"=>$customerName,
                "remote_ip"=>$this->remote->getRemoteAddress(),
                "action_date"=>null,
                "action"=>"Delete Request Submitted",
                "comment"=>"---",
                "created_at"=>null,
                "updated_at"=>null
            ];
            $model->setData($data);
            try {
                $this->objectRepository->save($model);
            } catch (AlreadyExistsException $e) {
            } catch (\Exception $e) {
            }
        }

        $resource = $this->resourceConnection;
        $connection = $resource->getConnection();
        $select = $connection->select()
            ->from($resource->getTableName('sales_order'))
            ->where('customer_id = '.$customerId);

        foreach ($connection->query($select)->fetchAll() as $row) {

            $customerId = $row['customer_id'];
            $flag = false;
            if ($this->helper->getAllowAnonyDeleteCustomer()) {
                $orderStatuses = explode(',', $this->helper->getOrderStatuses());
                foreach ($orderStatuses as $orderStatus) {
                    if ($orderStatus == $row['status']) {
                        $flag = true;
                    }
                }
                if ($flag) {
                    continue;
                }
            }

            $modelOrder = $this->orderModel->load($row['entity_id']);
            $modelCustomer = $this->customer->load($customerId);
            try {
                $modelOrder->delete();
                $modelCustomer->delete();
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
            }
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 deletion request(s) have been approved.', $collectionSize));
        $subject = "Account deletion request approved by the admin.";
        $template = $this->helper->getCustomerApprovalNotificationEmailTemplates();
        $senderName = $this->helper->getSenderName($this->helper->getCustomerApprovalNotificationEmailSender());
        $replyTo = $this->helper->getCustomerApprovalNotificationEmailReplyTo();
        $senderEmail = $this->helper->getSenderEmail($this->helper->getCustomerApprovalNotificationEmailSender());
        $this->helper->sendMail($template, $subject, $senderEmail, $senderName, $email, $replyTo, null, null, $customerName);

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('gdpr/deleterequests');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_GDPR::delete_requests');
    }
}
