<?php

namespace Meetanshi\GDPR\Controller\Adminhtml\DeleteRequests;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Ui\Component\MassAction\Filter;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\ActionLogs;
use Meetanshi\GDPR\Model\ResourceModel\DeleteRequests\CollectionFactory;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Meetanshi\GDPR\Model\ActionLogs as ActionLogModel;

/**
 * Class MassDenyRequest
 */
class MassDenyRequest extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var RemoteAddress
     */
    protected $remote;
    /**
     * @var ActionLogs
     */
    protected $objectRepository;
    /**
     * @var ActionLogModel
     */
    private $model;

    /**
     * MassDenyRequest constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ActionLogs $objectRepository
     * @param RemoteAddress $remote
     * @param Data $helper
     * @param ActionLogModel $model
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ActionLogs $objectRepository,
        RemoteAddress $remote,
        Data $helper,
        ActionLogModel $model
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->remote = $remote;
        $this->objectRepository = $objectRepository;
        $this->model = $model;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        $userData = $collection->getData();
        $email = null;
        $customerName = null;
        $customerId = null;
        foreach ($userData as $data) {
            $email = $data['email'];
            $customerName = $data['customer_name'];
            $customerId = $data['customer_id'];
        }

        foreach ($collection as $block) {
            $block->delete();
            $model = $this->model;
            $data = [
                "action_log_id" => null,
                "customer_id"=>$customerId,
                "customer_name"=>$customerName,
                "remote_ip"=>$this->remote->getRemoteAddress(),
                "action_date"=>null,
                "action"=>"Delete Request Rejected",
                "comment"=>"Sorry, we cannot approve your delete request due to your incomplete orders.",
                "created_at"=>null,
                "updated_at"=>null
            ];
            $model->setData($data);
            try {
                $this->objectRepository->save($model);
            } catch (AlreadyExistsException $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
            }
        }
        $this->messageManager->addSuccessMessage(__('A total of %1 deletion request(s) have been denied.', $collectionSize));
        $subject = "Account deletion request denied by the admin.";
        $template = $this->helper->getCustomerDenyNotificationEmailTemplates();
        $senderName = $this->helper->getSenderName($this->helper->getCustomerDenyNotificationEmailSender());
        $replyTo = $this->helper->getCustomerDenyNotificationEmailReplyTo();
        $senderEmail = $this->helper->getSenderEmail($this->helper->getCustomerDenyNotificationEmailSender());
        $this->helper->sendMail($template, $subject, $senderEmail, $senderName, $email, $replyTo, null, null, $customerName);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('gdpr/deleterequests');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_GDPR::delete_requests');
    }
}
