<?php

namespace Meetanshi\GDPR\Controller\Adminhtml\CheckboxConsents;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /**#@+
     * Constants
     */
    const ADMIN_RESOURCE = 'Meetanshi_GDPR::config';
    /**
     * @var
     */
    protected $dataPersistor;
    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes
     */
    protected $objectRepository;
    /**
     * @var \Meetanshi\GDPR\Model\ConcentsCheckboxes
     */
    protected $concentsCheckboxes;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes $objectRepository
     * @param \Meetanshi\GDPR\Model\ConcentsCheckboxes $concentsCheckboxes
     */
    public function __construct(
        Action\Context $context,
        \Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes $objectRepository,
        \Meetanshi\GDPR\Model\ConcentsCheckboxes $concentsCheckboxes
    ) {
        $this->objectRepository = $objectRepository;
        $this->concentsCheckboxes = $concentsCheckboxes;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data['checkbox_id'])) {
                $data['checkbox_id'] = null;
            }

            /** @var \Meetanshi\GDPR\Model\ConcentsCheckboxes $model */
            $model = $this->concentsCheckboxes;

            if (array_key_exists('checkbox_location', $data)) {
                $data['checkbox_location'] = implode(',', $data['checkbox_location']);
            }
            if (array_key_exists('countries', $data) && !empty($data['countries'])) {
                $data['countries'] = implode(',', $data['countries']);
            }

            $model->setData($data);

            try {
                $this->objectRepository->save($model);
                $this->messageManager->addSuccess(__('You saved the consent.'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['checkbox_id' => $model->getId(), '_current' => true]
                    );
                }
                return $resultRedirect->setPath('*/*/');

            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __($e->getMessage()));
            }

            return $resultRedirect->setPath(
                '*/*/edit',
                ['checkbox_id' => $this->getRequest()->getParam('checkbox_id')]
            );
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_GDPR::checkbox_consents');
    }
}
