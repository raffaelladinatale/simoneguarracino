<?php
namespace Meetanshi\GDPR\Controller\Adminhtml\CookieGroup;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /**#@+
     * Constants
     */
    const ADMIN_RESOURCE = 'Meetanshi_GDPR::config';

    /**
     * @var
     */
    protected $dataPersistor;

    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\CookieGroup
     */
    protected $objectRepository;
    /**
     * @var \Meetanshi\GDPR\Model\CookieGroup
     */
    protected $cookieGroup;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Meetanshi\GDPR\Model\ResourceModel\CookieGroup $objectRepository
     * @param \Meetanshi\GDPR\Model\CookieGroup $cookieGroup
     */
    public function __construct(
        Action\Context $context,
        \Meetanshi\GDPR\Model\ResourceModel\CookieGroup $objectRepository,
        \Meetanshi\GDPR\Model\CookieGroup $cookieGroup
    ) {
        $this->objectRepository = $objectRepository;
        $this->cookieGroup = $cookieGroup;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            if (empty($data['cookie_group_id'])) {
                $data['cookie_group_id'] = null;
            }

            /** @var \Meetanshi\GDPR\Model\PrivacyPolicy $model */
            $model = $this->cookieGroup;

            if (isset($data['assigned_cookies'])) {
                if ($data['assigned_cookies'] != null) {
                    $data['assigned_cookies'] = implode(',', $data['assigned_cookies']);
                } else {
                    $data['assigned_cookies'] = null;
                }
            }

            $model->setData($data);

            try {
                $this->objectRepository->save($model);
                $this->messageManager->addSuccess(__('You saved the cookie.'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['cookie_group_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, $e->getMessage());
            }

            return $resultRedirect->setPath('*/*/edit', ['cookie_group_id' => $this->getRequest()->getParam('cookie_group_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_GDPR::cookie_group');
    }
}
