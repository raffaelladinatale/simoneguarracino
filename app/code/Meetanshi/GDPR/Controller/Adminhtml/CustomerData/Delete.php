<?php

namespace Meetanshi\GDPR\Controller\Adminhtml\CustomerData;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\State;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Newsletter\Model\Subscriber;
use Magento\Sales\Model\Order;
use Meetanshi\GDPR\Helper\Data;
use Magento\Sales\Model\ResourceModel\Order\Collection;

/**
 * Class Delete
 */
class Delete extends Action
{
    /**
     * @var CustomerRepository
     */
    protected $customer;
    /**
     * @var Order
     */
    protected $orderModel;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var Collection
     */
    protected $orderCollection;
    /**
     * @var State
     */
    protected $state;
    /**
     * @var Subscriber
     */
    protected $subscriber;

    /**
     * Delete constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Data $helper
     * @param Order $orderModel
     * @param Collection $orderCollection
     * @param CustomerRepository $customer
     * @param State $state
     * @param Subscriber $subscriber
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Data $helper,
        Order $orderModel,
        Collection $orderCollection,
        CustomerRepository $customer,
        State $state,
        Subscriber $subscriber
    ) {
        parent::__construct($context);
        $this->registry = $registry;
        $this->helper = $helper;
        $this->orderModel = $orderModel;
        $this->customer = $customer;
        $this->state = $state;
        $this->orderCollection = $orderCollection;
        $this->subscriber = $subscriber;
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute()
    {
        $customerId = $this->getRequest()->getParam('id');

        $orders = $this->orderCollection->getData();
        $isAllow = $this->helper->isAllowAnonyDeleteAccount($customerId);
        if ($isAllow) {
            if (!empty($customerId)) {
                try {
                    $customer = $this->customer->getById($customerId);
                    $customerName = $customer->getFirstname().' '.$customer->getLastname();
                    $email = $customer->getEmail();
                    $this->customer->deleteById($customerId);
                    if (sizeof($orders) > 0) {
                        foreach ($orders as $order) {
                            $flag = false;

                            if (!$this->helper->getAllowAnonyDeleteCustomer()) {
                                $orderStatuses = explode(',', $this->helper->getOrderStatuses());
                                foreach ($orderStatuses as $orderStatus) {
                                    if ($orderStatus == $order['status']) {
                                        $flag = true;
                                    }
                                }
                                if ($flag) {
                                    continue;
                                }
                            }
                            if ($order['entity_id'] != null) {
                                $modelOrder = $this->orderModel->load($order['entity_id']);
                                if ($modelOrder->getCustomer()) {
                                    $customerName = $modelOrder->getCustomer()->getName();
                                    $email  =$modelOrder->getCustomer()->getEmail();
                                    try {
                                        $modelOrder->delete();
                                    } catch (\Exception $e) {
                                        $this->messageManager->addErrorMessage(__($e->getMessage()));
                                    }
                                }
                            }
                        }
                    }
                    $subject = "Your Account Delete Request has approved by store owner";
                    $template = $this->helper->getCustomerApprovalNotificationEmailTemplates();
                    $senderName = $this->helper->getSenderName($this->helper->getCustomerApprovalNotificationEmailSender());
                    $replyTo = $this->helper->getCustomerApprovalNotificationEmailReplyTo();
                    $senderEmail = $this->helper->getSenderEmail($this->helper->getCustomerApprovalNotificationEmailSender());
                    if ($customerName != '' && !empty($email)) {
                        $this->helper->sendMail($template, $subject, $senderEmail, $senderName, $email, $replyTo, null, null, $customerName);
                    }
                    $this->messageManager->addSuccessMessage(__('You deleted the customer.'));
                } catch (\Exception $exception) {
                    $this->messageManager->addErrorMessage(__($exception->getMessage()));
                }
            }
        } else {
            $this->messageManager->addErrorMessage(__('You cannot delete customer because of incomplete order(s).'));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('customer/index');
    }
}
