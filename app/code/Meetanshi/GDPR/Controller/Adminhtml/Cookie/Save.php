<?php
namespace Meetanshi\GDPR\Controller\Adminhtml\Cookie;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /**#@+
     * Constants
     */
    const ADMIN_RESOURCE = 'Meetanshi_GDPR::config';

    /**
     * @var
     */
    protected $dataPersistor;

    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\Cookie
     */
    protected $objectRepository;
    /**
     * @var \Meetanshi\GDPR\Model\Cookie
     */
    protected $cookie;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Meetanshi\GDPR\Model\ResourceModel\Cookie $objectRepository
     * @param \Meetanshi\GDPR\Model\Cookie $cookie
     */
    public function __construct(
        Action\Context $context,
        \Meetanshi\GDPR\Model\ResourceModel\Cookie $objectRepository,
        \Meetanshi\GDPR\Model\Cookie $cookie
    ) {
        $this->objectRepository = $objectRepository;
        $this->cookie = $cookie;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            if (empty($data['cookie_id'])) {
                $data['cookie_id'] = null;
            }
            /** @var \Meetanshi\GDPR\Model\Cookie $model */
            $model = $this->cookie;
            $model->setData($data);
            try {
                $this->objectRepository->save($model);
                $this->messageManager->addSuccess(__('You saved the cookie.'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['cookie_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['cookie_id' => $this->getRequest()->getParam('cookie_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_GDPR::cookie');
    }
}
