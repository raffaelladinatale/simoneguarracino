<?php

namespace Meetanshi\GDPR\Controller\Anonymization;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Authentication;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\GDPR\Helper\Data;

/**
 * Class Request
 */
class Request extends Action
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Authentication
     */
    private $authentication;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Request constructor.
     * @param Context $context
     * @param CustomerRepositoryInterface $customerRepository
     * @param Authentication $authentication
     * @param StoreManagerInterface $storeManager
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        Authentication $authentication,
        StoreManagerInterface $storeManager,
        Data $helper
    ) {
        $this->storeManager = $storeManager;
        $this->authentication = $authentication;
        $this->helper = $helper;
        $this->customerRepository = $customerRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        $password = $params['current_password'];
        $anonymous = 'anonymous';
        $isAllow = $this->helper->isAllowAnonyDeleteAccount($id);
        try {
            if ($isAllow) {
                if ($id) {
                    $customer = $this->customerRepository->getById($id);
                    if ($this->authentication->authenticate($id, $password)) {
                        $template = $this->helper->getAnonymizeNotificationEmailTemplate();
                        $subject = "Account anonymization acknowledgement email.";
                        $sender = $this->helper->getSenderName($this->helper->getAnonymizeNotificationSender());
                        $senderEmail = $this->helper->getSenderEmail($this->helper->getAnonymizeNotificationSender());
                        $replyTo = $this->helper->getAnonymizeNotificationReplyTo();
                        $email = stristr($customer->getEmail(), '@');
                        $newEmail = $anonymous.$this->generateRandomString().$email;
                        $this->helper->sendMail($template, $subject, $senderEmail, $sender, $customer->getEmail(), $replyTo, null, $newEmail);
                        if ($customer->getId()) {
                            $customer->setFirstname($anonymous.$this->generateRandomString());
                            $customer->setLastname($anonymous.$this->generateRandomString());
                            $customer->setMiddlename($anonymous.$this->generateRandomString());
                            $customer->setTaxvat($anonymous.$this->generateRandomString());
                            $customer->setPrefix($anonymous.$this->generateRandomString());
                            $customer->setSuffix($anonymous.$this->generateRandomString());
                            $customer->setEmail($newEmail);
                        }
                        $this->customerRepository->save($customer);

                        $this->messageManager->addSuccessMessage(__('Your data has been processed'));
                    }
                }
            } else {
                $message = __('We are unable to process your request right now. Please complete your order(s) and try again');
                $this->messageManager->addErrorMessage($message);
            }

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }

    /**
     * @param int $length
     * @return bool|string
     */
    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)))), 1, $length);
    }
}
