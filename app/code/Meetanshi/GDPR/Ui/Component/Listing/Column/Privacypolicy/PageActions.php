<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\Column\Privacypolicy;

use Magento\Backend\Model\UrlInterface;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

/**
 * Class PageActions
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * PageActions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Backend\Model\Auth\Session $authSession,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->authSession = $authSession;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["policy_id"])) {
                    $id = $item["policy_id"];
                }

                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "gdpr/privacypolicy/edit",
                        ["policy_id"=>$id]
                    ),
                    "label"=>__("Edit")
                ];
            }
        }

        return $dataSource;
    }
}
