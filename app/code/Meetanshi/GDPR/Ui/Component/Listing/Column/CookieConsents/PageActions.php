<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\Column\CookieConsents;

use Magento\Backend\Model\UrlInterface;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class PageActions
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var Collection
     */
    protected $customer;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection
     */
    private $cookieGroup;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * PageActions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Collection $customer
     * @param UrlInterface $urlBuilder
     * @param SerializerInterface $serializer
     * @param \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection $cookieGroup
     * @param ResourceConnection $resourceConnection
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Collection $customer,
        UrlInterface $urlBuilder,
        SerializerInterface $serializer,
        \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection $cookieGroup,
        ResourceConnection $resourceConnection,
        array $components = [],
        array $data = []
    ) {
        $this->customer = $customer;
        $this->urlBuilder = $urlBuilder;
        $this->serializer = $serializer;
        $this->cookieGroup = $cookieGroup;
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                if (isset($item['customer_id'])) {
                    $model = $this->customer->getData();
                    foreach ($model as $key => $value) {
                        if ($value['entity_id'] == $item['customer_id']) {
                            $url = $this->urlBuilder->getUrl('customer/index/edit', ['id'=>$item['customer_id']]);
                            $customerName = $model[$key]['firstname'].' '.$model[$key]['lastname'];
                            $html = '<a href="'.$url.'" target="_blank">'. __($customerName).'</a>';
                            $item['customer_name'] = $html;
                        }
                    }
                }
                if (isset($item['status'])) {
                    $statuses = json_decode($item['status']);
                    if (gettype($statuses) == 'object') {
                        $html = '';
                        $resource = $this->resourceConnection;
                        $connection = $resource->getConnection();
                        foreach ($statuses as $key => $value) {
                            $select = $connection->select()->from($resource->getTableName('meetanshi_gdpr_cookiegroups'));
                            $select = $select->where('cookie_group_code='."'$key'");
                            $title = '';
                            foreach ($connection->query($select)->fetchAll() as $row) {
                                $title = $row['cookie_group_name'];
                            }
                            $html .= "<b>$title: </b>$value </br>";
                        }
                        $item['status'] = $html;
                    }
                }
            }
        }
        return $dataSource;
    }
}
