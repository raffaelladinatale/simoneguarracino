<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\Column\Cookiegroup;

/**
 * Class PageActions
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["cookie_group_id"])) {
                    $id = $item["cookie_group_id"];
                }

                if (isset($item['is_enabled'])) {
                    if ($item['is_enabled']) {
                        $item['is_enabled'] = 'Yes';
                    } else {
                        $item['is_enabled'] = 'No';
                    }
                }
                if (isset($item['is_essential'])) {
                    if ($item['is_essential']) {
                        $item['is_essential'] = 'Yes';
                    } else {
                        $item['is_essential'] = 'No';
                    }
                }

                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "gdpr/cookiegroup/edit",
                        ["cookie_group_id"=>$id]
                    ),
                    "label"=>__("Edit")
                ];
            }
        }

        return $dataSource;
    }
}
