<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\Column\DeleteRequests;

use Magento\Backend\Model\UrlInterface;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

/**
 * Class PageActions
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var Collection
     */
    protected $customer;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * PageActions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Collection $customer
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Collection $customer,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->customer = $customer;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                if (isset($item['customer_id'])) {
                    $model = $this->customer->getData();
                    foreach ($model as $key => $value) {
                        if ($value['entity_id'] == $item['customer_id']) {
                            $url = $this->urlBuilder->getUrl('customer/index/edit', ['id'=>$item['customer_id']]);
                            $customerName = $model[$key]['firstname'].' '.$model[$key]['lastname'];
                            $html = '<a href="'.$url.'" target="_blank">'. __($customerName).'</a>';
                            $item['customer_name'] = $html;
                        }
                    }
                }
            }
        }
        return $dataSource;
    }
}
