<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\Column\Cookie;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

/**
 * Class PageActions
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection
     */
    private $cookieGroup;
    /**
     * PageActions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection $cookieGroup
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection $cookieGroup,
        array $components = [],
        array $data = []
    ) {
        $this->cookieGroup = $cookieGroup;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["cookie_id"])) {
                    $id = $item["cookie_id"];
                }
                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "gdpr/cookie/edit",
                        ["cookie_id"=>$id]
                    ),
                    "label"=>__("Edit")
                ];
            }
        }
        return $dataSource;
    }
}
