<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\Column\Checkboxconsents;

/**
 * Class PageActions
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["checkbox_id"])) {
                    $id = $item["checkbox_id"];
                }
                if (isset($item['is_required'])) {
                    if ($item['is_required']) {
                        $item['is_required'] = 'Yes';
                    } else {
                        $item['is_required'] = 'No';
                    }
                }
                if (isset($item['is_hide'])) {
                    if ($item['is_hide']) {
                        $item['is_hide'] = 'Yes';
                    } else {
                        $item['is_hide'] = 'No';
                    }
                }
                if (isset($item['log_status'])) {
                    if ($item['log_status']) {
                        $item['log_status'] = 'Yes';
                    } else {
                        $item['log_status'] = 'No';
                    }
                }
                if (isset($item['is_enabled'])) {
                    if ($item['is_enabled']) {
                        $html = "<p style='background: #d0e5a9 none repeat scroll 0 0;font-weight: bold;line-height: 17px;padding: 0 3px;text-align: center;border: 1px solid #5b8116;color: #185b00;'>ENABLED</p>";
                        $item['is_enabled'] = $html;
                    } else {
                        $html = "<p style='background: #f9d4d4 none repeat scroll 0 0;font-weight: bold;line-height: 17px;padding: 0 3px;text-align: center;border: 1px solid #e22626;color: #e22626;'>DISABLED</p>";
                        $item['is_enabled'] = $html;
                    }
                }

                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "gdpr/checkboxconsents/edit",
                        ["checkbox_id"=>$id]
                    ),
                    "label"=>__("Edit")
                ];
            }
        }

        return $dataSource;
    }
}
