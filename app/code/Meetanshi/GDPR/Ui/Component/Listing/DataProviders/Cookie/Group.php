<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\DataProviders\Cookie;

/**
 * Class Group
 */
class Group extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Group constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Meetanshi\GDPR\Model\ResourceModel\CookieGroup\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
