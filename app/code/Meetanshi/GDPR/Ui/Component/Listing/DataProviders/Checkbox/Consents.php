<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\DataProviders\Checkbox;

/**
 * Class Consents
 */
class Consents extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Consents constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes\CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
