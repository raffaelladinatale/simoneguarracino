<?php
namespace Meetanshi\GDPR\Ui\Component\Listing\DataProviders\Action;

/**
 * Class Logs
 */
class Logs extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Logs constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Meetanshi\GDPR\Model\ResourceModel\ActionLogs\CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Meetanshi\GDPR\Model\ResourceModel\ActionLogs\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
