define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select'
], function ($, _, uiRegistry, select) {
    'use strict';
    return select.extend({

        initialize: function (){
            var status = this._super().initialValue;
            this.fieldDepend(status);
            return this;
        },


        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {

            this.fieldDepend(value);
            return this._super();
        },

        /**
         * Update field dependency
         *
         * @param {String} value
         */
        fieldDepend: function (value) {
            setTimeout(function () {
                var cms_page = uiRegistry.get('index = cms_page');
                var countries = uiRegistry.get('index = countries');

                if (value == 'cms_page') {
                    cms_page.show()
                } else if (value == 'gdpr_privacy_policy'){
                    cms_page.hide();
                }

                if (value == 'specific_countries') {
                    countries.show()
                }
                else if(value == 'all_countries') {
                    countries.hide()
                }
                else if(value == 'eea_countries') {
                    countries.hide()
                }

            }, 500);
            return this;
        }
    });
});define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select'
], function ($, _, uiRegistry, select) {
    'use strict';
    return select.extend({

        initialize: function (){
            var status = this._super().initialValue;
            this.fieldDepend(status);
            return this;
        },


        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {

            this.fieldDepend(value);
            return this._super();
        },

        /**
         * Update field dependency
         *
         * @param {String} value
         */
        fieldDepend: function (value) {
            setTimeout(function () {
                var cms_page = uiRegistry.get('index = cms_page');
                var countries = uiRegistry.get('index = countries');

                if (value == 'cms_page') {
                    cms_page.show()
                } else if (value == 'gdpr_privacy_policy'){
                    cms_page.hide();
                }

                if (value == 'specific_countries') {
                    countries.show()
                }
                else if(value == 'all_countries') {
                    countries.hide()
                }
                else if(value == 'eea_countries') {
                    countries.hide()
                }

            }, 500);
            return this;
        }
    });
});