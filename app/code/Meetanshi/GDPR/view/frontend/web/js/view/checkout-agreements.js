/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'ko',
    'jquery',
    'uiComponent',
], function (ko, $, Component) {
    'use strict';

    var checkoutConfig = window.checkoutConfig,
        consentsConfig = checkoutConfig ? checkoutConfig.checkoutConsents : {};

    return Component.extend({
        defaults: {
            template: 'Meetanshi_GDPR/checkout/checkout-agreements'
        },
        consents: consentsConfig.consents,
        isVisible: consentsConfig.is_enable,
        isRequired: consentsConfig.is_required,
        consentName: consentsConfig.checkbox_name,
        consentText: consentsConfig.checkbox_text

    });
});
