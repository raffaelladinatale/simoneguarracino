define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Meetanshi_GDPR/js/model/consent-validator'
], function (Component, additionalValidators, consentValidator) {
    'use strict';

    additionalValidators.registerValidator(consentValidator);
    return Component.extend({});
});
