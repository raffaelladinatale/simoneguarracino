/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/model/messageList',
    'Magento_Checkout/js/model/quote',
    'mage/url'
], function ($, $t, messageList, quote, url) {
    'use strict';


    var checkoutConfig = window.checkoutConfig,
        consentsConfig = checkoutConfig ? checkoutConfig.checkoutConsents.consents : {},
        agreementsInputPath = '.payment-method._active div.checkout-agreements input.mt-consent-required',
        allConsents = '.payment-method._active div.checkout-agreements input';

    return {
        /**
         * Validate checkout agreements
         *
         * @returns {Boolean}
         */


        validate: function (hideError) {

            var shippingAddress = quote.shippingAddress();
            var isValid = true;
            var isChecked = false;
            shippingAddress['extension_attributes'] = {};

            $(agreementsInputPath).each(function (index, element) {
                if (!$.validator.validateSingleElement(element, {
                        errorElement: 'div',
                        hideError: hideError || false
                    })) {
                    isValid = false;
                }
            });
            var checkboxes = {};
            var controller = url.build('gdpr/consent/checkoutconsententry')

            $(allConsents).each(function (index, element) {

                var checkbox = document.getElementById(element.id)

                if(checkbox.checked){
                    isChecked = true
                }
                else {
                    isChecked = false
                }
                checkboxes[element.id] = (isChecked) ? 1 : 0;

            });
            if(isValid) {
                jQuery.ajax({
                    url: controller,
                    type: 'POST',
                    data : {
                        'consents':checkboxes
                    }
                });
            }
            return isValid;
        }
    };
});
