define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Meetanshi_GDPR/Meetanshi_GDPR/cookie-popup'
            },
            initialize: function () {
                this._super();
            }
        });
    }
);