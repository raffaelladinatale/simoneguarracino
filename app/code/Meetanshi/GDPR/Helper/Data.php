<?php

namespace Meetanshi\GDPR\Helper;

use Magento\Customer\Model\Session;
use Magento\Config\Model\Config\Source\Locale\Country;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Store\Model\StoreManager;
use Meetanshi\GDPR\Model\ResourceModel\DeleteRequests\CollectionFactory;
use Magento\Store\Model\ScopeInterface;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy\CollectionFactory as PolicyCollectionFactory;
use Meetanshi\GDPR\Model\ResourceModel\Cookie\CollectionFactory as CookieCollectionFactory;
use Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection as CookieGroupCollection;
use Magento\Directory\Model\CountryFactory;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy\CollectionFactory as PolicyCollection;
use Magento\Framework\App\ResourceConnection;
use Meetanshi\GDPR\Model\ConsentLogs;
use Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes\Collection as ConsentCollection;

/**
 * Class Data
 * @package Meetanshi\GDPR\Helper
 */
class Data extends AbstractHelper
{
    /**#@+
     * Constants
     */
    const COUNTRY_CODE_PATH = 'general/country/default';

    /**
     * @var CollectionFactory
     */
    protected $deleteRequestCount;
    /**
     * @var
     */
    protected $policyContent;
    /**
     * @var CookieCollectionFactory
     */
    protected $cookieContent;
    /**
     * @var CookieGroupCollection
     */
    protected $cookieGroupContent;
    /**
     * @var
     */
    protected $orderCollection;
    /**
     * @var StoreManager
     */
    private $storeManager;
    /**
     * @var StateInterface
     */
    private $state;
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;
    /**
     * @var Country
     */
    private $countryCollection;
    /**
     * @var CountryFactory
     */
    private $countryFactory;
    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var PolicyCollection
     */
    private $policyCollection;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var ConsentLogs
     */
    private $consentLogs;
    /**
     * @var CheckboxConsents
     */
    private $checkboxConsents;

    /**
     * Data constructor.
     * @param Context $context
     * @param CollectionFactory $deleteRequestCount
     * @param PolicyCollectionFactory $policyContent
     * @param CookieCollectionFactory $cookieContent
     * @param CookieGroupCollection $cookieGroupContent
     * @param StoreManager $storeManager
     * @param StateInterface $state
     * @param TransportBuilder $transportBuilder
     * @param Country $countryCollection
     * @param CountryFactory $countryFactory
     * @param CookieManagerInterface $cookieManager
     * @param PolicyCollection $policyCollection
     * @param ResourceConnection $resourceConnection
     * @param ConsentLogs $consentLogs
     * @param ConsentCollection $checkboxConsents
     * @param Session $session
     * @param OrderCollection $orderCollection
     */
    public function __construct(
        Context $context,
        CollectionFactory $deleteRequestCount,
        PolicyCollectionFactory $policyContent,
        CookieCollectionFactory $cookieContent,
        CookieGroupCollection $cookieGroupContent,
        StoreManager $storeManager,
        StateInterface $state,
        TransportBuilder $transportBuilder,
        Country $countryCollection,
        CountryFactory $countryFactory,
        CookieManagerInterface $cookieManager,
        PolicyCollection $policyCollection,
        ResourceConnection $resourceConnection,
        ConsentLogs $consentLogs,
        ConsentCollection $checkboxConsents,
        Session $session,
        OrderCollection $orderCollection
    ) {
        parent::__construct($context);
        $this->deleteRequestCount = $deleteRequestCount;
        $this->policyContent = $policyContent->create();
        $this->cookieContent = $cookieContent;
        $this->cookieGroupContent = $cookieGroupContent;
        $this->storeManager = $storeManager;
        $this->state = $state;
        $this->transportBuilder = $transportBuilder;
        $this->countryCollection = $countryCollection;
        $this->countryFactory = $countryFactory;
        $this->cookieManager = $cookieManager;
        $this->policyCollection = $policyCollection;
        $this->session = $session;
        $this->resourceConnection = $resourceConnection;
        $this->consentLogs = $consentLogs;
        $this->checkboxConsents = $checkboxConsents;
        $this->orderCollection = $orderCollection;
    }

    /**
     * @param $sender
     * @return mixed
     */
    public function getStoreEmail($sender)
    {
        $path = "trans_email/ident_".$sender."/email";
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $sender
     * @return mixed
     */
    public function getStoreEmailSender($sender)
    {
        $path = "trans_email/ident_".$sender."/name";
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getCurrentStoreCountry()
    {
        return $this->scopeConfig->getValue(
            self::COUNTRY_CODE_PATH,
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * @return int
     */
    public function getDeleteRequestCount()
    {
        $collection = $this->deleteRequestCount->create();
        return count($collection);
    }

    /**
     * @return mixed
     */
    public function isGdprEnabled()
    {
        return $this->scopeConfig->getValue('mt_gdpr/configuration/enable_disable', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/general/enable_disable', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isDisplayPrivacyPopup()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/general/display_privacypopup', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isLogGuestConsents()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/general/log_guest_consents', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isLogAutoCleaning()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/general/log_autocleaning', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function logAutoCleaningPeriod()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/general/log_autocleaning_period', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieStyle()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/cookie_bar_style', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieLocation()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/cookie_bar_location', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isEnabledDpoInfo()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/display_dpo_info', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getDpoSection()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/section_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAllowDownloadPersonalData()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/allow_downloading', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAllowAnonymisePersonalData()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/allow_anonymizing', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAllowDownloadEmptyFields()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/allow_download_empty_fields', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAllowDeletePersonalData()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/allow_deleting', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAllowOptinGivenConsents()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/allow_opt_out', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getDpoDescription()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/privacy_settings/dpo_information_label', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAllowAnonyDeleteCustomer()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/personal_data_operation/prevent_data_deletion/prevent_data_deletion_of_recent_orders_statuses', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getOrderStatuses()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/personal_data_operation/prevent_data_deletion/order_statuses', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isAllowAutomateDeleteData()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/personal_data_operation/automatic_personal_data_deletion/automate_data_deletion', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAutomateDeleteDataPeriod()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/personal_data_operation/automatic_personal_data_deletion/automate_data_deletion_period', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPreventDataDeletion()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/personal_data_operation/prevent_data_deletion/prevent_data_deletion_of_recent_orders', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPreventDataDeletionPeriod()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/personal_data_operation/prevent_data_deletion/prevent_data_deletion_period', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isEnabledCookie()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/general/enable_disable', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function hideCookiebarNoDecision()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/general/hide_cookie_bar', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieCountryRestricted()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/general/countries_restricted', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSpecificCountryRestrict()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/general/specific_countries', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isGuestLogCookieConsent()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/general/log_guest_cookie_consents', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieLogCleaningPeriod()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/general/cookie_log_cleaning_period', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiebarStyle()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/cookie_bar_style', ScopeInterface::SCOPE_STORE);
    }

    // Sidebar cookie style

    /**
     * @return mixed
     */
    public function getCookieSidebarNotificationText()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/notification_text', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarPolicyTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/policy_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarLinkColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/links_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarGroupColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/cookie_group_title_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarGroupDescColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/cookie_group_desc_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAcceptBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/accept_button/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAcceptBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/accept_button/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAcceptBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/accept_button/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAcceptBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/accept_button/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAcceptBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/accept_button/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isCookieSidebarDeclineBtnEnabled()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/decline_button/decline_btn_enable', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarDeclineBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/decline_button/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarDeclineBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/decline_button/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarDeclineBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/decline_button/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarDeclineBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/decline_button/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarDeclineBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/decline_button/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAllowAllBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/allow_all_btn/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAllowAllBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/allow_all_btn/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAllowAllBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/allow_all_btn/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAllowAllBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/allow_all_btn/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieSidebarAllowAllBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/sidebar_cookie_style/allow_all_btn/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    // Classic cookie style

    /**
     * @return mixed
     */
    public function getCookieClassicbarLocation()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/cookie_bar_location', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarNotificationText()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/notification_text', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarPolicyTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/policy_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarLinkColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/links_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarGroupColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/cookie_group_title_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarGroupDescColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/cookie_group_desc_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarAcceptBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/accept_button/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarAcceptBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/accept_button/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarAcceptBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/accept_button/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarAcceptBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/accept_button/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarAcceptBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/accept_button/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isCookieSClassicbarDeclineBtnEnabled()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/decline_button/decline_btn_enable', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarDeclineBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/decline_button/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarDeclineBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/decline_button/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarDeclineBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/decline_button/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarDeclineBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/decline_button/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarDeclineBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/decline_button/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarCustomizeBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/custom_settings_buttons/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarCustomizeBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/custom_settings_buttons/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarCustomizeBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/custom_settings_buttons/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarCustomizeBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/custom_settings_buttons/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookieClassicbarCustomizeBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/classic_cookie_style/custom_settings_buttons/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    // Popup cookie style

    /**
     * @return mixed
     */
    public function getCookiePopupLocation()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/cookie_bar_location', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupNotificationText()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/notification_text', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupPolicyTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/policy_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupLinkColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/links_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupGroupColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/cookie_group_title_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupGroupDescColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/cookie_group_desc_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupAcceptBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/accept_button/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupAcceptBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/accept_button/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupAcceptBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/accept_button/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupAcceptBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/accept_button/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupAcceptBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/accept_button/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isCookieSPopupDeclineBtnEnabled()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/decline_button/decline_btn_enable', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupDeclineBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/decline_button/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupDeclineBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/decline_button/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupDeclineBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/decline_button/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupDeclineBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/decline_button/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupDeclineBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/decline_button/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupCustomizeBtnName()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/custom_settings_buttons/btn_name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupCustomizeBtnColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/custom_settings_buttons/btn_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupCustomizeBtnColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/custom_settings_buttons/btn_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupCustomizeBtnTextColor()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/custom_settings_buttons/btn_text_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePopupCustomizeBtnTextColorOnHover()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_cookie_consents_config/cookiebar_customization/popup_cookie_style/custom_settings_buttons/btn_text_color_on_hover', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return null
     */
    public function getPolicyInformation()
    {
        $policies = $this->policyContent->addFieldToFilter('status', 'Enable')->getData();
        foreach ($policies as $policy) {
            return $policy;
        }
        return null;
    }

    /**
     * @param $version
     * @return bool
     */
    public function isPolicyVerNew($version)
    {
        $isNew = false;
        $policyContent = $this->policyCollection;
        $policies = $policyContent->create()->getData();
        if ($policies!=null) {
            foreach ($policies as $policy) {
                if (version_compare($policy['version_number'], $version, '<')) {
                    $isNew = true;
                    break;
                }
            }
        }
        return $isNew;
    }

    /**
     * @return array
     */
    public function getCookieGroups()
    {
        $cookieGroup = $this->cookieGroupContent->addFieldToFilter('is_enabled', 1)->setOrder('sort_order', 'asc')->getData();
        return $cookieGroup;
    }

    /**
     * @param $customerId
     * @return array
     */
    public function getOrderDetails($customerId)
    {
        $resource = $this->resourceConnection;
        $connection = $resource->getConnection();
        $select = $connection->select()
            ->from($resource->getTableName('sales_order'))
            ->where('customer_id = '.$customerId);
        $pendingOrders = 0;
        $completeOrders = 0;
        foreach ($connection->query($select)->fetchAll() as $row) {
            if ($row['status'] == 'pending') {
                $pendingOrders ++;
            } elseif ($row['status'] == 'complete') {
                $completeOrders ++;
            }
        }

        $ordersCount = [
            'pending'=>$pendingOrders,
            'complete'=>$completeOrders
        ];

        return $ordersCount;
    }

    /**
     * @param $customerId
     * @return bool
     * @throws \Zend_Db_Statement_Exception
     */
    public function isAllowAnonyDeleteAccount($customerId)
    {
        if (!$this->getAllowAnonyDeleteCustomer()) {
            $statuses = explode(',', $this->getOrderStatuses());
            $orders = $this->orderCollection
                        ->addFieldToFilter('customer_id', $customerId)
                        ->addFieldToFilter('status', $statuses)->getData();
            if (sizeof($orders) > 0) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getAnonymizeNotificationSender()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/anonymization_notifications/email_sender', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAnonymizeNotificationReplyTo()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/anonymization_notifications/email_reply_to', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAnonymizeNotificationEmailTemplate()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/anonymization_notifications/email_templates', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isManagerNotifyOnDeletionRequest()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/manager_notification/notify_manager', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getManagerEmailOnDeletionRequest()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/manager_notification/email_sender', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getSenderEmailsForDeletionRequest()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/manager_notification/send_email_to', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getManagerNotificationEmailTemplates()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/manager_notification/email_templates', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCustomerApprovalNotificationEmailSender()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/customer_approval_notification/email_sender', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCustomerApprovalNotificationEmailReplyTo()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/customer_approval_notification/email_reply_to', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCustomerApprovalNotificationEmailTemplates()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/customer_approval_notification/email_templates', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCustomerDenyNotificationEmailSender()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/customer_deny_notification/email_sender', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCustomerDenyNotificationEmailReplyTo()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/customer_deny_notification/email_reply_to', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCustomerDenyNotificationEmailTemplates()
    {
        return $this->scopeConfig->getValue('mt_gdpr/mt_gdpr_pdp/email_notification/deletion_notifications/customer_deny_notification/email_templates', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return array
     */
    public function getAllCountries()
    {
        return $this->countryCollection->toOptionArray();
    }

    /**
     * @return array
     */
    public function getEEACountries()
    {
        $eeaCountries = [
            ['label'=>__('Austria'), 'value'=>'AT'],
            ['label'=>__('Belgium'), 'value'=>'BE'],
            ['label'=>__('Bulgaria'), 'value'=>'BG'],
            ['label'=>__('Croatia'), 'value'=>'HR'],
            ['label'=>__('Republic of Cyprus'), 'value'=>'CY'],
            ['label'=>__('Czech Republic'), 'value'=>'CZ'],
            ['label'=>__('Denmark'), 'value'=>'DK'],
            ['label'=>__('Estonia'), 'value'=>'EE'],
            ['label'=>__('Finland'), 'value'=>'FI'],
            ['label'=>__('France'), 'value'=>'FR'],
            ['label'=>__('Germany'), 'value'=>'DE'],
            ['label'=>__('Greece'), 'value'=>'GR'],
            ['label'=>__('Hungary'), 'value'=>'HU'],
            ['label'=>__('Ireland'), 'value'=>'IE'],
            ['label'=>__('Iceland'), 'value'=>'IS'],
            ['label'=>__('Italy'), 'value'=>'IT'],
            ['label'=>__('Latvia'), 'value'=>'LV'],
            ['label'=>__('Liechtenstein'), 'value'=>'LI'],
            ['label'=>__('Lithuania'), 'value'=>'LT'],
            ['label'=>__('Luxembourg'), 'value'=>'LU'],
            ['label'=>__('Malta'), 'value'=>'MT'],
            ['label'=>__('Netherlands'), 'value'=>'NL'],
            ['label'=>__('Norway'), 'value'=>'NO'],
            ['label'=>__('Poland'), 'value'=>'PL'],
            ['label'=>__('Portugal'), 'value'=>'PT'],
            ['label'=>__('Romania'), 'value'=>'RO'],
            ['label'=>__('Slovakia'), 'value'=>'SK'],
            ['label'=>__('Slovenia'), 'value'=>'SI'],
            ['label'=>__('Spain'), 'value'=>'ES'],
            ['label'=>__('Sweden'), 'value'=>'SE']
        ];
        return $eeaCountries;
    }

    /**
     * @param $sender
     * @return mixed
     */
    public function getSenderName($sender)
    {
        return $this->scopeConfig->getValue('trans_email/ident_'.$sender.'/name', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $sender
     * @return mixed
     */
    public function getSenderEmail($sender)
    {
        return $this->scopeConfig->getValue('trans_email/ident_'.$sender.'/email', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return array
     */
    public function getSpecificCounties()
    {
        $specificCounties = explode(',', $this->getCookieSpecificCountryRestrict());
        $counties = [];

        foreach ($specificCounties as $specificCounty) {

            if ($specificCounty != null) {
                $country = $this->countryFactory->create()->loadByCode($specificCounty);
                $counties[] = [
                    'label' => $country->getName(),
                    'value' => $specificCounty
                ];
            }
        }
        return $counties;
    }

    /**
     * @return bool
     */
    public function getIsAllowCookies()
    {
        $allCountries = $this->getAllCountries();
        $eeaCountries = $this->getEEACountries();
        $specificCountries = $this->getSpecificCounties();
        $countriesRestricted = $this->getCookieCountryRestricted();

        $currentStoreCountry = $this->getCurrentStoreCountry();

        $isAllowCookie = false;

        $countries = [];

        switch ($countriesRestricted) {
            case 'all_countries':
                $countries = $allCountries;
                break;
            case 'eea_countries':
                $countries = $eeaCountries;
                break;
            case 'specific_countries':
                $countries = $specificCountries;
                break;
        }

        if (!empty($countries) && is_array($countries)) {
            foreach ($countries as $country) {
                if (in_array($currentStoreCountry, $country)) {
                    $isAllowCookie = true;
                    break;
                }
            }
        }

        return $isAllowCookie;
    }

    /**
     * @return bool
     */
    public function isAllCookiesAccepted()
    {
        $cookies = $this->cookieContent->create()->getData();
        $cookieDetails = [];
        $isAcceptAllCookies = false;
        foreach ($cookies as $cookie) {
            if ($this->cookieManager->getCookie($cookie['cookie_name'])) {
                $cookieDetails[] = $cookie['cookie_name'];
            }
        }

        if (sizeof($cookieDetails) == sizeof($cookies)) {
            $isAcceptAllCookies = true;
        }
        return $isAcceptAllCookies;
    }

    /**
     * @return bool
     */
    public function isPrivacyPolicyAccepted()
    {
        if ($this->cookieManager->getCookie('privacy_popup')) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isCookieBarHide()
    {
        if ($this->cookieManager->getCookie('mt_is_hide_cookiebar')) {
            return true;
        }
        return false;
    }

    /**
     * @param $cookieGroup
     * @return mixed
     */
    public function getCookieByCookieGroup($cookieGroup)
    {
        $cookies = $this->cookieContent->create()->addFieldToFilter('cookie_group_name', $cookieGroup)->getData();
        return $cookies;
    }

    public function sendMail($template, $subject, $fromEmail, $fromName, $toEmail, $replyTo = null, $emailSendTo = null, $anonymizeEmail = null, $customerName = null)
    {
        $website = $this->storeManager->getWebsite()->getName();
        try {
            $templateVars = [
                'websiteName'=>$website,
                'subject'=>$subject,
                'customer_email'=>$toEmail,
                'customer_name'=>$customerName,
                'anonymizeEmail'=>$anonymizeEmail
            ];

            $storeId = $this->storeManager->getStore()->getId();

            $from = ['email'=>$fromEmail, 'name'=>$fromName];
            $this->state->suspend();

            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];

            $builder = $this->transportBuilder->setTemplateIdentifier($template, $storeScope)
                        ->setFrom($from)
                        ->setTemplateOptions($templateOptions)
                        ->setTemplateVars($templateVars);

            if ($replyTo != null) {
                $builder->setReplyTo($replyTo);
            }
            if ($emailSendTo != null) {
                $builder->addCc($emailSendTo);
            }
            $builder->addTo($toEmail);
            $transport = $builder->setTemplateIdentifier($template, $storeScope)->getTransport();
            $transport->sendMessage();
            $this->state->resume();
        } catch (\Exception $e) {

        }
    }

    /**
     * @return array
     */
    public function getConsents()
    {
        $consents = $this->checkboxConsents
            ->addFieldToSelect('*')
            ->setOrder('checkbox_position', 'asc')
            ->getData();
        return $consents;
    }

    /**
     * @param $countriesRestricted
     * @param $country
     * @return bool
     */
    public function getCheckboxCountries($countriesRestricted, $country)
    {
        if ($countriesRestricted == 'all_countries') {
            return true;
        } elseif ($countriesRestricted == 'eea_countries') {
            foreach ($this->getEEACountries() as $EEACountry) {
                if ($this->getCurrentStoreCountry() === $EEACountry['value']) {
                    return true;
                }
            }
        } elseif ($countriesRestricted == 'specific_countries') {
            $country = explode(',', $country);
            if (in_array($this->getCurrentStoreCountry(), $country)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getConsentUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/consent/consentlogentry');
    }

    /**
     * @param $code
     * @return bool
     */
    public function getCustomerConsents($code)
    {
        $id = '';
        if ($this->session->getCustomer()) {
            $id = $this->session->getCustomer()->getId();
        }
        $consentsCollection = $this->consentLogs;
        $collections = $consentsCollection
            ->getCollection()
            ->addFieldToFilter('checkbox_code', $code)
            ->addFieldToFilter('customer_id', $id);
        $last = $collections->getLastItem();
        $action = false;
        if ($last['action'] == 'Accepted') {
            $action = true;
        }
        return $action;
    }

    /**
     * @param $checkbox
     * @param $isConsentLinkType
     * @param null $page
     * @return mixed
     */
    public function getCheckboxLinks($checkbox, $isConsentLinkType, $page = null)
    {
        if ($isConsentLinkType === 'gdpr_privacy_policy') {
            $page = 'gdpr-privacy-policy';
        }
        $link = $this->_urlBuilder->getUrl($page);
        $checkbox = str_replace('{link}', $link, $checkbox);

        return $checkbox;
    }
}
