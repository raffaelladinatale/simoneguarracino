<?php

namespace Meetanshi\GDPR\Setup\Patch\Data;

use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class GdprPrivacyCMSPage
 */
class GdprPrivacyCMSPage implements DataPatchInterface
{
    /**
     * ModuleDataSetupInterface
     *
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param PageFactory $pageFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        PageFactory $pageFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->pageFactory = $pageFactory;
    }
    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $pageData = [
            'title' => 'GDPR Privacy Policy',
            'page_layout' => '1column',
            'meta_keywords' => 'GDPR Privacy Policy',
            'meta_description' => 'GDPR Privacy Policy',
            'identifier' => 'gdpr-privacy-policy',
            'content_heading' => 'GDPR Privacy Policy',
            'content' => '<div><p>GDPR Privacy Policy content goes here</p></div>',
            'layout_update_xml' => '',
            'url_key' => 'gdpr-privacy-policy',
            'is_active' => 1,
            'stores' => [0],
            'sort_order' => 0,
        ];
        $this->moduleDataSetup->startSetup();
        $this->pageFactory->create()->setData($pageData)->save();
        $this->moduleDataSetup->endSetup();
    }
    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }
    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
