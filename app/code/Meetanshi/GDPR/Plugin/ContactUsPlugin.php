<?php

namespace Meetanshi\GDPR\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ConsentLogs;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy\Collection;
use Magento\Customer\Model\ResourceModel\Customer\Collection as CustomerCollection;
use Psr\Log\LoggerInterface as Logger;

/**
 * Class ContactUsPlugin
 */
class ContactUsPlugin
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var RemoteAddress
     */
    protected $remote;
    /**
     * @var Collection
     */
    protected $privacyPolicy;
    /**
     * @var \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs
     */
    protected $objectRepository;
    /**
     * @var ConsentLogs
     */
    protected $model;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var CustomerCollection
     */
    protected $customer;
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * ContactUsPlugin constructor.
     * @param RequestInterface $request
     * @param RemoteAddress $remote
     * @param Collection $privacyPolicy
     * @param \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs $objectRepository
     * @param ConsentLogs $model
     * @param StoreManagerInterface $storeManager
     * @param CustomerCollection $customer
     * @param Data $helper
     * @param Logger $logger
     */
    public function __construct(
        RequestInterface $request,
        RemoteAddress $remote,
        Collection $privacyPolicy,
        \Meetanshi\GDPR\Model\ResourceModel\ConsentLogs $objectRepository,
        ConsentLogs $model,
        StoreManagerInterface $storeManager,
        CustomerCollection $customer,
        Data $helper,
        Logger $logger
    ) {
        $this->request = $request;
        $this->privacyPolicy = $privacyPolicy;
        $this->remote = $remote;
        $this->model = $model;
        $this->helper = $helper;
        $this->objectRepository  = $objectRepository;
        $this->storeManager = $storeManager;
        $this->customer = $customer;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Contact\Controller\Index\Post $subject
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeExecute(\Magento\Contact\Controller\Index\Post $subject)
    {
        if ($this->helper->isGdprEnabled()) {
            $params = $this->request->getParams();
            $isLog = [];
            $isHide = [];
            $version = '---';
            $allConsents = [];
            $policyDetails = [];
            $consents = [];
            if (isset($params['mt_consents_data'])) {
                $allConsents = $params['mt_consents_data'];
            }
            if (isset($params['mt_consents'])) {
                $consents = $params['mt_consents'];
            }
            if (isset($params['is_log'])) {
                $isLog = $params['is_log'];
            }
            if (isset($params['is_hide'])) {
                $isHide = $params['is_hide'];
            }
            if (isset($params['version_number'])) {
                if (is_array($this->helper->getPolicyInformation()) && $this->helper->getPolicyInformation() != null) {
                    $policyDetails = $this->helper->getPolicyInformation();
                    if ($policyDetails['version_number'] != null) {
                        $version = $policyDetails['version_number'];
                    }
                }
            }

            foreach ($allConsents as $key => $value) {
                if (isset($consents[$key])) {
                    $allConsents[$key] = ($consents[$key] == 'on') ? true : false;
                } else {
                    $allConsents[$key] = false;
                }
            }

            $id = null;
            $name = 'Guest';

            $customerData = $this->customer->addFieldToFilter('email', $params['email'])->getData();

            if (sizeof($customerData) && $customerData != null) {

                foreach ($customerData as $customerDatum) {
                    $id = $customerDatum['entity_id'];
                    $name = $params['name'];
                }
            }
            foreach ($allConsents as $key => $value) {
                $status = true;
                if (!$isHide[$key]) {
                    if (!$isLog[$key]) {
                        $status = false;
                    }
                }

                if ($status) {
                    $data = [
                        'concents_log_id' => null,
                        'checkbox_location' => 'Contact Us',
                        'checkbox_code' => $key,
                        'customer_name' => $name,
                        'customer_id' => $id,
                        'email' => $params['email'],
                        'website_id' => $this->storeManager->getStore()->getWebsiteId(),
                        'remote_ip' => $this->remote->getRemoteAddress(),
                        'login_date' => '',
                        'policy_version' => $version,
                        'action' => ($value) ? 'Accepted' : 'Rejected'
                    ];

                    if ($data) {
                        if (isset($data['concents_log_id'])) {
                            if (empty($data['concents_log_id'])) {
                                $data['concents_log_id'] = null;
                            }
                        }
                        if (isset($data['customer_id'])) {
                            if (empty($data['customer_id'])) {
                                $data['customer_id'] = null;
                            }
                        }
                        $this->model->setData($data);
                        try {
                            $this->objectRepository->save($this->model);
                        } catch (AlreadyExistsException $e) {
                            $this->logger->info(__($e->getMessage()));
                        } catch (\Exception $e) {
                            $this->logger->info(__($e->getMessage()));
                        }
                    }
                }
            }
        }
    }
}
