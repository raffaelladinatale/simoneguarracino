<?php

namespace Meetanshi\GDPR\Plugin\Checkout;

use Magento\Customer\Model\Session;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes\Collection;

/**
 * Class LayoutProcessor
 */
class LayoutProcessor
{
    /**
     * @var Collection
     */
    private $consents;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var Data
     */
    private $helper;

    /**
     * LayoutProcessor constructor.
     * @param Collection $consents
     * @param Session $session
     * @param Data $helper
     */
    public function __construct(
        Collection $consents,
        Session $session,
        Data $helper
    ) {
        $this->consents = $consents;
        $this->session = $session;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        if ($this->helper->isGdprEnabled()) {
            $consentsCollection = [];
            $consentsArray = [];
            $consentsCollections = $this->consents->getData();
            foreach ($consentsCollections as $consentsCollection) {
                $location = explode(',', $consentsCollection['checkbox_location']);
                $isAllowCountry = $this->helper->getCheckboxCountries($consentsCollection['countries_restricted'], $consentsCollection['countries']);
                if ($isAllowCountry) {
                    if (in_array('Checkout', $location)) {
                        $consentsArray =
                            [
                                'component' => 'Meetanshi_GDPR/js/view/checkout-agreements',
                                'dataScope' => $consentsCollection['checkbox_code'],
                                'displayArea' => 'before-place-order',
                                'provider' => 'checkoutProvider',
                                'visible' => true,
                                'sortOrder' => 200
                            ];
                    }
                }
            }

            if (isset($consentsCollection['checkbox_code'])) {
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['payments-list']['children']['before-place-order']['children'][$consentsCollection['checkbox_code']] = $consentsArray;

                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
                ['additional-payment-validators']['children']['consents-validator'] = ['component' => 'Meetanshi_GDPR/js/view/consents-validation'];
            }
        }
        return $jsLayout;
    }
}
