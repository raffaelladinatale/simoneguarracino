<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class CookieConsentLogs
 */
class CookieConsentLogs extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\Cookie\CookieConsentsInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_cookies_concents_logs';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\CookieConsentLogs::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed|void
     */
    public function setCookieConsentsLogId($CONSENTS_LOG_ID)
    {
        // TODO: Implement setCookieConsentsLogId() method.
        $this->setData(self::CONSENTS_LOG_ID, $CONSENTS_LOG_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed|void
     */
    public function setCookieConsentsLogCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement setCookieConsentsLogCustomerId() method.
        $this->setData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed|void
     */
    public function setCookieConsentsLogCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement setCookieConsentsLogCustomerName() method.
        $this->setData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $REMOTE_IP
     * @return mixed|void
     */
    public function setCookieConsentsLogRemoteIP($REMOTE_IP)
    {
        // TODO: Implement setCookieConsentsLogRemoteIP() method.
        $this->setData(self::REMOTE_IP, $REMOTE_IP);
    }

    /**
     * @param $EMAIL
     * @return mixed|void
     */
    public function setCookieConsentsLogEmail($EMAIL)
    {
        // TODO: Implement setCookieConsentsLogEmail() method.
        $this->setData(self::EMAIL, $EMAIL);
    }

    /**
     * @param $LOGIN_DATE
     * @return mixed|void
     */
    public function setCookieConsentsLogLogin($LOGIN_DATE)
    {
        // TODO: Implement setCookieConsentsLogLogin() method.
        $this->setData(self::LOGIN_DATE, $LOGIN_DATE);
    }

    /**
     * @param $WEBSITE_ID
     * @return mixed|void
     */
    public function setCookieConsentsLogWebsiteId($WEBSITE_ID)
    {
        // TODO: Implement setCookieConsentsLogWebsiteId() method.
        $this->setData(self::WEBSITE_ID, $WEBSITE_ID);
    }

    /**
     * @param $ACTION
     * @return mixed|void
     */
    public function setCookieConsentsLogAction($ACTION)
    {
        // TODO: Implement setCookieConsentsLogAction() method.
        $this->setData(self::ACTION, $ACTION);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setCookieConsentsLogCreatedAt($CREATED_AT)
    {
        // TODO: Implement setCookieConsentsLogCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setCookieConsentsLogUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setCookieConsentsLogUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed
     */
    public function getCookieConsentsLogId($CONSENTS_LOG_ID)
    {
        // TODO: Implement getCookieConsentsLogId() method.
        return $this->getData(self::CONSENTS_LOG_ID, $CONSENTS_LOG_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getCookieConsentsLogCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement getCookieConsentsLogCustomerId() method.
        return $this->getData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getCookieConsentsLogCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement getCookieConsentsLogCustomerName() method.
        return $this->getData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function getCookieConsentsLogRemoteIP($REMOTE_IP)
    {
        // TODO: Implement getCookieConsentsLogRemoteIP() method.
        return $this->getData(self::REMOTE_IP, $REMOTE_IP);
    }

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function getCookieConsentsLogEmail($EMAIL)
    {
        // TODO: Implement getCookieConsentsLogEmail() method.
        return $this->getData(self::EMAIL, $EMAIL);
    }

    /**
     * @param $LOGIN_DATE
     * @return mixed
     */
    public function getCookieConsentsLogLogin($LOGIN_DATE)
    {
        // TODO: Implement getCookieConsentsLogLogin() method.
        return $this->getData(self::LOGIN_DATE, $LOGIN_DATE);
    }

    /**
     * @param $WEBSITE_ID
     * @return mixed
     */
    public function getCookieConsentsLogWebsiteId($WEBSITE_ID)
    {
        // TODO: Implement getCookieConsentsLogWebsiteId() method.
        return $this->getData(self::WEBSITE_ID, $WEBSITE_ID);
    }

    /**
     * @param $ACTION
     * @return mixed
     */
    public function getCookieConsentsLogAction($ACTION)
    {
        // TODO: Implement getCookieConsentsLogAction() method.
        return $this->getData(self::ACTION, $ACTION);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCookieConsentsLogCreatedAt($CREATED_AT)
    {
        // TODO: Implement getCookieConsentsLogCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCookieConsentsLogUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement getCookieConsentsLogUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
