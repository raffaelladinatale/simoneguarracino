<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class DeleteRequests
 */
class DeleteRequests extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\PersonalDataProtection\DeleteRequestInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_deleterequests';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\DeleteRequests::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $DELETE_REQUEST_ID
     * @return mixed|void
     */
    public function setDeleteRequestId($DELETE_REQUEST_ID)
    {
        // TODO: Implement setDeleteRequestId() method.
        $this->setData(self::DELETE_REQUEST_ID, $DELETE_REQUEST_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed|void
     */
    public function setDeleteRequestCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement setDeleteRequestCustomerId() method.
        $this->setData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed|void
     */
    public function setDeleteRequestCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement setDeleteRequestCustomerName() method.
        $this->setData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $EMAIL
     * @return mixed|void
     */
    public function setDeleteRequestCustomerEmail($EMAIL)
    {
        // TODO: Implement setDeleteRequestCustomerEmail() method.
        $this->setData(self::EMAIL, $EMAIL);
    }

    /**
     * @param $INITIATOR_OF_DELETION
     * @return mixed|void
     */
    public function setDeleteRequestInitiator($INITIATOR_OF_DELETION)
    {
        // TODO: Implement setDeleteRequestInitiator() method.
        $this->setData(self::INITIATOR_OF_DELETION, $INITIATOR_OF_DELETION);
    }

    /**
     * @param $REQUEST_DATE
     * @return mixed|void
     */
    public function setDeleteRequestDate($REQUEST_DATE)
    {
        // TODO: Implement setDeleteRequestDate() method.
        $this->setData(self::REQUEST_DATE, $REQUEST_DATE);
    }

    /**
     * @param $PENDING_ORDERS
     * @return mixed|void
     */
    public function setDeleteRequestPendingOrders($PENDING_ORDERS)
    {
        // TODO: Implement setDeleteRequestPendingOrders() method.
        $this->setData(self::PENDING_ORDERS, $PENDING_ORDERS);
    }

    /**
     * @param $COMPLETED_ORDERS
     * @return mixed|void
     */
    public function setDeleteRequestCompltedOrders($COMPLETED_ORDERS)
    {
        // TODO: Implement setDeleteRequestCompltedOrders() method.
        $this->setData(self::COMPLETED_ORDERS, $COMPLETED_ORDERS);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setDeleteRequestCreatedAt($CREATED_AT)
    {
        // TODO: Implement setDeleteRequestCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setDeleteRequestUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setDeleteRequestUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $DELETE_REQUEST_ID
     * @return mixed
     */
    public function getDeleteRequestId($DELETE_REQUEST_ID)
    {
        // TODO: Implement getDeleteRequestId() method.
        return $this->getData(self::DELETE_REQUEST_ID, $DELETE_REQUEST_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getDeleteRequestCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement getDeleteRequestCustomerId() method.
        return $this->getData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getDeleteRequestCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement getDeleteRequestCustomerName() method.
        return $this->getData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function getDeleteRequestCustomerEmail($EMAIL)
    {
        // TODO: Implement getDeleteRequestCustomerEmail() method.
        return $this->getData(self::EMAIL, $EMAIL);
    }

    /**
     * @param $INITIATOR_OF_DELETION
     * @return mixed
     */
    public function getDeleteRequestInitiator($INITIATOR_OF_DELETION)
    {
        // TODO: Implement getDeleteRequestInitiator() method.
        return $this->getData(self::INITIATOR_OF_DELETION, $INITIATOR_OF_DELETION);
    }

    /**
     * @param $REQUEST_DATE
     * @return mixed
     */
    public function getDeleteRequestDate($REQUEST_DATE)
    {
        // TODO: Implement getDeleteRequestDate() method.
        return $this->getData(self::REQUEST_DATE, $REQUEST_DATE);
    }

    /**
     * @param $PENDING_ORDERS
     * @return mixed
     */
    public function getDeleteRequestPendingOrders($PENDING_ORDERS)
    {
        // TODO: Implement getDeleteRequestPendingOrders() method.
        return $this->getData(self::PENDING_ORDERS, $PENDING_ORDERS);
    }

    /**
     * @param $COMPLETED_ORDERS
     * @return mixed
     */
    public function getDeleteRequestCompltedOrders($COMPLETED_ORDERS)
    {
        // TODO: Implement getDeleteRequestCompltedOrders() method.
        return $this->getData(self::COMPLETED_ORDERS, $COMPLETED_ORDERS);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getDeleteRequestCreatedAt($CREATED_AT)
    {
        // TODO: Implement getDeleteRequestCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getDeleteRequestUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement getDeleteRequestUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
