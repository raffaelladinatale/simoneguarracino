<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class DeleteOrderStatuses
 */
class DeleteOrderStatuses implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'Delete', 'value' => 1 ],
            [ 'label'=>"Don't Delete", 'value' => 0 ]
        ];
    }
}
