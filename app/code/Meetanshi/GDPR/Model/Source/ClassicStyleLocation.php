<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 26-07-2021
 * Time: 11:28
 */

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class ClassicStyleLocation
 */
class ClassicStyleLocation implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'Above Footer', 'value' => 1 ],
            [ 'label'=>'Top', 'value' => 2 ]
        ];
    }
}
