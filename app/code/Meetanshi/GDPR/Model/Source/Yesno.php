<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Yesno
 */
class Yesno implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'Yes (recommended)', 'value' => 1 ],
            [ 'label'=>"No", 'value' => 0 ]
        ];
    }
}
