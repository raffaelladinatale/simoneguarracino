<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Meetanshi\GDPR\Model\ResourceModel\CookieGroup\Collection;

/**
 * Class CookieGroup
 */
class CookieGroup implements ArrayInterface
{
    /**
     * @var Collection
     */
    protected $cookieGroup;

    /**
     * CookieGroup constructor.
     * @param Collection $cookieGroup
     */
    public function __construct(
        Collection $cookieGroup
    ) {
        $this->cookieGroup = $cookieGroup;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $groups = [];
        $cookieGroups = $this->cookieGroup->getData();
        foreach ($cookieGroups as $cookieGroup) {
            $groups[$cookieGroup['cookie_group_code']] = $cookieGroup['cookie_group_name'];
        }
        $cookieGroupNames = [];
        foreach ($groups as $key => $value) {
            $cookieGroupNames[] = [
                'label' => $value,
                'value' => $key
            ];
        }
        return $cookieGroupNames;
    }
}
