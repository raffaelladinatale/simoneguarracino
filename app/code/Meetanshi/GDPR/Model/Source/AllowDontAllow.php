<?php
namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

class AllowDontAllow implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['label' => 'Allow', 'value' => 1],
            ['label' => "Don't Allow for Status(es)", 'value' => 0]
        ];
    }
}
