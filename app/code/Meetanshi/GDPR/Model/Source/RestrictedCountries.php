<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class RestrictedCountries
 */
class RestrictedCountries implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'All Countries', 'value' => 'all_countries' ],
            [ 'label'=>'EEA Countries', 'value' => 'eea_countries' ],
            [ 'label'=>'Specific Countries', 'value' => 'specific_countries' ]
        ];
    }
}
