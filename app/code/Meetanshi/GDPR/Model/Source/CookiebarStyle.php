<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class CookiebarStyle
 */
class CookiebarStyle implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'Sidebar', 'value' => 1 ],
            [ 'label'=>'Classic bar', 'value' => 2 ],
            [ 'label'=>'Popup', 'value' => 3 ]
        ];
    }
}
