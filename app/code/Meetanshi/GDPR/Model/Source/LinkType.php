<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class LinkType
 */
class LinkType implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'GDPR Privacy policy', 'value' => 'gdpr_privacy_policy' ],
            [ 'label'=>'CMS Page', 'value' => 'cms_page' ]
        ];
    }
}
