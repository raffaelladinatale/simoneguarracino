<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class PrivacyPolicyStatus
 */
class PrivacyPolicyStatus implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'Draft', 'value' => 'Draft' ],
            [ 'label'=>'Disable', 'value' => 'Disable' ],
            [ 'label'=>'Enable', 'value' => 'Enable' ]
        ];
    }
}
