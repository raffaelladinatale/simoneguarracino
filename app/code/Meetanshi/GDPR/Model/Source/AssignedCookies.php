<?php
namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Meetanshi\GDPR\Model\ResourceModel\Cookie\Collection;

/**
 * Class AssignedCookies
 */
class AssignedCookies implements ArrayInterface
{
    /**
     * @var Collection
     */
    protected $cookie;

    /**
     * AssignedCookies constructor.
     * @param Collection $cookie
     */
    public function __construct(
        Collection $cookie
    ) {
        $this->cookie = $cookie;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $groups = [];
        $cookies = $this->cookie->getData();

        foreach ($cookies as $cookie) {
            $groups[] = $cookie['cookie_name'];
        }
        $cookieNames = [];
        foreach ($groups as $key => $value) {
            $cookieNames[] = [
                'label' => $value,
                'value' => $value
            ];
        }

        return $cookieNames;
    }
}
