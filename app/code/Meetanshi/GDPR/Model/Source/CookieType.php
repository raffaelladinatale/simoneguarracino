<?php

namespace Meetanshi\GDPR\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class CookieType
 */
class CookieType implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [ 'label'=>'1st Party', 'value' => '1st Party' ],
            [ 'label'=>'3rd Party', 'value' => '3rd Party' ]
        ];
    }
}
