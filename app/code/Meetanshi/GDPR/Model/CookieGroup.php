<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class CookieGroup
 */
class CookieGroup extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\Cookie\CookieGroupsInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_cookiegroups';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\CookieGroup::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $COOKIE_GROUP_ID
     * @return mixed|void
     */
    public function setCookieGroupId($COOKIE_GROUP_ID)
    {
        // TODO: Implement setCookieGroupId() method.
        $this->setData(self::COOKIE_GROUP_ID, $COOKIE_GROUP_ID);
    }

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed|void
     */
    public function setCookieGroupGroupname($COOKIE_GROUP_NAME)
    {
        // TODO: Implement setCookieGroupGroupname() method.
        $this->setData(self::COOKIE_GROUP_NAME, $COOKIE_GROUP_NAME);
    }

    /**
     * @param $COOKIE_GROUP_DESC
     * @return mixed|void
     */
    public function setCookieGroupDesc($COOKIE_GROUP_DESC)
    {
        // TODO: Implement setCookieGroupDesc() method.
        $this->setData(self::COOKIE_GROUP_DESC, $COOKIE_GROUP_DESC);
    }

    /**
     * @param $COOKIE_GROUP_ENABLED
     * @return mixed|void
     */
    public function setCookieGroupIsEnabled($COOKIE_GROUP_ENABLED)
    {
        // TODO: Implement setCookieGroupIsEnabled() method.
        $this->setData(self::COOKIE_GROUP_ENABLED, $COOKIE_GROUP_ENABLED);
    }

    /**
     * @param $COOKIE_GROUP_ESSENTIAL
     * @return mixed|void
     */
    public function setCookieGroupIsEssential($COOKIE_GROUP_ESSENTIAL)
    {
        // TODO: Implement setCookieGroupIsEssential() method.
        $this->setData(self::COOKIE_GROUP_ESSENTIAL, $COOKIE_GROUP_ESSENTIAL);
    }

    /**
     * @param $ASSIGNED_COOKIES
     * @return mixed|void
     */
    public function setCookieGroupAssignedCookies($ASSIGNED_COOKIES)
    {
        // TODO: Implement setCookieGroupAssignedCookies() method.
        $this->setData(self::ASSIGNED_COOKIES, $ASSIGNED_COOKIES);
    }

    /**
     * @param $SORT_ORDER
     * @return mixed|void
     */
    public function setCookieGroupSortOrder($SORT_ORDER)
    {
        // TODO: Implement setCookieGroupSortOrder() method.
        $this->setData(self::SORT_ORDER, $SORT_ORDER);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setCookieGroupCreatedAt($CREATED_AT)
    {
        // TODO: Implement setCookieGroupCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setCookieGroupUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setCookieGroupUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $COOKIE_GROUP_ID
     * @return mixed
     */
    public function getCookieGroupId($COOKIE_GROUP_ID)
    {
        // TODO: Implement getCookieGroupId() method.
        return $this->getData(self::COOKIE_GROUP_ID, $COOKIE_GROUP_ID);
    }

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed
     */
    public function getCookieGroupGroupname($COOKIE_GROUP_NAME)
    {
        // TODO: Implement getCookieGroupGroupname() method.
        return $this->getData(self::COOKIE_GROUP_NAME, $COOKIE_GROUP_NAME);
    }

    /**
     * @param $COOKIE_GROUP_DESC
     * @return mixed
     */
    public function getCookieGroupDesc($COOKIE_GROUP_DESC)
    {
        // TODO: Implement getCookieGroupDesc() method.
        return $this->getData(self::COOKIE_GROUP_DESC, $COOKIE_GROUP_DESC);
    }

    /**
     * @param $COOKIE_GROUP_ENABLED
     * @return mixed
     */
    public function getCookieGroupIsEnabled($COOKIE_GROUP_ENABLED)
    {
        // TODO: Implement getCookieGroupIsEnabled() method.
        return $this->getData(self::COOKIE_GROUP_ENABLED, $COOKIE_GROUP_ENABLED);
    }

    /**
     * @param $COOKIE_GROUP_ESSENTIAL
     * @return mixed
     */
    public function getCookieGroupIsEssential($COOKIE_GROUP_ESSENTIAL)
    {
        // TODO: Implement getCookieGroupIsEssential() method.
        return $this->getData(self::COOKIE_GROUP_ESSENTIAL, $COOKIE_GROUP_ESSENTIAL);
    }

    /**
     * @param $ASSIGNED_COOKIES
     * @return mixed
     */
    public function getCookieGroupAssignedCookies($ASSIGNED_COOKIES)
    {
        // TODO: Implement getCookieGroupAssignedCookies() method.
        return $this->getData(self::ASSIGNED_COOKIES, $ASSIGNED_COOKIES);
    }

    /**
     * @param $SORT_ORDER
     * @return mixed
     */
    public function getCookieGroupSortOrder($SORT_ORDER)
    {
        // TODO: Implement getCookieGroupSortOrder() method.
        return $this->getData(self::SORT_ORDER, $SORT_ORDER);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCookieGroupCreatedAt($CREATED_AT)
    {
        // TODO: Implement getCookieGroupCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCookieGroupUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement getCookieGroupUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
