<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class ConcentsCheckboxes
 */
class ConcentsCheckboxes extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\PersonalDataProtection\ConcentsCheckboxesInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_concentscheckboxes';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $CHECKBOX_ID
     * @return mixed|void
     */
    public function setCheckboxId($CHECKBOX_ID)
    {
        // TODO: Implement setCheckboxId() method.
        $this->setData(self::CHECKBOX_ID, $CHECKBOX_ID);
    }

    /**
     * @param $CHECKBOX_NAME
     * @return mixed|void
     */
    public function setCheckboxName($CHECKBOX_NAME)
    {
        // TODO: Implement setCheckboxName() method.
        $this->setData(self::CHECKBOX_NAME, $CHECKBOX_NAME);
    }

    /**
     * @param $CHECKBOX_CODE
     * @return mixed|void
     */
    public function setCheckboxCode($CHECKBOX_CODE)
    {
        // TODO: Implement setCheckboxCode() method.
        $this->setData(self::CHECKBOX_CODE, $CHECKBOX_CODE);
    }

    /**
     * @param $IS_ENABLED
     * @return mixed|void
     */
    public function setCheckboxIsEnabled($IS_ENABLED)
    {
        // TODO: Implement setCheckboxIsEnabled() method.
        $this->setData(self::IS_ENABLED, $IS_ENABLED);
    }

    /**
     * @param $IS_REQUIRED
     * @return mixed|void
     */
    public function setCheckboxIsRequired($IS_REQUIRED)
    {
        // TODO: Implement setCheckboxIsRequired() method.
        $this->setData(self::IS_REQUIRED, $IS_REQUIRED);
    }

    /**
     * @param $IS_HIDE
     * @return mixed|void
     */
    public function setCheckboxIsHide($IS_HIDE)
    {
        // TODO: Implement setCheckboxIsHide() method.
        $this->setData(self::IS_HIDE, $IS_HIDE);
    }

    /**
     * @param $LOG_STATUS
     * @return mixed|void
     */
    public function setCheckboxLogStatus($LOG_STATUS)
    {
        // TODO: Implement setCheckboxLogStatus() method.
        $this->setData(self::LOG_STATUS, $LOG_STATUS);
    }

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed|void
     */
    public function setCheckboxLocation($CHECKBOX_LOCATION)
    {
        // TODO: Implement setCheckboxLocation() method.
        $this->setData(self::CHECKBOX_LOCATION, $CHECKBOX_LOCATION);
    }

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed|void
     */
    public function setCheckboxPosition($CHECKBOX_POSITION)
    {
        // TODO: Implement setCheckboxPosition() method.
        $this->setData(self::CHECKBOX_POSITION, $CHECKBOX_POSITION);
    }

    /**
     * @param $CHECKBOX_TEXT
     * @return mixed|void
     */
    public function setCheckboxText($CHECKBOX_TEXT)
    {
        // TODO: Implement setCheckboxText() method.
        $this->setData(self::CHECKBOX_TEXT, $CHECKBOX_TEXT);
    }

    /**
     * @param $CONSENTS_LINK_TYPE
     * @return mixed|void
     */
    public function setCheckboxLinkType($CONSENTS_LINK_TYPE)
    {
        // TODO: Implement setCheckboxLinkType() method.
        $this->setData(self::CONSENTS_LINK_TYPE, $CONSENTS_LINK_TYPE);
    }

    /**
     * @param $CMS_PAGE
     * @return mixed|void
     */
    public function setCheckboxCmsPage($CMS_PAGE)
    {
        // TODO: Implement setCheckboxCmsPage() method.
        $this->setData(self::CMS_PAGE, $CMS_PAGE);
    }

    /**
     * @param $COUNTRIES_RESTRICTED
     * @return mixed|void
     */
    public function setCheckboxCountriesRestricted($COUNTRIES_RESTRICTED)
    {
        // TODO: Implement setCheckboxCountriesRestricted() method.
        $this->setData(self::COUNTRIES_RESTRICTED, $COUNTRIES_RESTRICTED);
    }

    /**
     * @param $COUNTRIES
     * @return mixed|void
     */
    public function setCheckboxCountries($COUNTRIES)
    {
        // TODO: Implement setCheckboxCountries() method.
        $this->setData(self::COUNTRIES, $COUNTRIES);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setCheckboxCreatedAt($CREATED_AT)
    {
        // TODO: Implement setCheckboxCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setCheckboxUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setCheckboxUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $CHECKBOX_ID
     * @return mixed
     */
    public function getCheckboxId($CHECKBOX_ID)
    {
        // TODO: Implement getCheckboxId() method.
        return $this->getData(self::CHECKBOX_ID, $CHECKBOX_ID);
    }

    /**
     * @param $CHECKBOX_NAME
     * @return mixed
     */
    public function getCheckboxName($CHECKBOX_NAME)
    {
        // TODO: Implement getCheckboxName() method.
        return $this->getData(self::CHECKBOX_NAME, $CHECKBOX_NAME);
    }

    /**
     * @param $CHECKBOX_CODE
     * @return mixed
     */
    public function getCheckboxCode($CHECKBOX_CODE)
    {
        // TODO: Implement getCheckboxCode() method.
        return $this->getData(self::CHECKBOX_CODE, $CHECKBOX_CODE);
    }

    /**
     * @param $IS_ENABLED
     * @return mixed
     */
    public function getCheckboxIsEnabled($IS_ENABLED)
    {
        // TODO: Implement getCheckboxIsEnabled() method.
        return $this->getData(self::IS_ENABLED, $IS_ENABLED);
    }

    /**
     * @param $IS_REQUIRED
     * @return mixed
     */
    public function getCheckboxIsRequired($IS_REQUIRED)
    {
        // TODO: Implement getCheckboxIsRequired() method.
        return $this->getData(self::IS_REQUIRED, $IS_REQUIRED);
    }

    /**
     * @param $IS_HIDE
     * @return mixed
     */
    public function getCheckboxIsHide($IS_HIDE)
    {
        // TODO: Implement getCheckboxIsHide() method.
        return $this->getData(self::IS_HIDE, $IS_HIDE);
    }

    /**
     * @param $LOG_STATUS
     * @return mixed
     */
    public function getCheckboxLogStatus($LOG_STATUS)
    {
        // TODO: Implement getCheckboxLogStatus() method.
        return $this->getData(self::LOG_STATUS, $LOG_STATUS);
    }

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed
     */
    public function getCheckboxLocation($CHECKBOX_LOCATION)
    {
        // TODO: Implement getCheckboxLocation() method.
        return $this->getData(self::CHECKBOX_LOCATION, $CHECKBOX_LOCATION);
    }

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed
     */
    public function getCheckboxPosition($CHECKBOX_POSITION)
    {
        // TODO: Implement getCheckboxPosition() method.
        return $this->getData(self::CHECKBOX_POSITION, $CHECKBOX_POSITION);
    }

    /**
     * @param $CHECKBOX_TEXT
     * @return mixed
     */
    public function getCheckboxText($CHECKBOX_TEXT)
    {
        // TODO: Implement getCheckboxText() method.
        return $this->getData(self::CHECKBOX_TEXT, $CHECKBOX_TEXT);
    }

    /**
     * @param $CONSENTS_LINK_TYPE
     * @return mixed
     */
    public function getCheckboxLinkType($CONSENTS_LINK_TYPE)
    {
        // TODO: Implement getCheckboxLinkType() method.
        return $this->getData(self::CONSENTS_LINK_TYPE, $CONSENTS_LINK_TYPE);
    }

    /**
     * @param $CMS_PAGE
     * @return mixed
     */
    public function getCheckboxCmsPage($CMS_PAGE)
    {
        // TODO: Implement getCheckboxCmsPage() method.
        return $this->getData(self::CMS_PAGE, $CMS_PAGE);
    }

    /**
     * @param $COUNTRIES_RESTRICTED
     * @return mixed
     */
    public function getCheckboxCountriesRestricted($COUNTRIES_RESTRICTED)
    {
        // TODO: Implement getCheckboxCountriesRestricted() method.
        return $this->getData(self::COUNTRIES_RESTRICTED, $COUNTRIES_RESTRICTED);
    }

    /**
     * @param $COUNTRIES
     * @return mixed
     */
    public function getCheckboxCountries($COUNTRIES)
    {
        // TODO: Implement getCheckboxCountries() method.
        return $this->getData(self::COUNTRIES, $COUNTRIES);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCheckboxCreatedAt($CREATED_AT)
    {
        // TODO: Implement getCheckboxCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCheckboxUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement getCheckboxUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
