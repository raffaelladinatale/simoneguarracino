<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class Cookie
 */
class Cookie extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\Cookie\CookiesInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_cookie';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\Cookie::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $COOKIE_ID
     * @return mixed|void
     */
    public function setCookieId($COOKIE_ID)
    {
        // TODO: Implement setCookieId() method.
        $this->setData(self::COOKIE_ID, $COOKIE_ID);
    }

    /**
     * @param $IS_ENABLED
     * @return mixed|void
     */
    public function setCookieIsEnabled($IS_ENABLED)
    {
        // TODO: Implement setCookieIsEnabled() method.
        $this->setData(self::IS_ENABLED, $IS_ENABLED);
    }

    /**
     * @param $COOKIE_NAME
     * @return mixed|void
     */
    public function setCookieName($COOKIE_NAME)
    {
        // TODO: Implement setCookieName() method.
        $this->setData(self::COOKIE_NAME, $COOKIE_NAME);
    }

    /**
     * @param $COOKIE_PROVIDER
     * @return mixed|void
     */
    public function setCookieProvider($COOKIE_PROVIDER)
    {
        // TODO: Implement setCookieProvider() method.
        $this->setData(self::COOKIE_PROVIDER, $COOKIE_PROVIDER);
    }

    /**
     * @param $COOKIE_DESC
     * @return mixed|void
     */
    public function setCookieDesc($COOKIE_DESC)
    {
        // TODO: Implement setCookieDesc() method.
        $this->setData(self::COOKIE_DESC, $COOKIE_DESC);
    }

    /**
     * @param $COOKIE_LIFETIME
     * @return mixed|void
     */
    public function setCookieLifetime($COOKIE_LIFETIME)
    {
        // TODO: Implement setCookieLifetime() method.
        $this->setData(self::COOKIE_LIFETIME, $COOKIE_LIFETIME);
    }

    /**
     * @param $COOKIE_TYPE
     * @return mixed|void
     */
    public function setCookieType($COOKIE_TYPE)
    {
        // TODO: Implement setCookieType() method.
        $this->setData(self::COOKIE_TYPE, $COOKIE_TYPE);
    }

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed|void
     */
    public function setCookieGroupname($COOKIE_GROUP_NAME)
    {
        // TODO: Implement setCookieGroupname() method.
        $this->setData(self::COOKIE_GROUP_NAME, $COOKIE_GROUP_NAME);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setCookieCreatedAt($CREATED_AT)
    {
        // TODO: Implement setCookieCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setCookieUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setCookieUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $COOKIE_ID
     * @return mixed
     */
    public function getCookieId($COOKIE_ID)
    {
        // TODO: Implement getCookieId() method.
        return $this->getData(self::COOKIE_ID, $COOKIE_ID);
    }

    /**
     * @param $IS_ENABLED
     * @return mixed
     */
    public function getCookieIsEnabled($IS_ENABLED)
    {
        // TODO: Implement getCookieIsEnabled() method.
        return $this->getData(self::IS_ENABLED, $IS_ENABLED);
    }

    /**
     * @param $COOKIE_NAME
     * @return mixed
     */
    public function getCookieName($COOKIE_NAME)
    {
        // TODO: Implement getCookieName() method.
        return $this->getData(self::COOKIE_NAME, $COOKIE_NAME);
    }

    /**
     * @param $COOKIE_PROVIDER
     * @return mixed
     */
    public function getCookieProvider($COOKIE_PROVIDER)
    {
        // TODO: Implement getCookieProvider() method.
        return $this->getData(self::COOKIE_PROVIDER, $COOKIE_PROVIDER);
    }

    /**
     * @param $COOKIE_DESC
     * @return mixed
     */
    public function getCookieDesc($COOKIE_DESC)
    {
        // TODO: Implement getCookieDesc() method.
        return $this->getData(self::COOKIE_DESC, $COOKIE_DESC);
    }

    /**
     * @param $COOKIE_LIFETIME
     * @return mixed
     */
    public function getCookieLifetime($COOKIE_LIFETIME)
    {
        // TODO: Implement getCookieLifetime() method.
        return $this->getData(self::COOKIE_LIFETIME, $COOKIE_LIFETIME);
    }

    /**
     * @param $COOKIE_TYPE
     * @return mixed
     */
    public function getCookieType($COOKIE_TYPE)
    {
        // TODO: Implement getCookieType() method.
        return $this->getData(self::COOKIE_TYPE, $COOKIE_TYPE);
    }

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed
     */
    public function getCookieGroupname($COOKIE_GROUP_NAME)
    {
        // TODO: Implement getCookieGroupname() method.
        return $this->getData(self::COOKIE_GROUP_NAME, $COOKIE_GROUP_NAME);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCookieCreatedAt($CREATED_AT)
    {
        // TODO: Implement getCookieCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCookieUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement getCookieUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
