<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class ConcentsCheckboxes
 */
class PrivacyPolicy extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_privacypolicy', 'policy_id');
    }
}
