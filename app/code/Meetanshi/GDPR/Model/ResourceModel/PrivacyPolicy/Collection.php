<?php
namespace Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'policy_id';
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(
            \Meetanshi\GDPR\Model\PrivacyPolicy::class,
            \Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy::class
        );
    }
}
