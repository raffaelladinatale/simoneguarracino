<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class ConcentsCheckboxes
 */
class DeleteRequests extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_deleterequests', 'request_id');
    }
}
