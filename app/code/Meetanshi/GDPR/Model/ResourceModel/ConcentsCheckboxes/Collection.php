<?php
namespace Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'checkbox_id';
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(
            \Meetanshi\GDPR\Model\ConcentsCheckboxes::class,
            \Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes::class
        );
    }
}
