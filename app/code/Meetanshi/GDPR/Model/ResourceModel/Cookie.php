<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class Cookie
 */
class Cookie extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_cookies', 'cookie_id');
    }
}
