<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class ConcentsCheckboxes
 */
class ActionLogs extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_actionlogs', 'action_log_id');
    }
}
