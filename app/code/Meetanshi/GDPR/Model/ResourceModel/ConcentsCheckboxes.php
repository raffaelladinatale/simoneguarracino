<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class ConcentsCheckboxes
 */
class ConcentsCheckboxes extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_concentscheckboxes', 'checkbox_id');
    }
}
