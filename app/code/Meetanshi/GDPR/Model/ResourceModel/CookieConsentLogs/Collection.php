<?php
namespace Meetanshi\GDPR\Model\ResourceModel\CookieConsentLogs;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'concents_log_id';
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(
            \Meetanshi\GDPR\Model\CookieConsentLogs::class,
            \Meetanshi\GDPR\Model\ResourceModel\CookieConsentLogs::class
        );
    }
}
