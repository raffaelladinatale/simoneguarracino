<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class ConcentsCheckboxes
 */
class ConsentLogs extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_concentslogs', 'concents_log_id');
    }
}
