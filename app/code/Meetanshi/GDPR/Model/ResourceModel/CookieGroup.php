<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class CookieGroup
 */
class CookieGroup extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_cookiegroups', 'cookie_group_id');
    }
}
