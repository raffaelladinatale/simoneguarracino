<?php
namespace Meetanshi\GDPR\Model\ResourceModel;

/**
 * Class CookieConsentLogs
 */
class CookieConsentLogs extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('meetanshi_gdpr_cookies_concents_logs', 'concents_log_id');
    }
}
