<?php
namespace Meetanshi\GDPR\Model\ResourceModel\Cookie;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'cookie_id';
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(
            \Meetanshi\GDPR\Model\Cookie::class,
            \Meetanshi\GDPR\Model\ResourceModel\Cookie::class
        );
    }
}
