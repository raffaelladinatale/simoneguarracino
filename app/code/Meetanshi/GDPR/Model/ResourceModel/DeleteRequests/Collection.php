<?php
namespace Meetanshi\GDPR\Model\ResourceModel\DeleteRequests;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'request_id';
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(
            \Meetanshi\GDPR\Model\DeleteRequests::class,
            \Meetanshi\GDPR\Model\ResourceModel\DeleteRequests::class
        );
    }
}
