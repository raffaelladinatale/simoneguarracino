<?php
namespace Meetanshi\GDPR\Model\ResourceModel\CookieGroup;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'cookie_group_id';
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(
            \Meetanshi\GDPR\Model\CookieGroup::class,
            \Meetanshi\GDPR\Model\ResourceModel\CookieGroup::class
        );
    }
}
