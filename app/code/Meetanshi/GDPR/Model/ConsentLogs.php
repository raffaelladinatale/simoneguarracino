<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class ConsentLogs
 */
class ConsentLogs extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\PersonalDataProtection\CosentsLogInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_concentslogs';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\ConsentLogs::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed|void
     */
    public function setConsentsLogId($CONSENTS_LOG_ID)
    {
        // TODO: Implement setConsentsLogId() method.
        $this->setData(self::CONSENTS_LOG_ID, $CONSENTS_LOG_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed|void
     */
    public function setConsentsLogCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement setConsentsLogCustomerId() method.
        $this->setData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed|void
     */
    public function setConsentsLogCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement setConsentsLogCustomerName() method.
        $this->setData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $REMOTE_IP
     * @return mixed|void
     */
    public function setConsentsLogRemoteIP($REMOTE_IP)
    {
        // TODO: Implement setConsentsLogRemoteIP() method.
        $this->setData(self::REMOTE_IP, $REMOTE_IP);
    }

    /**
     * @param $EMAIL
     * @return mixed|void
     */
    public function setConsentsLogEmail($EMAIL)
    {
        // TODO: Implement setConsentsLogEmail() method.
        $this->setData(self::EMAIL, $EMAIL);
    }

    /**
     * @param $LOGIN_DATE
     * @return mixed|void
     */
    public function setConsentsLogLogin($LOGIN_DATE)
    {
        // TODO: Implement setConsentsLogLogin() method.
        $this->setData(self::LOGIN_DATE, $LOGIN_DATE);
    }

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed|void
     */
    public function setConsentsLogCheckboxLocation($CHECKBOX_LOCATION)
    {
        // TODO: Implement setConsentsLogCheckboxLocation() method.
        $this->setData(self::CHECKBOX_LOCATION, $CHECKBOX_LOCATION);
    }

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed|void
     */
    public function setConsentsLogCheckboxPosition($CHECKBOX_POSITION)
    {
        // TODO: Implement setConsentsLogCheckboxPosition() method.
        $this->setData(self::CHECKBOX_POSITION, $CHECKBOX_POSITION);
    }

    /**
     * @param $VERSION_NO
     * @return mixed|void
     */
    public function setConsentsLogVersionNo($VERSION_NO)
    {
        // TODO: Implement setConsentsLogVersionNo() method.
        $this->setData(self::VERSION_NO, $VERSION_NO);
    }

    /**
     * @param $WEBSITE_ID
     * @return mixed|void
     */
    public function setConsentsLogWebsiteId($WEBSITE_ID)
    {
        // TODO: Implement setConsentsLogWebsiteId() method.
        $this->setData(self::WEBSITE_ID, $WEBSITE_ID);
    }

    /**
     * @param $ACTION
     * @return mixed|void
     */
    public function setConsentsLogAction($ACTION)
    {
        // TODO: Implement setConsentsLogAction() method.
        $this->setData(self::ACTION, $ACTION);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setConsentsLogCreatedAt($CREATED_AT)
    {
        // TODO: Implement setConsentsLogCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setConsentsLogUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setConsentsLogUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed
     */
    public function getConsentsLogId($CONSENTS_LOG_ID)
    {
        // TODO: Implement getConsentsLogId() method.
        return $this->getData(self::CONSENTS_LOG_ID, $CONSENTS_LOG_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getConsentsLogCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement getConsentsLogCustomerId() method.
        return $this->getData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getConsentsLogCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement getConsentsLogCustomerName() method.
        return $this->getData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function getConsentsLogRemoteIP($REMOTE_IP)
    {
        // TODO: Implement getConsentsLogRemoteIP() method.
        return $this->getData(self::REMOTE_IP, $REMOTE_IP);
    }

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function getConsentsLogEmail($EMAIL)
    {
        // TODO: Implement getConsentsLogEmail() method.
        return $this->getData(self::EMAIL, $EMAIL);
    }

    /**
     * @param $LOGIN_DATE
     * @return mixed
     */
    public function getConsentsLogLogin($LOGIN_DATE)
    {
        // TODO: Implement getConsentsLogLogin() method.
        return $this->getData(self::LOGIN_DATE, $LOGIN_DATE);
    }

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed
     */
    public function getConsentsLogCheckboxLocation($CHECKBOX_LOCATION)
    {
        // TODO: Implement getConsentsLogCheckboxLocation() method.
        return $this->getData(self::CHECKBOX_LOCATION, $CHECKBOX_LOCATION);
    }

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed
     */
    public function getConsentsLogCheckboxPosition($CHECKBOX_POSITION)
    {
        // TODO: Implement getConsentsLogCheckboxPosition() method.
        return $this->getData(self::CHECKBOX_POSITION, $CHECKBOX_POSITION);
    }

    /**
     * @param $VERSION_NO
     * @return mixed
     */
    public function getConsentsLogVersionNo($VERSION_NO)
    {
        // TODO: Implement getConsentsLogVersionNo() method.
        return $this->getData(self::VERSION_NO, $VERSION_NO);
    }

    /**
     * @param $WEBSITE_ID
     * @return mixed
     */
    public function getConsentsLogWebsiteId($WEBSITE_ID)
    {
        // TODO: Implement getConsentsLogWebsiteId() method.
        return $this->getData(self::WEBSITE_ID, $WEBSITE_ID);
    }

    /**
     * @param $ACTION
     * @return mixed
     */
    public function getConsentsLogAction($ACTION)
    {
        // TODO: Implement getConsentsLogAction() method.
        return $this->getData(self::ACTION, $ACTION);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getConsentsLogCreatedAt($CREATED_AT)
    {
        // TODO: Implement getConsentsLogCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getConsentsLogUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement getConsentsLogUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
