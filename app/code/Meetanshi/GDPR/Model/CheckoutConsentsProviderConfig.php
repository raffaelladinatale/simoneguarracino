<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Meetanshi\GDPR\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes\Collection;

/**
 * Class CheckoutConsentsProviderConfig
 */
class CheckoutConsentsProviderConfig implements ConfigProviderInterface
{
    /**
     * @var Collection
     */
    protected $consents;
    /**
     * @var Data
     */
    private $helper;

    /**
     * CheckoutConsentsProviderConfig constructor.
     * @param Collection $consents
     * @param Data $helper
     */
    public function __construct(
        Collection $consents,
        Data $helper
    ) {
        $this->consents = $consents;
        $this->helper = $helper;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        // TODO: Implement getConfig() method.

        $consents = [];
        $consents['checkoutConsents'] = $this->getCheckoutConsents();
        return $consents;
    }

    /**
     * @return array
     */
    private function getCheckoutConsents()
    {
        $consentsConfig = [];
        $consents = $this->consents->getData();
        foreach ($consents as $consent) {
            $location = explode(',', $consent['checkbox_location']);
            if ($consent['is_enabled']) {
                if (in_array('Checkout', $location)) {
                    $consent['checkbox_text'] = $this->helper->getCheckboxLinks($consent['checkbox_text'], $consent['consents_link_type'], $consent['cms_page']);
                    $consentsConfig['consents'][] = [
                        'consent' => $consent['checkbox_code'],
                        'is_enabled' => $consent['is_enabled'],
                        'is_required' => $consent['is_required'],
                        'checkbox_name' => $consent['checkbox_name'],
                        'checkbox_text' => $consent['checkbox_text'],
                        'countries' => $consent['countries'],
                    ];
                }
            }
        }

        return $consentsConfig;
    }
}
