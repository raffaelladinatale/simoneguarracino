<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class PrivacyPolicy
 */
class PrivacyPolicy extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\PersonalDataProtection\PrivacyPolicyInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_privacypolicy';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $POLICY_ID
     * @return mixed|void
     */
    public function setPrivacyPolicyId($POLICY_ID)
    {
        // TODO: Implement setPrivacyPolicyId() method.
        $this->setData(self::POLICY_ID, $POLICY_ID);
    }

    /**
     * @param $VERSION_NO
     * @return mixed|void
     */
    public function setPrivacyPolicyVersionNo($VERSION_NO)
    {
        // TODO: Implement setPrivacyPolicyVersionNo() method.
        $this->setData(self::VERSION_NO, $VERSION_NO);
    }

    /**
     * @param $EDITED_BY
     * @return mixed|void
     */
    public function setPrivacyPolicyEditedBy($EDITED_BY)
    {
        // TODO: Implement setPrivacyPolicyEditedBy() method.
        $this->setData(self::EDITED_BY, $EDITED_BY);
    }

    /**
     * @param $STATUS
     * @return mixed|void
     */
    public function setPrivacyPolicyStatus($STATUS)
    {
        // TODO: Implement setPrivacyPolicyStatus() method.
        $this->setData(self::STATUS, $STATUS);
    }

    /**
     * @param $POLICY_DESC
     * @return mixed|void
     */
    public function setPrivacyPolicyDesc($POLICY_DESC)
    {
        // TODO: Implement setPrivacyPolicyDesc() method.
        $this->setData(self::POLICY_DESC, $POLICY_DESC);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setPrivacyPolicyCreatedAt($CREATED_AT)
    {
        // TODO: Implement setPrivacyPolicyCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setPrivacyPolicyUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setPrivacyPolicyUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $POLICY_ID
     * @return mixed
     */
    public function getPrivacyPolicyId($POLICY_ID)
    {
        // TODO: Implement getPrivacyPolicyId() method.
        return $this->getData(self::POLICY_ID, $POLICY_ID);
    }

    /**
     * @param $VERSION_NO
     * @return mixed
     */
    public function getPrivacyPolicyVersionNo($VERSION_NO)
    {
        // TODO: Implement getPrivacyPolicyVersionNo() method.
        return $this->getData(self::VERSION_NO, $VERSION_NO);
    }

    /**
     * @param $EDITED_BY
     * @return mixed
     */
    public function getPrivacyPolicyEditedBy($EDITED_BY)
    {
        // TODO: Implement getPrivacyPolicyEditedBy() method.
        return $this->getData(self::EDITED_BY, $EDITED_BY);
    }

    /**
     * @param $STATUS
     * @return mixed
     */
    public function getPrivacyPolicyStatus($STATUS)
    {
        // TODO: Implement getPrivacyPolicyStatus() method.
        return $this->getData(self::STATUS, $STATUS);
    }

    /**
     * @param $POLICY_DESC
     * @return mixed
     */
    public function getPrivacyPolicyDesc($POLICY_DESC)
    {
        // TODO: Implement getPrivacyPolicyDesc() method.
        return $this->getData(self::POLICY_DESC, $POLICY_DESC);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getPrivacyPolicyCreatedAt($CREATED_AT)
    {
        // TODO: Implement getPrivacyPolicyCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getPrivacyPolicyUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement getPrivacyPolicyUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
