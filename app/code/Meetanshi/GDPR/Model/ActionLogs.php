<?php
namespace Meetanshi\GDPR\Model;

/**
 * Class ActionLogs
 */
class ActionLogs extends \Magento\Framework\Model\AbstractModel implements
    \Meetanshi\GDPR\Api\Data\PersonalDataProtection\ActionLogInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    /**#@+
     * Constants
     */
    const CACHE_TAG = 'meetanshi_gdpr_actionlogs';

    protected function _construct()
    {
        $this->_init(\Meetanshi\GDPR\Model\ResourceModel\ActionLogs::class);
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $ACTION_LOG_ID
     * @return mixed|void
     */
    public function setActionLogId($ACTION_LOG_ID)
    {
        // TODO: Implement setActionLogId() method.
        $this->setData(self::ACTION_LOG_ID, $ACTION_LOG_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed|void
     */
    public function setActionLogCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement setActionLogCustomerId() method.
        $this->setData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed|void
     */
    public function setActionLogCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement setActionLogCustomerName() method.
        $this->setData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $REMOTE_IP
     * @return mixed|void
     */
    public function setActionLogRemoteIP($REMOTE_IP)
    {
        // TODO: Implement setActionLogRemoteIP() method.
        $this->setData(self::REMOTE_IP, $REMOTE_IP);
    }

    /**
     * @param $ACTION_DATE
     * @return mixed|void
     */
    public function setActionLogActionDate($ACTION_DATE)
    {
        // TODO: Implement setActionLogActionDate() method.
        $this->setData(self::ACTION_DATE, $ACTION_DATE);
    }

    /**
     * @param $ACTION
     * @return mixed|void
     */
    public function setActionLogAction($ACTION)
    {
        // TODO: Implement setActionLogAction() method.
        $this->setData(self::ACTION, $ACTION);
    }

    /**
     * @param $COMMENT
     * @return mixed|void
     */
    public function setActionLogComment($COMMENT)
    {
        // TODO: Implement setActionLogComment() method.
        $this->setData(self::COMMENT, $COMMENT);
    }

    /**
     * @param $CREATED_AT
     * @return mixed|void
     */
    public function setActionLogCreatedAt($CREATED_AT)
    {
        // TODO: Implement setActionLogCreatedAt() method.
        $this->setData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed|void
     */
    public function setActionLogUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setActionLogUpdatedAt() method.
        $this->setData(self::UPDATED_AT, $UPDATED_AT);
    }

    /**
     * @param $ACTION_LOG_ID
     * @return mixed
     */
    public function getActionLogId($ACTION_LOG_ID)
    {
        // TODO: Implement setActionLogId() method.
        return $this->getData(self::ACTION_LOG_ID, $ACTION_LOG_ID);
    }

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getActionLogCustomerId($CUSTOMER_ID)
    {
        // TODO: Implement setActionLogCustomerId() method.
        return $this->getData(self::CUSTOMER_ID, $CUSTOMER_ID);
    }

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getActionLogCustomerName($CUSTOMER_NAME)
    {
        // TODO: Implement setActionLogCustomerName() method.
        return $this->getData(self::CUSTOMER_NAME, $CUSTOMER_NAME);
    }

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function getActionLogRemoteIP($REMOTE_IP)
    {
        // TODO: Implement setActionLogRemoteIP() method.
        return $this->getData(self::REMOTE_IP, $REMOTE_IP);
    }

    /**
     * @param $ACTION_DATE
     * @return mixed
     */
    public function getActionLogActionDate($ACTION_DATE)
    {
        // TODO: Implement setActionLogActionDate() method.
        return $this->getData(self::ACTION_DATE, $ACTION_DATE);
    }

    /**
     * @param $ACTION
     * @return mixed
     */
    public function getActionLogAction($ACTION)
    {
        // TODO: Implement setActionLogAction() method.
        return $this->getData(self::ACTION, $ACTION);
    }

    /**
     * @param $COMMENT
     * @return mixed
     */
    public function getActionLogComment($COMMENT)
    {
        // TODO: Implement setActionLogComment() method.
        return $this->getData(self::COMMENT, $COMMENT);
    }

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getActionLogCreatedAt($CREATED_AT)
    {
        // TODO: Implement setActionLogCreatedAt() method.
        return $this->getData(self::CREATED_AT, $CREATED_AT);
    }

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getActionLogUpdatedAt($UPDATED_AT)
    {
        // TODO: Implement setActionLogUpdatedAt() method.
        return $this->getData(self::UPDATED_AT, $UPDATED_AT);
    }
}
