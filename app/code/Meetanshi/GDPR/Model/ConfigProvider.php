<?php

namespace Meetanshi\GDPR\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

/**
 * Class ConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    /**
     * @var array
     */
    private $configProviders;

    /**
     * ConfigProvider constructor.
     * @param array $configProviders
     */
    public function __construct(array $configProviders)
    {
        $this->configProviders = $configProviders;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $config = [];
        foreach ($this->configProviders as $configProvider) {
            $config = array_merge_recursive($config, $configProvider->getConfig());
        }
        return $config;
    }
}
