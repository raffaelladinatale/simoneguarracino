<?php
namespace Meetanshi\GDPR\Block\PrivacySettings;

use Magento\Customer\Model\Session;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Meetanshi\GDPR\Helper\Data;
use Meetanshi\GDPR\Model\ResourceModel\ConcentsCheckboxes\Collection;
use Meetanshi\GDPR\Model\ResourceModel\ConsentLogs\Collection as ConsentsCollection;
use Magento\Framework\App\ResourceConnection;
use Magento\Cms\Model\Template\FilterProvider;

/**
 * Class PolicySettings
 */
class PolicySettings extends \Magento\Framework\View\Element\Template
{
    /**
     * @var UrlInterface
     */
    protected $url;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var RemoteAddress
     */
    protected $remote;
    /**
     * @var Collection
     */
    protected $checkboxes;
    /**
     * @var ConsentsCollection
     */
    protected $consentCollection;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var FilterProvider
     */
    private $filterProvider;

    /**
     * PolicySettings constructor.
     * @param Template\Context $context
     * @param Session $session
     * @param RemoteAddress $remote
     * @param Collection $checkboxes
     * @param ConsentsCollection $consentCollection
     * @param UrlInterface $url
     * @param ResourceConnection $resourceConnection
     * @param FilterProvider $filterProvider
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Session $session,
        RemoteAddress $remote,
        Collection $checkboxes,
        ConsentsCollection $consentCollection,
        UrlInterface $url,
        ResourceConnection $resourceConnection,
        FilterProvider $filterProvider,
        Data $helper,
        array $data = []
    ) {
        $this->url = $url;
        $this->session = $session;
        $this->checkboxes = $checkboxes;
        $this->consentCollection = $consentCollection;
        $this->remote = $remote;
        $this->resourceConnection = $resourceConnection;
        $this->helper = $helper;
        $this->filterProvider = $filterProvider;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->url->getUrl('gdpr/consent/consentlogentry');
    }

    /**
     * @return string
     */
    public function getCookieConsentUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/consent/cookieconsentslogentry', ['params'=>$this->getRequest()]);
    }

    /**
     * @return string
     */
    public function getDeleteRequestUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/deleterequests/request');
    }

    /**
     * @return string
     */
    public function getDownloadRequestUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/downloadrequest/request');
    }

    /**
     * @return string
     */
    public function getAnonymizationUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/anonymization/request');
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerDetails()
    {
        $customer = [];
        if ($this->session->isLoggedIn()) {
            $customer['name'] = $this->session->getCustomer()->getName();
            $customer['id'] = $this->session->getCustomer()->getId();
            $customer['website_id'] = $this->session->getCustomer()->getWebsiteId();
            $customer['email'] = $this->session->getCustomer()->getEmail();
            $customer['ip_address'] = $this->remote->getRemoteAddress();
            $customer['logged_in_date'] = $this->getCustomerLoginTimeStamp($this->session->getCustomer()->getId());
        } else {
            $customer['name'] = 'Guest';
            $customer['id'] = "Not Available";
            $customer['website_id'] = $this->_storeManager->getStore()->getWebsiteId();
            $customer['email'] = "Not Available";
            $customer['ip_address'] = $this->remote->getRemoteAddress();
            $customer['logged_in_date'] = null;
        }
        return $customer;
    }

    /**
     * @param $checkbox
     * @param $isConsentLinkType
     * @param null $page
     * @return mixed
     */
    public function getCheckboxLinks($checkbox, $isConsentLinkType, $page = null)
    {
        if ($isConsentLinkType === 'gdpr_privacy_policy') {
            $page = 'gdpr-privacy-policy';
        }
        $link = $this->_urlBuilder->getUrl($page);
        $checkbox = str_replace('{link}', $link, $checkbox);

        return $checkbox;
    }

    /**
     * @return array
     */
    public function getCheckboxConsents()
    {
        $consents = $this->checkboxes
                        ->addFieldToSelect('*')
                        ->setOrder('checkbox_position', 'asc')
                        ->getData();
        return $consents;
    }

    /**
     * @param $code
     * @return int
     */
    public function isConsentChecked($code)
    {
        $status = 0;
        $collection = $this->consentCollection
                        ->addFieldToFilter('checkbox_code', $code)
                        ->getData();
        if (sizeof($collection)) {
            foreach ($collection as $item) {
                $status = $item['action'];
            }
        }

        return $status;
    }

    /**
     * @return Data
     */
    public function getGdprHelper()
    {
        return $this->helper;
    }

    /**
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return parent::getRequest(); // TODO: Change the autogenerated stub
    }

    /**
     * @param $id
     * @return null
     */
    private function getCustomerLoginTimeStamp($id)
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from($this->resourceConnection->getTableName('customer_log'), 'last_login_at')
            ->where('customer_id = '.$id);
        $lastLogin = null;
        try {
            foreach ($connection->query($select)->fetchAll() as $row) {
                $lastLogin = $row['last_login_at'];
            }
        } catch (\Zend_Db_Statement_Exception $e) {
            $this->_logger->info($e->getMessage());
        }
        return $lastLogin;
    }

    /**
     * @param string $value
     * @return string
     */
    public function getCmsFilterContent($value = '')
    {
        $html = '';
        try {
            $html = $this->filterProvider->getPageFilter()->filter($value);
        } catch (\Exception $e) {
            $this->_logger->info($e->getMessage());
        }
        return $html;
    }
}
