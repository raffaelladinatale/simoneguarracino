<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 26-07-2021
 * Time: 10:01
 */

namespace Meetanshi\GDPR\Block\Adminhtml\System\Config;

/**
 * Class MultiSelect
 */
class MultiSelect extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $script = " <script>
                    require([
                        'jquery',
                        'chosen'
                    ], function ($, chosen) {
                        $('#" . $element->getId() . "').chosen({
                            width: '100%',
                            placeholder_text: '" . __('Select Options') . "'
                        });
                        $('#" . $element->getId() . "_inherit').change(function() {
                            $('#" . $element->getId() . "').prop('disabled', $(this).is(':checked'));
                              $('#" . $element->getId() . "').trigger('chosen:updated');
                        });
                    })
                </script>";
        return parent::_getElementHtml($element) . $script;
    }
}
