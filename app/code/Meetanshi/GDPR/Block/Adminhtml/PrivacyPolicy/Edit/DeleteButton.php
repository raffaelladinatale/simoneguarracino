<?php
namespace Meetanshi\GDPR\Block\Adminhtml\PrivacyPolicy\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Framework\App\RequestInterface;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy\Collection;

/**
 * Class DeleteButton
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var Collection
     */
    protected $privacyPolicy;

    /**
     * DeleteButton constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param Collection $privacyPolicy
     * @param RequestInterface $request
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        Collection $privacyPolicy,
        RequestInterface $request
    ) {
        $this->request = $request;
        $this->privacyPolicy = $privacyPolicy;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $id = $this->request->getParam('policy_id');
        $policy = $this->privacyPolicy->addFieldToFilter('policy_id', $id)->getData();

        foreach ($policy as $p) {
            if (array_key_exists('status', $p)) {
                if ($p['status'] == 'Enable') {
                    return [];
                }
            }
        }

        if (!$this->getObjectId()) {
            return [];
        }
        return [
                'label' => __('Delete Object'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm( \'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
    }
}
