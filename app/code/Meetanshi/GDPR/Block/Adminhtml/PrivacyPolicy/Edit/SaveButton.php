<?php
namespace Meetanshi\GDPR\Block\Adminhtml\PrivacyPolicy\Edit;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy\Collection;

/**
 * Class SaveButton
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var Collection
     */
    protected $privacyPolicy;

    /**
     * SaveButton constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param Collection $privacyPolicy
     * @param RequestInterface $request
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        Collection $privacyPolicy,
        RequestInterface $request
    ) {
        $this->request = $request;
        $this->privacyPolicy = $privacyPolicy;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $id = $this->request->getParam('policy_id');
        $policy = $this->privacyPolicy->addFieldToFilter('policy_id', $id)->getData();

        foreach ($policy as $p) {
            if (array_key_exists('status', $p)) {
                if ($p['status'] == 'Enable') {
                    return [];
                }
            }
        }

        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
