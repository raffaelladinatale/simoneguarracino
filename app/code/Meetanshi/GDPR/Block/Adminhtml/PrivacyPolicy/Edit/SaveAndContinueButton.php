<?php
namespace Meetanshi\GDPR\Block\Adminhtml\PrivacyPolicy\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Meetanshi\GDPR\Model\ResourceModel\PrivacyPolicy\Collection;
use Magento\Framework\App\RequestInterface;

/**
 * Class SaveAndContinueButton
 */
class SaveAndContinueButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var Collection
     */
    protected $privacyPolicy;

    /**
     * SaveAndContinueButton constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param Collection $privacyPolicy
     * @param RequestInterface $request
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        Collection $privacyPolicy,
        RequestInterface $request
    ) {
        $this->request = $request;
        $this->privacyPolicy = $privacyPolicy;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $id = $this->request->getParam('policy_id');

        $policy = $this->privacyPolicy->addFieldToFilter('policy_id', $id)->getData();

        foreach ($policy as $p) {
            if (array_key_exists('status', $p)) {
                if ($p['status'] == 'Enable') {
                    return [];
                }
            }
        }
        return [
            'label' => __('Save and Continue Edit'),
            'class' => 'save',
            'on_click' => sprintf("location.href = '%s';", $this->getSaveContinueUrl()),
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit'],
                ],
            ],
            'sort_order' => 80,
        ];
    }
}
