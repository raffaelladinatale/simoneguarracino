<?php
namespace Meetanshi\GDPR\Block\Adminhtml\PrivacyPolicy\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 */
class CloneButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Clone'),
            'class' => 'save primary',
            'on_click' => sprintf("location.href = '%s';", $this->getCloneUrl()),
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'clone']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
