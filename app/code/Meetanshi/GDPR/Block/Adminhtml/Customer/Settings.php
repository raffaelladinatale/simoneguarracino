<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 19-10-2021
 * Time: 18:45
 */

namespace Meetanshi\GDPR\Block\Adminhtml\Customer;

use Magento\Backend\Block\Template;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Customer\Model\Session;

/**
 * Class Settings
 */
class Settings extends Template
{
    /**
     * @var Session
     */
    private $session;
    /**
     * @var RemoteAddress
     */
    private $remote;

    /**
     * Settings constructor.
     * @param Template\Context $context
     * @param Session $session
     * @param RemoteAddress $remote
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Session $session,
        RemoteAddress $remote,
        array $data = []
    ) {
        $this->session = $session;
        $this->remote = $remote;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getAnonymizationUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/customerdata/anonymize', ['id'=>$this->getCurrentCustomerId()]);
    }

    /**
     * @return string
     */
    public function getDeleteRequestUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/customerdata/delete', ['id'=>$this->getCurrentCustomerId()]);
    }

    /**
     * @return string
     */
    public function getDownloadRequestUrl()
    {
        return $this->_urlBuilder->getUrl('gdpr/customerdata/download', ['id'=>$this->getCurrentCustomerId()]);
    }

    /**
     * @return mixed
     */
    public function getCurrentCustomerId()
    {
        return $this->_request->getParam('id');
    }
}
