<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\PersonalDataProtection\ConcentsCheckboxesInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ConcentsCheckboxesRepositoryInterface
 */
interface ConcentsCheckboxesRepositoryInterface
{
    /**
     * @param ConcentsCheckboxesInterface $page
     * @return mixed
     */
    public function save(ConcentsCheckboxesInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param ConcentsCheckboxesInterface $page
     * @return mixed
     */
    public function delete(ConcentsCheckboxesInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
