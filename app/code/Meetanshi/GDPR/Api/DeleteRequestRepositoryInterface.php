<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\PersonalDataProtection\DeleteRequestInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface DeleteRequestRepositoryInterface
 */
interface DeleteRequestRepositoryInterface
{
    /**
     * @param DeleteRequestInterface $page
     * @return mixed
     */
    public function save(DeleteRequestInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param DeleteRequestInterface $page
     * @return mixed
     */
    public function delete(DeleteRequestInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
