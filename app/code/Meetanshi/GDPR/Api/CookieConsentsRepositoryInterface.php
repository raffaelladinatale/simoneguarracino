<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\Cookie\CookieConsentsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface CookieConsentsRepositoryInterface
 */
interface CookieConsentsRepositoryInterface
{
    /**
     * @param CookieConsentsInterface $page
     * @return mixed
     */
    public function save(CookieConsentsInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param CookieConsentsInterface $page
     * @return mixed
     */
    public function delete(CookieConsentsInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
