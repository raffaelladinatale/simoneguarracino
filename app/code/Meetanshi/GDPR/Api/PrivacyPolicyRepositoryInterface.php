<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\PersonalDataProtection\PrivacyPolicyInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface PrivacyPolicyRepositoryInterface
 */
interface PrivacyPolicyRepositoryInterface
{
    /**
     * @param PrivacyPolicyInterface $page
     * @return mixed
     */
    public function save(PrivacyPolicyInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param PrivacyPolicyInterface $page
     * @return mixed
     */
    public function delete(PrivacyPolicyInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
