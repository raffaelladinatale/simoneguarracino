<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 10-11-2021
 * Time: 14:48
 */

namespace Meetanshi\GDPR\Api\Data\PersonalDataProtection;

/**
 * Interface DeleteRequestInterface
 */
interface DeleteRequestInterface
{
    /**#@+
     * Constants
     */
    const DELETE_REQUEST_ID  = 'request_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_NAME = 'customer_name';
    const EMAIL = 'email';
    const INITIATOR_OF_DELETION = 'intiator_of_deletion';
    const REQUEST_DATE = 'request_date';
    const COMPLETED_ORDERS = 'completed_orders';
    const PENDING_ORDERS = 'pending_orders';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $DELETE_REQUEST_ID
     * @return mixed
     */
    public function setDeleteRequestId($DELETE_REQUEST_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function setDeleteRequestCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function setDeleteRequestCustomerName($CUSTOMER_NAME);

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function setDeleteRequestCustomerEmail($EMAIL);

    /**
     * @param $INITIATOR_OF_DELETION
     * @return mixed
     */
    public function setDeleteRequestInitiator($INITIATOR_OF_DELETION);

    /**
     * @param $REQUEST_DATE
     * @return mixed
     */
    public function setDeleteRequestDate($REQUEST_DATE);

    /**
     * @param $PENDING_ORDERS
     * @return mixed
     */
    public function setDeleteRequestPendingOrders($PENDING_ORDERS);

    /**
     * @param $COMPLETED_ORDERS
     * @return mixed
     */
    public function setDeleteRequestCompltedOrders($COMPLETED_ORDERS);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setDeleteRequestCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setDeleteRequestUpdatedAt($UPDATED_AT);

    /**
     * @param $DELETE_REQUEST_ID
     * @return mixed
     */
    public function getDeleteRequestId($DELETE_REQUEST_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getDeleteRequestCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getDeleteRequestCustomerName($CUSTOMER_NAME);

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function getDeleteRequestCustomerEmail($EMAIL);

    /**
     * @param $INITIATOR_OF_DELETION
     * @return mixed
     */
    public function getDeleteRequestInitiator($INITIATOR_OF_DELETION);

    /**
     * @param $REQUEST_DATE
     * @return mixed
     */
    public function getDeleteRequestDate($REQUEST_DATE);

    /**
     * @param $PENDING_ORDERS
     * @return mixed
     */
    public function getDeleteRequestPendingOrders($PENDING_ORDERS);

    /**
     * @param $COMPLETED_ORDERS
     * @return mixed
     */
    public function getDeleteRequestCompltedOrders($COMPLETED_ORDERS);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getDeleteRequestCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getDeleteRequestUpdatedAt($UPDATED_AT);
}
