<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 01-09-2021
 * Time: 18:04
 */

namespace Meetanshi\GDPR\Api\Data\PersonalDataProtection;

/**
 * Interface PrivacyPolicyInterface
 */
interface PrivacyPolicyInterface
{
    /**#@+
     * Constants
     */
    const POLICY_ID  = 'policy_id';
    const VERSION_NO = 'version_number';
    const EDITED_BY = 'edited_by';
    const STATUS = 'status';
    const POLICY_DESC = 'policy_description';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $POLICY_ID
     * @return mixed
     */
    public function setPrivacyPolicyId($POLICY_ID);

    /**
     * @param $VERSION_NO
     * @return mixed
     */
    public function setPrivacyPolicyVersionNo($VERSION_NO);

    /**
     * @param $EDITED_BY
     * @return mixed
     */
    public function setPrivacyPolicyEditedBy($EDITED_BY);

    /**
     * @param $STATUS
     * @return mixed
     */
    public function setPrivacyPolicyStatus($STATUS);

    /**
     * @param $POLICY_DESC
     * @return mixed
     */
    public function setPrivacyPolicyDesc($POLICY_DESC);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setPrivacyPolicyCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setPrivacyPolicyUpdatedAt($UPDATED_AT);

    /**
     * @param $POLICY_ID
     * @return mixed
     */
    public function getPrivacyPolicyId($POLICY_ID);

    /**
     * @param $VERSION_NO
     * @return mixed
     */
    public function getPrivacyPolicyVersionNo($VERSION_NO);

    /**
     * @param $EDITED_BY
     * @return mixed
     */
    public function getPrivacyPolicyEditedBy($EDITED_BY);

    /**
     * @param $STATUS
     * @return mixed
     */
    public function getPrivacyPolicyStatus($STATUS);

    /**
     * @param $POLICY_DESC
     * @return mixed
     */
    public function getPrivacyPolicyDesc($POLICY_DESC);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getPrivacyPolicyCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getPrivacyPolicyUpdatedAt($UPDATED_AT);
}
