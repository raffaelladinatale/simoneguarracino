<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 01-09-2021
 * Time: 18:04
 */

namespace Meetanshi\GDPR\Api\Data\PersonalDataProtection;

/**
 * Interface CosentsLogInterface
 */
interface CosentsLogInterface
{
    /**#@+
     * Constants
     */
    const CONSENTS_LOG_ID  = 'concents_log_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_NAME = 'customer_name';
    const REMOTE_IP = 'remote_ip';
    const EMAIL = 'email';
    const LOGIN_DATE = 'login_date';
    const CHECKBOX_LOCATION = 'checkbox_location';
    const CHECKBOX_POSITION = 'checkbox_position';
    const VERSION_NO = 'policy_version';
    const WEBSITE_ID = 'website_id';
    const ACTION = 'action';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed
     */
    public function setConsentsLogId($CONSENTS_LOG_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function setConsentsLogCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function setConsentsLogCustomerName($CUSTOMER_NAME);

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function setConsentsLogRemoteIP($REMOTE_IP);

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function setConsentsLogEmail($EMAIL);

    /**
     * @param $LOGIN_DATE
     * @return mixed
     */
    public function setConsentsLogLogin($LOGIN_DATE);

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed
     */
    public function setConsentsLogCheckboxLocation($CHECKBOX_LOCATION);

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed
     */
    public function setConsentsLogCheckboxPosition($CHECKBOX_POSITION);

    /**
     * @param $VERSION_NO
     * @return mixed
     */
    public function setConsentsLogVersionNo($VERSION_NO);

    /**
     * @param $WEBSITE_ID
     * @return mixed
     */
    public function setConsentsLogWebsiteId($WEBSITE_ID);

    /**
     * @param $ACTION
     * @return mixed
     */
    public function setConsentsLogAction($ACTION);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setConsentsLogCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setConsentsLogUpdatedAt($UPDATED_AT);

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed
     */
    public function getConsentsLogId($CONSENTS_LOG_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getConsentsLogCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getConsentsLogCustomerName($CUSTOMER_NAME);

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function getConsentsLogRemoteIP($REMOTE_IP);

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function getConsentsLogEmail($EMAIL);

    /**
     * @param $LOGIN_DATE
     * @return mixed
     */
    public function getConsentsLogLogin($LOGIN_DATE);

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed
     */
    public function getConsentsLogCheckboxLocation($CHECKBOX_LOCATION);

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed
     */
    public function getConsentsLogCheckboxPosition($CHECKBOX_POSITION);

    /**
     * @param $VERSION_NO
     * @return mixed
     */
    public function getConsentsLogVersionNo($VERSION_NO);

    /**
     * @param $WEBSITE_ID
     * @return mixed
     */
    public function getConsentsLogWebsiteId($WEBSITE_ID);

    /**
     * @param $ACTION
     * @return mixed
     */
    public function getConsentsLogAction($ACTION);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getConsentsLogCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getConsentsLogUpdatedAt($UPDATED_AT);
}
