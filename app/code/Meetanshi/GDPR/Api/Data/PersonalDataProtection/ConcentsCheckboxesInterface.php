<?php
namespace Meetanshi\GDPR\Api\Data\PersonalDataProtection;

/**
 * Interface ConcentsCheckboxesInterface
 */
interface ConcentsCheckboxesInterface
{
    /**#@+
     * Constants
     */
    const CHECKBOX_ID = 'checkbox_id';
    const CHECKBOX_NAME = 'checkbox_name';
    const CHECKBOX_CODE = 'checkbox_code';
    const IS_ENABLED = 'is_enabled';
    const IS_REQUIRED = 'is_required';
    const IS_HIDE = 'is_hide';
    const LOG_STATUS = 'log_status';
    const CHECKBOX_LOCATION = 'checkbox_location';
    const CHECKBOX_POSITION = 'checkbox_position';
    const CHECKBOX_TEXT = 'checkbox_text';
    const CONSENTS_LINK_TYPE = 'consents_link_type';
    const CMS_PAGE = 'cms_page';
    const COUNTRIES_RESTRICTED = 'countries_restricted';
    const COUNTRIES = 'countries';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $CHECKBOX_ID
     * @return mixed
     */
    public function setCheckboxId($CHECKBOX_ID);

    /**
     * @param $CHECKBOX_NAME
     * @return mixed
     */
    public function setCheckboxName($CHECKBOX_NAME);

    /**
     * @param $CHECKBOX_CODE
     * @return mixed
     */
    public function setCheckboxCode($CHECKBOX_CODE);

    /**
     * @param $IS_ENABLED
     * @return mixed
     */
    public function setCheckboxIsEnabled($IS_ENABLED);

    /**
     * @param $IS_REQUIRED
     * @return mixed
     */
    public function setCheckboxIsRequired($IS_REQUIRED);

    /**
     * @param $IS_HIDE
     * @return mixed
     */
    public function setCheckboxIsHide($IS_HIDE);

    /**
     * @param $LOG_STATUS
     * @return mixed
     */
    public function setCheckboxLogStatus($LOG_STATUS);

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed
     */
    public function setCheckboxLocation($CHECKBOX_LOCATION);

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed
     */
    public function setCheckboxPosition($CHECKBOX_POSITION);

    /**
     * @param $CHECKBOX_TEXT
     * @return mixed
     */
    public function setCheckboxText($CHECKBOX_TEXT);

    /**
     * @param $CONSENTS_LINK_TYPE
     * @return mixed
     */
    public function setCheckboxLinkType($CONSENTS_LINK_TYPE);

    /**
     * @param $CMS_PAGE
     * @return mixed
     */
    public function setCheckboxCmsPage($CMS_PAGE);

    /**
     * @param $COUNTRIES_RESTRICTED
     * @return mixed
     */
    public function setCheckboxCountriesRestricted($COUNTRIES_RESTRICTED);

    /**
     * @param $COUNTRIES
     * @return mixed
     */
    public function setCheckboxCountries($COUNTRIES);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setCheckboxCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setCheckboxUpdatedAt($UPDATED_AT);

    /**
     * @param $CHECKBOX_ID
     * @return mixed
     */
    public function getCheckboxId($CHECKBOX_ID);

    /**
     * @param $CHECKBOX_NAME
     * @return mixed
     */
    public function getCheckboxName($CHECKBOX_NAME);

    /**
     * @param $CHECKBOX_CODE
     * @return mixed
     */
    public function getCheckboxCode($CHECKBOX_CODE);

    /**
     * @param $IS_ENABLED
     * @return mixed
     */
    public function getCheckboxIsEnabled($IS_ENABLED);

    /**
     * @param $IS_REQUIRED
     * @return mixed
     */
    public function getCheckboxIsRequired($IS_REQUIRED);

    /**
     * @param $IS_HIDE
     * @return mixed
     */
    public function getCheckboxIsHide($IS_HIDE);

    /**
     * @param $LOG_STATUS
     * @return mixed
     */
    public function getCheckboxLogStatus($LOG_STATUS);

    /**
     * @param $CHECKBOX_LOCATION
     * @return mixed
     */
    public function getCheckboxLocation($CHECKBOX_LOCATION);

    /**
     * @param $CHECKBOX_POSITION
     * @return mixed
     */
    public function getCheckboxPosition($CHECKBOX_POSITION);

    /**
     * @param $CHECKBOX_TEXT
     * @return mixed
     */
    public function getCheckboxText($CHECKBOX_TEXT);

    /**
     * @param $CONSENTS_LINK_TYPE
     * @return mixed
     */
    public function getCheckboxLinkType($CONSENTS_LINK_TYPE);

    /**
     * @param $CMS_PAGE
     * @return mixed
     */
    public function getCheckboxCmsPage($CMS_PAGE);

    /**
     * @param $COUNTRIES_RESTRICTED
     * @return mixed
     */
    public function getCheckboxCountriesRestricted($COUNTRIES_RESTRICTED);

    /**
     * @param $COUNTRIES
     * @return mixed
     */
    public function getCheckboxCountries($COUNTRIES);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCheckboxCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCheckboxUpdatedAt($UPDATED_AT);
}
