<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 10-11-2021
 * Time: 14:48
 */

namespace Meetanshi\GDPR\Api\Data\PersonalDataProtection;

/**
 * Interface ActionLogInterface
 */
interface ActionLogInterface
{
    /**#@+
     * Constants
     */
    const ACTION_LOG_ID  = 'action_log_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_NAME = 'customer_name';
    const REMOTE_IP = 'remote_ip';
    const ACTION_DATE = 'action_date';
    const ACTION = 'action';
    const COMMENT = 'comment';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $ACTION_LOG_ID
     * @return mixed
     */
    public function setActionLogId($ACTION_LOG_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function setActionLogCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function setActionLogCustomerName($CUSTOMER_NAME);

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function setActionLogRemoteIP($REMOTE_IP);

    /**
     * @param $ACTION_DATE
     * @return mixed
     */
    public function setActionLogActionDate($ACTION_DATE);

    /**
     * @param $ACTION
     * @return mixed
     */
    public function setActionLogAction($ACTION);

    /**
     * @param $COMMENT
     * @return mixed
     */
    public function setActionLogComment($COMMENT);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setActionLogCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setActionLogUpdatedAt($UPDATED_AT);

    /**
     * @param $ACTION_LOG_ID
     * @return mixed
     */
    public function getActionLogId($ACTION_LOG_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getActionLogCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getActionLogCustomerName($CUSTOMER_NAME);

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function getActionLogRemoteIP($REMOTE_IP);

    /**
     * @param $ACTION_DATE
     * @return mixed
     */
    public function getActionLogActionDate($ACTION_DATE);

    /**
     * @param $ACTION
     * @return mixed
     */
    public function getActionLogAction($ACTION);

    /**
     * @param $COMMENT
     * @return mixed
     */
    public function getActionLogComment($COMMENT);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getActionLogCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getActionLogUpdatedAt($UPDATED_AT);
}
