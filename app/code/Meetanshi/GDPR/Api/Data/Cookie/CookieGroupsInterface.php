<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 10-11-2021
 * Time: 15:26
 */

namespace Meetanshi\GDPR\Api\Data\Cookie;

/**
 * Interface CookieGroupsInterface
 */
interface CookieGroupsInterface
{
    /**#@+
     * Constants
     */
    const COOKIE_GROUP_ID  = 'cookie_group_id';
    const COOKIE_GROUP_NAME = 'cookie_group_name';
    const COOKIE_GROUP_DESC = 'description';
    const COOKIE_GROUP_ENABLED = 'is_enabled';
    const COOKIE_GROUP_ESSENTIAL = 'is_essential';
    const ASSIGNED_COOKIES = 'assigned_cookies';
    const SORT_ORDER = 'sort_order';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $COOKIE_GROUP_ID
     * @return mixed
     */
    public function setCookieGroupId($COOKIE_GROUP_ID);

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed
     */
    public function setCookieGroupGroupname($COOKIE_GROUP_NAME);

    /**
     * @param $COOKIE_GROUP_DESC
     * @return mixed
     */
    public function setCookieGroupDesc($COOKIE_GROUP_DESC);

    /**
     * @param $COOKIE_GROUP_ENABLED
     * @return mixed
     */
    public function setCookieGroupIsEnabled($COOKIE_GROUP_ENABLED);

    /**
     * @param $COOKIE_GROUP_ESSENTIAL
     * @return mixed
     */
    public function setCookieGroupIsEssential($COOKIE_GROUP_ESSENTIAL);

    /**
     * @param $ASSIGNED_COOKIES
     * @return mixed
     */
    public function setCookieGroupAssignedCookies($ASSIGNED_COOKIES);

    /**
     * @param $SORT_ORDER
     * @return mixed
     */
    public function setCookieGroupSortOrder($SORT_ORDER);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setCookieGroupCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setCookieGroupUpdatedAt($UPDATED_AT);

    /**
     * @param $COOKIE_GROUP_ID
     * @return mixed
     */
    public function getCookieGroupId($COOKIE_GROUP_ID);

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed
     */
    public function getCookieGroupGroupname($COOKIE_GROUP_NAME);

    /**
     * @param $COOKIE_GROUP_DESC
     * @return mixed
     */
    public function getCookieGroupDesc($COOKIE_GROUP_DESC);

    /**
     * @param $COOKIE_GROUP_ENABLED
     * @return mixed
     */
    public function getCookieGroupIsEnabled($COOKIE_GROUP_ENABLED);

    /**
     * @param $COOKIE_GROUP_ESSENTIAL
     * @return mixed
     */
    public function getCookieGroupIsEssential($COOKIE_GROUP_ESSENTIAL);

    /**
     * @param $ASSIGNED_COOKIES
     * @return mixed
     */
    public function getCookieGroupAssignedCookies($ASSIGNED_COOKIES);

    /**
     * @param $SORT_ORDER
     * @return mixed
     */
    public function getCookieGroupSortOrder($SORT_ORDER);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCookieGroupCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCookieGroupUpdatedAt($UPDATED_AT);
}
