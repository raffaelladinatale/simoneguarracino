<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 10-11-2021
 * Time: 15:26
 */

namespace Meetanshi\GDPR\Api\Data\Cookie;

/**
 * Interface CookiesInterface
 */
interface CookiesInterface
{
    /**#@+
     * Constants
     */
    const COOKIE_ID  = 'cookie_id';
    const IS_ENABLED = 'enable';
    const COOKIE_NAME = 'cookie_name';
    const COOKIE_PROVIDER = 'cookie_provider';
    const COOKIE_DESC = 'description';
    const COOKIE_LIFETIME = 'cookie_lifetime';
    const COOKIE_TYPE = 'cookie_type';
    const COOKIE_GROUP_NAME = 'cookie_group_name';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $COOKIE_ID
     * @return mixed
     */
    public function setCookieId($COOKIE_ID);

    /**
     * @param $IS_ENABLED
     * @return mixed
     */
    public function setCookieIsEnabled($IS_ENABLED);

    /**
     * @param $COOKIE_NAME
     * @return mixed
     */
    public function setCookieName($COOKIE_NAME);

    /**
     * @param $COOKIE_PROVIDER
     * @return mixed
     */
    public function setCookieProvider($COOKIE_PROVIDER);

    /**
     * @param $COOKIE_DESC
     * @return mixed
     */
    public function setCookieDesc($COOKIE_DESC);

    /**
     * @param $COOKIE_LIFETIME
     * @return mixed
     */
    public function setCookieLifetime($COOKIE_LIFETIME);

    /**
     * @param $COOKIE_TYPE
     * @return mixed
     */
    public function setCookieType($COOKIE_TYPE);

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed
     */
    public function setCookieGroupname($COOKIE_GROUP_NAME);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setCookieCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setCookieUpdatedAt($UPDATED_AT);

    /**
     * @param $COOKIE_ID
     * @return mixed
     */
    public function getCookieId($COOKIE_ID);

    /**
     * @param $IS_ENABLED
     * @return mixed
     */
    public function getCookieIsEnabled($IS_ENABLED);

    /**
     * @param $COOKIE_NAME
     * @return mixed
     */
    public function getCookieName($COOKIE_NAME);

    /**
     * @param $COOKIE_PROVIDER
     * @return mixed
     */
    public function getCookieProvider($COOKIE_PROVIDER);

    /**
     * @param $COOKIE_DESC
     * @return mixed
     */
    public function getCookieDesc($COOKIE_DESC);

    /**
     * @param $COOKIE_LIFETIME
     * @return mixed
     */
    public function getCookieLifetime($COOKIE_LIFETIME);

    /**
     * @param $COOKIE_TYPE
     * @return mixed
     */
    public function getCookieType($COOKIE_TYPE);

    /**
     * @param $COOKIE_GROUP_NAME
     * @return mixed
     */
    public function getCookieGroupname($COOKIE_GROUP_NAME);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCookieCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCookieUpdatedAt($UPDATED_AT);
}
