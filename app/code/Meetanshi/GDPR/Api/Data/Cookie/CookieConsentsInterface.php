<?php
/**
 * Created by PhpStorm.
 * User: Meetanshi
 * Date: 10-11-2021
 * Time: 15:26
 */

namespace Meetanshi\GDPR\Api\Data\Cookie;

/**
 * Interface CookieConsentsInterface
 */
interface CookieConsentsInterface
{
    /**#@+
     * Constants
     */
    const CONSENTS_LOG_ID  = 'concents_log_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_NAME = 'customer_name';
    const REMOTE_IP = 'remote_ip';
    const EMAIL = 'email';
    const LOGIN_DATE = 'login_date';
    const WEBSITE_ID = 'website_id';
    const ACTION = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed
     */
    public function setCookieConsentsLogId($CONSENTS_LOG_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function setCookieConsentsLogCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function setCookieConsentsLogCustomerName($CUSTOMER_NAME);

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function setCookieConsentsLogRemoteIP($REMOTE_IP);

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function setCookieConsentsLogEmail($EMAIL);

    /**
     * @param $LOGIN_DATE
     * @return mixed
     */
    public function setCookieConsentsLogLogin($LOGIN_DATE);

    /**
     * @param $WEBSITE_ID
     * @return mixed
     */
    public function setCookieConsentsLogWebsiteId($WEBSITE_ID);

    /**
     * @param $ACTION
     * @return mixed
     */
    public function setCookieConsentsLogAction($ACTION);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function setCookieConsentsLogCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function setCookieConsentsLogUpdatedAt($UPDATED_AT);

    /**
     * @param $CONSENTS_LOG_ID
     * @return mixed
     */
    public function getCookieConsentsLogId($CONSENTS_LOG_ID);

    /**
     * @param $CUSTOMER_ID
     * @return mixed
     */
    public function getCookieConsentsLogCustomerId($CUSTOMER_ID);

    /**
     * @param $CUSTOMER_NAME
     * @return mixed
     */
    public function getCookieConsentsLogCustomerName($CUSTOMER_NAME);

    /**
     * @param $REMOTE_IP
     * @return mixed
     */
    public function getCookieConsentsLogRemoteIP($REMOTE_IP);

    /**
     * @param $EMAIL
     * @return mixed
     */
    public function getCookieConsentsLogEmail($EMAIL);

    /**
     * @param $LOGIN_DATE
     * @return mixed
     */
    public function getCookieConsentsLogLogin($LOGIN_DATE);

    /**
     * @param $WEBSITE_ID
     * @return mixed
     */
    public function getCookieConsentsLogWebsiteId($WEBSITE_ID);

    /**
     * @param $ACTION
     * @return mixed
     */
    public function getCookieConsentsLogAction($ACTION);

    /**
     * @param $CREATED_AT
     * @return mixed
     */
    public function getCookieConsentsLogCreatedAt($CREATED_AT);

    /**
     * @param $UPDATED_AT
     * @return mixed
     */
    public function getCookieConsentsLogUpdatedAt($UPDATED_AT);
}
