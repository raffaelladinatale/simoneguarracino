<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\Cookie\CookiesInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface CookiesRepositoryInterface
 */
interface CookiesRepositoryInterface
{
    /**
     * @param CookiesInterface $page
     * @return mixed
     */
    public function save(CookiesInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param CookiesInterface $page
     * @return mixed
     */
    public function delete(CookiesInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
