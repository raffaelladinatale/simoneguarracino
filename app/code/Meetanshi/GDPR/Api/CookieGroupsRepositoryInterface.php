<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\Cookie\CookieGroupsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface CookieGroupsRepositoryInterface
 */
interface CookieGroupsRepositoryInterface
{
    /**
     * @param CookieGroupsInterface $page
     * @return mixed
     */
    public function save(CookieGroupsInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param CookieGroupsInterface $page
     * @return mixed
     */
    public function delete(CookieGroupsInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
