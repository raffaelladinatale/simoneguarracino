<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\PersonalDataProtection\CosentsLogInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ConsentsLogRepositoryInterface
 */
interface ConsentsLogRepositoryInterface
{
    /**
     * @param CosentsLogInterface $page
     * @return mixed
     */
    public function save(CosentsLogInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param CosentsLogInterface $page
     * @return mixed
     */
    public function delete(CosentsLogInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
