<?php
namespace Meetanshi\GDPR\Api;

use Meetanshi\GDPR\Api\Data\PersonalDataProtection\ActionLogInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ActionLogRepositoryInterface
 */
interface ActionLogRepositoryInterface
{
    /**
     * @param ActionLogInterface $page
     * @return mixed
     */
    public function save(ActionLogInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param ActionLogInterface $page
     * @return mixed
     */
    public function delete(ActionLogInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
