
var config = {
	map: {
        '*': {
            infinitescroll: 'Magetop_InfiniteScroll/js/plugin/infinitescroll',
        }
    },
	paths: {
		'magetop/infinitescroll': 'Magetop_InfiniteScroll/js/plugin/infinitescroll',
	},
	shim: {
		'magetop/infinitescroll': {
			deps: ['jquery']
		}
	}

};