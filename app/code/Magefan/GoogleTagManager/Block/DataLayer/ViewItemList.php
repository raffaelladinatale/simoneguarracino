<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

declare(strict_types=1);

namespace Magefan\GoogleTagManager\Block\DataLayer;

use Magefan\GoogleTagManager\Api\DataLayer\ViewItemListInterface;
use Magefan\GoogleTagManager\Block\AbstractDataLayer;
use Magefan\GoogleTagManager\Model\Config;
use Magento\Catalog\Model\Category;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Context;

class ViewItemList extends AbstractDataLayer
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var ViewItemListInterface
     */
    private $viewItemList;


    /**
     * Get GTM datalayer
     *
     * @param Product $product
     * @return array
     * @throws NoSuchEntityException
     */
    public function __construct(
        Context $context,
        Config $config,
        Registry $registry,
        viewItemListInterface $viewItemList,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->viewItemList = $viewItemList;
        parent::__construct($context, $config, $data);
    }

    /**
     * Get GTM datalayer for product page
     *
     * @return array
     * @throws NoSuchEntityException
     */
    protected function getDataLayer(): array
    {
        return $this->viewItemList->get($this->getCurrentCategory());
    }

    private function getCurrentCategory() : Category
    {
        return $this->registry->registry('current_category');
    }
}
