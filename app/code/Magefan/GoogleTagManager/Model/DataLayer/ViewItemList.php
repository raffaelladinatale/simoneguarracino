<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

declare(strict_types=1);

namespace Magefan\GoogleTagManager\Model\DataLayer;

use Magefan\GoogleTagManager\Api\DataLayer\Product\ItemInterface;
use Magefan\GoogleTagManager\Api\DataLayer\ViewItemListInterface;
use Magefan\GoogleTagManager\Model\AbstractDataLayer;
use Magefan\GoogleTagManager\Model\Config;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Store\Model\StoreManagerInterface;

class ViewItemList extends AbstractDataLayer implements ViewItemListInterface
{
    /**
     * @var \Magefan\GoogleTagManager\Api\DataLayer\Product\ItemInterface
     */
    private $gtmItem;


    public $categoryFactory;

    /**
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param ItemInterface $gtmItem
     */
    public function __construct(
        Config $config,
        StoreManagerInterface $storeManager,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        ItemInterface $gtmItem
    ) {
        $this->gtmItem = $gtmItem;
        $this->categoryFactory = $categoryFactory;
        parent::__construct($config, $storeManager, $categoryRepository);
    }

    /**
     * @inheritDoc
     */
    public function get(Category $category)
    {
        $items = [];
        $category = $this->categoryFactory->create()->load($category->getId());
        $categoryItems = $category->getProductCollection()->addAttributeToSelect('*');
        $counter = 0;

        foreach ($categoryItems as $item) {
            $items[] = $this->gtmItem->get($item);
            if ($counter >= 20)
                break;
            $counter++;
        }

        return $this->eventWrap([
            'event' => 'view_item_list',
            'item_list_id' => $category->getName(),
            'item_list_name' => $category->getName(),
            'ecommerce' => [
                'items' => $items
            ]
        ]);
    }
}
