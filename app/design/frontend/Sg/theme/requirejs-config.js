var config = {
    paths: {
        swiper_4_0_6: "js/swiper/js/swiper.min",
        swiperSliderWidget: "js/widget/theme-swiper",
        categorySwiper: "js/widget/category-swiper",
        layout:     'js/widget/layout',
        plyr: 'js/plyr/js/plyr',
        plyr_3_6_2: 'js/plyr/js/plyr_3.6.2.min',
        listingIsotope: 'js/widget/listing_isotope',
    },
    shim: {
        swiper_4_0_6: ['jquery'],
        swiperSliderWidget: ['jquery', 'swiper_4_0_6'],
        layout: ['jquery'],
        plyr_3_6_2: ['jquery', 'https://player.vimeo.com/api/player.js'],
        listingIsotope: ['jquery', 'isotope_lib'],
        isotope_lib: ['jquery'],
    },
    map: {
        "*": {
            plyrVideo: "js/widget/plyr-video",
            Plyr: 'js/plyr/js/plyr_3.6.2',
            blackbirdMenu:  "js/widget/theme-menu",
            isotope_lib: 'js/isotope-layout/isotope.pkgd'
        }
    }
};
