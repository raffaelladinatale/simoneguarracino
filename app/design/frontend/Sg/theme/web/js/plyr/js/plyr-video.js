/**
* Copyright © 2016 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
define([
    'jquery',
    'domReady',
    'matchMedia',
    'Magento_Ui/js/modal/modal',
    'plyr_3_6_2'
], function ($, domReady, mediaCheck, modal, Plyr) {
    'use strict';

    if(typeof window.plyrVideos == 'undefined'){
        window.plyrVideos = [];
    }

    $.widget('sg.plyr', {
        popup: {},
        options: {
            popup: false,
            popupContainer: false,
            popupVideo: false
        },

        _create: function(){

            var self = this,
            videoType = $(this.element).attr('data-plyr-provider'),
            videoContainerId = '#'+$(this.element).attr('id'),
            videoImageDesktop = $(this.element).attr('poster') ? $(this.element).attr('poster') : '',
            videoImageMobile = $(this.element).attr('poster-mobile') ? $(this.element).attr('poster-mobile') : $(this.element).attr('poster'),
            videoAutoplay = ($(this.element).attr('data-autoplay') == 'true') ? true : false,
            videoMuted = ($(this.element).attr('data-muted') == 'true') ? true : false,
            videoVolume = ($(this.element).attr('data-volume') == '1') ? 1 : 0,
            videoLoop = ($(this.element).attr('data-loop') == 'true') ? true : false,
            videoPoster = $(this.element).attr('data-poster'),
            videoCurrentTime = ($(this.element).attr('data-current-time') != '') ? $(this.element).attr('data-current-time') : '0';
            // set poster image only for HTML5 video
            mediaCheck({
                media: '(max-width: 991px)',
                entry: $.proxy(function () {

                    if(videoType != 'youtube' || videoType != 'vimeo'){
                        $(self.element).attr('poster', videoImageMobile);
                    }

                }, this),
                exit: $.proxy(function () {

                    if(videoType != 'youtube' || videoType != 'vimeo'){
                        $(self.element).attr('poster', videoImageDesktop);
                    }

                }, this)
            });


            if (window.localStorage && localStorage.getItem('plyr')) {
                localStorage.removeItem('plyr');
            }

            var plyrInstance = new Plyr(videoContainerId, {
                storage: {
                    enabled: false
                },
                autoplay: self.options.popup ? false : videoAutoplay,
                volume: videoVolume,
                muted: videoMuted,
                loop: {
                    active: videoLoop
                }
            });

            plyrVideos.push(plyrInstance);

            if(videoPoster && videoType !== "mp4"){
                setTimeout(function(){
                    plyrInstance.poster = videoPoster;
                });
            }

            $.each(plyrVideos,function(index,element){
                if(videoCurrentTime){
                    if($(self.element).attr('id') === element.elements.original.id){
                        element.on('ready',function(){
                            setTimeout(function(){
                                element.currentTime = videoCurrentTime;
                            },100);
                        });
                    }
                }
            });

            if(self.options.popup){
                self.initPopup(this.element, self.options.popupContainer,self.options.popupVideo,videoAutoplay);
            }

        },

        pauseAllVideos: function(){
            $.each(plyrVideos, function(){
                if(typeof this.media !== 'undefined'){
                    this.pause();
                }
            });
        },

        play: function(video){
            video.play();
        },

        pause: function(video){
            video.pause();
        },

        initPopup: function (el, container, video, autoplay) {
            var self = this;

                //modal options
                var modalOptions = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    modalClass: '__plyr_modal_popup',
                    title: '',
                    buttons: [],
                    opened: function () {
                        $("body").addClass("video-popup-showing");
                        $('.modal-overlay').fadeIn();
                        $('.modals-wrapper').addClass("active");

                        if(autoplay){
                            $.each(plyrVideos,function(index,element){
                                if(video === '#' + element.elements.original.id){
                                    element.play();
                                }
                            });
                        }
                    },
                    closed: function () {
                        self.pauseAllVideos();
                        $('.modal-overlay').fadeOut();
                        $('.modals-wrapper').removeClass("active");
                        $("body").removeClass("video-popup-showing");
                    }
                };

                //open modal
                var popup = modal(modalOptions, $(container));

                $(el).on('click', function(e){
                    e.preventDefault();
                    $(container).modal('openModal');
                });
        }

    });

    return $.sg.plyr;

});
