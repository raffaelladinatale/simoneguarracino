/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'js/isotope-layout/isotope.pkgd'
], function ($, Isotope) {
    var useStrict = 'use strict';

    $.widget('sg.listingIsotope',{
        options: {
            filterValue: false,
            filterActive: false,
            filtersDiv: false,
            comboFilter: false,
            columnWidthControls: false,
            filterElement: false,
            filterSubMenu: false,
            filterLabel: false,
            largeGrid: false
        },

        _init: function () {

            // store filter for each group
            var self = this,
                filters = {},
                filterValue = this.options.filterValue,
                filtersDiv = this.options.filtersDiv;

            window.iso =  new Isotope('.grid.blog', {
                itemSelector: '.grid-item',
                percentPosition: true,
                masonry: {
                    columnWidth: '.grid-sizer'
                }
            });

            $(filtersDiv).on( 'click', filterValue, function(event) {
                var $button = $( event.currentTarget ),
                    $buttonGroup = $button.parents('.menu-filter');
                var filterGroup = $buttonGroup.attr('data-filter-group');

                $('#sorter-content-list').on( 'click', 'a', function() {
                    var filterValue = $(this).attr('data-filter');
                    if (filterValue !== '') {
                        window.iso.arrange({
                            filter: filterValue
                        });
                    }
                    else {
                        window.iso.arrange({
                            filter: ""
                        });
                    }
                });
            });
        },
});
    return $.sg.listingIsotope;
});
