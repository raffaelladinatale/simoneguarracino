/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'matchMedia',
    'domReady',
], function ($, mediaCheck) {
    'use strict';

    $.widget('sg.layout', {
        options: {
            responsive: false,
            click: false,
            element: false,
            target: false,
            disabled: false,
            event: false,
            ratingContainer: false
        },

        _create: function(){

            var self = this;
            self._super();

            if(this.options.click){
                var el = self.options.element,
                    target = self.options.target,
                    event = self.options.event;

                $(el).on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(this).toggleClass('active');
                    $(target).toggleClass('active');
                    self.triggerEvent(event);
                });
            }
            if($('body').hasClass('catalog-product-view')) {
                $('.product.media').append($('.product-info-container .discount-percentage'))
            }


            $(this.options.ratingContainer).on('click', function(){
                console.log(this.options.ratingContainer)

                console.log($(this).last().prevAll())
                $(this).last().prevAll().addClass( "filled" );
            });

            if (this.options.responsive === true) {

                mediaCheck({
                    media: '(max-width: 992px)',
                    entry: $.proxy(function () {

                        var heightHeader = $('.page-header').height();
                        $('.nav-sections, .search-content').css('top', heightHeader);
                        $('.nav-sections .nav-sections-item-content').css('padding-bottom', heightHeader);
                        $(window).scroll(function() {
                            var scroll = $(window).scrollTop();

                            if (scroll >= heightHeader) {
                                $(".header.content").addClass("fixed");
                            } else {
                                $(".header.content").removeClass("fixed");
                            }
                        });

                    }, this),
                    exit: $.proxy(function () {

                        $('.nav-sections .nav-sections-item-content').css('padding-bottom', 0);
                    }, this)
                });

                var height = '';
                $('.page-products .products .product-item').each(function (){
                    height = Math.max(height,$(this).find('.product-item-name').outerHeight())
                });
                $('.page-products .products .product-item .product-item-name').outerHeight(height);

                $(window).scroll(function() {
                    if ($(this).scrollTop() > 300) {
                        $('#back_top').fadeIn();
                    } else {
                        $('#back_top').fadeOut();
                    }
                });

                $('#back_top').on('click',function(){
                    $('html,body').animate({
                        'scrollTop': '0px'
                    });
                });
            }
        },

        triggerEvent: function(eventName){

            var self = this;

            switch(eventName){
                case 'openNewsletter':
                    $('html,body').removeClass('nav-before-open');
                    $('html,body').removeClass('nav-open');
                    break;
                case 'openSearch':
                    $(".general-overlay").removeClass('_show');
                    self.triggerEvent('closeMinicart');
                    self.triggerEvent('closeMenuMobile');
                    break;
                case 'openMenuMobile':
                    self.triggerEvent('closeSearch');
                    self.triggerEvent('closeMinicart');
                    break;


                case 'toggleOverlay':
                    $(".general-overlay").fadeToggle();
                    break;

                case 'closeAll':
                    self.triggerEvent('closeSearch');
                    self.triggerEvent('closeMinicart');
                    self.triggerEvent('toggleOverlay');
                    break;
                case 'closeSearch':
                    $(".search-container,.block-search .field.search .label").removeClass('active');
                    break;
                case 'closeMinicart':
                    $(".block-minicart, a.showcart").removeClass('active');
                    break;
                case 'closeMenuMobile':
                    $("html").removeClass('nav-before-open nav-open');
                    break;

            }
        }

    });

    return $.sg.layout;
});
