/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'matchMedia',
    'jquery/ui',
    'mage/menu'
], function ($, mediaCheck) {
    'use strict';

    /**
     * Menu Widget - this widget is a wrapper for the jQuery UI Menu
     */
    $.widget('sg.menu', $.mage.menu, {
        options: {
            responsive: false,
            showDelay: 300,
            hideDelay: 100,
            delay: 300,
            mediaBreakpoint: '(max-width: 991px)'
        },

        _create: function () {
            var self = this;

            this.delay = this.options.delay;

            this._super();
            this._heightMobileMenu();
            $(window).on('resize', function () {
                self._heightMobileMenu();
                self.element.find('.submenu-reverse').removeClass('submenu-reverse');
            });

            var currentUrl = window.location.href.split('?')[0];
            var activeCategoryLink = this.element.find('a[href="' + currentUrl + '"]');
            activeCategoryLink.parents("li").addClass('active');
        },

        _heightMobileMenu: function (){

            var headerheight = ($('.header.content').outerHeight() + $('.page-header .panel.wrapper').outerHeight()),
                menuHeight = ($('.nav-sections-items').outerHeight() - headerheight),
                settingsHeight = $('.store-settings-mobile').height(),
                searchHeight = $('.store-search-mobile').height();

            mediaCheck({
                media: '(max-width: 992px)',
                entry: $.proxy(function () {

                    $('.store-menu-mobile').css('max-height',  menuHeight - settingsHeight - searchHeight - 43);

                }, this),
                exit: $.proxy(function () {
                    $('.store-menu-mobile').css('max-height','100%');
                }, this)
            });

        },

        toggle: function () {
            var html = $('html');

            if (html.hasClass('nav-open')) {
                html.removeClass('nav-open');
                setTimeout(function () {
                    html.removeClass('nav-before-open');
                }, this.options.hideDelay);
            } else {
                html.addClass('nav-before-open');
                setTimeout(function () {
                    html.addClass('nav-open');
                }, this.options.showDelay);
            }
        },

        _toggleMobileMode: function () {
            var subMenus;

            $(this.element).off('mouseenter mouseleave');
            this._on({
                /**
                 * @param {jQuery.Event} event
                 */
                'click .ui-menu-item:has(a)': function (event) {
                    var target;

                    event.preventDefault();
                    target = $(event.target).closest('.ui-menu-item')
                    var targetContext = target.context;
                    //add condition "!target.hasClass('no-link-to-first-child')" to not link to "undefined" url if the group item is not linking to url
                    if ((!target.hasClass('level-top') || !target.has('.ui-menu').length) && !target.hasClass('no-link-to-first-child')) {
                        window.location.href = $(targetContext).is( "span" ) ? $(targetContext).parent('a').attr('href') : $(targetContext).attr('href')
                    }
                },
            });
        },
        _toggleDesktopMode: function () {
            var categoryParent, html;

            $(this.element).off('click mousedown mouseenter mouseleave');
            this._on({

                /**
                 * Prevent focus from sticking to links inside menu after clicking
                 * them (focus should always stay on UL during navigation).
                 */
                'mousedown .ui-menu-item > a': function (event) {
                    event.preventDefault();
                },

                /**
                 * Prevent focus from sticking to links inside menu after clicking
                 * them (focus should always stay on UL during navigation).
                 */
                'click .ui-state-disabled > a': function (event) {
                    event.preventDefault();
                },

                /**
                 * @param {jQuer.Event} event
                 */
                'click .ui-menu-item:has(a)': function (event) {
                    var target = $(event.target).closest('.ui-menu-item');

                    if (!this.mouseHandled && target.not('.ui-state-disabled').length) {
                        this.select(event);

                        // Only set the mouseHandled flag if the event will bubble, see #9469.
                        if (!event.isPropagationStopped()) {
                            this.mouseHandled = true;
                        }

                        // Open submenu on click
                        if (target.has('.ui-menu').length) {
                            this.expand(event);

                        } else if (!this.element.is(':focus') &&
                            $(this.document[0].activeElement).closest('.ui-menu').length
                        ) {
                            // Redirect focus to the menu
                            this.element.trigger('focus', [true]);

                            // If the active item is on the top level, let it stay active.
                            // Otherwise, blur the active item since it is no longer visible.
                            if (this.active && this.active.parents('.ui-menu').length === 1) { //eslint-disable-line
                                clearTimeout(this.timer);
                            }
                        }
                    }
                },

                /**
                 * @param {jQuery.Event} event
                 */
                'mouseenter .ui-menu-item:not(.level1)': function (event) {
                    var target = $(event.currentTarget),
                        submenu = this.options.menus,
                        ulElement,
                        ulElementWidth,
                        width,
                        targetPageX,
                        rightBound;

                    if (target.has(submenu)) {
                        ulElement = target.find(submenu);
                        ulElementWidth = ulElement.outerWidth(true);
                        width = target.outerWidth() * 2;
                        targetPageX = target.offset().left;
                        rightBound = $(window).width();

                        if (ulElementWidth + width + targetPageX > rightBound) {
                            ulElement.addClass('submenu-reverse');
                        }

                        if (targetPageX - ulElementWidth < 0) {
                            ulElement.removeClass('submenu-reverse');
                        }
                    }


                    target.siblings().children('.ui-state-active').removeClass('ui-state-active');
                    this.focus(event, target);
                },

                /**
                 * @param {jQuery.Event} event
                 */
                'mouseleave': function (event) {
                    var target = $(event.currentTarget);

                    this.collapseAll(event, true);
                },

                /**
                 * Mouse leave.
                 */
                'mouseleave .ui-menu': 'collapseAll'

            });
        },
    });
    return $.sg.menu;
});
