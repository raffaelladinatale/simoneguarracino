define([
    'jquery',
    'matchMedia',
    'jquery/ui',
    'swiper_4_0_6'
], function ($, mediaCheck, ui, Swiper) {
    'use strict';

    $.widget("sg.swiperSliderWidget", {
        options: {
            init: true,
            initialSlide: 0,
            autoHeight: false,
            spaceBetween: 30,
            slidesPerView: 1,
            slidesPerGroup: 1,
            centeredSlides: false,
            direction: "horizontal",
            autoplay: false,
            loop: false,
            speed: 1000,
            keyboardControl: true,
            grabCursor: true,
            watchSlidesVisibility: true,
            paginationType: null,
            paginationContainer: null,
            paginationClickable: false,
            renderBullet: null,
            effect: "slide",
            fade: false,
            prevButton: "",
            nextButton: "",
            collapsibleElement: "",
            breakpoints: {
                767: {
                    slidesPerGroup: 1,
                    slidesPerView: 1,
                    spaceBetween: 15
                },
                991: {
                    slidesPerGroup: 2,
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                1199: {
                    slidesPerGroup: 1,
                    slidesPerView: 1,
                    spaceBetween: 30
                }
            }
        },


        _create: function() {
            this._super();

            var swiper;

            var self = this.element;
            var collapsibleElement = this.options.collapsibleElement;

            if ($(".swiper-slide", this.element).length > 1) {

                swiper = new Swiper(this.element, {
                    init: true,
                    initialSlide: this.options.initialSlide,
                    autoHeight: this.options.autoHeight,
                    spaceBetween: this.options.spaceBetween,
                    slidesPerView: this.options.slidesPerView,
                    slidesPerGroup: this.options.slidesPerGroup,
                    centeredSlides: this.options.centeredSlides,
                    direction: this.options.direction,
                    autoplay: this.options.autoplay,
                    loop: this.options.loop,
                    speed: this.options.speed,
                    keyboard: this.options.keyboardControl,
                    grabCursor: this.options.grabCursor,
                    watchSlidesVisibility: this.options.watchSlidesVisibility,
                    pagination: {
                        el: this.options.paginationContainer,
                        type: this.options.paginationType,
                        clickable: this.options.paginationClickable,
                        renderBullet: this.options.renderBullet
                    },
                    effect: this.options.effect,
                    fadeEffect: {
                        crossFade: this.options.fade
                    },
                    navigation: {
                        prevEl: this.options.prevButton,
                        nextEl: this.options.nextButton
                    },
                    breakpoints: {
                        // when window width is <= 767px
                        767: {
                            slidesPerGroup: this.options.breakpoints[767].slidesPerGroup,
                            slidesPerView: this.options.breakpoints[767].slidesPerView,
                            spaceBetween: this.options.breakpoints[767].spaceBetween
                        },
                        // when window width is <= 991px
                        991: {
                            slidesPerGroup: this.options.breakpoints[991].slidesPerGroup,
                            slidesPerView: this.options.breakpoints[991].slidesPerView,
                            spaceBetween: this.options.breakpoints[991].spaceBetween
                        },
                        // when window width is <= 1199px
                        1199: {
                            slidesPerGroup: this.options.breakpoints[1199].slidesPerGroup,
                            slidesPerView: this.options.breakpoints[1199].slidesPerView,
                            spaceBetween: this.options.breakpoints[1199].spaceBetween
                        }
                    }
                });

            }

        },

    });

    return $.sg.swiperSliderWidget;
});
