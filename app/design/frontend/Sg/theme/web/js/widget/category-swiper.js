define([
    'jquery',
    'matchMedia',
    'jquery/ui',
    'swiperSliderWidget'
], function ($, mediaCheck, ui, Swiper) {
    'use strict';

    $.widget("sg.categorySwiper", $.sg.swiperSliderWidget,{
        options: {
            init: true,
            sliderId: false,
            sliderOnMobile: false,
        },


        _create: function() {
            this._super();

            var self = this,
                sliderOnMobile = this.options.sliderOnMobile,
                sliderId =  this.options.sliderId;

            mediaCheck({
                media: '(max-width: 992px)',
                entry: $.proxy(function () {

                    if(sliderOnMobile){

                        if ($('.swiper-slide', self.element).length > 1) {

                            var mySwiper = new Swiper(sliderId, this.options);
                        }
                    }
                }, this),
                exit: $.proxy(function () {

                    if(sliderOnMobile){
                        var mySwiper = document.querySelector(sliderId).swiper

                        mySwiper.destroy(true, true);
                    }
                }, this)
            });

        },

    });

    return $.sg.categorySwiper;
});
