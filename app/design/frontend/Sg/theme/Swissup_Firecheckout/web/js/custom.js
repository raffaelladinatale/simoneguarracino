require([
    'Swissup_Firecheckout/js/utils/form-field/watcher',
    'jquery',
    'jquery/ui',
    'domReady!'
], function(watcher, $) {
    'use strict';

    // Shortest syntax: callback is triggered after each field manipulation
    watcher('[name="billing-address-same-as-shipping"]', function (res) {

        if(res) {
            $('[name="shippingAddress.custom_attributes.fiscal_company"]').removeClass('fc-hidden');
            $('[name="shippingAddress.company"]').removeClass('fc-hidden');
            $('[name="shippingAddress.vat_id"]').removeClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_sdi"]').removeClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_code"]').removeClass('fc-hidden');
        } else {
            $('[name="shippingAddress.custom_attributes.fiscal_company"]').addClass('fc-hidden');
            $('[name="shippingAddress.company"]').addClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_code"]').addClass('fc-hidden');
            $('[name="shippingAddress.vat_id"]').addClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_sdi"]').addClass('fc-hidden');
            if( $('#co-shipping-form').length > 0){
                $('#co-shipping-form')[0].reset()
            }
        }
    });

    $(document).ajaxComplete(function() {
        if(!$('[name="billing-address-same-as-shipping"]:checked').length > 0){
            $('[name="shippingAddress.custom_attributes.fiscal_company"]').addClass('fc-hidden');
            $('[name="shippingAddress.company"]').addClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_code"]').addClass('fc-hidden');
            $('[name="shippingAddress.vat_id"]').addClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_sdi"]').addClass('fc-hidden');
        }else{
            $('[name="shippingAddress.custom_attributes.fiscal_company"]').removeClass('fc-hidden');
            $('[name="shippingAddress.company"]').removeClass('fc-hidden');
            $('[name="shippingAddress.vat_id"]').removeClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_sdi"]').removeClass('fc-hidden');
            $('[name="shippingAddress.custom_attributes.fiscal_code"]').removeClass('fc-hidden');
        }
    });

    $(document).ready(function(){
        setTimeout(function(){
            if( $('#co-shipping-form').length > 0){
                $('#co-shipping-form')[0].reset()
            }
            if( $('.billing-address-form form').length > 0){
                $('.billing-address-form form')[0].reset()
            }

            if(!$('[name="billing-address-same-as-shipping"]').is(":checked")) {
                if( $('#co-shipping-form').length > 0){
                    $('#co-shipping-form')[0].reset()
                }
            }
        },1000);
    });

});
