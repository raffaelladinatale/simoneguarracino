<?php
return [
    'backend' => [
        'frontName' => 'admin_yb6o2p'
    ],
    'remote_storage' => [
        'driver' => 'file'
    ],
    'queue' => [
        'consumers_wait_for_messages' => 1
    ],
    'crypt' => [
        'key' => '2359175abdac146aa8ce9e8d30c23092'
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => 'localhost',
                'dbname' => 'simoneguarracino',
                'username' => 'simoneguarracino',
                'password' => '48d3dxFhk0WKdh1k',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1',
                'driver_options' => [
                    1014 => false
                ]
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'default',
    'session' => [
        'save' => 'files'
    ],
    'cache' => [
        'frontend' => [
            'default' => [
                'id_prefix' => '513_'
            ],
            'page_cache' => [
                'id_prefix' => '513_'
            ]
        ],
        'allow_parallel_generation' => false
    ],
    'lock' => [
        'provider' => 'db',
        'config' => [
            'prefix' => ''
        ]
    ],
    'directories' => [
        'document_root_is_pub' => true
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'compiled_config' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 1,
        'config_webservice' => 1,
        'translate' => 1,
        'vertex' => 1,
        'checkout' => 1
    ],
    'downloadable_domains' => [
        'www.simoneguarracino.eu'
    ],
    'install' => [
        'date' => 'Wed, 24 Feb 2021 20:29:29 +0000'
    ],
    'system' => [
        'default' => [
            'smile_elasticsuite_core_base_settings' => [
                'es_client' => [
                    'enable_https_mode' => '0',
                    'enable_http_auth' => '0',
                    'http_auth_user' => '',
                    'http_auth_pwd' => '',
                    'servers' => 'node-1:9200,node-2:9200'
                ]
            ]
        ]
    ]
];
