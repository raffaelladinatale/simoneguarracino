<?php

try {
    require __DIR__ . '/../app/bootstrap.php';
} catch (\Exception $e) {
    header(__('Autoload error: ' . $e->getMessage()));
    exit(1);
}

#if (!filter_input(INPUT_SERVER, 'HTTP_X_AUTHORIZATION')) {
#    header(__('HTTP/1.1 403 Admin authentication failed: Please check Username and Password. They can be empty'));
#    exit(2);
#}

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, filter_input_array(INPUT_SERVER));
require __DIR__ . '/../app/code/Webprojectsol/DaneaEasyFatt/App/AbstractApp.php';

if (!in_array(filter_input(INPUT_GET, 'app'), \Webprojectsol\DaneaEasyFatt\App\AbstractApp::getAllowedApps())) {
    header(__('Application error: please check url'));
    exit(3);
}

$daneaApp = filter_input(INPUT_GET, 'app');
require __DIR__ . '/../app/code/Webprojectsol/DaneaEasyFatt/App/' . $daneaApp . 'App.php';

$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get(\Magento\Framework\App\State::class);
$state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

/** @var \Webprojectsol\DaneaEasyFatt\App\{$daneaApp}App $app */
$app = $bootstrap->createApplication('\Webprojectsol\DaneaEasyFatt\App\\' . $daneaApp . 'App');

$bootstrap->run($app);
